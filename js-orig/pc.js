var pc_rxh_scrollbar;
var recentActivity_div_sb_scrollbar;
var tmpl_pinfo_area_var_html ='<h3 class="rsection_title">Contact &amp; Address Information</h3><div><span class="pc_pinfo_form_label">Name:</span> <span id="PatientFirstName" class="ji_edit">${PatientFirstName}</span> <span id="PatientLastName" class="ji_edit">${PatientLastName}</span>       </div>       <div>       	<span class="pc_pinfo_form_label">Date of Birth:</span> <span id="DOB" class="ji_edit">${DOB}</span>       </div>       <div>       	<span class="pc_pinfo_form_label">Sex:</span> <span id="sex" class="ji_edit">${Sex}</span>       </div>                     <br style="clear:none;" />       <div id="pc_contact_info_area">       	<div><span class="pc_pinfo_form_label">Daytime Tel:</span> <span class="ji_edit" id="PatientPhone">${PatientPhone}</span></div>       	<div><span class="pc_pinfo_form_label">Alternate Tel:</span> <span class="ji_edit" id="PatientWorkPhone">${PatientWorkPhone}</span></div>       	<div><span class="pc_pinfo_form_label">Email:</span><span href="mailto:${email}" class="ji_edit" id="email">${email}</span></div>       </div>       <div id="pc_shipping_choices">{{tmpl(rd.pobj.info.pinfo) "#tmpl_address_mgmt"}}</div>';



/* Compile the markup as a named template */
$.template( "tmpl_pinfo_area_var", tmpl_pinfo_area_var_html );
  


var activate_pc_wobj = function(data, opts) {
	update_recent_patients_db();
rd.pobj=data;
		var pid=data.info.pinfo.patientid;
			rd.gRxN= typeof rd.pobj.instances.official[0] != "undefined" ? rd.pobj.instances.all[rd.pobj.instances.official[0]].RxNumber : null;

console.log(17 + ' - st - ' + $(window).scrollTop() );

				$('#tmpl_patient_chart').tmpl({pat:rd.pobj}).appendTo('#ma');
				//var tabs=[{'active': true, 'id': 'mainchart', 'title': 'Summary', 'class': ''}, {'active': false, 'id': 'labs', 'title': 'Labs', 'class': ''}];
				activate_patient_notes(rd.pobj.notes);

				createMedList();
				fillMedHistory(0);

				fillImpInfo();

				showOTCHistory();				
console.log(29  +' - st - ' + $(window).scrollTop() );
				// show rx history:
				$('#tmpl_rx_history').tmpl({rxs:getRxHistory(pid)}).appendTo('#rxHist_tbody');
				var rxh_c_to = setTimeout("pc_rxh_scrollbar=$('#rxhist_sb').tinyscrollbar({	wheel: 40.15	});", 1600);

				showSymptomHistory();
				showMedicalHistory();

		$('#HormoneSummary').html(determineDefs(rd.pobj['instances'][rd.gRxN],rd.pobj));
				
				showReportsList();
				showLabArea();				
				//fill_lab_order_area(); was for all labs, now they are split in a new area
				view_lab_chart();
				// investigate symptom graphing
console.log(44 +' - st - ' + $(window).scrollTop() );
				show_recent_activity(pid);	
//					setTimeout('show_pc_sum_symptoms();', 2200); // replaced with ups tracking
				//ups in summary:
				show_pc_ups_tracking();
				//ups in pinfo area:
				// - moved - activate_ups_tracking_window(rd.pobj.shipments, '#pc_pinfo_ups_track_tbl');

				show_pc_sum_report_list();
				show_patient_refills();
				getSymGraph()	;
console.log(55 +' - st - ' + $(window).scrollTop() );
				show_pinfo_area();
console.log(57 +' - st - ' + $(window).scrollTop() );
				activate_main_tabs();
	console.log(59 +' - st - ' + $(window).scrollTop() );
				activate_pc_chart_tabs();
	console.log(61 +' - st - ' + $(window).scrollTop() );
				activate_pc_history_tabs();
					$('#pc_hist_shipping').empty();
					$('#tmpl_ups_track_tbl').tmpl(rd.pobj.shipments).appendTo('#pc_hist_shipping');
					activate_ups_tracking_window(rd.pobj.shipments, '#ups_track_tbl');
					activate_messages_tbl(rd.pobj.messages);

				
				
	console.log(63 +' - st - ' + $(window).scrollTop() );
				activate_pc_pinfo_tabs();
	console.log(65 +' - st - ' + $(window).scrollTop() );
				show_recent_activity_pc_hist(pid);
	console.log(67 +' - st - ' + $(window).scrollTop() );				
				var rxh_u_to = setTimeout("pc_rxh_scrollbar.tinyscrollbar_update();", 3200);
	console.log(69 +' - st - ' + $(window).scrollTop() );
		if (typeof opts =='object' && opts['active_tab']) {
			console.log(opts['active_tab']);
		 $( "#ma_parent" ).tabs('select', opts['active_tab']);
		}
		
		
		
		
		$('#tmpl_msg_hist').tmpl(rd.pobj).appendTo('#pc_msg_hist'); 
		
		
		$( "#tb_pc_rxh" ).parent().bind( "tabsselect", function(event, ui) {console.log('selectl')});

	$('.ji_edit').editable(function(value, settings) { 
					$.postJSON('/api/v1/savePatientInformationChange', {key: this.id, val:value, patientID: rd.patientid}, function(msg) {
						display_errs(msg.errs, null, function(){eval(msg.errs[0].cb)} );
						if (msg.success) {
							$('#pinfo_msgs').html('').show()
							$('#pinfo_msgs').html('<span class="success">Saved!</span>').fadeOut(3600);

							} else {}
					});


			     return(value);
			  }, { 
			        submit  : 'OK',
			 }
			 );
			 	$('body').animate({scrollTop: $('body').offset().top}, 1); //hack fix
			 	$("div.contentbox").trigger('patient_chart_built');;
};

var activate_patientchart = function(pid, opts) {
		lh.push({loc: 'apc', self:[pid,opts] });
				jQuery.bbq.pushState({loc:lh.length-1});
		select_patient(pid, plist[pid]);
		//,  disabled: true
		var pct = [
			{active: true, id:'tb_pc_sum', title: 'Summary', href: 'pc_summary'   },
			{active: false, id:'tb_pc_rxp', title: 'Rx Pad', href: 'pc_rxpad'   , onclick_event: 'display_rxpad(' + pid + '); return false;'},
			{active: false, id:'tb_pc_lab', title: 'Chart', href: 'pc_full_chart'   },
			{active: false, id:'tb_pc_hist', title: 'History', href: 'pc_history'   },
			{active: false, id:'tb_pc_pinfo', title: 'Patient Info', href: 'pc_pinfo_area'   },

		];
							activate_area({ title: 'Patient Chart', tabs: pct});		
							
							if (typeof opts !='undefined' &&  opts.active_tab !='undefined' && $('#' + opts.active_tab)) {		
									$( "#ma_parent" ).tabs('select', opts.active_tab);
							}

							
/*
			{active: false, id:'tb_pc_hh', title: 'Health History', href: 'pc_hh'   },
			{active: false, id:'tb_pc_notes', title: 'Notes', href: 'pc_notes'   },
			{active: false, id:'tb_pc_labs', title: 'Labs', href: 'pc_labs'   },
			{active: false, id:'tb_pc_symptoms', title: 'Symptoms', href: 'pc_sym'   },
			{active: false, id:'tb_pc_rxh', title: 'Rx/OTC History', href: 'rx_history_pc'   }
*/				

				// get patient obj
	if (typeof opts == 'object' && typeof opts.pobj == 'object') { console.log('object available'); activate_pc_wobj(opts.pobj, opts); } else {
	
		$.postJSON('/api/v1/getPatientObj', {patientid: pid}, function(data) {  
			rd.pobj=data['data'];
			activate_pc_wobj(rd.pobj, opts);
	});

	
	


			lh.push({loc: 'pc', self:[pid,opts] });
				jQuery.bbq.pushState({loc:lh.length-1});
						//	$('#tb_pc_sum').hide();
}
};


var view_note_file = function(nfd) {
//	console.log(nfd);
// href="/private_files/' + encodeURIComponent(xat) + '/' + encodeURIComponent(nfd.noteid) + '/' + encodeURIComponent(nfd.notefile) + '/' + encodeURIComponent(nfd.filename) + '" target="_download"
$('#some_hidden_elm').html(
			'			<form method="post" target="_download" id="download_form" action="/private_files/' + encodeURIComponent(nfd.noteid) +'">' +
			'				<input type="hidden" name="xat" value="' +encodeURIComponent(sxat || xat) + '" />' +
			'				<input type="hidden" name="noteid" value="' +encodeURIComponent(nfd.noteid) + '" />' +
			'				<input type="hidden" name="notefile" value="' +encodeURIComponent(nfd.notefile) + '" />' +
			'				<input type="hidden" name="filename" value="' +encodeURIComponent(nfd.filename) + '" />' +
			'			</form>'
);
				
				$('#download_form').submit();
				$('#some_hidden_elm').empty();

};

var pn_table = new Object();
var activate_patient_notes = function(notes) {

	pn_table=$('#pat_notes_tbl').dataTable( {
	"bJQueryUI": true,
	'bAutoWidth':false,
		"aaData": toTableArray( jQuery.map(rd.pobj.notes, function(k,v) { 
			if (typeof k != 'object') {return;}
				var k_disp_txt = $('<span>' + k['noteText'] + '</span>');
						k_disp_txt.find('.soap_note').each(function(i, elm) {
							elm.textContent+=' ';
						});
						//$(k['noteText']).each(function(i, elm) { elm.textContent+=' '; } )


				var knt=k_disp_txt.text();
			k['noteText_s'] = (  knt.length > 67 ?  knt.substr(0,64) + '...' : knt);
			k['noteFile_f']='' ; if (k['noteFile']) { k['noteFile_f']='<a class="func_view_note_file" style="z-index:19;" data-notefile="' + encodeURIComponent(k['noteFile']) + '"  data-filename="' + encodeURIComponent(k['filename']) + '" data-noteid="' + encodeURIComponent(k['noteID']) + '" ><img class="file_icon"  src="/images/file.png" /></a>'; }  return k;  }), ['noteText_s', 'dateEntered_f', 'author', 'noteFile_f',  'data' ]),
		"aoColumnDefs": [
			{ "sTitle": "Note", "sClass": "center noteSumText", "aTargets": [ 0 ] , "sWidth": "40em"},
			{ "sTitle": "Date" , "aTargets": [ 1 ], "sWidth": "5em"},
			{ "sTitle": "Author" , "sName": "RxNumber", "aTargets": [ 2 ],  "sClass": "refillable", "sWidth": "8em"},
			{ "sTitle": "File", "sClass": "center small" , "aTargets": [ 3 ] , "sWidth": "3em"},
			{"bSearchable": false, "bVisible": false, "aTargets": [4]}
						],
		"0aoColumns": [
		],
		"aaSorting": [[1,'desc']],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			//var cn = nCloneTd.cloneNode( true );
			var jdata=JSON.parse(aData[4]);
			jQuery.data( nRow, 'note_id', jdata.note_id );
			
			if ( jdata.noteFile ) {	jdata.refillMsg='<span class="refillWarning">This prescription will need to be faxed to your health care provider for authorization to refill</span>';	}
			nRow.onclick=function(telm) {
	
			if ($(telm.target).parent('a.func_view_note_file')[0]) { return;
			var dobj=$(telm.target).parent('a.func_view_note_file')[0].dataset();
				
			} else {				popup('<div id="note_viewer">'+jdata['noteText']+'</div>', { title: "Note - " + jdata['dateEntered_f'] + ' by ' + jdata['author']  , width: 800});	}
			};

			// var sTitle = $('#tmpl_rollover_pinfo').tmpl(jdata).html();
			// nRow.setAttribute( 'title', sTitle );
			
			$(nRow).addClass(jdata.noteFile ? 'has_file' : 'no_file');
			return nRow;
		},
		"fnDrawCallback": function () {	//$('#plist_tbl tbody tr[title]').tooltip( {												"delay": 750,												"track": true,												"fade": 250											} );
		}
	} );




};








/* patient reported cmeds: */
var createMedList = function()
	{

		 $.each(rd.pobj['instances']['all'], function(i, conv){  if (conv['RxNumber']) {	$.tmpl(cmHeader, conv).appendTo('#curmedlist');}	});
		 		$('#curMedContainer').show('slow');
	};
	
var fillMedHistory = function(inst) {
$('#medhistorydisplay').empty();
var mhhtml=''; var mhcnt=0; $.each( rd.pobj['MedHistory'][inst],  function(index, conv){
if (typeof rd.pobj['MedHistory'][inst][index+1] != 'undefined') { 
	conv['showCombinedInfo']=rd.pobj['MedHistory'][inst][index+1]['medicationCombined'] == 'no' ? 'showCombinedInfo' : ' ';
if (conv['showCombinedInfo']==null) {conv['showCombinedInfo']='showLastMed';}
} else {conv['showCombinedInfo']='showLastMed';}
mhcnt % 2 ? 
			$.tmpl( cmListE, conv ).appendTo('#medhistorydisplay') : 
			$.tmpl( cmListO, conv ).appendTo('#medhistorydisplay') ;
		 mhcnt++;}); if (mhcnt < 1) {$('#medhistorydisplay').html('<div class="cmmed even">No Hormone Therapy Reported</div>');	}	};
	
	
	
	// Templates:
var cmListE = $.template( null,  '<div id="cm_${ID}" class="cmmed even combined${medicationCombined} ${showCombinedInfo}">${medicationName} ${medicationDosage} ${medicationUnit} <span class="medicationDelMethod">${medicationDeliveryMethod}</span><div class="cmnotes">${medicationNotes}</div></div>');
var cmListO =$.template( null, '<div  id="cm_${ID}"  class="cmmed odd combined${medicationCombined} ${showCombinedInfo}">${medicationName} ${medicationDosage} ${medicationUnit} <span class="medicationDelMethod">${medicationDeliveryMethod}</span><div class="cmnotes">${medicationNotes}</div></div>');
var cmHeader = $.template( null, '<li class="${active} ${RxNumber}" id="cm_${RxNumber}" onclick="activateMedList(\'${RxNumber}\');">${ShortDate}</li>');

var piDisplay = $.template( null, 
'	<div id="patientMoreInfo">	<img id="patientImage" src="/private_imgs/${patientID}patientPhoto.jpg?DoctorCode=${DoctorCode}" alt=" " style=" float:left;" />		${PatientLastName}, ${PatientFirstName} ${PatientMiddleInit}	<img id="morepatientinfoimage" style="max-height: 1.25em;" src="/images/aa_icons/next_icon.gif" onclick="showmorePinfo();" alt="More..."  class="showControl">	<ul id="morepinfo" style=""><li>DOB:  ${DOB}</li>			<li> ${PatientAddressLine1} <span style="display:block"> ${PatientAddressLine2} </span></li>			<li>${PatientCity}, ${PatientState} ${PatientZIP}</li>			<li>H: ${PatientPhone} W: ${PatientWorkPhone}</li>			<li><a href="mailto: ${email}">${email}</a></li>		</ul>	</div>'
);



var activateMedList = function(rxn) {fillMedHistory(rd.pobj.instances[rxn]); $('#curmedlist li.active').removeClass('active'); $('#cm_' + rxn).addClass('active');};

/* Health History Info */

var statusesLI = $.template( null, '<li class="${boldMe}">${shortName}: ${sVal}</li>');
var quotedHI=new Array('lastPeriod', 'currentIRMA.lastPeriod', 'hysterectomyDate', 'hysterectomy');


var fillImpInfo=
function () {
if (0 && typeof  rd.pobj["HH"][rd.pobj['instances'][rd.gRxN]] && rd.pobj["HH"][rd.pobj['instances'][rd.gRxN]]['EvaluationText']['importantInfo'] && rd.pobj["HH"][rd.pobj['instances'][rd.gRxN]]['hhstatus'] !='New') {
			// THIS WILL NEED TO BE ADDRES IN THE FUTUE: $('#importantInfo').innerHTML=rd.pobj["HH"][rd.pobj['instances'][rd.gRxN]]['EvaluationText']['importantInfo'];
} else {


		$.each(Object.keys(impInfs), function(i, sv) {

		if (rd.pobj['HH'][rd.pobj['instances'][rd.gRxN] || 0]['EAV'][sv]) {
			var snVal=rd.pobj['HH'][rd.pobj['instances'][rd.gRxN] || 0]['EAV'][sv];
			
						if (jQuery.inArray(sv, quotedHI) > -1 && snVal.match("[a-zA-Z]") && (jQuery.inArray( snVal, ['yes', 'no', 'Yes', 'No']) == -1 ) ) {				snVal='"'+snVal+'"';			} 
			 $.tmpl( statusesLI, {shortName: impInfs[sv], sVal: snVal} ).appendTo('.healthConditionsList');
			}
			});
	}
};




/* RxHist / OTC Hist */

var rxhist_row =  $.template( null, '<div id="RxHistory_${rxhist_num}" class="${rwClass} rxDrop" ><span>${DrugName}</span><span>${QuantityDisp}</span><span>${DatePrescribedADJ}</span><span class="RX-FillDates">${FillDatesADJ}</span><span>${RemainingRefills}</span><div class="sig">${Sig}</div></div>');

var otchist_row =  $.template( null, '<div id="OTCHistory_${rxhist_num}" class="${rwClass} otc_hist_entry rxDrop pq_entry" ><span class="otc_name">${DrugName}</span><span class="qty">${QuantityDisp}</span><span class="date_prescribed">${DatePrescribedADJ}</span><span class="OTC-FillDates">${FillDates}</span><div class="sig">${Sig}</div></div>');


var showOTCHistory =
function () {
var recHTML='';
if (typeof rd.pobj != 'object' || !rd.pobj["info"]["OTCHistory_all"] || !rd.pobj["info"]["OTCHistory_all"][0]) {
  $('#otcHist_tbody').html('<div style="text-align:center;">No OTCs on file for this Patient</div>'); return "<tr><td colspan='6'>No OTCs on file for this Patient</td></tr>"; }
 for (var o=0; o < rd.pobj["info"]["OTCHistory_all"].length; o++) {
 	var rec=rd.pobj["info"]["OTCHistory_all"][o];
 	rec["rxhist_num"]=o;
 	rec["rwClass"]= o %2 ? 'rodd' : 'reven';
 	
 	rec['FillDatesADJ']=rec['FillDates'] || rec['DateFilled_formatted'];
	rec['DatePrescribedADJ']=rec['DatePrescribed_formatted'];
			
	$.tmpl( otchist_row, rec ).appendTo('#otcHist_tbody');
 	}
 	return;
 	};
 	


var showLabArea = function() {

		$.postJSON('/api/v1/getLabHistory', {patientid: rd.pobj.info.pinfo.patientID, as_html: true}, function(data) {  
			$('#LabArea').html(data.data);
			});
	};


var showSymptomHistory = function() {
	// replace with a template / show default !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		$.postJSON('/api/v1/getSymptomHistory', {patientid: rd.pobj.info.pinfo.patientID, as_html: true}, function(data) {  
			$('#symptomList').html(data.data);
			$('#symptomsKeyChart').hide();
			});
	};


var showMedicalHistory = function(rxn) {
		rxn = rxn ? rxn : rd.gRxN;
		$.postJSON('/api/v1/getMedicalHistory', {rxn: rxn, patientid: rd.pobj.info.pinfo.patientID, as_html: true}, function(data) {  
			$('#medicalHistoryArea').html(data.data);
			});
	};


/* Report list */
var showReportsList = function() {

		$('#tmpl_report_list').tmpl({}).appendTo('#report_list'); 

};


var view_lab_report = function(rxn) {
//console.log('Try to view lab report: ' + rxn);
activate_patientchart(rlu[rxn], {'tab': 3});
};
/*.tickers[this.x]*/
var chart_ticks=new Object();
var lab_chart = new Object();
var view_lab_chart = function(self) {
if (typeof self !="object") {self=new Object(); self.labs=null;}
		$.postJSON('/api/v1/getLabGraph', {patientid: rd.pobj.info.pinfo.patientID, graphs: self.labs }, function(data) {  
					data.data.tooltip.formatter=function () {   return '<b>'+ chart_ticks['lab0'][this.series.name][this.x]['val'] +'</b><br/>'+ this.x +': '+ chart_ticks['lab0'][this.series.name][this.x]['hilo'] +'';	};
					lab_chart = new Highcharts.Chart(data.data);
					chart_ticks['lab0']=data.data.tickers;
			});
};



var update_chart = function(ld) {
lab_chart.series[0].setData(ld.data);
//lab_chart.name=ld.name;
lab_chart.chart.setTitle=ld.title;
lab_chart.chart.redraw();
};







var stored_notes;
var add_note_prompt = function(self) {
	stored_notes=$('#notes_storage').detach();
//could be IF mobile:	$('#tmpl_note_prompt').tmpl().appendTo('#pc_notes');
	$('#tmpl_note_prompt_mobile').tmpl().appendTo('#pc_notes');

	$('#decaf_area').hide();
	$('#soap_area').hide();
//	$(".fresheditable").fresheditor("edit", true);
//	$('#a_p_n_SOAP').prop('checked', true);
	
	
};




//var order_labs_tmpl =  $.template( null, '<div><span class="showControl" onclick="showMoreInfo( \'lab_product_list\', \'loKeyImg\');"><b>Order Labs</b> <img src="/images/buttons/next_icon.png" id="loKeyImg" style="margin: 0 .5em; max-height: 1.75em; position:relative; top: 0.5em;" alt="Key"></span></div><div id="lab_product_list" style="display:none;"><form id="order_labs_frm"><input type="hidden" name="patientid" value="${rd.patientid}" />{{each(i, lab) restoreProducts}}	<div class="lab_product">		<label class="label_check" for="labp_${i}">       <input type="checkbox" id="labp_${i}" name="lab_product" value="${lab.productID}"> ${lab.productDisplayName} - &#36;${lab.productPrice} (${lab.productCategory})</label>	</div>{{/each}}<div><textarea name="instructions" class="styled" onfocus="this.value=''; $(this).css(\'background-color\', \'#B6DFE7\');" onblur="$(this).css(\'background-color\', \'white\');">Enter your comment here...</textarea></div><div id="lo_subm"><input type="submit" id="lo_submit" value="Order Labs"  class=" ui-button ui-widget ui-state-default ui-corner-all" /></div></form></div>');



var fill_lab_order_area = function() {
	$('#tmpl_order_labs').tmpl({}).appendTo('#lab_order_area');
	
};

var save_note = function() {
$(".fresheditable").fresheditor("save", function(id, content) { console.log(id); console.log(content);});
};

var save_note_mobile = function() {
//	console.log(     $('#add_note_form').serializeObject()     );
	var newnotes= $('#add_note_form').serializeObject() ;
	if (newnotes.note_type == "decaf_area") {		}
	else {}
$(".fresheditable").fresheditor("save", function(id, content) { console.log(id); console.log(content);});
};


var add_note_and_refresh = function(noteid) {
	$.postJSON('/api/v1/getPatientNotes', {patientid: rd.patientid, noteid: noteid}, function(data) {  
									pn_table.fnClearTable();
									rd.pobj.notes.push(data.data[0]);	
									var new_data = toTableArray( jQuery.map(rd.pobj.notes , function(k,v) { 
										if (typeof k == 'object') {
											var knt=$('<span>' + k['noteText'] + '</span>').text();
											k['noteText_s'] = (  knt.length > 27 ?  knt.substr(0,24) + '...' : knt);
											k['noteFile_f']='' ; if (k['noteFile']) { k['noteFile_f']='<a class="func_view_note_file" style="z-index:19;" data-notefile="' + encodeURIComponent(k['noteFile']) + '"  data-filename="' + encodeURIComponent(k['filename']) + '" data-noteid="' + encodeURIComponent(k['noteID']) + '" ><img class="file_icon"  src="/images/file.png" /></a>'; }
											 return k;
											}
									   }), ['noteText_s', 'dateEntered_f', 'author', 'noteFile_f',  'data' ]);
									pn_table.fnAddData(new_data);
									pn_table.fnSort( [  [1,'desc'] ] );
									});
									};

var order_labs = function(lo) {
			$.postJSON('/api/v1/create_lab_order', lo, function(data) {  
								if (data && data.errs && data.errs[0]) {display_errs(data.errs, null, function(){eval(data.errs[0].cb) } );} else {
									$('#lab_product_list_' + lo.lab_type).html(data.data);
									/*	$('html, body').animate({scrollTop: $("#lab_order_area").offset().top}, 600); */

								}
			});

};

var show_recent_activity = function(pid, opt) {

		$.postJSON('/api/v1/getRecentActivity', {patientid: pid || rd.pobj.info.pinfo.patientID, as_html: true, height: 500}, function(data) {  
			$('#pc_recent_activity').html(data.data);
			var to = setTimeout("$('#recent_activity_sb').tinyscrollbar({	wheel: 0.15	});", 1600);
			});
	};

var show_recent_activity_pc_hist = function(pid, opt) {

		$.postJSON('/api/v1/getRecentActivity', {height: 600, patientid: pid || rd.pobj.info.pinfo.patientID, as_html: true, sb_id: 'recentActivity_div_sb'}, function(data) {  
			$('#recentActivity_div').html(data.data);
			var ra_c_to = setTimeout("recentActivity_div_sb_scrollbar=$('#recentActivity_div_sb').tinyscrollbar({	wheel: 0.15	});", 1100);
			var rxh_u_to = setTimeout("recentActivity_div_sb_scrollbar.tinyscrollbar_update();", 1900);


			});
	};



var show_patient_refills = function() {
	//var prr=$.map(acct.refill_reqs, function(rf) {if (rf.patientid == rd.patientid) {return rf} });
	$('#pc_refill_reqs').html('');
	$('#tmpl_pending_pat_rx_ref').tmpl({}).appendTo('#pc_refill_reqs'); 
	
};

var show_patient_refill = function(rfqn) {
	popup($('#tmpl_refill_q_entry').tmpl(acct.refill_reqs[rfqn]), { title: 'Refill Rx', height: 660, width: 700}); 
	$('#rfq_expanded-' + acct.refill_reqs[rfqn]['rxn']).show();
	//setTimeout("$('.pat_' + rd.patientid).slideDown();", 300);
	$('#li_rfq_' + acct.refill_reqs[rfqn]['patientid'] + '-' +acct.refill_reqs[rfqn]['rxn']).hide();
	};
	


var show_pc_ups_tracking = function() {
		$('#pc_package_tracking').html('');
		$('#tmpl_ups_track_list').tmpl(rd.pobj).appendTo('#pc_package_tracking');
		

};


	
var show_pc_sum_symptoms = function() {
	if(  $('#symptomListingTHEAD')[0]  ) {
		$('#pc_top_symptoms').html(pc_sum_tmpl); // turned off - instead using ups tracking here
		$('#pc_sum_symptomListingTHEAD').html($('#symptomListingTHEAD').html());
		$('#pc_sum_symptomListingTFOOT').html($('#symptomListingTFOOT').html());
		$('#pc_sum_symptomListingTBODY').html($('#symptomListingTBODY').html());
	}
};
	
var show_pc_sum_report_list = function() {
	//$('#pc_sum_report_list')
				
					$('#tmpl_report_list').tmpl({}).appendTo('#pc_sum_report_list');
};








var activate_pc_chart_tabs = function () {
	         $( "#pc_full_chart" ).tabs( "destroy" ); $( "#pc_full_chart" ).tabs({
	         select: function(event, ui) {
			if (ui.panel.id=="rx_history_pc") { 
				if(typeof pc_rxh_scrollbar == "object") { setTimeout('pc_rxh_scrollbar.tinyscrollbar_update();', 1100); }
			}
		} } );
	};





var activate_pc_history_tabs = function () {
	         $( "#pc_history" ).tabs( "destroy" ); $( "#pc_history" ).tabs({
	         select: function(event, ui) {
			if (ui.panel.id=="rx_history_pc") { 
				if(typeof pc_rxh_scrollbar == "object") { setTimeout('pc_rxh_scrollbar.tinyscrollbar_update();', 1100); }
			}
		} } );
	};

var activate_pc_pinfo_tabs = function () {
	         $( "#pc_pinfo_area" ).tabs( "destroy" ); $( "#pc_pinfo_area" ).tabs({
	         select: function(event, ui) {
			if (ui.panel.id=="rx_history_pc") { 
				if(typeof pc_rxh_scrollbar == "object") { setTimeout('pc_rxh_scrollbar.tinyscrollbar_update();', 1100); }
			}
		} } );
	};








var show_pinfo_area = function() {

$( "#pc_pinfo_address" ).html('');
//tmpl_address_mgmt_test
//	$('#tmpl_address_mgmt_test').tmpl(rd.pobj.info.pinfo).appendTo('#pc_pinfo_address'); 
var tpi=rd.pobj.info.pinfo;
$.tmpl( "tmpl_pinfo_area_var", tpi ).appendTo( "#pc_pinfo_address" );


//$('#tmpl_pinfo_area').tmpl(rd.pobj.info.pinfo).appendTo('#pc_pinfo_address');
	$('#pc_pinfo_billing').html('');
	$('#tmpl_add_patient_billing_info').tmpl(rd.pobj.info.pinfo).appendTo('#pc_pinfo_billing'); 

	//$('#pc_pinfo_billing').html($('#tmpl_add_patient_billing_info').tmpl(rd.pobj.info.pinfo).html());


//	$('#pc_pinfo_shipping').html($('#tmpl_ups_track_tbl').tmpl(rd.pobj.shipments).html());


	$('.use_datepicker').datepicker();
};




var changeShipTo=function(st){
$.postJSON('/api/v1/savePatientInformationChange', {key: 'shipTo', val:st.value, patientID: rd.patientid}, function(msg) {
						display_errs(msg.errs, null, function(){eval(msg.errs[0].cb)} );
						if (msg.success) {
							$('#pinfo_msgs').html('').show()
							$('#pinfo_msgs').html('<span class="success">Saved!</span>').fadeOut(3600);
							
							} else {}
					});
};


var pt_msg_popup = function(data) {
		if (typeof rd.pobj.messages[data.indx] == 'object') {
			popup(rd.pobj.messages[data.indx].message, {width: 750, title: 'Message from ' + rd.pobj.messages[data.indx].date_authored_f } );
			} else if (data.msgid) {
				var lookformi=data.msgid;
				var mmid = jQuery.grep(rd.pobj.messages, function( msg, indx ) { if( msg.id==lookformi ) {return msg;} }); 
				popup(mmid[0].message, {width: 750, title: 'Message from ' + mmid[0].date_authored_f } );
			}
};





