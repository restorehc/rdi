

var pc_rxh_scrollbar=new Object();


var isNum =function (n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
};



var activate_area = function(header) {

				$('#ma').empty();
				$('#ma_title_bar').empty();
				$('#tmpl_ma_tabs').tmpl(header).appendTo('#ma_title_bar');
				
};
var activate_main_tabs=function() {
         $( "#ma_parent" ).tabs( "destroy" ); $( "#ma_parent" ).tabs({select: function(event, ui) {
		if (ui.panel.id=="rx_history_pc") { 
			if(typeof pc_rxh_scrollbar == "object") { setTimeout('pc_rxh_scrollbar.tinyscrollbar_update();', 1100); }
		}
	} } );
};


//Popup dialog
var popup = function (message, opts) {

		if(! (typeof opts == 'object')) {opts=new Object(); }
	$( "#dialog-message" ).dialog( "destroy" );
		// display the message- moved up because of subsequent calls...
			$('#dialog-message').html(message);
			if (! typeof opts.modal) {opts.modal=true; }
			if (! typeof opts.width) {opts.width=660;}
			$('#dialog-message').dialog(opts);
			return;
	// calculate the values for center alignment
	var dialogTop =  (maskHeight/3) - ($('#dialog-box').height());  
	var dialogLeft = (maskWidth/2) - ($('#dialog-box').width()/2); 
	
	// assign values to the overlay and dialog box
	$('#dialog-overlay').css({height:maskHeight, width:maskWidth}).show();
	$('#dialog-box').css({top:dialogTop, left:dialogLeft}).show();
	
	// display the message
//	$('#dialog-message').html(message);
			
};

var closePopUp = function () {$('#dialog-overlay, #dialog-box').hide();                 $("#dialog-message").dialog("close" );             return false;};










var display_errs = function (errs, title, cb, data, opts) {
if (typeof errs && errs[0]) {
	if(! (typeof opts == 'object')) {opts=new Object(); }
	opts.title=title || "Messages";
	popup($('#tmpl_display_errs').tmpl({errs:errs, cb: cb, opt: opts || {}}), opts); 

		}
	};


$.postJSON = function(url, data, callback, async) {
		var t_data= data.data ? JSON.stringify(data) : JSON.stringify({data:data});
		async = ((typeof async == "undefined") ? true : async);
	if (async==false) {

		var rjson = $.ajax({
					  type: 'POST',
					  url: url,
					  data: t_data,
					  success: callback,
					  dataType: 'json',
					  async: async
				 }).responseText;
				 //console.log(url + ': ' + rjson);
				 var json = JSON.parse(rjson);
		 return json;
	}
	else {
		$.ajax({
			  type: 'POST',
			  url: url,
			  data: t_data,
			  success: callback,
			  dataType: 'json',
			  async: async
			});
	} 
 

};


$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};






//toggle patient selection
var select_patient_toggle = function(onoff) {
				if (onoff) {
						 		$('.selecting_patient').hide();
						 		$('.selected_patient').show();
						 		$('.patient_selected').show();
						 		
					 		} else {
						 		$('.selecting_patient').show();
						 		$('.selected_patient').hide();
						 		$('.patient_selected').hide();

						 		$('input.patient_search').focus();
						 		$('input.patient_search').autocomplete( "search", $('input.patient_search').val());
					 		}
};




var select_patient = function(pid, pdata) {
							console.log('pids: ' + rd.patientid + ' ?? ' + pid);
							if (rd.patientid != pid) {
							console.log('pids: ' + rd.patientid + ' =! ' + pid);
								rd.patientid=pid;
					 			rxpad.patientid=pid;
					 			if (lh[lh.length-1]['loc']=='pc') {
					 				activate_patientchart(pid, lh[lh.length-1]['self'][1]);
					 			}
					 			
															
							}
							rd.patientid=pid;
					 		rxpad.patientid=pid;
					 		delete rd.pobj;
					 		delete rxpad.recommendations;
					 		delete rxpad.rx_history;
					 		fill_patient_info(rd.patientid);
					 		// hide / loading Recs + Hist !!!!!! CHANGE
					 		getRecommendations(pid);
					 		getRxHistory(pid);
					 		pdata.label= pdata.label ? pdata.label : pdata.last_name +', ' + pdata.first_name + ' - '  + pdata.dob_f;
					 		$('.selected_patient').text(pdata.label);
					 		$('.selected_patient').dataset(pdata);
					 		$('input.patient_search').val(pdata.label);
					 		select_patient_toggle(true);
					 		};
					 		
var deselect_patient = function() {

							rd.patientid='';
					 		rxpad.patientid='';
					 		delete rxpad.recommendations;
					 		delete rxpad.rx_history;

					 		$('.selected_patient').text('');
					 		$('.selected_patient').dataset({});
					 		$('input.patient_search').val('');
					 		select_patient_toggle(false);

};	 		
					 		
var fill_patient_info = function(pid) {
var img_uri=encodeURI(Crypto.SHA256(doc.DoctorCode + rd.patientid));
var xat_uri=encodeURI(xat);
$('#action_info_area').replaceWith($('#tmpl_patient_area').tmpl({xu: xat_uri, iu:img_uri})); 
};
					 		
					 		
					 		
					 		
					 		
var display_dashboard = function() {
		lh.push({loc: 'dashboard', self:[] });
				jQuery.bbq.pushState({loc:lh.length-1});

				$('#rxpad').remove();
				activate_area({title: 'Dashboard'});
				
				deselect_patient();
				$('#tmpl_dashboard').tmpl({}).appendTo('#ma'); 
				if (Object.keys(plist).length) {list_recent_patients();} else {setTimeout('list_recent_patients();', 1800);}
				
				$('#tmpl_db_notices').tmpl({}).appendTo('#db_notices'); 
				$('#tmpl_db_diag_issues').tmpl({}).appendTo('#diag_issues'); 
				display_recent_diag_issues();
				acct.refill_reqs = jQuery.grep(acct.refill_reqs, function(ai) {if (ai.rxn) {return ai;}} );
				$('#tmpl_db_rx_issues').tmpl({}).appendTo('#rx_issues');
				//activate_main_tabs - none at this time
				
};					 		
					 		

var list_recent_patients = function() {
	$('#tmpl_db_recent_patients').tmpl({}).appendTo('#recent_patients'); 
};




var display_recent_diag_issues = function() {
	if ('list over table') {
			$('#diag_issues').empty();
			$('#tmpl_db_recent_diag_list').tmpl({}).appendTo('#diag_issues'); return;
			

	} else {
	if (! acct.diag_issues[0]) { return false;}
	$('#diag_issues').html( '<div class="listing"><table cellpadding="0" cellspacing="0" border="0" class="display" id="r_diag_i_tbl"></table></div>' );

	$('#r_diag_i_tbl').dataTable( {
	"bJQueryUI": true,
	"bAutoWidth": false,
		"aaData": toTableArray( jQuery.map(acct.diag_issues, function(k,v) {
				var pt=plist[k.patientid]; k.pname = pt['last_name'] + ', ' + pt['first_name'];	
					k['view_status_f']='' ; if (k['view_status']) { k['view_status_f']='<a class="func_view_lab_report" style="z-index:19;" data-rxn="' + encodeURIComponent(k['rxn']) + '"   ><img class="lab_viewed_icon"  src="/images/checkmark.png" /></a>'; }  	  return k;}), ['pname', 'sample_date_f', 'view_status_f', 'rx_status',  'data' ]),
		"aoColumnDefs": [
			{ "sTitle": "Name", "sClass": "center", "aTargets": [ 0 ] , "sWidth": "10em"},
			{ "sTitle": "Lab Date" , "aTargets": [ 1 ], "sWidth": "85px"},

			{ "sTitle": "Viewed" , "sName": "RxNumber", "aTargets": [ 2 ],  "sClass": "center refillable", "sWidth": "85px"},

			{ "sTitle": "Rx Updated", "sClass": "center small" , "aTargets": [ 3 ] , "sWidth": "85px"},
			{"bSearchable": false, "bVisible": false, "aTargets": [4]}
						],
		"0aoColumns": [

		],
		"aaSorting": [[2,'desc']],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			//var cn = nCloneTd.cloneNode( true );
			var jdata=JSON.parse(aData[4]);
			jQuery.data( nRow, 'patientid', jdata.patientid );
			var refillable=test(jdata);
			
			if (! refillable ) {	jdata.refillMsg='<span class="refillWarning">This prescription will need to be faxed to your health care provider for authorization to refill</span>';	}
			nRow.onclick=function(telm) {
				view_lab_report(jdata.rxn);
			};

			var sTitle = $('#tmpl_rollover_lab_info').tmpl(jdata).html();
			nRow.setAttribute( 'title', sTitle );
			
			$(nRow).addClass(refillable ? 'refillable' : 'needRefillAuth');
			return nRow;
		},
		"fnDrawCallback": function () {//	$('#r_diag_i_tbl tbody tr[title]').tooltip( {												"delay": 750,												"track": true,												"fade": 250											} );
		}
	} );
}


};


















var test= function(){;};

var activate_report=function(arg){console.log(arg);};



















var display_patient_list = function() {
				$('#rxpad').remove();
						lh.push({loc: 'pl', self:[] });
				jQuery.bbq.pushState({loc:lh.length-1});
				activate_area({title: 'Patient List'});
				deselect_patient();


	$('#ma').html( '<table cellpadding="0" cellspacing="0" border="0" class="display" id="plist_tbl"></table>' );

	$('#plist_tbl').dataTable( {
	"bJQueryUI": true,
		"aaData": toTableArray( jQuery.map(plist, function(k,v) {return k;}), ['last_name', 'first_name', 'dob_f', 'tstamp',  'data' ]),
		"aoColumnDefs": [
			{ "sTitle": "Last Name", "sClass": "center", "aTargets": [ 0 ] , "sWidth": "10em"},
			{ "sTitle": "First Name" , "aTargets": [ 1 ], "sWidth": "10em"},

			{ "sTitle": "DOB" , "sName": "RxNumber", "aTargets": [ 2 ],  "sClass": "refillable", "sWidth": "3em"},

			{ "sTitle": "Last Update", "sClass": "center small" , "aTargets": [ 3 ] , "sWidth": "6em"},
			{"bSearchable": false, "bVisible": false, "aTargets": [4]}
						],
		"0aoColumns": [

		],
		"aaSorting": [[3,'desc']],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			//var cn = nCloneTd.cloneNode( true );
			var jdata=JSON.parse(aData[4]);
			jQuery.data( nRow, 'patientid', jdata.patientid );
			var refillable=test(jdata);
			
			if (! refillable ) {	jdata.refillMsg='<span class="refillWarning">This prescription will need to be faxed to your health care provider for authorization to refill</span>';	}
			nRow.onclick=function(telm) {
					console.log('jdata');
					console.log(jdata.patientid);
					activate_patientchart(jdata.patientid);
			};

			var sTitle = $('#tmpl_rollover_pinfo').tmpl(jdata).html();
			nRow.setAttribute( 'title', sTitle );
			
			$(nRow).addClass(refillable ? 'refillable' : 'needRefillAuth');
			return nRow;
		},
		"fnDrawCallback": function () {	//$('#plist_tbl tbody tr[title]').tooltip( {												"delay": 750,												"track": true,												"fade": 250											} );
		}
	} );



};

var alter_css = function(selectorText, newStyle, newValue) {
	var ss = document.styleSheets;
	for (var i=0; i<ss.length; i++) {
            var rules = ss[i].cssRules || ss[i].rules;

            for (var j=0; j<rules.length; j++) {
                if (rules[j].selectorText === selectorText) {
                    rules[j].style[newStyle] = newValue;
                    return true;
                }
            }
        }
};









var set_top_tabs = function(tabs, bar) {

				$('#tmpl_tabs').tmpl(tabs).appendTo(bar);


};



var toTableArray = function(data, cols) {
	var na = new Array();
	$.each(data, function(index, value) { 	
		var nna = new Array();
		for (var ci =0; ci < cols.length;  ci++) {
			if (cols[ci] == 'data') {
				nna.push(JSON.stringify(value));
			} else {
				nna.push(value[cols[ci]]);
			}
			 }
		na.push(nna); 
	} );
	return na; 
};

var showMoreInfo=
function (mim, controller) {
	var info_d=$('#' +  mim);
	var cont=$('#' +  controller);
	if (info_d.css('display') == 'none') {
		cont.rotate({
	            angle: 0, 
	            animateTo:90
	          });
		info_d.show('slow');	          
	} else {
		cont.rotate({
	            animateTo:0
	          });
		info_d.hide('slow');
	}
};








 // BEGIN QTIP2:  
 
 // A shared object containing all the values you want shared between your tooltips
var sharedq = {
   position: {
      my: 'top center', 
      at: 'bottom center',
   },

   style: {
   classes: 'ui-tooltip-shadow-qtip2',
      tip: true,
      widget: true
   }
};
 
 
 
 
 
 

 
 
 
 
 



 // END QTIP2
