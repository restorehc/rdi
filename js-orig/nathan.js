var intervalWatcher;
$(document).ready(function() {

    $('#footer').wrapInner('<div id="footer-inner" />');

    $('#ma').bind('mouseenter', function() {

        // --- Interactivity on the Refill Prescription Page
        $('div.pat_:not(.ui-hover-bound)').bind('mouseenter', function() {
            $(this).addClass('hover');
        }).bind('mouseleave', function() {
            $(this).removeClass('hover');
        }).addClass('ui-hover-bound');

        // --- Generates the close button on the Refill Prescription Page
        $('#ma .pat_:not(.child2-close-bound) > div:nth-child(2)').each(function() {
//            $(this).append('<div class="child2-close">x</div>')
//            $(this).parent().addClass('child2-close-bound')
        });
    /* replaced below */    
        // --- Shows and hides the trigger/child rows on the Refill Prescription Page       
/*
        $('#ma .pat_ .rfg_refillLine').bind('click', function() {
            $(this).addClass('hide');
            $(this).next().removeClass('hide');
        });
		
		
        $('#ma .pat_ div:nth-child(2) .child2-close').delegate('click', function() {
            $(this).parent().addClass('hide');          
            $(this).parent().prev().removeClass('hide');            
        });                 
*/
/* with */

		$('body').delegate('.child2-close', click_event, function() {
            $(this).parent().hide();
            $(this).parent().prev().show();
        }); 


		$('body').delegate('.rfg_refillLine', click_event, function() {
				var rfpid=$(this).dataset('patientid');
				$('#refillQ_area .rfqx.pat_' + rfpid).show();
				$('#refillQ_area .rfql.pat_' + rfpid).hide();
        }); 


		$('body').delegate('#ma .pat_ div:nth-child(2) .child2-close', click_event, function() {
				var rfpid=$(this).dataset('patientid');
				$('#refillQ_area .rfqx.pat_' + rfpid).hide();
				$('#refillQ_area .rfql.pat_' + rfpid).show();
        }); 


/* END replacement */













        //--- Facilitates the plusses on the Recommendations form and the active class on the Template form
        /*
        $('#rxp_toolset span.ui-button-text:not(.ui-interval-watch-bound)').bind('click', function() {
            clearInterval(intervalWatcher);
            intervalWatcher = setInterval('watchForForm();',1);
        }).addClass('ui-interval-watch-bound');
*/
        // --- On hover, sets a delay on the sig rows, to expand and show full data
        var timeout;
        $('.ui-dialog .sig').hover(function() {
                timeout = setTimeout("$('.ui-dialog .sig:hover:not(.sig-active)').addClass('sig-active')",800);
            },function() {
                $(this).removeClass('sig-active');
                clearTimeout(timeout);
            }
        );

        // --- On hover, shows the "Fill Date List"
        $('.rx_hist_fill_dates').bind('mouseenter', function() {
            $(this).next().addClass('show');
        }).bind('mouseleave', function() {
            $(this).next().removeClass('show');
        });






    // --- Summary Page
    
        if($('#pc_sum_report_list').length > 0) {
            $('#pc_sum_report_list:not(.action-bound) h4').prependTo('#pc_sum_report_list');
            $('#pc_sum_report_list:not(.action-bound)').addClass('action-bound');
        }   
        if($('#pc_refill_reqs').length > 0) {
            $('#pc_refill_reqs:not(.action-bound) h4').prependTo('#pc_refill_reqs');
            $('#pc_refill_reqs:not(.action-bound)').addClass('action-bound');
        }   
        








        //--- Apply a corrective class on the UI Tabs
/*
        $('#ma #tabs .ui-tabs-nav').addClass('tabs-correct');       
        $('#ma_title_bar .smltabs').addClass('tabs-correct-2');             
   */
        
    });

});

//--- Adds the plusses on the Recommendations form
function watchForForm() {
    if($('#dialog-message').length > 0 && $('#dialog-message').closest('.ui-dialog').css('display')=='block') {
        /* Dialog is up!  Clear the interval Watcher */
        clearInterval(intervalWatcher);

        /* Special effects in the recommendations dialog */
/*        if($('#rxp_recommendations_prompt_form').length > 0) {
            $('form#rxp_recommendations_prompt_form .rec_medication.andor_and:not(.andor_and-processed)').each(function() {
                if($(this).closest('.rec_category').children().length > 1) {
                    $(this).append('<div class="rec_plus">+</div>');
                    $(this).addClass('andor_and-processed');
                }
            });
		}
        /* Special effects in the Template dialog */
        /* else if($('#rxp_rx_template0').length > 0) {
            $('#rxp_rx_template :checkbox').click(function() { 
          $(this).parents('form').toggleClass("rxtmp_active");
            });     
        }
        */

        /* Adds a div around the sig rows for styling purposes */
   /*
        else if($('#rxp_rx_history_prompt_form').length > 0) {
            $('.sig').each(function() {
                $(this).wrapInner('<div class="sig-corners" />');
            });
        }


*/
    }
}








