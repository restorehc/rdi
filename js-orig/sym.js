

	var defLabels = {'E_deficiency': 'Estrogen Deficiency', 'Estriol_deficiency':'Estriol Deficiency', 'P_deficiency': 'Progesterone Deficiency',
					 'T_deficiency': 'Testosterone Deficiency', 'DHEA_deficiency':'DHEA Deficiency', 'Thyroid_deficiency': 'Thyroid Deficiency',
					 'Cortisol_deficiency': 'Cortisol Deficiency', 'Melatonin_deficiency':'Melatonin Deficiency', 'HGH_deficiency': 'HGH Deficiency',
					 'Pregnelone_deficiency': 'Pregnelone Deficiency', 'Aldosterone_deficiency':'Aldosterone Deficiency',
					 
					 'E_excess': 'Estrogen Dominance / Progesterone - Ratio', 'Estriol_excess':'Estriol Excess', 'P_excess': 'Progesterone Excess',
					 'T_excess': 'Testosterone Dominance / Estrogen - Ratio', 'DHEA_excess':'DHEA Excess', 'Thyroid_excess': 'Thyroid Excess',
					 'Cortisol_excess': 'Cortisol Excess', 'Melatonin_excess':'Melatonin Excess', 'HGH_excess': 'HGH Excess',
					 'Pregnelone_excess': 'Pregnelone Excess', 'Aldosterone_excess':'Aldosterone Excess', 'default':'History of Reported'
					};
					
	var hs=new Array('E_deficiency','E_excess','P_deficiency','T_deficiency','T_excess', 'Cortisol_excess',  'Cortisol_deficiency',  'Thyroid_deficiency'); // 'P_excess', 'Thyroid_excess', has been removed

var focusHormone_hoverInt = function(h) {
	//console.log(h);
	var th=h.target;

	var rh=$(th).dataset('def');
	// update symptom list:
	if (rh != 'default' && rh) {
	switchSymptomsListing(rh);
	$('html, body').animate({scrollTop: $("#pc_symptoms_list").offset().top}, 600);
	} else {
		// should update Patient.PM to include this
	switchSymptomsListing('default');
	}
	
};



var focusHormone = function(rh) {
	// clear selected:
	$('.hs_li').removeClass('active');
	
	// update symptom list:
	if (rh != 'default' && rh) {
	switchSymptomsListing(rh);
	$('html, body').animate({scrollTop: $("#pc_symptoms_list").offset().top}, 600);
	$('.hs_li.' + rh).addClass('active');
	} else {
		// should update Patient.PM to include this
	switchSymptomsListing('default');
	}
	
	
};





	var focusHormoneTimer;
		startHormoneTimer = function(def, tme) {// alert("Starting "+ def + " with " + tme);
		 focusHormoneTimer = setTimeout ( 'focusHormone("'+def+'")', tme || 1500 );}
        determineDefs = function(inst) {
        	if (typeof inst != "number") {  return "<ul class='HormoneSummaryList'><li class='no_symptom_sum_hist'></li></ul>"; }
        	var dlist="<ul class='HormoneSummaryList'>";
			if (typeof rd.pobj["HH"][inst]['SymptomsTotal'] != 'undefined' && typeof rd.pobj["HH"][inst]['SymptomsDefTotal'] != 'undefined' ) {
        	for (var vv=0; vv <= hs.length; vv++ ) {	//hormones _e or _d
        		def=hs[vv];
				if (rd.pobj["HH"][inst]['SymptomsTotal'][def] && rd.pobj["HH"][inst]['SymptomsDefTotal'][def]) { 
        		var dP = parseInt(100*(
        		rd.pobj["HH"][inst]['SymptomsDefTotal'][def] / rd.pobj["HH"][inst]['SymptomsTotal'][def]
        		));
				if (dP > 20) 
				{
					dlist+="<li  class='hs_li func_focus_hormone "+ def +"' data-def='"+ def +"'>"+defLabels[def]+": "+ '<span class="click_info">Click for History</span><span class="symptomPoints">'+ dP +"%</span>\n\t<ul>";
					for (var vvv=0; vvv <= rd.pobj["HH"][inst]['order'][def].length; vvv++)
					{
						var key = rd.pobj["HH"][inst]['order'][def][vvv];
						if (!key) {continue;}
						var defP = parseInt(100*(	rd.pobj["HH"][inst]['Symptoms'][key][def]	/	rd.pobj["HH"][inst]['SymptomsDefTotal'][def]	)	);
						if (defP >= 5) 
						{
							// do something
							dlist+="\n\t\t<li  class=\"" + rd.pobj["HH"][inst]["Symptoms"][key]['keyValue']+ "\" onclick=\"triggerMoreInfo('"+key+"')\" class=''>"+symptomLabels[key]+'<span class="symptomPoints">('+defP+")</span> </li>\n";
							
						}
					}
					dlist+="\n\t</ul>\n</li>\n";
				}	//	else {dlist+="<li>"+defLabels[def]+" is OK</li>";}
	        } }
	        }
	        if (dlist == "<ul class='HormoneSummaryList'>") {
	        		if (Object.keys(rd.pobj["HH"][inst]['Symptoms']).length > 1) {	dlist +='<li style="color: green; font-weight:bold;" >No issues found <br/> based on reported symptoms</li>';	}
	        		else {	dlist+=	'<li style="color: black; font-weight:bold;" >No current symptoms reported</li>';	}
	        	
	        	} 
	        dlist+="\n</ul>";
	        return dlist; 
	        
        };

var symBEW=new Object();
symBEW['0']= 'symequal';
symBEW[ '1']= 'symbetter';
symBEW[ '-1']= 'symworse';

var swSymTR = $.template(null, '<tr id="${res}" class="symListingTR ${eoClass} ${symBEW}">\n'+
'			<th scope="row"  valign="top" class="column1 ${symptomslineClass}">\n' +
'			 <span class=" ${symBEW} ${otherClass}">${labelText}</span></th>\n'+ 
'{{each(i, td) tds }}{{tmpl(td) swSymTD}}{{/each}}\n</tr>');
//	{{each(i, td) tds }}{{tmpl swSymTD(td)}}{{/each}}
var swSymTD = $.template(null, '<td  class="${symptomslineClass}"><span style="z-index:10;" class="symptomsImg ${symLevel}" >{{if symLevel}}<img class="symptomsImg" src="/images/symptoms/${symLevel.toLowerCase()}.png">{{/if}}</span></td>');



var hi_hs_config = {    
     over: focusHormone, // function = onMouseOver callback (REQUIRED)    
     timeout: 500, // number = milliseconds delay before onMouseOut    
     out: function() {} // function = onMouseOut callback (REQUIRED)    
};

var switchSymptomsListing =
function (sorter, limiter, appender) {
	
	if (! appender) {appender=$('#symptomListingTBODY');}
		var patt1=/^currentSymptoms\.(.+)/;
	if(!sorter) {sorter='default';}
	// rd.gRxN
	//console.log(sorter);
	appender.empty();
	$.each(rd.pobj['HH'][rd.pobj['instances'][rd.gRxN]]['order'][sorter], function( num, res) {
			swSymTDHTML='';
			swSymTDHTML_array=new Array();
				var rres=res;
				var nres=res;
//			console.log(num); 			console.log(res);
			$.each(rd.pobj['HH'], function(i, hh){ 
				if(typeof hh['Symptoms'] != 'object') {return;}
				nres=res; 
				var mtch=patt1.exec(nres);
				if (mtch) {nres=mtch[1]; }
				
//			console.log(nres);				console.log(hh['Symptoms'][nres] || hh['Symptoms']);
				if (! hh['Symptoms'][nres] || ! hh['Symptoms'][nres]['keyValue'] ) { 
				 	if ( typeof hh['Symptoms']['currentSymptoms.' + nres] == "object" && hh['Symptoms']['currentSymptoms.' + nres]['keyValue']) { nres = 'currentSymptoms.' + nres; } else {
				 	swSymTDHTML_array.unshift({'symptomslineClass': (num % 5 ? '' : 'symptomsline'), 'symLevel': ''});
				 	return;} 
				 }
//				 console.log(nres + ': ' + hh['Symptoms'][nres]['keyValue']);
			//	 swSymTDHTML+=swSymTD.evaluate({'symptomslineClass': (num % 5 ? '' : 'symptomsline'), 'symLevel': hh['Symptoms'][nres]['keyValue']});
			swSymTDHTML_array.unshift({'symptomslineClass': (num % 5 ? '' : 'symptomsline'), 'symLevel': hh['Symptoms'][nres]['keyValue']});
			});
			//rd.gRxN
			if (typeof rd.pobj['HH'][
			(rd.pobj['instances'][rd.gRxN] || rd.pobj['instances']['official'][0]) ]
			['Symptoms'][rres] =="undefined") {
			return;
}
//			console.log(num + '. ' + nres + ' : ' + rres);

			var cobj={'res': res, 'eoClass': (num % 2 ? 'odd' : 'even'), 'symBEW': symBEW[
			rd.pobj['HH'][
			(rd.pobj['instances'][rd.gRxN] || rd.pobj['instances']['official'][0]) ]
			['Symptoms'][rres]['relativeToLast']], 'symptomslineClass': (num % 5 ? '' : 'symptomsline'), 'otherClass': rd.pobj['HH'][rd.pobj['instances'][rd.gRxN]]['Symptoms'][rres]['otherClass'], 'labelText': symptomLabels[res], tds: swSymTDHTML_array};
			$.tmpl(swSymTR, cobj).appendTo(appender);
//			console.log('tried');
//			console.log($.tmpl(swSymTR, cobj)[0]);
	});
	
	$('#pc_sym_caption').html(defLabels[sorter] + ' Symptoms');
	
};

var switchSymptomsListing_orig =
function (sorter) {
		var patt1=/^currentSymptoms\.(.+)/;
	if(!sorter) {sorter='default';}
	// rd.gRxN
	//console.log(sorter);
	$('#symptomListingTBODY').empty();
	$.each(rd.pobj['HH'][rd.pobj['instances'][rd.gRxN]]['order'][sorter], function( num, res) {
			swSymTDHTML='';
			swSymTDHTML_array=new Array();
				var rres=res;
				var nres=res;
//			console.log(num); 			console.log(res);
			$.each(rd.pobj['HH'], function(i, hh){ 
				nres=res; 
				var mtch=patt1.exec(nres);
				if (mtch) {nres=mtch[1]; }
				
//			console.log(nres);				console.log(hh['Symptoms'][nres] || hh['Symptoms']);
				if (! hh['Symptoms'][nres] || ! hh['Symptoms'][nres]['keyValue'] ) { 
				 	if ( typeof hh['Symptoms']['currentSymptoms.' + nres] == "object" && hh['Symptoms']['currentSymptoms.' + nres]['keyValue']) { nres = 'currentSymptoms.' + nres; } else {
				 	swSymTDHTML_array.unshift({'symptomslineClass': (num % 5 ? '' : 'symptomsline'), 'symLevel': ''});
				 	return;} 
				 }
//				 console.log(nres + ': ' + hh['Symptoms'][nres]['keyValue']);
			//	 swSymTDHTML+=swSymTD.evaluate({'symptomslineClass': (num % 5 ? '' : 'symptomsline'), 'symLevel': hh['Symptoms'][nres]['keyValue']});
			swSymTDHTML_array.unshift({'symptomslineClass': (num % 5 ? '' : 'symptomsline'), 'symLevel': hh['Symptoms'][nres]['keyValue']});
			});
			//rd.gRxN
			if (typeof rd.pobj['HH'][
			(rd.pobj['instances'][rd.gRxN] || rd.pobj['instances']['official'][0]) ]
			['Symptoms'][rres] =="undefined") {
			return;
}
//			console.log(num + '. ' + nres + ' : ' + rres);

			var cobj={'res': res, 'eoClass': (num % 2 ? 'odd' : 'even'), 'symBEW': symBEW[
			rd.pobj['HH'][
			(rd.pobj['instances'][rd.gRxN] || rd.pobj['instances']['official'][0]) ]
			['Symptoms'][rres]['relativeToLast']], 'symptomslineClass': (num % 5 ? '' : 'symptomsline'), 'otherClass': rd.pobj['HH'][rd.pobj['instances'][rd.gRxN]]['Symptoms'][rres]['otherClass'], 'labelText': symptomLabels[res], tds: swSymTDHTML_array};
			$.tmpl(swSymTR, cobj).appendTo('#symptomListingTBODY');
//			console.log('tried');
//			console.log($.tmpl(swSymTR, cobj)[0]);
	});
	
	$('#pc_sym_caption').html(defLabels[sorter] + ' Symptoms');
	
};



var chart_ticks=new Object();
var sym_chart = new Object();

var getSymGraph= function(self) {
if (typeof self !="object") {self=new Object(); self.sym_def=null;}
		$.postJSON('/api/v1/getSymGraph', {patientid: rd.pobj.info.pinfo.patientID, graphs: self.sym_def }, function(data) {  if(typeof data.data =='object' && data.data != 
	null ) {
					data.data.tooltip.formatter=function () {    return '<b>'+ chart_ticks['sym0'][this.series.name][this.x]['val'] +'</b>';	};
					sym_chart = new Highcharts.Chart(data.data);
					chart_ticks['sym0']=data.data.tickers;
					}
			});
};



var update_chart = function(ld) {
lab_chart.series[0].setData(ld.data);
//lab_chart.name=ld.name;
lab_chart.chart.setTitle=ld.title;
lab_chart.chart.redraw();
};






var pc_sum_tmpl =  '	<div id="pc_sum_symptomBox">   		<style>   		#pc_sum_symptomsKeyChart td {background:transparent;}   		</style>   		<table id="pc_sum_pc_symptoms_list" class="symtoms2" summary="Patient Symptoms">   			<caption id="pc_sum_pc_sym_caption">Report Symptoms</caption>   			<thead id="pc_sum_symptomListingTHEAD">   			</thead>   		    <tfoot id="pc_sum_symptomListingTFOOT">	   			</tfoot>   			<tbody id="pc_sum_symptomListingTBODY">   			</tbody>   		</table>   	</div>';



var triggerMoreInfo = function(symptom) {return;}