 /* delegates */

$(document).ready(function(){

/* refillQ */
		$("body").delegate('.func_unfilled_rx', click_event, function(href) { href.preventDefault(); display_refillQ(acct.refill_reqs);       });


		$('body').delegate('.rfg_refillLine', click_event, function() {
				var rfpid=$(this).dataset('patientid');
				$('#refillQ_area .rfqx.pat_' + rfpid).slideDown();
				$('#refillQ_area .rfql.pat_' + rfpid).hide();
        }); 


		$('body').delegate('#ma .pat_ div:nth-child(2) .child2-close', click_event, function() {
				var rfpid=$(this).dataset('patientid');
				$('#refillQ_area .rfqx.pat_' + rfpid).hide();
				$('#refillQ_area .rfql.pat_' + rfpid).show();
        }); 
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	$("body").delegate('.rq_form', 'submit', function(elm) {
			elm.preventDefault(); 
			if(this.dorefill.checked || this.dnr.checked || this.patContact.checked ) {
				sendRefillQ(this);
				this.style.background='#E0ECE1';
				$('#submit-' + this.rxn.value)[0].style.color='#999';
			} else {
			display_errs([{msg: 'Please check to authoize, decline, or have the patient contact you', type: 'warning'}]);
			}
			
		} );
	$("body").delegate('.func_goto_chart', click_event, function(elm) {
		elm.preventDefault(); activate_patientchart($(this).dataset('patientid') || rd.patientid);
		} );


	$("body").delegate('.func_goto_dashboard', click_event, function(elm) {
		elm.preventDefault(); display_dashboard(); 
		} );

	$("body").delegate('.func_goto_acct_mgmt', click_event, function(elm) {
		elm.preventDefault(); display_acct_mgmt(); 
		} );


	$("body").delegate('.func_patient_list', click_event, function(elm) {
		elm.preventDefault(); display_patient_list();
		} );

	$("body").delegate('.func_patient_chart', click_event, function(elm) {
		elm.preventDefault(); activate_patientchart($(this).dataset('patientid'));
		} );

	$("body").delegate('.func_view_lab_report', click_event, function(elm) {
		elm.preventDefault(); view_lab_report($(this).dataset('rxn'));
		} );

	$("body").delegate('.func_view_note_file', click_event, function(elm) {
		elm.preventDefault(); view_note_file($(this).dataset());
		} );

	$("body").delegate('.func_add_note', click_event, function(elm) {
		elm.preventDefault(); add_note_prompt($(this).dataset());
		} );

	$("body").delegate('.func_choose_note_type', click_event, function(elm) {
		//elm.preventDefault(); 
		$('.note_type_area').hide(); $('#' + $(this).val() ).show('slow');
		} );
	$("body").delegate('.func_cancel_note', click_event, function(elm) {
		elm.preventDefault(); 
						$('#pc_notes').empty();
						stored_notes.appendTo('#pc_notes');
		});
		

	$("body").delegate('.func_save_note', click_event, function(elm) {
		elm.preventDefault(); 
		var new_note='';
		var n_n=$(this).dataset();
		$(".fresheditable").fresheditor("save", function(id, content) { 
				new_note+='<div id="'+ ( id || '') +'">' + ( content || '' ) + '</div>';
			} );
			n_n['note_content']=encodeURIComponent(new_note);
			n_n['note_type']='Patient';
			console.log(n_n);
			$.postJSON('/api/v1/addPatientNote', n_n, function(data) {	
				if (data.success && data.data.noteID) {
									rd.pobj.notes.push(data.data);
									$('#add_note_form').find(':input:not(:disabled)').prop('disabled',true);
									// show success
									
									// fade
									$('#add_note_form').fadeOut("slow");
									// reinstate stored_notes
									stored_notes.appendTo('#pc_notes');
									// redraw them
									var new_data = toTableArray( jQuery.map([data.data], function(k,v) { 
				var knt=$('<span>' + k['noteText'] + '</span>').text();
			k['noteText_s'] = (  knt.length > 27 ?  knt.substr(0,24) + '...' : knt);
			k['noteFile_f']='' ; if (k['noteFile']) { k['noteFile_f']='<a class="func_view_note_file" style="z-index:19;" data-notefile="' + encodeURIComponent(k['noteFile']) + '"  data-filename="' + encodeURIComponent(k['filename']) + '" data-noteid="' + encodeURIComponent(k['noteID']) + '" ><img class="file_icon"  src="/images/file.png" /></a>'; }  return k;  }), ['noteText_s', 'dateEntered_f', 'author', 'noteFile_f',  'data' ]);
									
									pn_table.fnAddData(new_data);
									
									
				} else {
				//show failure! via display_errs
				}
				$('#add_note_form').remove();
				} );
    });



	$("body").delegate('.func_save_note_mobile', click_event, function(elm) {
		elm.preventDefault(); 
		var new_note='';
		var n_n=$(this).dataset();
		
					var newnotes= $('#add_note_form').serializeObject() ;

					if (newnotes.note_type == "decaf_area") {	new_note+='<div id="decaf_area">' + ( newnotes.decaf_area || '' ) + '</div>';	}
					else {
						var soaps=['soap_s', 'soap_o','soap_a','soap_p'];
						jQuery.each( soaps,  function(i, id) { 
								new_note+='<div class="soap_note" id="'+ ( id || '') +'">' + ( newnotes[id] || '' ) + '</div>';
								});
					}

			// need to uri encode for utf8: n_n['note_content']=new_note;
			n_n['note_content']=encodeURIComponent(new_note);

			n_n['note_type']='Patient';

			$.postJSON('/api/v1/addPatientNote', n_n, function(data) {	
				if (data.success && data.data.noteID) {
									rd.pobj.notes.push(data.data);
									$('#add_note_form').find(':input:not(:disabled)').prop('disabled',true);
									// show success
									
									// fade
									$('#add_note_form').fadeOut("slow");
									// reinstate stored_notes
									stored_notes.appendTo('#pc_notes');
									// redraw them
									var new_data = toTableArray( jQuery.map([data.data], function(k,v) { 
				var knt=$('<span>' + k['noteText'] + '</span>').text();
			k['noteText_s'] = (  knt.length > 27 ?  knt.substr(0,24) + '...' : knt);
			k['noteFile_f']='' ; if (k['noteFile']) { k['noteFile_f']='<a class="func_view_note_file" style="z-index:19;" data-notefile="' + encodeURIComponent(k['noteFile']) + '"  data-filename="' + encodeURIComponent(k['filename']) + '" data-noteid="' + encodeURIComponent(k['noteID']) + '" ><img class="file_icon"  src="/images/file.png" /></a>'; }  return k;  }), ['noteText_s', 'dateEntered_f', 'author', 'noteFile_f',  'data' ]);
									
									pn_table.fnAddData(new_data);
									
									
				} else {
				//show failure! via display_errs
				}
				$('#add_note_form').remove();
				} );
    });

	$("body").delegate('.func_cancel_note_mobile', click_event, function(elm) {
		elm.preventDefault(); 
		var new_note='';
		$('#add_note_form').find(':input:not(:disabled)').prop('disabled',true);
											// show success
											
											// fade
											$('#add_note_form').fadeOut("slow");
											// reinstate stored_notes
											stored_notes.appendTo('#pc_notes');		
											 $('.note_type_area').hide();
											 $('#add_note_form').remove();
		});



	$("body").delegate('.func_view_lab_chart', click_event, function(elm) { var tmp_arr=new Array(); var tmp_sc=$(this).dataset('lab');       tmp_arr = tmp_sc.split(',');   	view_lab_chart({labs:tmp_arr});	
		$('html, body').animate({scrollTop: $("#lab_graphing_area").offset().top}, 1200);
		} );



			$("body").on('submit', '#order_labs_saliva_frm',   function(elm) {elm.preventDefault();      var data=$(this).serializeObject(); 	data.patientid=rd.patientid;     order_labs(data);	return 1;	} );

//	$("body").delegate('#order_labs_saliva_frm', 'submit', function(elm) { 			elm.preventDefault();      var data=$(this).serializeObject(); 	data.patientid=rd.patientid;     order_labs(data);	return 1;	} );
	$("body").delegate('#order_labs_serum_frm', 'submit', function(elm) { 			elm.preventDefault();      var data=$(this).serializeObject(); 	data.patientid=rd.patientid;     order_labs(data);	return 1;	} );
	$("body").delegate('#order_otc_frm', 'submit', function(elm) { 							elm.preventDefault();      var data=$(this).serializeObject(); 	data.patientid=rd.patientid;     order_otcs(data); return 1;	} );   

	$("body").undelegate('#order_otc_frm', 'submit', function(elm) { 							elm.preventDefault();      var data=$(this).serializeObject(); 	data.patientid=rd.patientid;     order_otcs(data);	return 1;	} );   

		$("body").delegate('.label_check, .label_radio', click_event, function(href) {			setupLabel();        });
        setupLabel(); 



		$("body").delegate('.func_diag_issue', click_event, function(elm) {elm.preventDefault();  activate_patientchart($(this).dataset('patientid'), { active_tab: 'pc_full_chart'});      });



		$("body").delegate('.func_view_refill_req', click_event, function(elm) {elm.preventDefault();   show_patient_refill($(this).dataset('rfqn'));      });

		$("body").delegate('.func_focus_hormone', click_event, function(elm) {elm.preventDefault();  focusHormone( $(this).dataset('def'));      });







$('body').delegate('.qselector', 'mouseover', function(elm) {

	if (training_mode) {
   $(this).qtip($.extend({}, sharedq, {
      overwrite: false,
      content: qt2_msg(this),
      show: {
         event: event.type,
         ready: true
      }
   }));
}

});

	/*
   $(this).qtip($.extend({}, sharedq, {
      overwrite: false,
      content: qt2_msg(this),
      show: {
         event: event.type,
         ready: true
      }
   }));
   */



/* from nathan.js */
	$("body").delegate('.tmpl_checkbox', click_event, function(elm) {	$(this).parents('form').toggleClass("rxtmp_active");		} );
	
	/* rxpad */
		/* hijacks */	
		$("body").delegate('#return_to_pc_summary', click_event, function(elm) {			  elm.preventDefault();			activate_patientchart(rd.patientid, {pobj: rd.pobj});		} );
		$("body").delegate('#return_to_pc_full_chart', click_event, function(elm) {		 	elm.preventDefault();			activate_patientchart(rd.patientid, {pobj: rd.pobj, active_tab: 'pc_full_chart'});		} );
		$("body").delegate('#return_to_pc_history', click_event, function(elm) {			      elm.preventDefault();			activate_patientchart(rd.patientid, {pobj: rd.pobj, active_tab: 'pc_history'});		} );
		$("body").delegate('#return_to_pc_pinfo', click_event, function(elm) {			elm.preventDefault();			activate_patientchart(rd.patientid, {pobj: rd.pobj, active_tab: 'pc_pinfo_area'});		} );


		
	/* physioage */
	
	$('body').delegate( 'div.contentbox', 'patient_chart_built', function(elm) {
	
	//activate_ups_tracking_window(rd.pobj.shipments);
	// make date pickers
	$('.use_datepicker').datepicker();
	
}); 
	
	$('body').delegate( 'div.contentbox', 'patient_messages_update', function(elm) {
			$.postJSON('/api/v1/getPatientMessages', {patientID: rd.patientid, patientid: rd.patientid}, function(data) {	
					if (data.success && (typeof data.data =='object' || typeof data.data =='array')) {
									rd.pobj.messages=data.data;
									// rebuild summary
									$('#pc_msg_hist').html('');
									$('#tmpl_msg_hist').tmpl(rd.pobj).appendTo('#pc_msg_hist'); 
									// rebuild table
									activate_messages_tbl(rd.pobj.messages);
					}
				});
	}); 






	
	$('body').delegate( '.func_pb_tracking', click_event, function(elm) {
		  var data=$(this).dataset();
		  if (! data.trackNum) {data.trackNum=data.tracknum;}
			elm.preventDefault(); get_ups_track_info(data);
		});
	
	$("body").delegate('#get_ins_frm', 'submit', function(elm) { 	elm.preventDefault();      var data=$(this).serializeObject(); 	     store_billing(data);	return 1;	} );

	$('body').delegate( '.func_view_pmsg', click_event, function(elm) {
		  var data=$(this).dataset();
			elm.preventDefault(); pt_msg_popup(data);
		});


	$('body').delegate( '.func_take_to_message_hist', click_event, function(elm) {
		  var data=$(this).dataset();
			elm.preventDefault(); 
			$( "#ma_parent" ).tabs('select', '#pc_history');
			$( "#pc_history" ).tabs('select', '#pc_hist_messages');
		});
		
/* addresses */

	$("body").delegate(".add_address_button", "click", function(elm) {
		elm.preventDefault(); add_address_prompt($(this).dataset());
	});
	
	$("body").delegate(".add_address_form_post", "submit", function(elm) {
		  var data=$(this).serializeObject();
		elm.preventDefault(); 
			if(data.addresstype == 'primary') {save_new_address(data);}
			else {add_adr(data);}
	});
	
	
	$("body").delegate(".func_add_alt_address", "submit", function(elm) {
		  var data=$(this).serializeObject();
		elm.preventDefault(); add_adr(data);
	});
	
	$("body").delegate(".func_del_alt_address", "click", function(elm) {
		  var data=$(this).dataset();
		elm.preventDefault(); del_adr_confirm(data);
	});
	
	$("body").delegate(".func_confirmed_del_adr", "click", function(elm) {
		  var data=$(this).dataset();
		elm.preventDefault(); del_adr(data);
	});
		
	
	$("body").delegate(".func_edit_alt_address", "submit", function(elm) {
		  var data=$(this).serializeObject();
		elm.preventDefault(); edit_adr(data);
	});
	
	$("body").delegate(".func_add_alt_address_date", "submit", function(elm) {
		  var data=$(this).serializeObject();
		elm.preventDefault(); add_adr_date(data);
	});
	
	$("body").delegate(".func_del_alt_address_date", "submit", function(elm) {
		  var data=$(this).serializeObject();
		elm.preventDefault(); del_adr_date(data);
	});
	
	$("body").delegate(".toggle_edit_alt_address", "click", function(elm) {
		elm.preventDefault(); 
		var aid = $(this).dataset('id');
		$('#alt_add_' + aid).hide();
		$('#alt_add_form_' + aid).show();
		$('#toggle_save_alt_address_' + aid).show();
		$(this).hide();

	});

	$("body").delegate(".toggle_save_alt_address", "click", function(elm) {
		elm.preventDefault(); 
		var aid = $(this).dataset('id');
		$('#alt_add_' + aid).show();
		var aform=$('#address_alt_' + aid);
		  var data=aform.serializeObject();
		  console.log(aform);
		  console.log(data);
		aform.hide();
		$('#toggle_edit_alt_address_' + aid).show();
		$(this).hide();
		// save form:
		edit_adr(data);		
		
		
	});	









	$("body").delegate(".toggle_edit_pri_address", "click", function(elm) {
		elm.preventDefault(); 

		$('#pri_add').hide();
		$('#address_primary').show();
		$('#toggle_save_pri_address').show();
		$(this).hide();

	});

	$("body").delegate(".toggle_save_pri_address", "click", function(elm) {
		elm.preventDefault(); 

		$('#pri_add').show();
		var aform=$('#address_primary');
		  var data=aform.serializeObject();
		aform.hide();
		$('#toggle_edit_pri_address').show();
		$(this).hide();
		// save form:
		edit_adr(data);		
		
		
	});	








	$("body").delegate(".toggle_edit_pinfo", "click", function(elm) {
		elm.preventDefault(); 

		$('#pinfo_display').hide();
		$('#pinfo_edit_form').show();
		$('#toggle_save_pinfo').show();
		$(this).hide();

	});

	$("body").delegate(".toggle_save_pinfo", "click", function(elm) {
		elm.preventDefault(); 

		$('#pinfo_display').show();
		var aform=$('#pinfo_edit_form');
		  var data=aform.serializeObject();
		aform.hide();
		$('#toggle_edit_pinfo').show();
		$(this).hide();
		// save form:
		edit_pinfo(data);		
		
		
	});	













 $("body").delegate('.clear_on_click', click_event, function(elm) {$(this).val('');} );


 $("body").delegate('.func_view_all_diag_issues', click_event, function(elm) {var data=$(this).dataset(); activate_all_diag_issues(); if(data.hidelist=="true") {$('#diag_issues_shortlist').hide();}} ); 

	$("body").delegate('#update_email', 'submit', function(elm) {
			elm.preventDefault(); 
			var data=$(this).serializeObject();
			update_provider_email(data);
		} );


	$("body").delegate('#change_password_form', 'submit', function(elm) {
			elm.preventDefault(); 
			var data=$(this).serializeObject();
			update_provider_auth(data);
		} );




	$("body").delegate('#help_button', click_event, function(elm) {
			elm.preventDefault(); 
			console.log(lh[lh.length-1]);
			popup(help[lh[lh.length-1].loc].msg, help[lh[lh.length-1].loc].opts);
			
		} );






/* title/history for HIPAA */
/*
	$(window).bind('hashchange', function() {
		document.title='RestoreDirect.com'
	});
*/
/* /HIPAA */
			/* fixes */
			/* broken */
			


/* TURNING OFF 2012-07-02 because this event already fires and seems to break if not on the element itself -- strange...

/* TURNING ON 2012-07-10 because this event does fire when rx.js:76 creates the element */
					$("body").delegate('#tb_pc_rxp', click_event, function(elm) {
	console.log('delegate.js 241');
				elm.preventDefault();
				display_rxpad({patientid:rd.patientid});	
					} );
/*
*/
/* fix tiny sb */
$("body").delegate('.jqscrollbar .thumb', 'hover', function(elm) {
				console.log( $(this).parent().parent().parent().tinyscrollbar_update('relative') );
			} );	

$("body").delegate('#ma_tabs li', 'tabsselect', function(elm) {
	$("div.contentbox").trigger('view_change');
});
$("body").delegate('div.contentbox', 'view_change', function(elm) {
	$('.jqscrollbar').each(function(ie, ve) {$(ve).tinyscrollbar_update();})
});


   });

   
   
 /* END fixes */
 
 




