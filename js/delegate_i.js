 /* delegates */

$(document).ready(function(){

		$("body").delegate('.func_view_queue_all', click_event, function(href) { href.preventDefault(); display_Qs();       });
		$("body").delegate('.func_view_shipping', click_event, function(href) { href.preventDefault(); display_shipping_area();       });   
		$("body").delegate('.func_view_autoship', click_event, function(href) { href.preventDefault(); display_autoship();       });   
   		$("body").delegate('.func_get_ship_quote', 'submit', function(elm) { 
   				  var data=$(this).serializeObject();
				elm.preventDefault(); 
				$('#sq_return').html('<img src="/images/ajax-loader-blue.gif" />');
				get_ship_quote(data);
   		});
		$("body").delegate('.func_update_as_entry', click_event, function(href) { href.preventDefault(); 
				var tmp_vals = new Object();
				var tmp_rxn=$(this).dataset('rxn');
				tmp_vals['rxn']=tmp_rxn;
				$('.as_rxn_' + tmp_rxn).each(function(i, elm) { console.log(elm); console.log( $(elm).prop('type') + $(elm).val());
					if ( $(elm).prop('type')  == 'checkbox'  ) {tmp_vals[$(elm).prop('name')]=$(elm).prop('checked');	}	else {	tmp_vals[$(elm).prop('name')]=$(elm).val();	}
				});
				$.postJSON('/api/v1.i/updateAutoShipItem', tmp_vals, function(data) {
				display_autoship();
		       });
		       });

			$("body").delegate('.asrd_f30', click_event, function(href) { href.preventDefault(); 
					var tmp_ds=$(this).dataset();
					var ldf=new Date(tmp_ds['lf']);
					var days=tmp_ds.skip *1 || 60;
					console.log(days)
					var stds = new Date(ldf.getTime() + days *24*60*60*1000);

					var tmpdate=stds.getFullYear() + '-' + (stds.getMonth() +1 > 9 ? ''+ (stds.getMonth() +1) : '0' + (stds.getMonth() +1)) + '-' + (stds.getDate() > 9 ? ''+ stds.getDate()  : '0' + (stds.getDate() ) );
					console.log(tmpdate);
					$('#asrd_' + tmp_ds.rxn).val(  tmpdate   );
				});

			$("body").delegate('.func_view_as_detail', click_event, function(href) { href.preventDefault(); 
				var tmp_vals = new Object();
				var tmp_rxn=$(this).dataset('rxn');
				tmp_vals['rxn']=tmp_rxn;
				$.postJSON('/api/v1.i/getASDetail', tmp_vals, function(data) {
				var msgs=new Array();
				//msgs=data.errs;
				msgs.push({msg: $('#tmpl_as_details_addl_funcs').tmpl(data.data).html(), type: 'info', html: true});
				console.log( "msgs.push({msg: $('#tmpl_as_details_addl_funcs').tmpl(" +  JSON.stringify(data.data) + ").html(), type: 'info', html: true})");
				console.log( 'display_errs( ' + JSON.stringify(msgs) +", tmp_rxn, null , {}, {width: '500'});");
					display_errs(msgs, tmp_rxn, null , {}, {width: '500'});
				});
			});
							 

		$("body").delegate('.func_remove_as_entry', click_event, function(href) { href.preventDefault(); 
				var tmp_vals = new Object();
				var tmp_rxn=$(this).dataset('rxn');
				tmp_vals['rxn']=tmp_rxn;
				$.postJSON('/api/v1.i/removeAutoShipItem', tmp_vals, function(data) {display_autoship();});
				});
				
		$("body").delegate('.func_getASReport_off', 'submit', function(href) { href.preventDefault(); 
							var startstop= $('#pull_as_report').serializeObject() ;
							$.postJSON('/api/v1.i/getASReport', startstop, function(data) {
								$('#as_return_link').html(data.data.link);
							});
				});
				

		$("body").delegate('.func_getASReport0', 'submit', function(href) { href.preventDefault(); 
							var startstop= $(this).serializeObject() ;
							$.postJSON('/api/v1.i/get_as_barcode_report', startstop, function(data) {
							popup_new_window(data.data.as_html);
								
							});
				});
				



//getAS0RefillReport

		$("body").delegate('.func_add_as_entry', 'submit', function(href) { href.preventDefault(); 
							var startstop= $('#add_as_entry').serializeObject() ;
							$.postJSON('/api/v1.i/addAutoShipItem', startstop, function(data) {
								// if success? !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
								display_errs(data.errs);
								$('.clear_on_escape').val('');
								$('#add_as_confirm').html('');
								$('#add_as_entry').each(function(){	this.reset();	});
								$('#aas_rdate').val('');
								display_autoship();
								as_table.fnFilter('', 4);
								as_table.fnFilter('', ast_pid_col);
								as_table.fnFilter('');
								as_table.fnFilter( startstop.rxn )
							});
				});
				
				
				
				
		$("body").delegate('#as_tbl_filter > label > input', click_event, function() {as_table.fnFilter('', ast_pid_col); as_table.fnFilter('', 4);});

		$("body").delegate('.func_ast_filter_pid', click_event, function(href) {href.preventDefault(); as_table.fnFilter(''); as_table.fnFilter('^'+$(this).dataset('pid')+'$', ast_pid_col, true); return false; });



		$("body").delegate('#add_as_rxn_conf', click_event, function(eml) {
										if ($('#add_as_rxn').val().length < 7) { return;}
										$.postJSON('/api/v1.i/confirmAutoShipItem', {rxn: $('#add_as_rxn').val()} , function(data) {
												$('#add_as_confirm').html(data.data.confirm); return false; });
												}
												);



				$("body").on("change click", ".as_change_trigger", function() {
						console.log($(this).dataset('rxn'));
							  $('#as_save_rxn_' + $(this).dataset('rxn')).addClass('as_entry_changed');
				});


				$("body").on("submit", ".func_save_as_addl", function(rfm) {
						rfm.preventDefault(); 
		   				var fdata=$(this).serializeObject();
						$.postJSON('/api/v1.i/updateAutoShipItem', fdata , function(data) {
														display_errs(data.errs);
														display_autoship();
						});
						});


				$("body").on(click_event, ".func_view_as_holds", function(rfm) {
						rfm.preventDefault(); 
								as_table.fnFilter('', ast_pid_col);
								as_table.fnFilter('');
								as_table.fnFilter('Hold', 4);
								
								});


				$("body").on("dblclick", ".as_note_row", function(rfm) {
						//rfm.preventDefault(); 
		   				//var fdata=$(this).serializeObject();
		   				var fdata=$(this).dataset();
		   				var nfd=new Object();
		   				nfd.nstatus= fdata.nstatus == 1 ? 0 : 1;
		   				nfd.rxn=fdata.rxn;
						nfd.id=fdata.nid;
						var relm=this;
						$.postJSON('/api/v1.i/updateASNote', nfd , function(data) {
														if (data.data.success > 0) {
														console.log($(rfm.target)[0]);


														$(relm).toggleClass('nstatus_on', nfd.nstatus == 1);
														$(relm).toggleClass('nstatus_off', nfd.nstatus == 0);
														$(relm).dataset('nstatus', nfd.nstatus);


														/*
														$(rfm.target).toggleClass('nstatus_on', nfd.nstatus == 1);
														$(rfm.target).toggleClass('nstatus_off', nfd.nstatus == 0);
														$(rfm.target).dataset('nstatus', nfd.nstatus);
														*/
														}
						});
				});




				$("body").on(click_event, ".func_refresh_as_tbl", function() {	display_autoship();	});


		$("body").on('submit', '#rxq_update',  function(eml) {

							var rxi= $(this).serializeObject() ;
							 eml.preventDefault(); 
							$.postJSON('/api/v1.i/updateRxQInfo', rxi , function(data) {	display_Qs(); return false; });
				});

		$("body").on(click_event, '.func_add_rmd_prov',  function(eml) {
							var rxi= $(this).dataset() ;
							rxi.address=$('#' + rxi.addresselm)[0].innerHTML;
							$('#tmpl_rmd_prov_insert').tmpl(rxi).appendTo('#rmd_email_body_provs');
				});



		$("body").on(click_event, '.func_show_addl_funcs.cs',  function(eml) {
							activate_cs_addl_funcs();
						});
		$("body").on(click_event, '.func_show_addl_funcs.lab',  function(eml) {
							activate_lab_addl_funcs();
						});
		$("body").on(click_event, '.func_show_addl_funcs.rx',  function(eml) {
							activate_cs_addl_funcs();
						});



		$("body").on(click_event, '.func_search_rmd_prov',  function(eml) {
						$('#ma').html('');	
						$('#tmpl_rmd_search_area').tmpl({}).appendTo('#ma');
						});

		$("body").on('submit', '#rmd_email',  function(eml) {
							var rxi= $(this).serializeObject() ;
							eml.preventDefault(); 
							
							$.postJSON('/api/v1.i/email_via_ses', rxi, function(data) {
								if (data.data.resp[0] == 200 ) {$('#rmd_prov_results').html('<div style="color: #649B5D;">Sent to ' + rxi.to + '</div>'); $('#rmd_email_body_provs').html(''); }
								});
							this.reset();
						
						});
						

		$("body").on('submit', '#send_sms_away',  function(eml) {
							var rxi= $(this).serializeObject() ;
							eml.preventDefault(); 
							
							$.postJSON('/api/v1.i/send_sms_via_2600hz', rxi, function(data) {
								if (data.data.resp[0] == 200 ) {$('#sms_resp_results').html('<div style="color: #649B5D;">Sent ' + scnt + 'Messages</div>');  }
								});
							this.reset();
						
						});





			$("body").on('submit', '#lo_search_frm',  function(eml) {
										var lo= $(this).serializeObject() ;
										eml.preventDefault(); 
										
										$.postJSON('/api/v1.i/get_lab_order', lo, function(data) {
											if (data.data.success ) {
												fill_lo_form(data.data.lo);
												
											}
											});
										this.reset();
									
									});

			
			$("body").on('change', '.order_labs_frm input',  function(eml) {
				lo_check_price();
											});
			$("body").off('submit', '.order_labs_frm');
			$("body").on('submit', '.order_labs_frm',   function(eml) {
										var lo= $(this).serializeObject() ;
										lo.price=lo_check_price;
										eml.preventDefault(); 
										$.postJSON('/api/v1.lab/update_order', lo, function(data) {
											if (data.data.success ) {
														fill_lo_form(data.data.lo);
														lo_update_indicator(1);
											}
										});
									});

			$("body").on('submit', '#lo_update_payment_frm',   function(eml) {
										var lo= $(this).serializeObject() ;
										eml.preventDefault(); 
										
										$.postJSON('/api/v1.lab/update_paid', lo, function(data) {
											if (data.data.success ) {
												$.postJSON('/api/v1.i/get_lab_order', lo, function(datau) {
													if (datau.data.success ) {
														fill_lo_form(datau.data.lo);
														lo_update_indicator(1);
													}
												});
											}
											});
										this.reset();
									});




			$("body").on('submit', '#lk_rec_kit',   function(eml) {
										var lk= $(this).serializeObject() ;
										eml.preventDefault(); 
										
										$.postJSON('/api/v1.lab/update_kit', lk, function(data) {
											if (data.success ) {
												lo_update_indicator(1);
												display_errs(data.errs);
												 rd.tv.lo=data.data.lo;
											}
											});
										
									});
	$("body").on('submit', '#rsf0',   function(eml) {
										var lk= $(this).serializeObject() ;
										eml.preventDefault(); 
										console.log(lk);
												$.postJSON('/api/v1.lab/update_scans', lk, function(data) {
														if (data.success ) {
															display_errs(data.errs);
															display_recent_scans();
															}
											});
					});
										
			$("body").on('submit', '#lk_ssts',   function(eml) {
										var lk= $(this).serializeObject() ;
										eml.preventDefault(); 
										
										$.postJSON('/api/v1.lab/update_kit_times', lk, function(data) {
											if (data.success ) {
												lo_update_indicator(1);
												display_errs(data.errs);
												 rd.tv.lo=data.data.lo;
											}
											});
										
									});
									
			$("body").on(click_event, '.func_display_lab_kit_review',   function(eml) {
										var lk=$(this).dataset();
										eml.preventDefault(); 
										console.log(lk.id)
										lo_receive_kit(lk.id);
									});




				$("body").on(click_event, ".func_refresh_loq_tbl", function() {	display_loq();	});
				$("body").on(click_event, ".func_refresh_view_scans", function() {	display_recent_scans();	});


				$("body").on(click_event, ".func_refresh_lkrq_tbl", function() {	display_lksrq();	});
				$("body").on(click_event, ".func_refresh_lsrq_tbl", function() {	display_lsrq();	});

				$("body").on(click_event, ".func_view_lkrq", function() {	display_lksrq();	});
				$("body").on(click_event, ".func_view_lom", function() {	activate_lab_order_area();	});
				$("body").on(click_event, ".func_display_lab_order", function(eml) {	
										var lo=$(this).dataset();
										eml.preventDefault(); 
										popup($('#tmpl_lab_order_mgmt_area').tmpl({}).html(), {title: "Kit: " + lo.id, width: '720px'});
										$.postJSON('/api/v1.lab/get_lab_order', lo, function(data) {
											if (data.data.success ) {
												fill_lo_form(data.data.lo);
											}
											});
										});




			$("body").on('submit', '#lab_results_select_id',   function(eml) {
										var lk= $(this).serializeObject() ;
										eml.preventDefault(); 
										display_enter_labs(lk.rxn);
										
									});

			$("body").on('submit', '.func_post_lab_results',   function(eml) {
										var lr= $(this).serializeObject() ;
										eml.preventDefault(); 

										$.postJSON('/api/v1.lab/post_lab_results', lr, function(data) {
											if (data.data.success ) {
												console.log(lr['labResults.0allrows::RxNumber']);
												console.log(data.errs)
												display_enter_labs(lr['labResults.0allrows::RxNumber']);
												console.log('data.errs now')
												display_errs(data.errs);
											}
											});

										
									});



				$("body").on(click_event, ".func_display_lab_send_report_review", function(eml) {	
										var lo=$(this).dataset();
										eml.preventDefault(); 
										$.postJSON('/api/v1.lab/display_send_report', lo, function(data) {
											if (data.data.success ) {
											popup(data.data.html, {title: "Report: " + lo.id, width: '720px', html: 1});
											}
											});
										});




			$("body").on('submit', '.func_as_estimate_report',   function(eml) {
										var lr= $(this).serializeObject() ;
										eml.preventDefault(); 

										$.postJSON('/api/v1.i/get_as_estimate', lr, function(data) {
														popup($('#tmpl_as_estimate_area').tmpl({days: data.data}).html(), {title: "Autoship " + lr.start_date + ' - ' + lr.end_date, width: '720px'});
											});
									});




			$("body").on('submit', '.func_insurance_receipt_gen',   function(eml) {
										var lr= $(this).serializeObject() ;
										eml.preventDefault(); 

										$.postJSON('/api/v1.i/rx_insurance_receipt', lr, function(data) {
														popup_new_window(data.data.receipt_html);
											});
									});


			$("body").on('click', '.md_float_clipper', function(elm) {console.log(this); $(this).addClass('on');});

			$("body").on('submit', '#lo_transfer_frm',   function(eml) {
										var lr= $(this).serializeObject() ;
										eml.preventDefault(); 

										$.postJSON('/api/v1.lab/transfer_rowID', lr, function(data) {
														display_errs(data.errs);
														if (data.data.success) {
															
															$.postJSON('/api/v1.lab/get_lab_order', {id: lr.new_id}, function(data) {
																if (data.data.success ) {
																	fill_lo_form(data.data.lo);
																	}
																});
														}
											});
									});				


			$("body").on('submit', '.lo_transfer_id',   function(eml) {
										var lr= $(this).serializeObject() ;
										eml.preventDefault(); 

										$.postJSON('/api/v1.lab/transfer_rowID', lr, function(data) {
														display_errs(data.errs);
														if (data.data.success) {
																
														}
											});
									});




			$("body").on('submit', '#lo_change_qtype_frm',   function(eml) {
										var lr= $(this).serializeObject() ;
										eml.preventDefault(); 

										$.postJSON('/api/v1.lab/change_qType', lr, function(data) {
														display_errs(data.errs);
														if (data.data.success) {	}
										});
					});
					
					
			$("body").on('submit', '.func_update_hh_values',   function(eml) {
										var lr= $(this).serializeObject() ;
										eml.preventDefault(); 

										$.postJSON('/api/v1.lab/update_hh_values', lr, function(data) {
														display_errs(data.errs);
														if (data.data.success) {	}
										});
					});




			$("body").on('submit', '.func_update_hh_status',   function(eml) {
										var lr= $(this).serializeObject() ;
										eml.preventDefault(); 

										$.postJSON('/api/v1.lab/update_hh_status', lr, function(data) {
														display_errs(data.errs);
														if (data.data.success) {	}
										});
					});




			$("body").on('submit', '.func_add_reported_med',   function(eml) {
										var lr= $(this).serializeObject() ;
										eml.preventDefault(); 

										$.postJSON('/api/v1.lab/add_reported_med', lr, function(data) {
														display_errs_inline(data.errs, 'cmed_msg_area');
														if (data.data.success) {	
																														if(! lr['lab_id']) { lr['lab_id']=lr.labid;}
																									setTimeout(function(){display_cmeds_area(lr);}, 500);

															
														}
										});
					});

			$("body").on('click', '.func_del_reported_med',   function(eml) {
										var lr= $(this).dataset() ;
										eml.preventDefault(); 

										$.postJSON('/api/v1.lab/del_reported_med', lr, function(data) {
														display_errs_inline(data.errs, 'cmed_msg_area');
														if (data.data.success) {	
																														if(! lr['lab_id']) { lr['lab_id']=lr.labid;}
																									setTimeout(function(){display_cmeds_area(lr);}, 500);
														}
										});
					});




			$("body").on('submit', '.func_add_logic_meds',   function(eml) {
										var lr= $(this).serializeObject() ;
										eml.preventDefault(); 

										$.postJSON('/api/v1.lab/add_logic_meds', lr, function(data) {
														display_errs(data.errs);
														if (data.data.success) {	console.log('needs refreshing...')}
										});
					});


					
			$("body").on('submit', '.func_prep_reports_area',   function(eml) {
										var lr= $(this).serializeObject() ;
										eml.preventDefault(); 

										$.postJSON('/api/v1.lab/get_prep_reports', lr, function(data) {
														display_errs(data.errs);
														$('#prep_reports_area').html(data.data.html);
														if (data.data.success) {	
															
															
														}
										});
					});
					

			$("body").on('click', '.func_show_edit_hh_manual',   function(eml) {
										var lr= $(this).dataset() ;
										eml.preventDefault();
										$('#hhAreaDivo').show();
										});


			$("body").on('submit', '.func_print_lab_sample_sheets',   function(eml) {
										var lr= $(this).serializeObject() ;
										eml.preventDefault();
										$.postJSON('/api/v1.lab/print_sample_sheets', lr, function(data) {
														display_errs(data.errs);
														if (data.data.success) {	
																popup_new_window(data.data.ss_html);
														}
										});
					});




			$("body").on('submit', '.func_generate_office_supply_kits',   function(eml) {
										var lr= $(this).serializeObject() ;
										eml.preventDefault();
										$.postJSON('/api/v1.lab/generate_office_supply_kits', lr, function(data) {
														display_errs(data.errs);
														if (data.data.success) {	
																var noids=data.data.oids.join("\n");
																$('#generate_barcodes_ta').prop('value', noids);
														}
										});
					});
			$("body").on('click', '.func_gaok',   function(eml) {
										var lr= $(this).serializeObject() ;
										eml.preventDefault();
										display_gaok();
					});


			$("body").on('click', '.func_get_cmeds_area',   function(eml) {
										var lr= $(this).dataset() ;
										if(! lr['lab_id']) { lr['lab_id']=lr.id || lr.labid;}
										eml.preventDefault();
										display_cmeds_area(lr);
										
					});


				$("body").on(click_event, ".func_view_lk_dashboard", function() {	display_gaok();	});

				$("body").on(click_event, ".func_getExpiringCreditCards", function(eml) {	
										eml.preventDefault();
										$.postJSON('/api/v1.i/getExpiringCreditCards', {}, function(data) {
														display_errs(data.errs);
														if (data.data) {	
																$('#expcc_return_area').html('');

																$('#tmpl_expiring_cc').tmpl({expp: data.data}).appendTo('#expcc_return_area');
														}
										});
					});
					
				$("body").on(click_event, ".func_pt_contacted_exp_cc", function(eml) {	
										eml.preventDefault();
										var lr = $(this).dataset() ;
										$(this).html('Contacted Today'); $(this).addClass('ccexp_last_contacted');
										$.postJSON('/api/v1.i/set_pt_contacted', lr, function(data) {
														display_errs(data.errs);
														
												});
										});


	$("body").on(click_event, ".func_close_popup",  function(elm) {		elm.preventDefault(); closePopUp();	});
	$("body").on(click_event, ".func_view_patient_smss",  function(elm) {		elm.preventDefault();  var lr = $(this).dataset() ;
					$.postJSON('/api/v1.i/view_patient_smss', lr, function(data) {
								$('#tmpl_display_sms_history').tmpl({smss: data.data.smses, sms_pt: lr.smsbnbr}).appendTo('.pt_sms_history');
												});
});

/* SMS-related */


	$('body').on(click_event, '.func_send_expiring_cc_sms' , function(elm) {		elm.preventDefault();  var ds = $(this).data() ; 
			console.log(ds);
				ds.message='RESTORE HEALTH: Your credit card has expired.  Update here: ' + ds.url + ' or call 800-558-7046.  [Opt-Out: "STOP"]';
				ds.contacts=ds.sms_number;
											$.postJSON('/api/v1.i/send_sms_via_2600hz', ds, function(data) {
														display_errs(data.errs);
												});
	} );

	$('body').on(click_event, '.meta_search_plist' , function(elm) {	elm.preventDefault(); $(this).val(''); });

	$('body').on('focus', '.meta_search_plist' , function(elm) {	elm.preventDefault(); 	activate_meta_search_on_elm($(this) , function(tmp_pid){     					
																					$.postJSON('/api/v1.i/view_patient_smss', {patientid: tmp_pid || rd.patientid}, function(data) {
																										$('.pt_sms_history').html('');
																										$('#tmpl_display_sms_history').tmpl({smss: data.data.smses, sms_pt: data.data.sms_number}).appendTo('.pt_sms_history');
																										
								
																				});
								});
		});


	$('body').on(click_event, '.func_send_ccd_sms' , function(elm) {		elm.preventDefault();  var ds = $(this).data() ; 
				ds.contacts=ds.sms_number || plist[rd.patientid]['sms_nbr'];
				ds.pid=rd.patientid;

					$.postJSON('/api/v1.i/get_shorten_link', {patientid: ds.pid, url: 'https://secure.restorehc.com/api/v1.e/func/update_billing_form.html'}, function(ln_data) {
										ds.url=ln_data.data.short_link;
										ds.message='RESTORE HEALTH: An error occurred in processing payment for your order.  Update here: ' + ds.url + ' or call 800-558-7046.  [Opt-Out: "STOP"]';

												$.postJSON('/api/v1.i/send_sms_via_2600hz', ds, function(data) {
														display_errs(data.errs);
															$.postJSON('/api/v1.i/set_pt_contacted', ds, function(condata) {
																									display_errs(condata.errs);
														
															});
												});
				});
	} );




	$('body').on(click_event, '.func_send_ccd_sms' , function(elm) {		elm.preventDefault();  var ds = $(this).data() ; 
				ds.contacts=ds.sms_number || plist[rd.patientid]['sms_nbr'];
				ds.pid=rd.patientid;

					$.postJSON('/api/v1.i/get_shorten_link', {patientid: ds.pid, url: 'https://secure.restorehc.com/api/v1.e/func/update_billing_form.html'}, function(ln_data) {
										ds.url=ln_data.data.short_link;
										ds.message='RESTORE HEALTH: An error occurred in processing payment for your order.  Update here: ' + ds.url + ' or call 800-558-7046.  [Opt-Out: "STOP"]';

												$.postJSON('/api/v1.i/send_sms_via_2600hz', ds, function(data) {
														display_errs(data.errs);
															$.postJSON('/api/v1.i/set_pt_contacted', ds, function(condata) {
																									display_errs(condata.errs);
														
															});
												});
				});
	} );






/*
PRIM_REJEC,C,3 = NOT NULL
PCTYPE = 3
NEED_TO_PO,C,3	 = YES
NO_LABEL_P,C,3 = YES
CAN_BE_CHE,C,3 = NO
*/





/* custom ma_partner dashboard */
	$('body').on('submit', '.func_get_ma_partner_data' , function(elm) {		elm.preventDefault();
											var ds= $(this).serializeObject() ;
											ds.no_voids=true;
	$.postJSON('/api/v1.2.reports/get_ma_partner_data',ds, function(data) {
											show_ma_partner_data('#ma_partner_data_table', data.data);
											$('<a href="#" class="func_tbl_to_csv" data-tableid="ma_partner_data_table">Export to CSV</a> ').appendTo('#ma_partner_return_link');
											});
									});
/* END ma_partner */





/* charging */


	$('body').on(click_event, '.func_get_unpaid_charges' , function(elm) {		elm.preventDefault();
											var ds= $(this).serializeObject() ;
											$.postJSON('/api/v1.2.i/get_unpaid_charges',{}, function(data) {  $('#charges_area').html(''); charges=data.data;
												$('#tmpl_view_charge_list').tmpl({charges: data.data, sorted_charges: Object.keys(data.data).sort(function(a, b) {   if ( (typeof plist[a] =="object" && typeof plist[b] == "object") && (plist[a]['last_name'] && plist[b]['last_name'])) {
														return plist[a]['last_name'].toLowerCase().localeCompare(plist[b]['last_name'].toLowerCase());
													} else {return} })
													
												}).appendTo('#charges_area');
													
											});
									});

	$('body').on('submit', '.func_charge_patients',function(elm) {		elm.preventDefault();
											var ds= $(this).serializeObject() ;
											$.postJSON('/api/v1.2.i/post_charges',{transactions:ds}, function(data) { console.log(data.data);  $('#charges_area').html(''); charges=data.data;
											display_errs(data.errs);
											});
									}); 



	$('body').on('submit', '.func_update_pt_cc', function(elm) {		elm.preventDefault();
											var ds= $(this).serializeObject() ;
											$.postJSON('/api/v1.2.i/update_an_cc', {'new_card': ds.new_card, 'new_exp': ds.new_exp, patientID: rd.patientid}, function(data) { console.log(data.data);  
												display_errs(data.errs);
											});
									}); 

	$('body').on('submit', '.func_list_charges', function(elm) {		elm.preventDefault();
											var ds= $(this).serializeObject() ;
											$.postJSON('/api/v1.2.i/update_an_cc', {'new_card': ds.new_card, 'new_exp': ds.new_exp, patientID: rd.patientid}, function(data) { console.log(data.data);  
												$('#charge_list_return_area').html(''); 
												$('#tmpl_show_charges_line').tmpl({charge_list:data.data}).appendTo('#charge_list_return_area');

												});
									}); 



	$('body').on(click_event, '.func_toggle_cb_class' , function(elm) {		elm.preventDefault(); 	var bcct = $(this).data("cbtype"); if ($(this).data("cboff")) { $('input.' +bcct +':checkbox').removeAttr('checked');	} else {$('input.' +bcct +':checkbox').prop("checked", true);}							});

	$('body').on(click_event, '.func_toggle_pt_cbs' , function(elm) {		elm.preventDefault(); 	var tpid = $(this).data("patientid"); 			$('input.pid_' + tpid).prop("checked", !$('input.pid_' + tpid).prop("checked"));				});
	
	$('body').on('submit', '.func_get_pt_update_billing_link', function(elm) {		elm.preventDefault();
											var ds= $(this).serializeObject() ;
											$.postJSON('/api/v1.i/get_shorten_link', ds, function(data) {
											console.log(data.data);
											$('#billing_url_return_area').html(data.data.short_link); 
												});
									});





/* /END CHARGING */


	$('body').on(click_event, '.func_show_lab_info_form' , function(elm) {		elm.preventDefault(); 	var trxn = $(this).data("rxn"); 			$('#lab_info_form_area_Divo').show();
					});








/* CF partners */



			$("body").on('click', '.func_popup_rxtrans_details',   function(eml) {
										var lo= $(this).data()
										eml.preventDefault(); 
										$.postJSON('/api/v1.2.cf/get_rx_trans_details', lo, function(data) {
											if (data.data.success ) {
														
														popup($('#tmpl_ndc_dialog_tbody').tmpl(data.data).html(), {title: "NDCs", width: '420px'});
											}
										});
									});






		$("body").on('submit', '#rxq_cf_accept',  function(eml) {

							var rxi= $(this).serializeObject();
							rxi.rx_ids=$.map($('#rxp_meds input[name="rx_ids"]:checked'), function(elm, ei) {return $(elm).prop('value');});
							 eml.preventDefault(); 
							$.postJSON('/api/v1.2.cf/accept_cf_rx', rxi , function(data) {	
							display_errs(data.errs);
							 });
				});
		$("body").on('submit', '#rxq_cf_reject',  function(eml) {

							var rxi= $(this).serializeObject();
							rxi.rx_ids=$.map($('#rxp_meds input[name="rx_ids"]:checked'), function(elm, ei) {return $(elm).prop('value');});
							 eml.preventDefault(); 
							$.postJSON('/api/v1.2.cf/reject_cf_rx', rxi , function(data) {								display_errs(data.errs);});
				});


		$("body").on('submit', '.func_generate_dispensing_label', function(href) { href.preventDefault(); 
							var linfo = $(this).serializeObject() ;
							$.postJSON('/api/v1.2.disp/generate_cf_dispensing_label', linfo, function(data) {
							popup_new_window(data.data.label_html);
								
							});
				});



/*  END CF partners	*/














			$("body").on('click', '.func_tbl_to_csv',   function(eml) {
										var edata= $(this).dataset() ;
										eml.preventDefault();
										
										var csv_win=window.open((URL.createObjectURL(datauri_to_blob(("data:text/tsv;charset=utf-8," + encodeURIComponent($('#' + edata.tableid).table2CSV({separator: '\t', delivery: 'value'} ) ))   ) ) ));
										//csv_win.close();
					});









			$("body").on('click', '.func_change_date_input',   function(eml) {
										var lo= $(this).data()
										eml.preventDefault(); 
										console.log(lo);
										change_date_input(lo.dateinput, lo.days);
								});


$(document).keyup(function(e) {
  //if (e.keyCode == KEYCODE_ENTER) { $('.save').click(); } 
  if (e.keyCode == KEYCODE_ESC) { console.log('escape'); $('.clear_on_escape').val(''); } 
});



/* Did not work??
		$("body").delegate('.as_program_select', click_event, function(eml) {
		console.log($(eml).val());
								if ($(eml).val()=='DS') { $('.as_program_asdate.as_rxn_' + $(this).dataset('rxn')).hide();}
								else if ($(eml).val()=='nth') { $('.as_program_asdate.as_rxn_' + $(this).dataset('rxn')).show();}
								});

*/


   	   });
		
   
