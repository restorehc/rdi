


var pts_table=new Object();
var ups_tracking_table = new Object();
var activate_ups_tracking_window = function(ups_tracks, tblid) {
	if (! tblid) { tblid='#ups_track_tbl';}
	pts_table=$(tblid).dataTable( {
	"bDestroy": true,
	"bJQueryUI": true,
	'bAutoWidth':false,
		"aaData": toTableArray( jQuery.map(ups_tracks, function(k,v) { 
				var knt=$('<span>' + k['noteText'] + '</span>').text();
			k['noteText_s'] = (  knt.length > 67 ?  knt.substr(0,64) + '...' : knt);
			k['noteFile_f']='' ; if (k['noteFile']) { k['noteFile_f']='<a class="func_view_note_file" style="z-index:19;" data-notefile="' + encodeURIComponent(k['noteFile']) + '"  data-filename="' + encodeURIComponent(k['filename']) + '" data-noteid="' + encodeURIComponent(k['noteID']) + '" ><img class="file_icon"  src="/images/file.png" /></a>'; }  return k;  }), [ 'shipDate_f', 'trackNum', 'serviceType',  'data' ]),
		"aoColumnDefs": [
			{ "sTitle": "Ship Date", "sClass": "center", "aTargets": [ 0 ] , "sWidth": "8em"},
			{ "sTitle": "Tracking" , "aTargets": [ 1 ], "sWidth": "15em"},
			{ "sTitle": "Service", "sClass": "center small" , "aTargets": [ 2 ] , "sWidth": "9em"},
			{"bSearchable": true, "bVisible": true, "aTargets": [0]}
						],
		"0aoColumns": [
		],
		"aaSorting": [[1,'desc']],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			//var cn = nCloneTd.cloneNode( true );
//			console.log('pa-26');
//			console.log(aData[3]);
			
			var jdata=JSON.parse(aData[3]);
			jQuery.data( nRow, 'note_id', jdata.note_id );
			
			if ( jdata.noteFile ) {	jdata.refillMsg='<span class="refillWarning">This prescription will need to be faxed to your health care provider for authorization to refill</span>';	}
			nRow.onclick=function(telm) {
	
			if ($(telm.target).parent('a.func_view_note_file')[0]) { return;
			var dobj=$(telm.target).parent('a.func_view_note_file')[0].dataset();
				
			} else { get_ups_track_info(jdata);	}
			};

			// var sTitle = $('#tmpl_rollover_pinfo').tmpl(jdata).html();
			// nRow.setAttribute( 'title', sTitle );
			
			$(nRow).addClass(jdata.noteFile ? 'has_file' : 'no_file');
			return nRow;
		},
		"fnDrawCallback": function () {	
		}
	} );
};

var rtups='';
var get_ups_track_info = function(tid) {
				
			$.postJSON('/api/v1.i/getTracking', {tracking_num: tid.trackNum  }, function(msg) {
			console.log(tid);
			console.log(msg.data);
			msg.data['tracking_num']=tid.trackNum;
				popup($('#tmpl_ups_tracking_info').tmpl(msg.data).html(),  { title: "Shipped - " + (typeof msg.data.raw !='undefined' ? msg.data.raw.Shipment.PickupDate : tid.shipDate_f) + ' via ' + (typeof msg.data.raw !='undefined' ? msg.data.raw.Shipment.Service.Description  : tid.serviceType) , width: 800});

				
			});



};









/* Add Patient with billing option after dup check: */


var duplicate_check_diplay = function(msg) {
	var apresp=$('#ap_resp');
	apresp.fadeOut('fast', function() { apresp.empty(); 
		$('#tmpl_display_errs').tmpl({errs:msg.errs, opt:{noclose:true}}).appendTo(apresp);
		if (typeof msg.data == 'object' && (typeof msg.data.dc == 'array' || typeof msg.data.dc == "object" )) {
			$('#tmpl_dupl_patients').tmpl(msg.data).appendTo(apresp);
			// activate pa.dataset()
			$('#d_p_c_ap').dataset(msg.data.ap);
			
		}
		if (typeof msg.data == 'object' && typeof msg.data.pad == 'object') {
			
			// successfully added patient, add to list
			plist[msg.data.pad.patientid]=msg.data.pad;
			// switch to it...
			select_patient(msg.data.pad.patientid, msg.data.pad);


/* Add Billing +++ */
		var add_billing_link='<a href="#" onClick="add_billing_info_prompt(); return false;">Add Billing Information</a>';
		$('#tmpl_display_errs').tmpl({errs:[{msg: add_billing_link, type: 'option', html: true}], opt:{noclose:true}}).appendTo(apresp);

/* END BILLING +++ */

			}
apresp.fadeIn('fast');
			});
//			 $('#ap_resp').fadeIn('fast');;
	};





var add_billing_info_prompt = function() {
	popup('<div id="tmp_billingprompt"></div>',  { title: "Add Billing Info", width: 800});
	$('#tmpl_add_patient_billing_info').tmpl({}).appendTo('#tmp_billingprompt');
};


var store_billing = function(lo) {
			lo.patientID=rd.patientid;
			
			$.postJSON('/api/v1/updateBillingInfo', lo, function(data) {  
								if (data && data.errs && data.errs[0]) {display_errs(data.errs, null, function(){eval(data.errs[0].cb) } );} else {
								}
			});
			update_recent_patients_db();

};


var getCityStateFromZIP = function(zip) {
			
				$.postJSON('/api/v1/getCityStateFromZIP', {zip: zip}, function(data) {  
					$('#got_city_state').html("City:  <input type='text' name='city' value='" + data.data.city + "'/> &nbsp; State: <input type='text' name='state' value='" + data.data.state + "'/>");
				});
};


var send_ssl_msg = function(lo) { console.log($(lo).dataset('respdiv'));
				$.postJSON('/api/v1/sendSecureMsg', {msg:lo.secure_msg.value, patientID: rd.patientid}, function(data) {  
								if (data && data.errs && data.errs[0]) {display_errs(data.errs, null, function(){eval(data.errs[0].cb) } );} else {
									if (data.success) {
										var rspdiv=('#' + ( $(lo).dataset('respdiv') ) || '.pinfo_msgs');
											$(rspdiv).html('').show(); console.log($(rspdiv));
											$(rspdiv).html('<span class="success">Sent!</span>').fadeOut(5600);
											$('.secure_msg_ta').fadeTo(2600,  0.5, 'easeOutQuint', function() {	$('.secure_msg_ta').val('');	setTimeout("$('.secure_msg_ta').css('opacity', 1.0);", 2600); } );
											$("div.contentbox").trigger('patient_messages_update');
									}
								}

    });
};



var add_address_prompt = function(self) {
	popup($('#tmpl_add_patient_address').tmpl(self).html(),  { title: "Add Address", width: 800 });
	
	
};

var save_new_address = function(lo) {
				$.postJSON('/api/v1/saveNewPatientAddress', lo, function(data) {  
								if (data && data.errs && data.errs[0]) {display_errs(data.errs, null, function(){eval(data.errs[0].cb) } );
									if (data.success) {
										rd.pobj.info.pinfo=data.data;
										//redraw pinfo area
										$('#pc_pinfo_address').html('');
//										$('#tmpl_pinfo_area').tmpl(data.data).appendTo('#pc_pinfo_address');
										$.tmpl( "tmpl_pinfo_area_var", rd.pobj.info.pinfo ).appendTo( "#pc_pinfo_address" );										
									}
								} else {
									if (data.success) {
									$('#pc_pinfo_address').html('');
										$('#tmpl_pinfo_area').tmpl(data.data).appendTo('#pc_pinfo_address');
											$('#pinfo_msgs').html('').show()
											$('#pinfo_msgs').html('<span class="success">Sent!</span>').fadeOut(3600);
											
							} else {}
					}


	$('.ji_edit').editable(function(value, settings) { 
					$.postJSON('/api/v1/savePatientInformationChange', {key: this.id, val:value, patientID: rd.patientid}, function(msg) {
						display_errs(msg.errs, null, function(){eval(msg.errs[0].cb)} );
						if (msg.success) {
							$('#pinfo_msgs').html('').show()
							$('#pinfo_msgs').html('<span class="success">Saved!</span>').fadeOut(3600);

							} else {}
					});


			     return(value);
			  }, { 
			        submit  : 'OK',
			 }
			 );

			});
};




/* save CS order */

var order_otcs =
function (lo) {
			$.postJSON('/api/v1/create_otc_order', lo, function(data) {  
								if (data && data.errs && data.errs[0]) {display_errs(data.errs, null, function(){eval(data.errs[0].cb) } );} else {
									$('#otc_product_list').html(data.data);
									/*	$('html, body').animate({scrollTop: $("#lab_order_area").offset().top}, 600); */

								}
			});

};
















/* Messages */

var activate_messages_tbl = function(msgs, tblid) {
	if (! tblid) { tblid='#pc_hist_msgs_tbl';}
	pts_table=$(tblid).dataTable( {
	"bDestroy": true,
	"bJQueryUI": true,
	'bAutoWidth':false,
		"aaData": toTableArray( jQuery.map(msgs, function(k,v) { 
				var knt=$('<span>' + k['message'] + '</span>').text();
			k['msgText_s'] = (  knt.length > 107 ?  knt.substr(0,104) + '...' : knt);
		  return k;  }), [ 'date_authored_f', 'msgText_s', 'author',  'data' ]),
		"aoColumnDefs": [
			{ "sTitle": "Date", "sClass": "center", "aTargets": [ 0 ] , "sWidth": "8em"},
			{ "sTitle": "Message" , "aTargets": [ 1 ], "sWidth": "15em"},
			{ "sTitle": "Author", "sClass": "center small" , "aTargets": [ 2 ] , "sWidth": "9em"},
			{"bSearchable": true, "bVisible": true, "aTargets": [0]}
						],
		"0aoColumns": [
		],
		"aaSorting": [[1,'desc']],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			//var cn = nCloneTd.cloneNode( true );
//			console.log('pa-26');
//			console.log(aData[3]);
			
			var jdata=JSON.parse(aData[3]);
			jQuery.data( nRow, 'note_id', jdata.note_id );
			
			if ( jdata.noteFile ) {	jdata.refillMsg='<span class="refillWarning">This prescription will need to be faxed to your health care provider for authorization to refill</span>';	}
			nRow.onclick=function(telm) {
	
			if ($(telm.target).parent('a.func_view_note_file')[0]) { return;
			var dobj=$(telm.target).parent('a.func_view_note_file')[0].dataset();
				
			} else { pt_msg_popup({msgid: jdata.id});	}
			};

			// var sTitle = $('#tmpl_rollover_pinfo').tmpl(jdata).html();
			// nRow.setAttribute( 'title', sTitle );
			
			$(nRow).addClass(jdata.noteFile ? 'has_file' : 'no_file');
			return nRow;
		},
		"fnDrawCallback": function () {	
		}
	} );
};






