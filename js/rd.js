

var pc_rxh_scrollbar=new Object();


var isNum =function (n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
};

var lh_actions =
	{
		refillq: function(self) {console.log(self); self['prevent_lh']=true; display_refillQ(acct.refill_reqs, self)},
		acct_mgmt: function(self) {console.log(self); display_acct_mgmt(self)},
		dashboard: function(self) {console.log(self); display_dashboard({lh:-1})},
		pl: function(self) {console.log(self); display_patient_list()},
		all_diag_issues: function(self) {activate_all_diag_issues()},
		rxpad: function(self) {console.log(self); display_rxpad(self)},
		apc: function(self) {activate_patientchart(self[0], self[1])},
		pc: function(self) {activate_patientchart(self[0], self[1])},
	};
	var lh_on=true;
var push_lh = function(time_circuits) {
		if (lh_on ) { 
				lh.push(time_circuits);
				history.pushState(null, null, lh.length-1);
				//jQuery.bbq.pushState({loc:lh.length-1});
				} else { lh_on=true; }
	};

// lh_actions[lh[24].loc](lh[24].self)
var setup_lh_bbq = function(e , loc_pos_tmp) {
var loc_pos_tmp2=loc_pos_tmp.match(/\d+/);
	if (loc_pos_tmp2) {
		var loc_pos=loc_pos_tmp2[0];
		console.log(lh[loc_pos]);
		lh_actions[lh[loc_pos].loc](lh[loc_pos].self);	
	} else if (loc_pos_tmp == '/main.html' && lh.length > 2) { lh_on=false; display_dashboard();} else  {console.log(e);}
/*
	if (loc_pos > 1 && loc_pos < lh.length -1) {
	if (loc_pos > 1 && loc_pos < lh.length -1 && jQuery.bbq.getState('loc') == loc_pos  ) {		lh_on=false;}
	console.log('loc_pos: ' + loc_pos + '   bbq: ' + jQuery.bbq.getState('loc'));
console.log('pass - lh_on:' + lh_on);
*/


};


window.addEventListener("popstate", function(e) {
	lh_on=false;
    console.log('popsstate:');

    console.log(location.pathname);
    setup_lh_bbq(e, location.pathname);
});





var activate_area = function(header) {
		//clear other reqs:
		clear_display_reqs('refillq');

				$('#ma').empty();
				$('#ma_title_bar').empty();
				$('#tmpl_ma_tabs').tmpl(header).appendTo('#ma_title_bar');
				
};
var activate_main_tabs=function() {
         $( "#ma_parent" ).tabs( "destroy" ); $( "#ma_parent" ).tabs({
         select: function(event, ui) {
			if (ui.panel.id=="rx_history_pc") { 
				if(typeof pc_rxh_scrollbar == "object") { setTimeout('pc_rxh_scrollbar.tinyscrollbar_update();', 1100); }
			}
		},
		show: function(event, ui) {
			setTimeout("$('.jqscrollbar').each(function(ie, ve) {$(ve).tinyscrollbar_update();})", 100);	
		}
		 
	
	} );
};


//Popup dialog
var popup = function (message, opts) {

		if(! (typeof opts == 'object')) {opts=new Object(); }
				$( "#dialog-message" ).dialog(); $( "#dialog-message" ).dialog( "destroy" );
		// display the message- moved up because of subsequent calls...
			$('#dialog-message').html(message);
			if ( typeof opts.modal == 'undefined') {opts.modal=true; }
			if ( typeof opts.width == 'undefined') {opts.width=660;}
			//if (! typeof opts.autoOpen) {opts.autoOpen=false;}
			if ( typeof opts.close == 'undefined') {opts.close= function(event, ui)        {    console.log(this); console.log(event);           ; $(this).dialog('destroy') ;  $(this).empty();     }}
			console.log(opts);
			$('#dialog-message').dialog(opts);
			return;
	// calculate the values for center alignment
	var dialogTop =  (maskHeight/3) - ($('#dialog-box').height());  
	var dialogLeft = (maskWidth/2) - ($('#dialog-box').width()/2); 
	
	// assign values to the overlay and dialog box
	$('#dialog-overlay').css({height:maskHeight, width:maskWidth}).show();
	$('#dialog-box').css({top:dialogTop, left:dialogLeft}).show();
	
	// display the message
//	$('#dialog-message').html(message);
			
};
// queue popup didnt work without the second part (dialog.close) 2013-07-09:
var closePopUp = function () {$('#dialog-overlay, #dialog-box').hide(); $("#dialog-message").dialog("close" );  return false;};










var display_errs = function (errs, title, cb, data, opts) {
if (typeof errs && errs[0]) {
	if(! (typeof opts == 'object')) {opts=new Object(); }
	opts.title=title || "Messages";
	popup($('#tmpl_display_errs').tmpl({errs:errs, cb: cb, opt: opts || {}}), opts); 

		}
	};

function userAborted(xhr) {
  return !xhr.getAllResponseHeaders();
}

$.postJSON = function(url, data, callback, async) {
		var t_data= data.data ? JSON.stringify(data) : JSON.stringify({data:data});
		console.log(t_data);
		async = ((typeof async == "undefined") ? true : async);
	if (async==false) {
//				    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		var rjson = $.ajax({
					  type: 'POST',
					  url: url,
					  data: t_data,
					  success: callback,
					  dataType: 'json',

					scriptCharset: "utf-8" ,

					contentType: "application/json; charset=utf-8",
			processData: false,
					  async: async,
					   error: function(x, t, m) {
				        if(t==="timeout") {
				            alert("Request timed out.  Please refresh your browser.");
				        } else {
				            alert(t);
				        }
				    }
					  
					  
				 }).responseText;
				 //console.log(url + ': ' + rjson);
// why?				 var json = JSON.parse(rjson);
//does this even do anything:		 return json;
return rjson;
	}
	else {
		var tmp_xhr=$.ajax({
			  type: 'POST',
			  url: url,
			  data: t_data,
			  success: callback,
			  dataType: 'json',

					scriptCharset: "utf-8" ,
					contentType: "application/json; charset=utf-8",
			processData: false,
			  async: async,
			  error: function(x, t, m) {
				        if(t==="timeout") {
				            alert("Request timed out.  Please refresh your browser.");
				        } else if (t==="abort" && userAborted(x)) {	console.log('aborted: ' + x +t +m);	}
				        else {
				            alert("Request timed out.  Please refresh your browser.  \n\n" + t);
				        }
				    }
					  

			});
			return tmp_xhr;
	} 
 

};


$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};







var xhrs=new Object();


var clear_display_reqs = function(rt) {

	$.each(xhrs, function( ixhr, xhr) {
		if (ixhr == "rx_hist") {return; } else {
			if(xhr && xhr.readyState != 4){
	            xhr.abort();
	        }
       }
	});

/*
xhrs['refillq'];
xhrs['rxpad'];
xhrs['acct_mgmt'];
xhrs['dashboard'];
xhrs['pat_list'];
xhrs['diag_events'];
xhrs['pat_chart'];
*/
/*  skip:
xhrs['rx_hist'];
*/
};




//toggle patient selection
var select_patient_toggle = function(onoff) {
				if (onoff) {
						 		$('.selecting_patient').hide();
						 		$('.selected_patient').show();
						 		$('.patient_selected').show();
						 		
					 		} else {
						 		$('.selecting_patient').show();
						 		$('.selected_patient').hide();
						 		$('.patient_selected').hide();

						 		$('input.patient_search').focus();
						 		$('input.patient_search').autocomplete( "search", $('input.patient_search').val());
					 		}
};




var select_patient = function(pid, pdata) {
							if (rd.patientid != pid) {
								rd.patientid=pid;
					 			rxpad.patientid=pid;
					 			if (lh[lh.length-1]['loc']=='pc') {
					 				activate_patientchart(pid, lh[lh.length-1]['self'][1]);
					 			}
					 			
															
							}
							rd.patientid=pid;
					 		rxpad.patientid=pid;
					 		delete rd.pobj;
					 		delete rxpad.recommendations;
					 		delete rxpad.rx_history;
					 		
					 		fill_patient_info(rd.patientid);
					 		// hide / loading Recs + Hist !!!!!! CHANGE
					 		getRecommendations(pid);
					 		getRxHistory(pid);
					 		pdata.label= pdata.label ? pdata.label : pdata.last_name +', ' + pdata.first_name + ' - '  + pdata.dob_f;
					 		$('.selected_patient').text(pdata.label);
					 		$('.selected_patient').dataset(pdata);
					 		$('input.patient_search').val(pdata.label);
					 		select_patient_toggle(true);
							//added 07/24:
					 		$('#meta_search').val('');
					 		//	document.title=pdata.label;
					 		};
					 		
var deselect_patient = function() {

							rd.patientid='';
					 		rxpad.patientid='';
					 		delete rxpad.recommendations;
					 		delete rxpad.rx_history;

					 		$('.selected_patient').text('');
					 		$('.selected_patient').dataset({});
					 		$('input.patient_search').val('');
					 		select_patient_toggle(false);

};	 		
					 		
var fill_patient_info = function(pid) {
var img_uri=encodeURI(Crypto.SHA256(doc.DoctorCode + rd.patientid));
var xat_uri=encodeURI(xat);
$('#action_info_area').replaceWith($('#tmpl_patient_area').tmpl({xu: xat_uri, iu:img_uri})); 
};
					 		
					 		
					 		

					 		
var display_dashboard = function(opts) {
	
					push_lh({loc: 'dashboard', self:[] });

				$('#rxpad').remove();
				activate_area({title: 'Dashboard'});
				
				deselect_patient();
				$('#tmpl_dashboard').tmpl({}).appendTo('#ma'); 
				if (Object.keys(plist).length) {list_recent_patients();} else {setTimeout('list_recent_patients();', 1800);}
				
				$('#tmpl_db_notices').tmpl({}).appendTo('#db_notices'); 
				$('#tmpl_db_diag_issues').tmpl({}).appendTo('#diag_issues'); 
				display_recent_diag_issues();
				acct.refill_reqs = jQuery.grep(acct.refill_reqs, function(ai) {if (ai.rxn) {return ai;}} );
				$('#tmpl_db_rx_issues').tmpl({}).appendTo('#rx_issues');
				//activate_main_tabs - none at this time
				
};					 		
					 		

var list_recent_patients = function() {
	$('#tmpl_db_recent_patients').tmpl({}).appendTo('#recent_patients'); 
};





var display_all_recent_diag_issues = function() {
	if (! acct.diag_issues[0]) { return false;}
	$('#all_diag_issues').html( '<div class="listing"><table cellpadding="0" cellspacing="0" border="0" class="display" id="r_diag_i_tbl"></table></div>' );

	$('#r_diag_i_tbl').dataTable( {
	"bJQueryUI": true,
	"bAutoWidth": false,
		"aaData": toTableArray( jQuery.map(acct.diag_issues, function(k,v) {
				if (typeof plist[k.patientid] == 'undefined') {return;}
				var pt=plist[k.patientid]; k.pname = pt['last_name'] + ', ' + pt['first_name'];	
					k['view_status_f']='' ; if (k['view_status']) { k['view_status_f']='<a class="func_view_lab_report" style="z-index:19;" data-rxn="' + encodeURIComponent(k['rxn']) + '"   ><img class="lab_viewed_icon"  src="/images/checkmark.png" /></a>'; }  	  return k;}), ['pname', 'sample_date_f', 'completed_date_f', 'view_status_f',  'data' ]),
		"aoColumnDefs": [
			{ "sTitle": "Name",  "aTargets": [ 0 ] , "sWidth": "10em"},
			{ "sTitle": "Sample Date" , "sClass": "center", "aTargets": [ 1 ], "sWidth": "85px"},

			{ "sTitle": "Report Date" , "sName": "RxNumber", "aTargets": [ 2 ],  "sClass": "center refillable", "sWidth": "85px"},

			{ "sTitle": "Viewed", "sClass": "center small" , "aTargets": [ 3 ] , "sWidth": "85px"},
			{"bSearchable": false, "bVisible": false, "aTargets": [4]}
						],
		"0aoColumns": [

		],
		"aaSorting": [[2,'desc']],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			//var cn = nCloneTd.cloneNode( true );
			var jdata=JSON.parse(aData[4]);
			jQuery.data( nRow, 'patientid', jdata.patientid );
			var refillable=test(jdata);
			
			if (! refillable ) {	jdata.refillMsg='<span class="refillWarning">This prescription will need to be faxed to your health care provider for authorization to refill</span>';	}
			nRow.onclick=function(telm) {

				view_lab_report(jdata);
				
			};

			var sTitle = $('#tmpl_rollover_lab_info').tmpl(jdata).html();
			nRow.setAttribute( 'title', sTitle );
			
			$(nRow).addClass(refillable ? 'refillable' : 'needRefillAuth');
			return nRow;
		},
		"fnDrawCallback": function () {//	$('#r_diag_i_tbl tbody tr[title]').tooltip( {												"delay": 750,												"track": true,												"fade": 250											} );
		}
	} );





};






var display_recent_diag_issues = function() {
	if ('list over table') {
			$('#diag_issues').empty();
			$('#tmpl_db_recent_diag_list').tmpl({}).appendTo('#diag_issues'); return;
			

	} else {
	if (! acct.diag_issues[0]) { return false;}
	$('#diag_issues').html( '<div class="listing"><table cellpadding="0" cellspacing="0" border="0" class="display" id="r_diag_i_tbl"></table></div>' );

	$('#r_diag_i_tbl').dataTable( {
	"bJQueryUI": true,
	"bAutoWidth": false,
		"aaData": toTableArray( jQuery.map(acct.diag_issues, function(k,v) {
				var pt=plist[k.patientid]; k.pname = pt['last_name'] + ', ' + pt['first_name'];	
					k['view_status_f']='' ; if (k['view_status']) { k['view_status_f']='<a class="func_view_lab_report" style="z-index:19;" data-rxn="' + encodeURIComponent(k['rxn']) + '"   ><img class="lab_viewed_icon"  src="/images/checkmark.png" /></a>'; }  	  return k;}), ['pname', 'sample_date_f', 'completed_date_f', 'rx_status',  'data' ]),
		"aoColumnDefs": [
			{ "sTitle": "Name", "sClass": "center", "aTargets": [ 0 ] , "sWidth": "10em"},
			{ "sTitle": "Sample Date" , "aTargets": [ 1 ], "sWidth": "85px"},

			{ "sTitle": "Report Date" , "sName": "RxNumber", "aTargets": [ 2 ],  "sClass": "center refillable", "sWidth": "85px"},

			{ "sTitle": "Rx Updated", "sClass": "center small" , "aTargets": [ 3 ] , "sWidth": "85px"},
			{"bSearchable": false, "bVisible": false, "aTargets": [4]}
						],
		"0aoColumns": [

		],
		"aaSorting": [[2,'desc']],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			//var cn = nCloneTd.cloneNode( true );
			var jdata=JSON.parse(aData[4]);
			jQuery.data( nRow, 'patientid', jdata.patientid );
			var refillable=test(jdata);
			
			if (! refillable ) {	jdata.refillMsg='<span class="refillWarning">This prescription will need to be faxed to your health care provider for authorization to refill</span>';	}
			nRow.onclick=function(telm) {
				view_lab_report(jdata.rxn);
			};

			var sTitle = $('#tmpl_rollover_lab_info').tmpl(jdata).html();
			nRow.setAttribute( 'title', sTitle );
			
			$(nRow).addClass(refillable ? 'refillable' : 'needRefillAuth');
			return nRow;
		},
		"fnDrawCallback": function () {//	$('#r_diag_i_tbl tbody tr[title]').tooltip( {												"delay": 750,												"track": true,												"fade": 250											} );
		}
	} );
}


};


















var test= function(){;};

var activate_report=function(arg){console.log(arg);};



















var display_patient_list = function() {
				$('#rxpad').remove();
					push_lh({loc: 'pl', self:[] });

				activate_area({title: 'Patient List'});
				deselect_patient();


	$('#ma').html( '<table cellpadding="0" cellspacing="0" border="0" class="display" id="plist_tbl"></table>' );

	$('#plist_tbl').dataTable( {
	"bJQueryUI": true,
		"aaData": toTableArray( jQuery.map(plist, function(k,v) {return k;}), ['last_name', 'first_name', 'dob_f', 'tstamp',  'data' ]),
		"aoColumnDefs": [
			{ "sTitle": "Last Name", "sClass": " hipaa_name", "aTargets": [ 0 ] , "sWidth": "10em"},
			{ "sTitle": "First Name" ,"sClass": " hipaa_name", "aTargets": [ 1 ], "sWidth": "10em"},

			{ "sTitle": "DOB" , "sName": "RxNumber", "aTargets": [ 2 ],  "sClass": "refillable", "sWidth": "3em"},

			{ "sTitle": "Last Update", "sClass": " small" , "aTargets": [ 3 ] , "sWidth": "6em"},
			{"bSearchable": false, "bVisible": false, "aTargets": [4]}
						],
		"0aoColumns": [

		],
		"aaSorting": [[3,'desc']],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			//var cn = nCloneTd.cloneNode( true );
			var jdata=JSON.parse(aData[4]);
			jQuery.data( nRow, 'patientid', jdata.patientid );

			nRow.onclick=function(telm) {
					activate_patientchart(jdata.patientid);
			};
			return nRow;
		},
		"fnDrawCallback": function () {	//$('#plist_tbl tbody tr[title]').tooltip( {												"delay": 750,												"track": true,												"fade": 250											} );
		}
	} );
};




var activate_all_diag_issues = function() {
				push_lh({loc: 'all_diag_issues', self:[] });

				$('#rxpad').remove();
				activate_area({title: 'Recent Diagnostic Events'});
				
				deselect_patient();
				$('#tmpl_all_diag_events').tmpl({}).appendTo('#ma'); 
				display_all_recent_diag_issues();
				//activate_main_tabs - none at this time
				};
				




var alter_css = function(selectorText, newStyle, newValue) {
	var ss = document.styleSheets;
	for (var i=0; i<ss.length; i++) {
            var rules = ss[i].cssRules || ss[i].rules;

            for (var j=0; j<rules.length; j++) {
                if (rules[j].selectorText === selectorText) {
                    rules[j].style[newStyle] = newValue;
                    return true;
                }
            }
        }
};









var set_top_tabs = function(tabs, bar) {

				$('#tmpl_tabs').tmpl(tabs).appendTo(bar);


};



var toTableArray = function(data, cols) {
	var na = new Array();
	$.each(data, function(index, value) { 	
		var nna = new Array();
		for (var ci =0; ci < cols.length;  ci++) {
			if (cols[ci] == 'data') {
				nna.push(JSON.stringify(value));
			} else {
				nna.push(value[cols[ci]]);
			}
			 }
		na.push(nna); 
	} );
	return na; 
};

var showMoreInfo=
function (mim, controller) {
	var info_d=$('#' +  mim);
	var cont=$('#' +  controller);
	if (info_d.css('display') == 'none') {
		cont.rotate({
	            angle: 0, 
	            animateTo:90
	          });
		info_d.show('slow');	          
	} else {
		cont.rotate({
	            animateTo:0
	          });
		info_d.hide('slow');
	}
};


var hipaa_mode_toggle = function(onoff) {
	if (onoff) {hipaa_mode=true;} else {hipaa_mode=false;}
		if (hipaa_mode) {
		$('body').addClass('hipaa_mode_true');
		$('body').removeClass('hipaa_mode_false');
	} else {
		$('body').removeClass('hipaa_mode_true');
		$('body').addClass('hipaa_mode_false');
	}
};

var clean_hipaa_val =
function (val, val_type) {
	if (hipaa_mode) {
		if (val_type) {
			console.log(val);
		} else {
			var hret;
		if (val.length  > 2) {
			hret=val.substr(0,2) + Array(( val.length  > 2 ? val.length-2 +1 : 5)).join("X");
		} else {			hret=val;		}
			if (hret.length < 2 ) {hret= 'XXXXX';}
			return hret || 'XXXXX';
		}
	} else {return val;}
};




 // BEGIN QTIP2:  
 
 // A shared object containing all the values you want shared between your tooltips
var sharedq = {
   position: {
      my: 'top center', 
      at: 'bottom center',
   },

   style: {
   classes: 'ui-tooltip-shadow-qtip2',
      tip: true,
      widget: true
   }
};
 
 
 
 
 
 

 
 
 
 
 



 // END QTIP2
