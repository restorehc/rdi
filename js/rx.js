			
// patient select:

var addPatient = function(frm){

	$(frm, ' input').each( function(index, elm) {	elm.type="hidden";	} );
	//winkstart.publish('patient.add', frm.serializeObject());

	$.ajax({
      url: "/api/paient",
      global: true,
      type: "PUT",
	  data: frm.serializeObject(),
      dataType: "json",
      async:true,
      success: function(msg){
				if (msg && msg.errs && msg.errs[0]) {display_errs(msg.errs, null, function(){eval(msg.errs[0].cb); update_recent_patients_db();  } );}
				

      }
   }
);

	
	
	
	}
			
			
			
// activate rxp_patient_searching	
var rxp_activate_patient_searching = function() {
				$('input.patient_search').autocomplete({ 
									    minLength: 4,
					    source:function(event, ui) { 
					    var re=new RegExp(event.term, 'i');
					    // work on regex that parse dates better
							var res_arr=$.map(plist, function(value, key) { var l_pname=clean_hipaa_val(value.last_name) + ', ' + clean_hipaa_val(value.first_name) + ' - ' + value.dob_f;
							var pname=value.last_name + ', ' + value.first_name + ' - ' + value.dob_f;
								  if (     re.test(pname) ) {return {label: l_pname, value: l_pname, pid: value.patientid}; }	});
							ui(res_arr);
					},
					 select: function(event, ui) {
					 		select_patient(ui.item.pid, ui.item);

					 		$('input.med_medication_i')[0].focus();
					 },
					 close: function(event, ui) {
					 console.log(event);					 console.log(ui);
					 	// not sure what to do yet...
					 }
				});
};


// activate 4 left tabs:
var activate_rxp_left_tabs = function () {
	         $( "#rxp_left_tabs" ).tabs( "destroy" ); $( "#rxp_left_tabs" ).tabs({
	         select: function(event, ui) {
			if (ui.panel.id=="rx_history_pc") { 
			;
			}
		} } );
	};


// rxpad
	var display_rxpad = function(self) {
				$('#rxpad').remove();
		push_lh({loc: 'rxpad', self:self });

						var pct = [];
						if (self.patientid ) {
							pct=[
								{active: false,  title: 'Summary', href: 'pc_summary' , id: 'return_to_pc_summary' , li_id: 'return_to_pc_summary_li'},
								{active: true, id:'tb_pc_rxp', title: 'Rx Pad', href: '#'  , li_id:'tb_pc_rxp_li' },
								{active: false,  title: 'Chart', href: 'pc_full_chart' , id: 'return_to_pc_full_chart' , li_id: 'return_to_pc_full_chart_li'},
								{active: false,  title: 'History', href: 'pc_history' , id: 'return_to_pc_history' , li_id: 'return_to_pc_history_li'},
								{active: false,  title: 'Patient Info', href: 'pc_pinfo_area' , id: 'return_to_pc_pinfo' , li_id: 'return_to_pc_pinfo_li'}
								];
						}
				
				
				
				activate_area({title: 'Rx Pad', tabs:pct});
				// activate_main_tabs();
				delete rxpad.recommendations;
				delete rxpad.patientid;
		 		delete rxpad.rx_history;				
				$('#tmpl_rxpad').tmpl(self).appendTo('#ma'); 
				/* 4 tabs leftbar */
				
				
				activate_rxp_left_tabs();
					$('#tmpl_lab_order_saliva').tmpl().appendTo('#lab_saliva_order_area'); 
					$('#tmpl_lab_order_serum').tmpl().appendTo('#lab_serum_order_area'); 
					$('#tmpl_OTC_order').tmpl().appendTo('#otc_order_area'); 
				
				
				
				if (typeof self == 'object') {
					if (typeof self['meds'] == 'array' || typeof self['meds'] == 'object') {
				
						$.each(self['meds'], function(medid, med) {
							med.medid=medid;
							add_rx_row(med);
						});	
					} else {add_rx_row();}
					if (self.patientid ) {
						rxpad['patientid']=self.patientid;
						//set in header and search area
						var pname=plist[rxpad['patientid']].last_name + ', ' + plist[rxpad['patientid']].first_name + ' - ' + plist[rxpad['patientid']].dob_f;
							$('.selected_patient').text(pname);
					 		$('.selected_patient').dataset(plist[rxpad['patientid']]);
					 		select_patient_toggle(true);
					 		$('input.med_medication_i')[0].focus();
	
						/* for lack of understanding why not... 2012-03-05 */
						rxp_activate_patient_searching();
						
						
						} else {rxp_activate_patient_searching();}
				} else { add_rx_row(); rxp_activate_patient_searching();}
			activate_rxp_row($('div.rxp_med'));
			


			$('.rx_verify').hide();
			$('.rx_entry').show();
	};
			
//medication:
	var activate_rxp_med_acs = function(elm) {
			elm.find('input.med_medication_i').autocomplete({ option: { medid: 1 },
					    source: $.map(medList, function(value, key) {  return key;	}) ,
					    old_source: function(event, ui) { 
					    	// search local data only, it should stay synced.  
						 return $.map(medList, function(value, key) {  return key;	});	} });  
					    	};
//dose:
	var activate_rxp_dose_acs = function(elm) {
			elm.find('input.med_dose_i').autocomplete({ 
					
					    //n_source: $.map(medList[$('#rxp_med_medication_' + $(this).dataset('medid')).val()         ], function(value, key) {  return key;	}),
					    source:
					    function(event, ui) { 

						    var medid=this.element.dataset('medid');
					    		var rxp_med = $('#rxp_med_medication_' + medid).val();
					    		var ac_d_sort_regex = new RegExp("[0-9\.]+[0-9]*");
								var res_arr=$.map(medList[rxp_med], function(value, key) { var re=new RegExp(event.term, 'i');  if (     re.test(key) ) {return key; } if (event.term =='') {return key; }	});
								res_arr.sort(function(a, b) {	
									if (ac_d_sort_regex.exec(a)[0]*1 > ac_d_sort_regex.exec(b)[0]*1) {
									return 1; } return -1;
										});
								ui(res_arr);
					    	},
					    minLength: 0,
							}).focus(function(){
								if (this.value == "")
									$(this).trigger('keydown.autocomplete');
					    	});
					    	};
					    	

//qty:
	var activate_rxp_qty_acs = function(elm) {
			elm.find('input.med_qty_i').focus(function(){
								if (this.value == "")
									$(this).val('30 Day Supply');
					    	});
					    	};
					    	
					    						    	

//sig:
	var activate_rxp_sig_acs = function(elm) {
			elm.find('textarea.med_sig_i').autocomplete({ 
					    minLength: 3,
					    source:function(event, ui) { 

						    var medid=this.element.dataset('medid');
					    		var rxp_med = $('#rxp_med_medication_' + medid).val();
					    		var ac_d_sort_regex = new  RegExp("^" + event.term);

								var res_arr=$.map(sigList(), function(key) {var re=new RegExp(event.term, 'i');  if (     re.test(key)  ) {return key; }	});
								res_arr.sort(function(a, b) {	
									if (ac_d_sort_regex.exec(a) > ac_d_sort_regex.exec(b)) {
									return 1; } return -1;
										});
								ui(res_arr);
					    	},
							});
 
 						elm.find('textarea.med_sig_i').autoResize({
				    // On resize:
				    onResize : function() {
				        $(this).css({opacity:0.8});
				    },
				    // After resize:
				    animateCallback : function() {
				        $(this).css({opacity:1});
				    },
				    // Quite slow animation:
				    animateDuration : 300,
				    // More extra space:
				    extraSpace : 10
				});		
		};

	var activate_rxp_row = function(elm) {
		activate_rxp_med_acs(elm);
		activate_rxp_dose_acs(elm);
		activate_rxp_sig_acs(elm);
		activate_rxp_qty_acs(elm);
	};
			
// /end

// collect data from rx pad:
	var collect_rxp_meds=function() {
		var collected_meds=new Array();
		var fields= new Array('RxN', 'medication', 'dose', 'qty', 'refill', 'sig');
	$.each($('#rxp_meds div.rxp_med input, #rxp_meds div.rxp_med textarea'), function(i, elm) {
			var mid=$(elm).dataset('medid');
			mid = parseInt(mid);
		if ( typeof collected_meds[parseInt(mid)] !='object') { collected_meds[mid]=new Object(); }
		var fname=$(elm).attr('name');
		if (fname && jQuery.inArray(fname, fields)) {		collected_meds[mid][fname]=$(elm).val();	}

		});
		collected_meds=$.grep(collected_meds,function(n,i){    return(n);	});
		return collected_meds;
		};



var collect_rxp_meds_submit = function () {
		var collected_meds=new Array();
		var fields= new Array('RxN', 'medication', 'dose', 'qty', 'refill', 'sig');
	$.each($('#rxp_meds div.rxp_med'), function(i, elm) {
			var mid=i ; //$(elm).dataset('medid') ;
			collected_meds[mid]=$(elm).dataset();
			collected_meds[mid].medid=mid;
		});
		
		return collected_meds;
		};






		var add_rx_row = function(data) {
			// clear empty
			remove_empty_meds();
			var mi = ($('#rxp_meds>div.rxp_med:last-of-type').dataset('medid') +1) || 0;
			if(typeof data=="undefined") {data=new Object(); data.medid=mi; data.rx=new Object();}
			if(typeof data.rx=="undefined") { data.rx=new Object(); }
			data.medid=data.medid || mi;
			$('#add_rxp_med').tmpl(data).appendTo('#rxp_meds'); 
				var ml = $('#rxp_meds>div.rxp_med:last-of-type');
				activate_rxp_row(ml);
			};
			
	var del_rx_row = function(ri) {
			ri.remove();			
	};

// change this to afterBuildComponent:




			var remove_empty_meds = function() {
				// remove blanks
				$('#rxp_meds div.rxp_med').each(function(ii, lm) {  if ($.grep($(lm).find('input, textarea').map(function(){ if ($(this).val() !='') {return $(this).val()} })  ,function(n,i){    return(n);	})[0] ) { return true; } else {del_rx_row($(lm))};   })
			};




var display_rxp_meds = function(meds) {

		$('#rxp_meds').empty();
		 $('.rx_entry').hide();
		 $('.rx_verify').show();
$.each(meds, function(medid, rx) { 
	rx.medid=medid;
	$('#display_rxp_meds').tmpl({rx:rx}).appendTo('#rxp_meds'); 
});		
	$('.rx_entry').hide();		
			};


// check issues, --collect_meds -> --verify -> --display errs -> --redraw -> [confirm] -> submit -> cb = watermark
//
 var editRxPad = function() {display_rxpad({patientid:rxpad.patientid, meds:collect_rxp_meds_submit()	}	)};
var  disable_rxpad = function() {
	$('#rxpad').find('input').prop('disabled', true);
	$('#completed_rx_options').find('input').prop('disabled', false);
	$('#rx_submit').hide();
	$('#edit_rxpad_verify').hide();
};

var submitRxPad=function() {
	//validate?!
	 var rx=new Object();
	 rx.patientid=rxpad.patientid;
	 rx.esig=$('#rxp_esig').val();

	rx.meds=collect_rxp_meds_submit(); 

	 	if (! rx.patientid) {display_errs({msg: 'No patient is selected.  Please use the search at the top or add a new patient.', type: 'warning' }, null, function(){editRxPad();} ); return;	}
		if (! rx.esig) {display_errs({msg: 'Please sign this prescription.', type: 'warning' } ); return;	}
		
	if (submitRx(rx)) {
		$('#completed_rx').show('slow');

		disable_rxpad(true);
		$('#rxpad').addClass('submitted-success');
		$('#completed_rx_options').show('slow');
		
		
	}
	// change this: rxpad=new Object();
		delete rxpad.recommendations;
		delete rxpad.rx_history;

	return JSON.stringify(rx);
};

//submit Rx
var submitRx=function(rx) {
		if (typeof rx !="object") {return;}
	// validate patientid, doctorcode, esig

	if (! rx.patientid) {
		display_errs({msg: 'No patient is selected.  Please use the search at the top or add a new patient.' }, null, function(){editRxPad(rx);} );
		 return;	}
	if (! rx.esig) {display_errs({msg: 'Please sign this prescription.' } ); return;	}
	
	
			
			var rt;
			$.postJSON('/api/v1/submitRx', rx, function(msg) {
				display_errs(msg.errs, null, function(){eval(msg.errs[0].cb)} );
				if (msg.success) {
					if ( rx_return_check(msg.data.rx_return, rx)) {
						$(this).removeClass( 'errors_present' );
						rt = true;
						}
					} else {rt=false;}
			});

			return true;
	};



var rx_return_check = function(hsh, jsn) {
if (hsh) {
	return true;
	} return false;
};
// next add template and rx history and recs1
			
	var getRecommendations =function(pid) {
		if (rxpad.recommendations) {return rxpad.recommendations;} else {
			var rj = $.postJSON('/api/v1/getRecommendations', {pID: pid}, function(data) { return; }, false);
			rxpad.recommendations = typeof rj == "object" ? getORecs( rj.data ) : {};
		return rxpad.recommendations;
		}
	};

getRecommendations = function (pid) {
		if (rxpad.recommendations) {return rxpad.recommendations;} else {
			 $.postJSON('/api/v1/getRecommendations', {pID: pid}, function(rj) { 
			
			
			rxpad.recommendations = typeof rj == "object" ? getORecs( rj.data ) : {};

			var rec_cnt=0;
			$.each(Object.keys(rxpad.recommendations), function(i1, k1) {
				$.each(Object.keys(rxpad.recommendations[k1]), function(i2, k2) {
					$.each(Object.keys(rxpad.recommendations[k1][k2]), function(i3, k3) {
						$.each(Object.keys(rxpad.recommendations[k1][k2][k3]), function(i4, k4) { 
						if (typeof rxpad.recommendations[k1][k2][k3][k4]['orecs'] !='undefined') {
							rec_cnt+= rxpad.recommendations[k1][k2][k3][k4]['orecs'].length ;
							//	console.log('length: ' +  'rxpad.recommendations[' + "'" + k1 + "']['" + k2 +"']['" +k3 + "']['" + k4 + "']['orecs']");
							//	$.each(rxpad.recommendations[k1][k2][k3][k4]), function(i5, k5) { console.log('k5:' + k5);
						}
					 }) })  })  })  ;

					rj.has_recommendations= rec_cnt >0 ? true : false;
					rxpad.has_recommendations(rj.has_recommendations);
			
			
			
			return; }, false);
		return rxpad.recommendations;
		}
	};



var getORecs1 = function(data) {
console.log('ORECS: ');
console.log(data);
var recs = new Object();
$.each(data, function(sectName, k1) { recs[sectName]=new Object();     if (! k1) {return;} if (typeof k1 != "object" || k1.length ) {	return;	}
	$.each(k1, function(subSectName, k2) {  recs[sectName][subSectName]=new Object();
		recs[sectName][subSectName]['omedSystem']=$.map(data[sectName][subSectName], function(value, index){return index ;}).sort(function (a, b){if (a == 'Systemic') {return -4;}  if (a == 'OTC') {return 1;}  if (b == 'Systemic' ) {return 4;} return -2;});
		$.each(k2, function(medSystem, k3) { recs[sectName][subSectName][medSystem]=new Object();	if ( medSystem == 'undefined' || typeof rxpad.order[medSystem] == 'undefined' ) {return;}
		recs[sectName][subSectName][medSystem]['omedCategory']=$.map(data[sectName][subSectName][medSystem], function(value, index){return index ;}).sort(function (a, b){if (typeof rxpad.order[medSystem] != 'undefined' &&       rxpad.order[medSystem][a] > rxpad.order[medSystem][a] ) {return -1;}    if (rxpad.order[medSystem][a] < rxpad.order[medSystem][a] ) {return 1;}      if (rxpad.order[medSystem][a] == rxpad.order[medSystem][a] ) {return 0;}     });
					$.each(k3, function(medCategory, k4) {	 recs[sectName][subSectName][medSystem][medCategory]=new Object();
					recs[sectName][subSectName][medSystem][medCategory]['orecs']=$.map(data[sectName][subSectName][medSystem][medCategory], function(value, index){return value ;}).sort(function(a, b){if (a.methodPreference > b.methodPreference) {return 1;} if (a.methodPreference == b.methodPreference) {return 0;} if (a.methodPreference < b.methodPreference ) {return -1;} });
					});
				});
			});
		});
		return recs;
};






var getORecs_orig = function(data) {
console.log('ORECS: ');
console.log(data);
var recs = new Object();
$.each(data, function(sectName, k1) { recs[sectName]=new Object();     if (! k1) {return;} if (typeof k1 != "object" || k1.length ) {	return;	}
	$.each(k1, function(subSectName, k2) {  recs[sectName][subSectName]=new Object();
		recs[sectName][subSectName]['omedSystem']=$.map(data[sectName][subSectName], function(value, index){return index ;}).sort(function (a, b){if (a == 'Systemic') {return -4;}  if (a == 'OTC') {return 1;}  if (b == 'Systemic' ) {return 4;} return -2;});
		$.each(k2, function(medSystem, k3) { recs[sectName][subSectName][medSystem]=new Object();	if ( medSystem == 'undefined' || typeof rxpad.order[medSystem] == 'undefined' ) {return;}
		recs[sectName][subSectName][medSystem]['omedCategory']=$.map(data[sectName][subSectName][medSystem], function(value, index){return index ;}).sort(function (a, b){if (typeof rxpad.order[medSystem] != 'undefined' &&       rxpad.order[medSystem][a] > rxpad.order[medSystem][a] ) {return -1;}    if (rxpad.order[medSystem][a] < rxpad.order[medSystem][a] ) {return 1;}      if (rxpad.order[medSystem][a] == rxpad.order[medSystem][a] ) {return 0;}     });
					$.each(k3, function(medCategory, k4) {	 recs[sectName][subSectName][medSystem][medCategory]=new Object();
					recs[sectName][subSectName][medSystem][medCategory]['orecs']=$.map(data[sectName][subSectName][medSystem][medCategory], function(value, index){return value ;}).sort(function(a, b){if (a.methodPreference > b.methodPreference) {return 1;} if (a.methodPreference == b.methodPreference) {return 0;} if (a.methodPreference < b.methodPreference ) {return -1;} });
					});
				});
			});
		});
		return recs;
};


var getORecs = function(data) {
console.log('ORECS: ');
console.log(data);
var recs = new Object();
$.each(data, function(sectName, k1) { recs[sectName]=new Object();     if (! k1) {return;} if (typeof k1 != "object" || k1.length ) {	return;	}

	$.each(k1, function(subSectName, k2) {  recs[sectName][subSectName]=new Object(); 	
		if (! k2) {return;} if (typeof k2 != "object" || k2.length ) {	return;	}
		recs[sectName][subSectName]['omedSystem']=$.map(data[sectName][subSectName], function(value, index){return index ;}).sort(function (a, b){if (a == 'Systemic') {return -4;}  if (a == 'OTC') {return 1;}  if (b == 'Systemic' ) {return 4;} return -2;});
		 
		$.each(k2, function(medSystem, k3) {         recs[sectName][subSectName][medSystem]=new Object();	if ( medSystem == 'undefined' || typeof rxpad.order[medSystem] == 'undefined' ) {return;}
		recs[sectName][subSectName][medSystem]['omedCategory']=$.map(data[sectName][subSectName][medSystem], function(value, index){return index ;}).sort(function (a, b){if (typeof rxpad.order[medSystem] != 'undefined' &&       rxpad.order[medSystem][a] > rxpad.order[medSystem][a] ) {return -1;}    if (rxpad.order[medSystem][a] < rxpad.order[medSystem][a] ) {return 1;}      if (rxpad.order[medSystem][a] == rxpad.order[medSystem][a] ) {return 0;}     });
					$.each(k3, function(medCategory, k4) {	 recs[sectName][subSectName][medSystem][medCategory]=new Object();
					recs[sectName][subSectName][medSystem][medCategory]['orecs']=$.map(data[sectName][subSectName][medSystem][medCategory], function(value, index){return value ;}).sort(function(a, b){if (a.methodPreference > b.methodPreference) {return 1;} if (a.methodPreference == b.methodPreference) {return 0;} if (a.methodPreference < b.methodPreference ) {return -1;} });
					});
				});
			});
		});
		return recs;
};




	var getRxHistory =function(pid) {
		if (rxpad.rx_history) {return rxpad.rx_history;} else {
			var t_data=  JSON.stringify({data:{pID: pid, includeUnfilled: 1}});
		var nd = $.ajax({
		  url: '/api/v1/getRxHist_summary_for_provider',
		  async: false,
		  type: 'post',
		  data: t_data,
		  dataType: 'json',
		  timeout: 5000
 		});
 		xhrs['rx_hist']=nd;
			//$.postJSON('/api/v1/getRxHist_summary_for_provider', , function(data) {  rxpad.rx_history = data.data}, false);
			var njd=jQuery.parseJSON(nd.responseText);
			rxpad.rx_history=njd.data;
			rxpad.has_rxhistory(typeof rxpad.rx_history == 'object' && rxpad.rx_history.length > 0);
		return rxpad.rx_history;
		}
	};
			
			
var popup_rx_recs = function() {popup(
	$('#tmpl_recommendations').tmpl({recs:getRecommendations(rxpad.patientid)})[0], {width:650, title: 'Recommendations'}
	);
	$('.andor_and').prev('.andor_or').addClass("next_andor_and");
/*	activate scrolling */

	};
	
var popup_rx_hist = function(pobj) {

	var rxpid = (typeof pobj =="object" && typeof pobj.patientid =="object" ) ? pobj.patientid :  rxpad.patientid;
	console.log('rx.js 456:');	console.log(rxpid);
	if (xhrs['rx_hist'].readyState != 4) {
		$(document).ajaxStop(function() {
			console.log('459-if is true');
			popup(
				$('#tmpl_rx_history').tmpl({rxs:getRxHistory(rxpid)})[0], {width:650, title: 'Rx History'}
			);
		});
	} else {
			popup(
				$('#tmpl_rx_history').tmpl({rxs:getRxHistory(rxpid)})[0], {width:650, title: 'Rx History'}
			);
	}
/*	activate scrolling */
var to = setTimeout("$('#rxhist_sb').tinyscrollbar({	wheel: 40.15	});", 600);
return to;
};	
		
		
var popup_rx_tempate = function(dobj)	{

	popup(
				$('#tmpl_rx_template').tmpl({rxs:templateMeds})[0], {width:650, title: 'Rx Template'}
			);
/*	activate scrolling */
var to = setTimeout("$('#rxtempl_sb').tinyscrollbar({	wheel: 40.15	});", 600);
var toto = setTimeout("$('.tmpl_doses.doses_deactivated').find('input').prop('disabled', true);", 300);
};

var activate_tmpl_med = function(spn)	{
	return spn;
};

		










$(document).ready(function(){






rxpad.has_recommendations = function( hr ) {
	if (hr) {
		$('body').addClass('has_recommendations_true');
		$('body').removeClass('has_recommendations_false');
	} else {
		$('body').removeClass('has_recommendations_true');
		$('body').addClass('has_recommendations_false');
	}
};

rxpad.has_rxhistory = function( hrh ) {
	if (hrh) {
		$('body').addClass('has_rxhistory_true');
		$('body').removeClass('has_rxhistory_false');
	} else {
		$('body').removeClass('has_rxhistory_true');
		$('body').addClass('has_rxhistory_false');
	}
};















	
		$("body").delegate('.tmpl_checkbox', 'click', function(ckc) {
			$(this).parents().parent().next('.tmpl_doses').toggleClass('doses_deactivated', (! this.checked));
			$(this).parent().parent().next('.tmpl_doses').find('input').prop('disabled', false);
			$(this).parent().parent().next('.tmpl_doses.doses_deactivated').find('input').prop('disabled', true);
			$(this).parent().parent().next('.tmpl_doses.doses_deactivated').find('input').prop('checked', false);
  		});

	$("body").delegate("#rxp_submit_v", "click", function(){
		$("#rxp_form").submit();
	});
// rxpad verification 
	$("body").delegate("#rxp_form", "submit", function(frm){
			frm.preventDefault(); 
			
			remove_empty_meds();			
			
			console.log('556');			
			//clear previous invalid:
			$('#rxp_form div.rxp_med textarea:required').css({background: '#FFF'});
			$('#rxp_form div.rxp_med input:required').css({background: '#FFF'});
			//check for invalid
			if ($('#rxp_form div.rxp_med input:required:invalid')[0]) {
				// clear empty meds, try 
			$('#rxp_form div.rxp_med input:required:invalid').css({background: '#FFC5C5'});
			$('#rxp_form div.rxp_med   input.warn_only:required:invalid').css({background: '#EAF0C0'});
			if ($('#rxp_form div.rxp_med input.med_refills_i:required:invalid')[0]) {
					$('#rxp_form div.rxp_med input.med_refills_i:required:invalid')[0].focus();
			}
			
			}



// check for invalid instructions:



			if ($('#rxp_form div.rxp_med textarea:required:invalid')[0]) {
			console.log('578');
				// clear empty meds, try 
			$('#rxp_form div.rxp_med textarea:required:invalid').css({background: '#FFC5C5'});
			$('#rxp_form div.rxp_med   textarea.warn_only:required:invalid').css({background: '#EAF0C0'});
			
			}




			
			var c_rxp_m=collect_rxp_meds();
			$.postJSON('/api/v1/verifyRx', {meds: collect_rxp_meds()}, function(msg) {
				display_errs(msg.errs, null, function(){eval(msg.errs[0].cb)} );
				if (msg.success) {
					$(this).removeClass( 'errors_present' );

					display_rxp_meds(msg.data);
					//$('.rx_entry').hide();
				} else {	$(this).addClass( 'errors_present' );	}
			});
			});
			
	$("body").delegate(".remove_med", "click", function(){
		if(isNum($(this).dataset('medid')))  {
		del_rx_row($(this).parent());
			}
			});

	$("body").delegate("form.patient_search", "submit", function(frm) {frm.preventDefault();  
	addPatient($(this)); return false;} );



	$("body").delegate("#rxp_submit_s", "click",  function(){submitRxPad()}	);

		$("body").delegate("#add_rxpad_row", "click", function(){
				add_rx_row({});
			});


	$("body").delegate(".click_to_change_patient", "dblclick" , function() { if($('#completed_rx').is(':hidden')) {   select_patient_toggle(false); } else {  display_errs([{msg: 'This prescription has been submitted.  Please use the RxPad button above to reset and choose a new patient to work with.', type: 'warning' }]);    } });
	$("body").delegate('#rxp_change_pt', click_event, function() { $('#rxp_selected_pt_changer').trigger('dblclick');})
	$("body").delegate("#edit_rxpad_verify", 'click', function() {editRxPad()});
	
	$("body").delegate(".recommendation_popup", 'click', function() {popup_rx_recs(); });
	$("body").delegate(".rx_history_popup", 'click', function() {console.log('popup_rx_hist'); popup_rx_hist(); });
	$("body").delegate(".rx_template_popup", 'click', function() {popup_rx_tempate() });



	$("body").delegate("#rxp_recommendations_prompt_form", 'submit', function(frm) {			frm.preventDefault();  $('input[type=checkbox]:checked').each(	function(){        add_rx_row($(this).dataset());      });	$( "#dialog-message" ).dialog( "destroy" );	
	
	
	});

	$("body").delegate("#rxp_rx_history_prompt_form", 'submit', function(frm) {			frm.preventDefault();           var tmp_arr=new Array();   $('input[type=checkbox]:checked').each(	function(){       tmp_arr.push($(this).dataset());      });
		console.log(JSON.stringify(tmp_arr));
		if (! $('#rxpad')[0]){ display_rxpad({patientid:rd.patientid, meds: tmp_arr});	} else {
			jQuery.each(tmp_arr, function(i, med) { console.log(med); add_rx_row(med);   });
			}
			
		$('#rxp_form div.rxp_med input.med_refills_i:required:invalid')[0].focus();
		$( "#dialog-message" ).dialog( "destroy" );	
		});

	$("body").delegate("#rxp_rx_template_add", 'click', function(frm) {			frm.preventDefault();  $('input.tmpl_med_radio[type=radio]:checked').each(	function(){        add_rx_row($(this).dataset());      });	$( "#dialog-message" ).dialog( "destroy" );	});

	

	
	$("body").delegate("a.rx_hist_fill_dates", 'click', function(href) {			href.preventDefault();   $(this).next("div.fill_date_list").slideToggle('slow');	});
	

	
	
	});