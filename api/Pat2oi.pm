  {
  	package Pat2;
  	use strict;
  #	use Params::Validate;
  #	use Attribute::Params::Validate qw(:all);
  	use WHA;
	use Hash::Merge::Simple;
  #	use MIME::Lite;
  #	use Geo;
  	use Math::BaseArith; #for barcoding...
  	use JSON::XS;
#  	use feature(':5.10');
	use CHI;

#   # exported functions, they croak on error
#   # and expect/generate UTF-8
#  

  			our $dbh=WHA::connect;
  			
  			

sub what_to_do {


		# enable flotr clicking and rollover
		# display relevant symptoms
		# link relevant syms from above to wiki
		# enable wiki
		# build interface
		# fix caching

	return 'tested';

}






  	sub getLabs {   my $dbh=shift;
					my $self=shift;
					
					my @als=RD2::getLabResult($dbh, {patientID => $self->{patientID}, RxNumber => $self->{RxN} || $self->{RxNumber}, RxN => $self->{RxN}});
					my $labs;
					@als=reverse @als;
					if ($self->{single}) {
#						for my $lb (keys %{$als[0]}) {$labs->{$lb->{labTest}}=$lb;} 
					return $als[0];
#					return $labs;
					}
					return \@als;
				}








    sub getPatient {
    my $dbh=shift;
#    #check timestamps:

 use Time::HiRes qw(gettimeofday tv_interval  );
                      
				my $self=shift;
				my $patientID=$self->{patientID};
				return unless $patientID;
				my $jonly=$self->{jonly};				
				my ($patient, $pjson);
		
				warn q{SELECT  LDU > dateCached AS LDU,																dateCached <= labsDate AS getlabs, dateCached <= symptomsDate AS getsymptoms,  dateCached <= medhistoryDate AS getmedhistory,  dateCached <= rxsDate AS getrxhistory,  dateCached <= recDate AS getRecommendations,  dateCached <= pinfoDate AS getpinfo 																FROM jsonCache WHERE rowID=}.$patientID;
				my $times = $dbh->selectrow_hashref(q{SELECT  LDU > dateCached AS LDU,
																(dateCached <= labsDate OR labsDate IS NULL)  AS getlabs,
																(dateCached <= symptomsDate OR symptomsDate IS NULL) AS getsymptoms, 
																(dateCached <= medhistoryDate  OR medhistoryDate IS NULL) AS getmedhistory, 
																(dateCached <= rxsDate  OR rxsDate IS NULL) AS getrxhistory, 
																(dateCached <= recDate  OR recDate IS NULL) AS getRecommendations, 
																(dateCached <= pinfoDate OR pinfoDate IS NULL) AS getpinfo,
																(dateCached	IS NOT NULL) AS DC																							FROM jsonCache WHERE rowID=?}, undef, ( $patientID ));
			my ($nocache, $changes, $updateCache);
			if (ref $times && $times->{DC}) {
				#check tstamps
				
		#		#qw( getsymptoms getlabs getmedhistory getrxhistory getrecommendations getpinfo)
		#		#some can only be 1 - this is to improve DB speed on prescriptions tables
		#    	
					if ($times->{getsymptoms}) {
					my $t0 = [Time::HiRes::gettimeofday];
					warn "getting HH: " . join('  ' , @$t0);
						$pjson->{HH}=getsymptoms($dbh, {patientID => $patientID, hs => [qw(E P T Cortisol DHEA Thyroid Iron)], opts => $self->{opts}});
						$changes++;
						$nocache+=2;
					}
					if ($times->{getlabs}) {
					my $t0 = [Time::HiRes::gettimeofday];
					warn "getting labs: " . join('  ' , @$t0);

		  			$pjson->{Labs}=getLabs($dbh, {patientID => $patientID , opts => $self->{opts}});
		  			$pjson->{Lab_Orders}=getLabOrders($dbh, {patientID => $patientID , opts => $self->{opts}});

						$changes++;
						$nocache++;
					}
					if ($times->{getmedhistory}) {
										my $t0 = [Time::HiRes::gettimeofday];
										warn "getting medHist: " . join('  ' , @$t0);

			 			$pjson->{MedHistory}=getmedhistory($dbh, {patientID => $patientID, opts => $self->{opts}});
						$changes++;
						$nocache++;
					}
					if ($times->{getrxhistory}) {
					
					my $t0;
					$t0 = [Time::HiRes::gettimeofday];					warn "getting RxHist: " . join('  ' , @$t0);

					
# change back soon						$pjson->{info}->{RxHistory}=getrxhistorySummary($dbh, $patientID);
						$pjson->{info}->{RxHistory}=getrxhistory($dbh, {patientID => $patientID, opts => $self->{opts}});
						$pjson->{info}->{OTCHistory}=getotchistory($dbh, {patientID => $patientID, opts => $self->{opts}});
						$pjson->{info}->{OTCHistory_all}=getotchistory($dbh, {patientID => $patientID, includeGroup => 'allOTCs', opts => $self->{opts}});
						$changes++;
						$nocache++;
					}
					if ($times->{getRecommendations}) {
					
					my $t0 = [Time::HiRes::gettimeofday];
					warn "getting Recs: " . join('  ' , @$t0);

						$pjson->{info}->{Recommendations}=getRecommendations($dbh, {patientID => $patientID, opts => $self->{opts}});
#						($pjson->{info}->{RecommendationComment})=$dbh->selectrow_array(q{SELECT keyValue FROM aa_eav;
						$changes++;
						$nocache++;
					}
					if ($times->{getpinfo}) {warn 'no cache available for pinfo';
					my $t0 = [Time::HiRes::gettimeofday];
					warn "getting pinof: " . join('  ' , @$t0);

						$pjson->{info}->{pinfo}=getpinfo($dbh, { patientID => $patientID, opts => $self->{opts} });
						$changes++;
						$nocache++;    			
					}
					if ($changes) {	$pjson->{instances}=getInstances($dbh, { patientID => $patientID, opts => $self->{opts}, rev => 'rev' });	}
				} else { # no cache available
				my $t0;
				warn 'no cache available';
$t0 = [Time::HiRes::gettimeofday];					warn "getting inst: " . join('  ' , @$t0);
						$pjson->{instances}=getInstances($dbh, { patientID => $patientID, opts => $self->{opts}, rev => 'rev' });
$t0 = [Time::HiRes::gettimeofday];					warn "getting Syms: " . join('  ' , @$t0);
						$pjson->{HH}=getsymptoms($dbh, {patientID => $patientID, hs => [qw(E P T Cortisol DHEA Thyroid Iron)],   opts => $self->{opts}});
$t0 = [Time::HiRes::gettimeofday];					warn "getting Labs: " . join('  ' , @$t0);
						$pjson->{Labs}=getLabs($dbh, {patientID => $patientID, patientID => $patientID, opts => $self->{opts}});
$t0 = [Time::HiRes::gettimeofday];					warn "getting medH: " . join('  ' , @$t0);
						$pjson->{MedHistory}=getmedhistory($dbh, {patientID => $patientID, opts => $self->{opts}});
# change back soon						$pjson->{info}->{RxHistory}=getrxhistorySummary($dbh, $patientID);
$t0 = [Time::HiRes::gettimeofday];					warn "getting rxHist: " . join('  ' , @$t0);
						$pjson->{info}->{RxHistory}=getrxhistory($dbh, {patientID => $patientID, opts => $self->{opts}});
						$pjson->{info}->{OTCHistory}=getotchistory($dbh, {patientID => $patientID, opts => $self->{opts}});
$t0 = [Time::HiRes::gettimeofday];					warn "getting Recs: " . join('  ' , @$t0);
						$pjson->{info}->{Recommendations}=getRecommendations($dbh, {patientID => $patientID, opts => $self->{opts}});
#						$pjson->{info}->{RecommendationComment}='$patientID';						
$t0 = [Time::HiRes::gettimeofday];					warn "getting pinfo: " . join('  ' , @$t0);
						$pjson->{info}->{pinfo}=getpinfo($dbh, { patientID => $patientID, opts => $self->{opts} });
						$changes = 1;
				}
				
				# others regardless:
				my $t0;
$t0 = [Time::HiRes::gettimeofday];					warn "getting pReps: " . join('  ' , @$t0);				
				$pjson->{info}->{Reports}=getpatientreports($dbh, { patientID => $patientID, opts => $self->{opts} });
$t0 = [Time::HiRes::gettimeofday];					warn "getting pract: " . join('  ' , @$t0);
				$pjson->{info}->{Orders}=getPatientRecentActivity($dbh, { patientID => $patientID, opts => $self->{opts} } );
$t0 = [Time::HiRes::gettimeofday];					warn "getting statusinfo: " . join('  ' , @$t0);
				$pjson->{info}->{Status}=getPatientStatusInfo($dbh, { patientID => $patientID, opts => $self->{opts} });
				
		#FastMmap
					my $cache = CHI->new(	driver     => 'FastMmap',	root_dir   => '/var/spool/mason/pobj.restoredirect.com/', on_set_error => 'warn'	);
				if ($nocache < 3 && $times ) {
					# get our cache:
					my $pat =  $cache->get('Patient_'. $patientID);
					if ( !defined $pat ) {	
						# means we need to set into memory cache
						$updateCache=1;
					$pat = $dbh->selectrow_array(q{SELECT json FROM jsonCache WHERE rowID=?}, undef, ($patientID));	}
					if ($pat) {$patient = JSON::XS::decode_json($pat) ; }
			}
				
				
				
				
				if ($changes || $updateCache) { 
						#set cache
						#merge the hashes:
						#%$patient = (%$patient, %$pjson);
						#@$patient{keys %$pjson} = values %$pjson  if defined $pjson && ref $pjson eq 'HASH'; # too much memory...
						if (defined $pjson && ref $pjson eq 'HASH') {
							#warn "merging hashes for cache update, num of changes: ", $changes, "tims: $times->{getsymptoms} | $jonly";
						$patient=Hash::Merge::Simple::merge(	 $patient, $pjson	);	}
												$patient->{createdBy}='Pat_test.pm';
					my $json=JSON::XS::encode_json($patient);
		#    	# after assembly cache:
					$dbh->do(q{REPLACE INTO jsonCache SET json=?, dateCached=NOW(), rowID=?  }, undef, ($json, $patientID)) if $changes;
		#    	# set Mason cache:
						#$cache->set( 'Patient_'.$patientID, $json, "3 w" ) or warn $!;
				# if we just want json, give it to them...
				return $json if $jonly;

					}
	if (0)				{
#					$cache->set( 'Patient_'.$patientID, '"$json"', "3 w" );
# 					 my $object = $cache->get_object('Patient_'.$patientID);
# 	    			    my $key        = $object->key();
# 						my $value      = $object->value();
# 						my $expires_at = $object->expires_at();
# 						my $created_at = $object->created_at();
# 						warn $key;
# 						warn $value;
# 						warn $expires_at;
# 						warn $created_at;
			    }
			    $patient->{createdBy}='Pat2';
			    
					if ($jonly) {
						my $json=JSON::XS::encode_json($patient);
						warn "IT IS NULL!" unless $json;
						return $json|| "IT IS NULL!";
						}
						
					return $patient;
	}












  	sub getInstances {
  	    my $dbh=shift;
				my $self=shift;
  					my $patientID=$self->{patientID};
  					my $rev=$self->{rev};
  					my @officialHHs = shift || qw(RFF maleHH aaF aaM aRFF amaleHH aYf0 aYm0); #official HH to look at in determining last2
  					### FIX: replace currentIRMA with aa_eav
  						#	 get Rxs and Dates:



  					 my $oldgrdSQL=q{SELECT DISTINCT RxLookUp.RxNumber AS RxNumber, DATE_FORMAT(IF (rr.surveyDate, rr.surveyDate, hS.hhDate),"%m/%d/%y") AS dateA, qType, DATE_FORMAT(IF (rr.surveyDate, rr.surveyDate, hS.hhDate),"%m/%y") AS ShortDate, hhstatus  , rr.`LastDateUpdated`, rr.`Status`, rr.lastUser, rr.sampleDate, rr.surveyDate, rr.odtID, rr.pdfID, rr.reportType, rr.author FROM RxLookUp 
  					 JOIN hhStatus hS ON RxLookUp.RxNumber=hS.rowID AND hS.hhstatus IS NOT NULL
  					 	JOIN aa_eav aae ON hS.rowID=aae.rowID
  					 LEFT JOIN restoreReports rr ON RxLookUp.RxNumber=rr.RxNumber
  					WHERE RxLookUp.patientID = ?
  					 AND (hS.DateEntered > '2005-01-01' OR hS.DateEntered IS NULL)
  					 AND RxLookUp.RxNumber != '' AND hS.rowID IS NOT NULL
  					 ORDER BY hS.DateEntered DESC LIMIT 8};

  						
  						### FIX: 2009-02-13 new query looks at qType vs labStatus and only pulls non-Labs and completed labs




  						
  						my $getRxsDatesSQL=q{SELECT DISTINCT RxLookUp.RxNumber AS RxNumber, DATE_FORMAT(IF (rr.surveyDate, rr.surveyDate, hS.hhDate),"%m/%d/%y") AS dateA, qType, DATE_FORMAT(IF (rr.surveyDate, rr.surveyDate, hS.hhDate),"%m/%y") AS ShortDate, hhstatus  , rr.`LastDateUpdated`, rr.`Status`, rr.lastUser, rr.sampleDate, rr.surveyDate, rr.odtID, rr.pdfID, rr.reportType, rr.author ,
  						RxLookUp.DC AS DoctorCode
FROM RxLookUp 
  					 JOIN hhStatus hS ON RxLookUp.RxNumber=hS.rowID AND hS.hhstatus IS NOT NULL
  					 	JOIN aa_eav aae ON hS.rowID=aae.rowID
  					 LEFT JOIN restoreReports rr ON RxLookUp.RxNumber=rr.RxNumber
					LEFT JOIN labStatus ls ON  RxLookUp.RxNumber=ls.RxNumber
  					WHERE RxLookUp.patientID = ?	
  					 AND (hS.DateEntered > '2005-01-01' OR hS.DateEntered IS NULL)
  					 AND RxLookUp.RxNumber != '' AND hS.rowID IS NOT NULL
					AND ((ls.`Status`= 'Complete' && hS.qType IN ('RFF', 'maleHH')) OR hS.qType NOT IN ('RFF', 'maleHH'))
	   					 ORDER BY IF(rr.surveyDate, rr.surveyDate, IF( hS.hhDate, hS.hhDate, hS.DateEntered)) DESC LIMIT 8};
  					my $HHs = $dbh->selectall_arrayref($getRxsDatesSQL, {Columns =>{}}, ($patientID));
  					 
  					 
  					 
  					 
	#### FROM SYMPTOMHISTORY.HTML:
		#determine better or worse:
			# first against last same qType HH of standard type (RFF, maleHH), then against last test
			###CHANGE: This feature is not yet implemented
			#find the last "Official" survey which matches, and second to last:
			my @last2;
		for my $num (0..@{$HHs}) {
#			 if (@officialHHs ~~ $HHs->[$num]->{qType}) {push(@last2, $num);}
						warn "grepping2 " . $HHs->[$num]->{qType} . $HHs->[$num]->{RxN};
			 			 if (grep( /^$HHs->[$num]->{qType}$/, @officialHHs ) )	#	@officialHHs ~~ $HHs->[$num]->{qType}) 
			 	{push(@last2, $num); warn "grepped2";}

				# Note: we are taking ANY oHH, an aaF and an RFF, ANY combo.
				# So the wording must be the same across surveys
			last if @last2 >1;
			}
	####/ FROM SYMPTOMHISTORY.HTML:
  					 
  					 if ($rev) {
  					 	my $cnt=0;
  					 	my %insts;
  					 	for my $i (@$HHs){
  					 		$insts{$i->{RxNumber}}=$cnt if $i->{RxNumber};
  					 		$cnt++;
  					 	}
  					 	$insts{'all'}=$HHs if $HHs->[0];
  					 	my $icnt=0;
  					 	for my $inst (@{$insts{'all'}}) {
  					 	delete $insts{'all'}->[$icnt] unless $inst->{RxNumber};	$icnt++;
  					 	# delete $inst unless $inst->{RxNumber};
  					 	}

  					 	$insts{'official'}=\@last2;
  					 	$insts{'all'}->[0]->{active}='active';
  					 	return \%insts;
  					 }
  					 return $HHs;
  	}
  
  
  
  	sub getsymptoms {
  	    my $dbh=shift;
	
  					my $self=shift;	
  					my $opts=$self->{opts};
  					my $patientID = $self->{patientID} || $self->{patientid};
  					my $dateFormat = $self->{dateFormat} || '%m/%d/%y';
  					my $sort = $self->{sort} // 1;
  					my $hs = $self->{hs} || [qw{E P T}];
  					my @last2 = (); #last 2 to pay attention to
  					my @officialHHs = qw(RFF maleHH aaF aaM aRFF amaleHH aYf0 aYm0); #official HH to look at in determining last2
  					warn "getting symptoms 362: ", $patientID;
  					warn join(' ,', %$self) . ' getsymptoms args';
  				 unless ($patientID) {
  				 $patientID=RD2::getpatientidfromrxnumber($dbh, RxNumber=>$self->{RxN}||$self->{RxNumber});
  				 }
  				 unless (defined $patientID) {
  					return;
  				 }
  				warn qq{308: } . join(' ,', @$hs);
  				use List::MoreUtils;
  				
  				
  				return if $self->{noReports}==-1;
  				# get the correct hormones to calc and display
  				my @selHsqls;
  				push(@selHsqls, 'sH.'. $_ .'_excess * sC.calcValue AS '. $_ .'_excess, sH.'. $_ .'_deficiency * sC.calcValue AS '. $_ .'_deficiency') for @$hs;
  				
  				my @selHCsqls;
  				push(@selHCsqls, 'sH.'. $_ .'_excess * MAX(sC.calcValue) AS '. $_ .'_excess, sH.'. $_ .'_deficiency * MAX(sC.calcValue) AS '. $_ .'_deficiency') for @$hs;
  				
  				
  				#	 get Rxs and Dates:
  				
  					my $HHs = getInstances($dbh, { patientID => $patientID, opts => $self->{opts} } );
  					
  						my $sql0=q{
  										SELECT eav.keyName AS keyNameHR, eav.keyValue, sC.calcValue, hQ.questionOrder, hQ.qType, hQ.questionOrder IS NULL AS is_null,} .
  									join(',', @selHsqls) . 
  									q{	FROM aa_symHormones sH 
  											JOIN aa_eav eav ON  sH.keyName=eav.keyName
  											JOIN aa_symCalc sC ON sH.keyName=sC.keyName
  											AND  eav.keyValue=sC.keyValue
  											LEFT JOIN hhStatus hS ON hS.rowID=eav.rowID
  											LEFT JOIN hhQuestions hQ ON hQ.saveValue=sH.keyName AND hQ.qType=IFNULL(hS.qType, 'RFF')
  											WHERE eav.rowID=?
  											AND sC.qType=IFNULL(hS.qType, 'RFF')
  											ORDER BY is_null ASC, hQ.questionOrder ASC};
  				
  						my $sql1=q{
  										SELECT eav.keyName, MAX(sC.calcValue) AS `MaxValue`, } .
  									join(',', @selHCsqls) . 
  									q{	FROM aa_symHormones sH 
  											JOIN aa_eav eav ON  sH.keyName=eav.keyName
  											JOIN aa_symCalc sC ON sH.keyName=sC.keyName
  											LEFT JOIN hhStatus hS ON hS.rowID=eav.rowID
  											LEFT JOIN hhQuestions hQ ON hQ.saveValue=sH.keyName AND hQ.qType=IFNULL(hS.qType, 'RFF')
  											WHERE eav.rowID=?
  											AND sC.qType=IFNULL(hS.qType, 'RFF')
  											GROUP BY sC.keyName
  											};
  				
  				 my $sth = $dbh->prepare($sql0);
  				 warn $sql0;
  				 my $sth2 = $dbh->prepare($sql1);
  				 warn $sql1;
  				# $m->out($sql1);
  				
  					for my $hh (@$HHs) {
  					next unless $hh->{RxNumber};
  				  $sth->execute($hh->{RxNumber});
  				  my $tmphr=$sth->fetchall_hashref('keyNameHR');
# if there ARE Symptoms:
if (  			keys	  %{$tmphr}  > 0 ) {
				$hh->{Symptoms}=$tmphr;
  				
  				
  				  $sth2->execute($hh->{RxNumber});
  				  $hh->{SymptomsCalc} = $sth2->fetchall_hashref('keyName');
  				
  									for my $k (keys %{$hh->{SymptomsCalc}})
  									 { 
  										for my $key (keys %{$hh->{SymptomsCalc}->{$k}}) {
  										$hh->{SymptomsTotal}->{$key}+=$hh->{SymptomsCalc}->{$k}->{$key} if $hh->{SymptomsCalc}->{$k}->{$key} =~ m/^(\d+\.?\d*|\.\d+)$/ ;
  										}
  									}

  						for my $k (@$hs) { for my $ed ('_excess', '_deficiency') {
  							$hh->{order}->{$k.$ed} = [sort {$hh->{Symptoms}->{$b}{$k.$ed} <=> $hh->{Symptoms}->{$a}{$k.$ed} }  grep { $hh->{Symptoms}->{$_}{$k.$ed} == $hh->{Symptoms}->{$_}{$k.$ed} && $hh->{Symptoms}->{$_}{$k.$ed} > 0 } keys %{$hh->{SymptomsCalc}}] ;  #also prevents NaN
  						} }

} # /if Symptoms ###########################################################################################  				
#  	else {delete $hh->{Symptoms};}  				
  					}
  				
  				my $odd=0;
  				
  				####################################################################
  				my @defaultOrder;
  				my @defaultQuestionOrder;
  				
  				#unless (@$HHs) { $m->out("<br><h3>No Symptom History</h3><br>\n"); return;}
  				if (@$HHs > 1) {
  				
  				
  				
  				#determine better or worse:
  					# first against last same qType HH of standard type (RFF, maleHH), then against last test
  					###CHANGE: This feature is not yet implemented
  					#find the last "Official" survey which matches, and second to last:
  				for my $num (0..@{$HHs}) {
  				warn "grepping";
			 if ($HHs->[$num]->{Symptoms} && grep( /^$HHs->[$num]->{qType}$/, @officialHHs ) )	#	@officialHHs ~~ $HHs->[$num]->{qType}) 
				 	{push(@last2, $num);
				 	  				warn "grepped";
				 	}
#  					 if (@officialHHs ~~ $HHs->[$num]->{qType}) {push(@last2, $num);}
  						# Note: we are taking ANY oHH, an aaF and an RFF, ANY combo.
  						# So the wording must be the same across surveys
  					last if @last2 >1;
  					}
  				
  				
  				
  				for my $kN (keys %{$HHs->[$last2[0]]->{Symptoms}}) {
  					if ($HHs->[$last2[-1]]->{SymptomsCalc}->{$kN}->{MaxValue}) {
  						# again, because of different qTypes, we need to calc the difference
  					my $curHH  =  $HHs->[$last2[0]]->{Symptoms}->{$kN}->{calcValue}/$HHs->[$last2[0]]->{SymptomsCalc}->{$kN}->{MaxValue};
  					
  						my $lastHH  =  $HHs->[$last2[-1]]->{Symptoms}->{$kN}->{relativeValue} = $HHs->[$last2[-1]]->{Symptoms}->{$kN}->{calcValue}/$HHs->[$last2[-1]]->{SymptomsCalc}->{$kN}->{MaxValue};
  						
  						$HHs->[$last2[0]]->{Symptoms}->{$kN}->{relativeToLast} =
  						 $curHH == $lastHH ? 0 : $curHH < $lastHH ? 1 : -1;
  						 } elsif ($HHs->[$last2[-1]]->{SymptomsCalc}->{'currentSymptoms.' . $kN}->{MaxValue}) {
  						 my $kN2='currentSymptoms.' . $kN;
	  						# again, because of different qTypes, we need to calc the difference
		  					my $curHH  =  $HHs->[$last2[0]]->{Symptoms}->{$kN}->{calcValue}/$HHs->[$last2[0]]->{SymptomsCalc}->{$kN}->{MaxValue};
	  						my $lastHH  =  $HHs->[$last2[-1]]->{Symptoms}->{$kN2}->{relativeValue} = $HHs->[$last2[-1]]->{Symptoms}->{$kN2}->{calcValue}/$HHs->[$last2[-1]]->{SymptomsCalc}->{$kN2}->{MaxValue};
	  						
	  						$HHs->[$last2[0]]->{Symptoms}->{$kN}->{relativeToLast} =
	  						 $curHH == $lastHH ? 0 : $curHH < $lastHH ? 1 : -1;
  						 }
  					}
  					$HHs->[0]->{kns}=[keys %{$HHs->[$last2[0]]->{Symptoms}}];
  					
  				#/Determine better or worse	
  				
  				# Compute Defiencies/Excesses:
  				
  				
  				# could be more efficient...
  				
  				
  				# by question order:

  					for my $hh (@$HHs) { next unless $hh->{Symptoms};
  					push @defaultQuestionOrder, sort { $hh->{Symptoms}{$b}->{questionOrder} <=> $hh->{Symptoms}{$a}->{questionOrder} ||  $hh->{Symptoms}->{$a}->{relativeToLast} <=> $hh->{Symptoms}->{$b}->{relativeToLast} } keys %{$hh->{Symptoms}};
  					}
  				
  					# clean array
  					@defaultQuestionOrder=List::MoreUtils::uniq(reverse @defaultQuestionOrder);
  					$HHs->[0]->{order}->{questionOrder}=\@defaultQuestionOrder;



  				
  				if ($sort) {
  					# sort by Severity:
  					for my $hh (@$HHs) { next unless $hh->{Symptoms};
  					push @defaultOrder, sort { $hh->{Symptoms}{$b}->{calcValue} <=> $hh->{Symptoms}{$a}->{calcValue} ||  $hh->{Symptoms}->{$a}->{relativeToLast} <=> $hh->{Symptoms}->{$b}->{relativeToLast} || $hh->{Symptoms}{$a}->{questionOrder} <=> $hh->{Symptoms}{$b}->{questionOrder}  } keys %{$hh->{Symptoms}};
  					}
  				
  					# clean array
  					@defaultOrder=List::MoreUtils::uniq(@defaultOrder);
  					$HHs->[0]->{order}->{default}=\@defaultOrder;
  					
  				}
  					
  				} else {@defaultOrder = sort {$HHs->[0]->{Symptoms}->{$a}->{questionOrder} <=> $HHs->[0]->{Symptoms}->{$b}->{questionOrder}}keys %{$HHs->[0]->{Symptoms}};
  				$HHs->[0]->{order}->{default}=$HHs->[0]->{order}->{questionOrder};
  				}
  				
  				
  				
  				unless ($defaultOrder[1]) {@defaultOrder = sort {$HHs->[0]->{Symptoms}->{$a}->{questionOrder} <=> $HHs->[0]->{Symptoms}->{$b}->{questionOrder}}keys %{$HHs->[0]->{Symptoms}};}
  				
  				
  				
  				
  				################################
  				# Next we calc symptoms defs   #
  				################################
				for my $hh (@$HHs) { next unless $hh->{Symptoms};
					for my $k (keys %{$hh->{SymptomsCalc}}) {
  						for my $key (@$hs) { for my $ed ('_excess', '_deficiency') {
  								$hh->{SymptomsDefTotal}->{$key.$ed}+=$hh->{Symptoms}->{$k}->{$key.$ed}; #determine defs
  							}	}
  						}
  					}
			
  #				my %labelText=@{$dbh->selectcol_arrayref(q{SELECT keyName, question FROM aa_symHormones sH JOIN hhQuestions hQ ON sH.keyName=hQ.saveValue},{Columns => [1,2]})};
  				
  				## All of this should go into PObject ##

## get rx numbers:
				my @arxs;
 				for my $hh (@$HHs) {push(@arxs, $hh->{RxNumber});}
 				
 				 				## push notes into EvalText:
 				for my $hh (@$HHs) { 
 				# 2009-01-09 Added symptom notes into rowbo - could create a problem since we already used symptom notes elsewhere on this date...
 				my $rowbo=$dbh->selectcol_arrayref(q{SELECT keyValue FROM aa_eav WHERE keyName IN ('currentIRMA.NotesGeneral_Information', 'currentIRMA.NotesCurrent_Medications', 'currentIRMA.NotesCycling_Questions','currentIRMA.NotesLifestyle',  'currentIRMA.NotesMedical_History', 'currentIRMA.NotesSymptoms', 'SymptomNotes') AND rowID=?}, undef, ($hh->{RxNumber}));
 				warn "534: $hh->{RxNumber}: $hh->{hhstatus}";
 				#check for these first:
 				my ($newornot) = $dbh->selectrow_array(q{SELECT COUNT(*) FROM aa_reports WHERE keyName IN ('HealthStatusNotes2', 'SymptomNotes') AND rowID=?}, undef, ($hh->{RxNumber}));

 				unless ($newornot && $hh->{hhstatus} ne 'New') {
 					$dbh->do(q{INSERT INTO aa_reports (rowID, keyName, keyValue) VALUES (?, 'HealthStatusNotes2',?) ON DUPLICATE KEY UPDATE id=id}, undef, ($hh->{RxNumber},  join("<br/>\n", @$rowbo ))) if (ref $rowbo eq 'ARRAY' && $hh->{RxNumber});
 					warn qq(ref $rowbo eq 'ARRAY' : $hh->{RxNumber});
 					# 2009-01-09 Now we give direct access to this value
 					#$dbh->do(q{INSERT INTO aa_reports (rowID, keyName, keyValue)  SELECT rowID, 'SymptomNotes', keyValue FROM aa_eav WHERE rowID=? AND (keyName='currentIRMA.NotesSymptoms' OR keyName='SymptomNotes') ON DUPLICATE KEY UPDATE id=id}, undef, ( $hh->{RxNumber} ));
 					}
 					}



  				## get any evaluation text:
  				my $arepSQL=q{SELECT t1.keyName, t1.keyValue
								FROM aa_reports t1
								LEFT JOIN aa_reports t2
								ON t1.rowID = t2.rowID
								AND t1.keyName = t2.keyName
								AND t1.id < t2.id
								WHERE t2.id IS NULL AND t1.rowID=?};
				## for setting all the sym defs:
				 my $aesth = $dbh->prepare(q{ REPLACE INTO aa_eav (rowID, keyName, keyValue) VALUES (?, ?, ?)  }) or die $dbh->errstr;								
# last iteration of HHs:
 				for my $hh (@$HHs) { next unless $hh->{Symptoms};
 				# final set of the symptoms info:
 				my %rdl;
					for my $k (keys %{$hh->{SymptomsDefTotal}}) {
					$rdl{$k . '_straight'} =  sprintf("%.2f", 100*($hh->{SymptomsDefTotal}->{$k}/$hh->{SymptomsTotal}->{$k}) ) if $hh->{SymptomsTotal}->{$k} > 0;
					#adapt to S-curve
					$rdl{$k}=sprintf("%.2f",  (exp(-(exp(-.07 * ((100*($hh->{SymptomsDefTotal}->{$k}/$hh->{SymptomsTotal}->{$k})) -28.5)))) *100) ) if $hh->{SymptomsTotal}->{$k} > 0;;

					    	$aesth->execute($hh->{RxNumber}, $k . '_straight', $rdl{$k . '_straight'}) or die $dbh->errstr;
					    	$aesth->execute($hh->{RxNumber}, $k, $rdl{$k}) or die $dbh->errstr;
################# Adjustable threshold					    	
							$aesth->execute($hh->{RxNumber}, 'hasSyms_' . $k, $rdl{$k} > 25 ? 'y' : 'n') if defined $hh->{SymptomsDefTotal}->{$k};
					}


# get full EAV:
			$hh->{EAV}=getEAV($dbh, { rowID => $hh->{RxNumber}, opts => $self->{opts} });
			$hh->{Labs}=getLabs($dbh, { RxN => $hh->{RxNumber}, single => 1, opts => $self->{opts} });
# get Status info and default logic
  				$hh->{Status} = getPatientStatusInfo($dbh, { rowID => $hh->{RxNumber},  opts => $self->{opts} });
  				$hh->{TextLogic} = getLogicText($dbh, {rowID => $hh->{RxNumber}, p => $hh, opts => $self->{opts}}) unless $self->{opts}{noReports}==1;
				$hh->{EvaluationText} ={ @{$dbh->selectcol_arrayref($arepSQL, {Columns =>[1,2]}, ($hh->{RxNumber}))}}  unless $self->{opts}{noReports}==1; 
				

								
				}
				
				


  				## get followed protocol or not (might move to labs some day)
     				my $tfps = $dbh->selectcol_arrayref(q{SELECT rowID, keyValue FROM aa_eav WHERE keyName='followedProtocol' AND rowID IN (} . join(', ', ('?') x @$HHs) . q{)}, {Columns =>[1,2]}, (@arxs)); #'
     				my %fps=@$tfps;
 				for my $hh (@$HHs) {$hh->{followedProtocol} = $fps{$hh->{RxNumber}};}
# @{$HHs->{RxNumber}}
				getAllRecommendations($dbh, $HHs) unless $opts->{noRecommendations};
 
  			return $HHs;
  	}
  	sub getmedhistory {
  	    my $dbh=shift;

				my $self=shift;
  					my $patientID=$self->{patientID};
			
		my $oRx=$self->{oRx};
  		my $HHs =  $oRx ? [{RxNumber => $oRx}] : getInstances($dbh, {patientID => $patientID, opts => $self->{opts}});
  		my @meds;
  		for my $inst (@$HHs) {
  		warn $inst->{RxNumber} . 'at 582';
  			push(@meds,	$dbh->selectall_arrayref(q{SELECT * FROM currentMedication WHERE RxNumber = ? ORDER BY medicationCombined, medicationNotes, medicationType}, {Columns=>{}}, ($inst->{RxNumber})));
  		}
  		#for my $hh (@$HHs)
  		return \@meds;
  	}
#  	
# sub getlabs {
#  	my $labs=$dbh->selectall_hashref(	
#  		q{SELECT  RLU.RxNumber, RLU.sampleDate, lR.*, lRC.* FROM patients p 
#  			JOIN labStatus RLU ON p.patientID=RLU.patientID 
#  			JOIN labResults lR ON RLU.RxNumber=lR.RxNumber  
#  			JOIN  labRangesCalc lRC ON p.Sex=lRC.Sex AND lR.labType=lRC.labType AND lR.clia=lRC.clia AND lR.labTest=lRC.rangeType
#  			WHERE p.patientID=?
#  			AND RLU.`Status`='Complete'
#  			AND lR.labValueAM IS NOT NULL
#  			ORDER BY RLU.sampleDate DESC
#  			LIMIT 500	
#  		},
#  		['RxNumber', 'labTest'], ($patientID));
#  		return $labs;
#  	}
#  	
#	}
  	sub getrxhistory {    my $dbh=shift;
# FAILS HIPAA!!!
  						my $self=shift;
  					my $patientID=$self->{patientID};

  		my $includeFilled = $self->{includeFilled};
  
  
  				my @columns =('RxNumber', 'DatePrescribed', 'DATE_FORMAT(DatePrescribed,"%b %d %Y") AS DatePrescribed_formatted', 'DateFilled', 'DATE_FORMAT(DateFilled,"%b %d %Y") AS DateFilled_formatted', 'DrugName', 'QuantityDisp', 'Sig', 'RemainingRefills', 'StopDate', 'refills_authorized');
  				my $options = "ORDER BY DateFilled DESC";
  				
  				my $stmt2=qq{SELECT } . join(' ,', @columns) . qq{ FROM prescriptions WHERE patientID=? AND VOIDED="NO" AND RxNumber REGEXP '^[64]' };
  				$stmt2.=qq{ UNION SELECT } . join(' ,', @columns) . qq{ FROM prescriptions_unfilled WHERE patientID=? AND StopDate > CURDATE() AND RxNumber REGEXP '^[64]' } unless $includeFilled;
  				$stmt2.=$options;
  
  
  				my $rxhist = $dbh->selectall_arrayref($stmt2, {Columns=>{}}, ($patientID, $patientID));
  				for my $ref (@$rxhist){
  									 	$ref->{Sig}=~s/\n/  /g;
  									 	$ref->{DateFilled} =  undef if $ref->{DateFilled} eq '0000-00-00';
  				}
  				return $rxhist;
  
  	}
  	
  	
  	sub getrxhistorySummary {

  	
  	my $dbh=shift;


  						my $self=shift;
  					my $patientID=$self->{patientID};

  		my $includeFilled = $self->{includeFilled};
  
  
  				my @columns =('RxNumber', 'DatePrescribed', 'DateFilled', 'DrugName', 'QuantityDisp', 'Sig', 'RemainingRefills', 'DATE_FORMAT(DatePrescribed,"%b %d") AS DatePrescribed_formatted', 'DATE_FORMAT(DateFilled,"%b %d") AS DateFilled_formatted', 'refills_authorized');
  				my $options = q{  ORDER BY DatePrescribed DESC  , DateFilled DESC};
  				
  				my $stmt2=qq{SELECT } . join(' ,', @columns) . qq{ , GROUP_CONCAT(CONCAT_WS(': ', DATE_FORMAT(DateFilled, '%b %y'), DaySupply) ORDER BY TransactionRcd DESC SEPARATOR ', ') AS FillDates
FROM prescriptions WHERE patientID=? AND VOIDED="NO" AND RxNumber REGEXP '^[64]'
GROUP BY RxNumber 
};

  				$stmt2.=qq{ UNION SELECT } . join(' ,', @columns) . qq{ 
, GROUP_CONCAT(CONCAT_WS(': ', DATE_FORMAT(DateFilled, '%b %y'), NULL) ORDER BY DatePrescribed DESC SEPARATOR ', ') AS FillDates
FROM prescriptions_unfilled WHERE patientID=?
 AND StopDate > CURDATE() 
AND RxNumber REGEXP '^[64]' 
GROUP BY RxNumber 
} unless $includeFilled;
  				$stmt2.=$options;
  
  warn $stmt2;
  				my $rxhist = $dbh->selectall_arrayref($stmt2, {Columns=>{}}, ($patientID, $patientID));
  				for my $ref (@$rxhist){
  									 	$ref->{Sig}=~s/\n/  /g;
  									 	$ref->{DateFilled} =  undef if $ref->{DateFilled} eq '0000-00-00';
  				}
  				return $rxhist;
  	}

  
    	
  	sub getotchistory {   my $dbh=shift;


  						my $self=shift;
  					my $patientID=$self->{patientID} || $self->{patientid};

  		my $includeFilled = $self->{includeFilled} || $self->{opts}{includeFilled};
		my $includeGroup = $self->{includeGroup} || $self->{opts}{includeGroup} || 'Restore';
  
  				my @columns =('RxNumber', 'DatePrescribed', 'DateFilled', 'displayProducts.content AS DrugName', 'QuantityDisp', 'Sig', 'RemainingRefills');
  				my $options = " ORDER BY DateFilled DESC";
  				
  				my $stmt2=qq{SELECT } . join(' ,', @columns)  . qq{ , GROUP_CONCAT(CONCAT_WS(', ', DATE_FORMAT(DateFilled, '%m/%y')) ORDER BY TransactionRcd DESC SEPARATOR ', ') AS FillDates  FROM prescriptions JOIN displayProducts ON prescriptions.DrugCode=displayProducts.DrugCode AND displayProducts.includeGroup=? WHERE patientID=? AND VOIDED="NO" AND RxNumber REGEXP '^[8]'  GROUP BY prescriptions.RxNumber };
					$stmt2.=$options;

 				my $rxhist = $dbh->selectall_arrayref($stmt2, {Columns=>{}}, ($includeGroup, $patientID));
  				for my $ref (@$rxhist){
  									 	$ref->{Sig}=~s/\n/  /g;
  									 	$ref->{DateFilled} =  undef if $ref->{DateFilled} eq '0000-00-00';
  				}
  				return $rxhist;

  
  	}



  	sub getotchistory_transactions {   my $dbh=shift;


  						my $self=shift;
  					my $patientID=$self->{patientID};

  		my $includeFilled = $self->{includeFilled};
  
  
  				my @columns =('RxNumber', 'DatePrescribed', 'DateFilled', 'displayProducts.content AS DrugName', 'QuantityDisp', 'Sig', 'RemainingRefills');
  				my $options = " ORDER BY DateFilled DESC";
  				
  				my $stmt2=qq{SELECT } . join(' ,', @columns) . q{ FROM prescriptions JOIN displayProducts ON prescriptions.DrugCode=displayProducts.DrugCode AND displayProducts.includeGroup=? WHERE patientID=? AND VOIDED="NO" AND RxNumber REGEXP '^[8]'  };
					$stmt2.=$options;

 				my $rxhist = $dbh->selectall_arrayref($stmt2, {Columns=>{}}, ( $self->{includeGroup} || 'Restore', $patientID));
  				for my $ref (@$rxhist){
  									 	$ref->{Sig}=~s/\n/  /g;
  									 	$ref->{DateFilled} =  undef if $ref->{DateFilled} eq '0000-00-00';
  				}
  				return $rxhist;

  
  	}

  			sub getAllRecommendations {    my $dbh=shift;

  				my $HH=shift;
  				for my $hh (@$HH) {$hh->{Recommendations}=getRecommendations($dbh, undef, $hh->{RxNumber});}
  			}
  			sub getRecommendations {    my $dbh=shift;

  				my $patientID=shift;
  				my ($RxNumber)=shift || $dbh->selectrow_array(q{SELECT rR.rowID FROM hhStatus AS rR LEFT JOIN RxLookUp AS RxL ON RxL.RxNumber=rR.rowID WHERE rR.hhstatus="Complete" AND RxL.patientID=? ORDER BY rR.DateEntered DESC LIMIT 1}, undef, ($patientID));
  				my $self=shift;
  				my $table_name = $self->{fresh} ? 'recommendations_fresh' : 'recommendations' ;
  				#warn "RXN: $RxNumber";
  					# - CHANGED 2011-06-24 $dbh->do(q{UPDATE } . $table_name . q{ SET medCategory='Estradiol' WHERE medCategory='Estrogen' AND RxNumber=?}, undef, ($RxNumber));
  				return RD2::getrecommendations2($dbh,  RxNumber => $RxNumber, include => 'yes', fresh => $self->{fresh});
  						}
  			sub getpinfo {    my $dbh=shift;

  		

  						my $self=shift;
  					my $patientID=$self->{patientID};

  		my $includeFilled = $self->{includeFilled};
  					# important!  Do not use * because it will send CCNum
  										my @pcolumns = ('patients.patientID', 'patients.DoctorCode', 'patients.PatientFirstName', 'patients.PatientMiddleInit,	patients.PatientLastName', 'patients.PatientAddressLine1', 'patients.PatientAddressLine2', 'patients.PatientCity', 'patients.PatientState', 'patients.PatientZIP', 'patients.PatientPhone', 'patients.PatientWorkPhone',	"DATE_FORMAT(patients.DOB,'%M %e, %Y') AS DOB", 'patients.Sex', 'patients.patientImage', 'patients.QS1CODE', 'patients.CCExp', 'SUBSTR(patients.CCNum, -4) AS CCNum4', 'patients.MsgCode', 'SUBSTRING_INDEX(MsgCode, "A", -1)  AS autoship_date' , 'patients.shippingAddressLine1', 'patients.shippingAddressLine2', 'patients.shippingCity', 'patients.shippingState', 'patients.shippingZIP', 'patients.DOB AS dob_iso',  'patients.email', 'patients.shipTo',
  										 'ins_group', 'ins_policy_id', 'ins_pcode', 'ins_relationship', 'bin_num AS ins_bin_num', 'ip.description' , 'ip.processor AS ins_processor',
  										 'DATE_FORMAT(patients.CCExp, "%m") AS CCExpMM',  'DATE_FORMAT(patients.CCExp, "%y") AS CCExpYY'
  										 );
  										my $cols=join(', ',@pcolumns);
  					
  					my $sqlstmt= qq{SELECT $cols FROM patients  LEFT JOIN insurance_plans ip ON patients.ins_pcode=ip.pcode WHERE patientID=? LIMIT 1};
  					my $patient=$dbh->selectrow_hashref($sqlstmt, undef, ($patientID));
  
  					# for forms:
  					$patient->{PatientPhone}=~m/^(\d{3})\D?(\d{3})\D?(\d{4})$/;
  					$patient->{PatientHomePhoneAreaCode}=$1;
  					$patient->{PatientHomePhonePrefix}=$2;
  					$patient->{PatientHomePhoneEnd}=$3;
  					$patient->{PatientWorkPhone}=~m/^\D?(\d{3})\D?(\d{3})\D?(\d{4})$/;
  					$patient->{PatientPhoneAreaCode}=$1;
  					$patient->{PatientPhonePrefix}=$2;
  					$patient->{PatientPhoneEnd}=$3;
  					if ($patient->{SSN}) {
  						$patient->{SSN}=~m/^(\d{3})\D?(\d{2})\D?(\d{4})$/;
  						$patient->{SSN3}=$patient->{PatientSSN3}=$1; #some where a mistake was made...
  						$patient->{SSN2}=$patient->{PatientSSN2}=$2;
  						$patient->{SSN4}=$patient->{PatientSSN4}=$3;
  					}
  				$patient->{PatientPhone}=~s/(\d\d\d)(\d\d\d)(\d\d\d\d)/($1) $2-$3/;
  				$patient->{PatientWorkPhone}=~s/(\d\d\d)(\d\d\d)(\d\d\d\d)/($1) $2-$3/;
  				$patient->{_hasRestoreNotes}=$dbh->selectrow_array(q{SELECT COUNT(*) FROM notes WHERE patientID=? AND noteType='Restore'}, undef, ($patientID));
  				$patient->{addresses} = getAddresses($dbh, $self) || 'no addresses';
  				return $patient;
  				}
  
  	sub getpatientreports {    my $dbh=shift;

  						my $self=shift;
  					my $patientID=$self->{patientID};
		my $Status = $self->{Status};
		$Status = ref $Status ? $Status : [($Status|| 'Completed' )]; #force arrayref # was 'Complete'
				my @columns = qw(  restoreReports.surveyDate restoreReports.LastDateUpdated restoreReports.Status pdfID reportType );
				my $options = " ORDER BY surveyDate DESC";
				my $rsqlstmt=qq{SELECT DISTINCT(restoreReports.RxNumber), } . join(', ', @columns) . qq{ FROM restoreReports JOIN RxLookUp AS p ON p.RxNumber=restoreReports.RxNumber WHERE p.patientID=? AND p.patientID IS NOT NULL AND `Status` IN (} . join(', ',('?') x @$Status) .q{) } . $options;
				my $rr = $dbh->selectall_arrayref($rsqlstmt, {Columns =>{}}, ($patientID, @$Status));
  	}

#'
sub getPatientRecentActivity {    my $dbh=shift;

  						my $self=shift;
  					my $patientID=$self->{patientID};
							# orders:			
							my $rorders=$dbh->selectall_arrayref(q{SELECT rP.productDisplayName, oQ.OrderStatus, oQ.LastDateUpdated, 'lab' FROM ordersQueue AS oQ JOIN restoreProducts AS rP on oQ.productID=rP.productID WHERE patientID=? UNION } . q{SELECT  CONCAT_WS(" ", oQ.Medication, oQ.Dose, oQ.Qty) AS med, oQ.RxStatus, oQ.LastDateUpdated, 'rx' FROM prescriptionQueue AS oQ  WHERE patientID=? ORDER BY LastDateUpdated DESC LIMIT  10}, undef, ($patientID, $patientID));
		}  
		
		
	sub getLogicText {    my $dbh=shift;

  						my $self=shift;
  					my $patientID=$self->{patientID};

	my $rowID=$self->{rowID};
	my $p=$self->{p};
	my $m=$HTML::Mason::Commands::m;
	my $rid_qType = $dbh->selectrow_array(q{SELECT qType FROM hhStatus WHERE  rowID=?}, undef, ($rowID));
	#### BEGIN EAV-Code ####
			my $logicstmt=qq {SELECT RestoreLogicTable2.rowID AS Log1
			  FROM aa_eav AS T 
			LEFT OUTER JOIN
			 RestoreLogicTable2
				ON RestoreLogicTable2.keyName = T.keyName  
				 AND RestoreLogicTable2.keyValue = T.keyValue 
			WHERE T.rowID=?
				AND RestoreLogicTable2.qType IN (?, '')
			GROUP BY
			 T.rowID, RestoreLogicTable2.rowID         
			HAVING COUNT(RestoreLogicTable2.rowID) = 
			( SELECT COUNT(*) FROM RestoreLogicTable2 WHERE RestoreLogicTable2.rowID=Log1 					AND RestoreLogicTable2.qType IN (?, '')	GROUP BY RestoreLogicTable2.rowID)
			};
			
			# the follow is good, but seems to have lost the qType
			my $logicstmt_new=q{SELECT RestoreLogicTable2.rowID AS Log1
  FROM aa_eav AS T 
LEFT OUTER JOIN
 RestoreLogicTable2
    ON RestoreLogicTable2.keyName = T.keyName  
	 AND (RestoreLogicTable2.keyValue = T.keyValue OR (RestoreLogicTable2.keyValue IS NULL AND T.keyValue IS NULL))
WHERE T.rowID=? 				AND RestoreLogicTable2.qType IN (?, '')
GROUP BY
 T.rowID, RestoreLogicTable2.rowID         
HAVING COUNT(RestoreLogicTable2.rowID) = 
( SELECT COUNT(*) FROM RestoreLogicTable2 WHERE RestoreLogicTable2.rowID=Log1 				AND RestoreLogicTable2.qType IN (?, '')
																							GROUP BY RestoreLogicTable2.rowID)
};
			my $ncntr;
			
#			my $rowIDs=$dbh->selectcol_arrayref($logicstmt, undef, ($rowID, $rid_qType, $rid_qType));
						my $rowIDs=$dbh->selectcol_arrayref($logicstmt_new, undef, ($rowID, $rid_qType, $rid_qType));
			#setKeys:
			my $klist=join(', ', ('?') x @$rowIDs);
			my $setValsStmt = qq`SELECT keyName, keyValue FROM RestoreLogicSetKeys WHERE rowID IN (` . $klist . ')';
#			$m->out($setValsStmt); $m->flush_buffer;
			warn 'SET: ' . $setValsStmt;
			warn 'SET: ' . join(', ', @$rowIDs);
			my %setValsHash = @{$dbh->selectcol_arrayref($setValsStmt, { Columns=>[1,2] }, (@$rowIDs))} if @$rowIDs && $klist ; #prevents empty list in SQL
			#@{$p->{EAV}}->{keys %setValsHash}=values %setValsHash;
			if (keys %setValsHash) {for my $k (keys %setValsHash) {$p->{EAV}->{$k} = $setValsHash{$k};}}
			
			# general comments:
			#push(@$rowIDs, @{$Ldbh->selectcol_arrayref(qq{SELECT rowID FROM RestoreLogicTable2 WHERE keyName='currentIRMA.generalLogic'})});
			
									   # need to get the comments from textStorage...
	                       my $txtStorageStmt = qq{SELECT RLC.keyName AS section, textLogic.commentSection AS subSection,   textLogic.commentOrder AS cOrder,  SentenceOrder, textLogic.logicID AS logicID, textLogic.textID, commentText AS text, commentHTML AS HTML, textLogic.class FROM textStorage
	                       							 JOIN textLogic ON textLogic.textID=textStorage.textID 
										JOIN RestoreLogicComments RLC ON logicID=RLC.keyValue
	                       							 WHERE textLogic.active='on' AND RLC.rowID IN (}.join(',',('?')x@$rowIDs).q{) 
                               ORDER BY section , subSection, commentOrder, SentenceOrder     
	                       };
#	                       warn $txtStorageStmt . q{}.join(',', @$rowIDs).q{)};
	                       # this was going to be LogicComments (RLC) which contained only info for providers, right now we are doing letters so we pull from RestoreLogicComments for Kali
#warn qq{<div>Symptom of '$p->{Symptoms}->{"currentSymptoms.vaginalDryness"}->{keyValue}' vaginal dryness may be relieved with Estriol.</div>};
#'
	                       $ncntr++;
	                                my $text=$dbh->selectall_hashref($txtStorageStmt, [qw(section subSection cOrder SentenceOrder logicID textID)], undef, (@$rowIDs) ) if @$rowIDs;
	                                	my %cH;
	                                	for my $sect (sort keys %{$text}){
	                                		for my $subSect (sort keys %{$text->{$sect}}) {
	                                			for my $cOrder (sort {$a<=>$b} keys %{$text->{$sect}->{$subSect}}) {
	                                				for my $SO (sort {$a<=>$b} keys %{$text->{$sect}->{$subSect}->{$cOrder}}) {
	                                					for my $lid (keys %{$text->{$sect}->{$subSect}->{$cOrder}->{$SO}}) {
	                                					my $sarr;
		                                					for my $tid (keys %{$text->{$sect}->{$subSect}->{$cOrder}->{$SO}->{$lid}}) {
	# Make an anonymous component
    my $a_comp = eval {   $m->interp->make_component( comp_source => q{<%args>$p</%args>} . $text->{$sect}->{$subSect}->{$cOrder}->{$SO}->{$lid}{$tid}->{HTML}) }; 		warn $@ if $@;
		$text->{$sect}->{$subSect}->{$cOrder}->{$SO}->{$lid}{$tid}->{HTML} = eval {$m->scomp($a_comp, p => $p) };
		warn $@ if $@;
																


		                                					push(@$sarr, $text->{$sect}->{$subSect}->{$cOrder}->{$SO}->{$lid}{$tid});
	                                			}
		                               			push(@{$cH{$sect}->{$subSect}}, {$lid => $sarr});
	                                		}
	                                	}
	                                	}
	                                	}
	                                	}
	                                	return \%cH;

	
	}
	sub getPatientStatusInfo {
	    my $dbh=shift;
  						my $self=shift;
  					my $rowID=$self->{rowID};
	# must change for other qTypes!  Please update -- FIX ME 09-2009
		my $rowID=shift;
		my %h=@{$dbh->selectcol_arrayref(q{SELECT keyName, keyValue FROM aa_eav WHERE rowID=? AND keyName IN (
		'currentIRMA.numOvaries', 'currentIRMA.stress', 'currentIRMA.idealWeight',
		'currentIRMA.weeklyExercise', 'currentIRMA.periodsLastYear', 'currentIRMA.weight',
		'currentIRMA.lastPeriod', 'currentIRMA.menstruation', 'currentIRMA.hysterectomy',
		'currentIRMA.regularMenstrualCycle', 'currentIRMA.cholesterolLevel', 'currentIRMA.smoker',
		 'currentIRMA.alcohol', 'currentIRMA.weight',
		  'currentIRMA.brokenBone', 'currentIRMA.baby', 'currentIRMA.babyBreastFeeding', 'currentIRMA.methodPreference',
		  'currentIRMA.pregnancy', 'currentIRMA.pregnantCurrently', 'currentIRMA.generalHealth')}, { Columns=>[1,2] }, ($rowID))};
		return \%h;
		}
	
	sub getEAV {
			    my $dbh=shift;
				my $self=shift;
  					my $rowID=$self->{rowID};

		my %h=@{$dbh->selectcol_arrayref(q{SELECT keyName, keyValue FROM aa_eav WHERE rowID=? }, { Columns=>[1,2] }, ($rowID))};
		return \%h;

	}
	
	
	
	
	
	
	sub getLabGraph {
	
	
			    my $dbh=shift;
			    my $errs=shift;
				my $self=shift;

my $patientid = $self->{patientid};
my $RxNumber = $self->{rxn};
my @graphs = ref $self->{graphs} ? @{$self->{graphs}} : ('Estradiol', 'Testosterone', 'Progesterone', 'UrineNTx');
my @displays = ref $self->{displays} ? @{$self->{displays}} : ();
use Math::Complex; # used for ln, http:# //search.cpan.org/~nwclark/perl-5.8.7/lib/Math/Complex.pm
use JSON::XS;
#use feature ':5.10';
my @labs;
my $tess =$RxNumber;
	my $m=$HTML::Mason::Commands::m;


if ($displays[0]) {
foreach my $display (@displays) {
	next unless $display;
	my @results=RD2::getLabResult($dbh,  patientID => $patientid, DoctorCode => $self->{doc}{DoctorCode}, RxNumber => $display);
	push (@labs, $results[0]);
 }
}

else {
	$patientid||=$dbh->selectrow_array(q{SELECT patientID FROM RxLookUp WHERE RxNumber=?}, undef, ($tess));
 @labs=RD2::getLabResult($dbh,  patientID => $patientid) ;

#warn 'getLabResult' . qq{(patientID => $patientid)};
}

#warn RD2::jd([$patientid, $self, \@labs]);
#return unless @labs;
my $labType = 'saliva';

my @labcolumns = ('labTest', 'labLabel', 'graph');
my %labquery = (
		labUsed => 'yes',
		labType => "$labType",
		drDisplay => 'list'
);
		
my $laboptions = "ORDER BY labOrder";
my @chart_title;

my %labLabel=@{$dbh->selectcol_arrayref(q{SELECT labTest, shortLabLabel FROM labTestsUsed}, {Columns =>[1,2]})};
my %K = @{$dbh->selectcol_arrayref(q{SELECT rangeType, ((TTRHigh+TTRLow)/2) AS TTR FROM labRangesCalc l JOIN patients  p ON l.Sex=p.Sex WHERE patientID=?}, {Columns =>[1,2]}, ($patientid))};


my %K2=(
	Estradiol => 6.5,
	Progesterone => 0.25,
	Testosterone => 40,
	DHEA => 250,
	CortisolAM => 7,
	CortisolPM => 1.8,
	Cortisol_1 => 7,
	Cortisol_3 => 1.8,
	NTx => 38,			# aka UrineNTx
	UrineNTx => 38,
	DHT => 92				# DHT has no TTR
	);
	
	
my @labresults=RD2::sqlsearch($dbh,   columns => \@labcolumns, tables => 'labTestsUsed', query => \%labquery, options => $laboptions);
my (@labLabels, @graph, @labTests);
foreach my $result (@labresults) {
	push (@labTests, $result->{'labTest'});
	push (@labLabels, $result->{'labLabel'});
	push (@graph, $result->{'graph'});
}













# get x-axis
my @whichlabs;
my @series ;
my %tick_s;
# = 
# 	({
#          "points" => {
#             "show" => 0
#          },
#          "data" => [
#          ],
#          "label" => undef
# 	});
my @xaxis;
		my $lcntr=0;
		my $xaxiscnt=1;
		foreach my $lab (@labs) {
			foreach my $graph (@graphs) {                
				if ($lab->{$graph}->{labValueAM}) {
					push(@whichlabs, $lcntr);
					(my $tdate=$lab->{Test_Date}->{labValueAM} ) =~s{(\d\d\D)(\d\d\D)20(\d\d)}{$1$3};
					push(@xaxis,  $tdate);
					$xaxiscnt++;
					last; 
					}
		
			}
		$lcntr++;

		 }
#push(@{$series[0]->{data}}, [undef, undef],  [$lcntr+1, undef]);

# for min-low-high-max:
my @hilo_labels =('Below Reference Range', 'Below Therapeutic Target Range', 'Within Therapeutic Target Range', 'Above Therapeutic Target Range', 'Above Reference Range');
my %better_hilo_labels = (
	UrineNTx => ['Normal', 'Normal', 'Normal', 'Elevated', 'High'],
	default => \@hilo_labels
);
	#line legend
				foreach my $graph (@graphs) {
						my @tickers;				
						my %tmp_ticks;
                        my $lcnt=0;
                        my @dataVals;
		                        foreach my $lab (@labs[@whichlabs]) {
 if ( $lab->{$graph}->{labValuePM} || $lab->{$graph}->{labValueAM} ) {
# 		say '<div>',  $lab->{$graph}->{labValuePM} || $lab->{$graph}->{labValueAM}, ": $K{$graph}: $graph</div>\n";
						$lab->{$graph}->{labValuePMVal}=$lab->{$graph}->{labValuePM};
						$lab->{$graph}->{labValuePMVal}=~s/[^0-9\.\-]//gi;
						#~s/[^0-9\.\-]# //gi;
						$lab->{$graph}->{labValueAMVal}=$lab->{$graph}->{labValueAM};
						$lab->{$graph}->{labValueAMVal}=~s/[^0-9\.\-]//gi;
						#~s/[^0-9\.\-]# //gi;
 #  $m->out(qq{graph: $graph}); $m->flush_buffer; #ususally a problem with NULL patients.Sex
 						my $Y= 100*(1-exp((ln(0.50)/($K{$graph}|| $K2{$graph}))*($lab->{$graph}->{labValuePM} ? $lab->{$graph}->{labValuePMVal}  : $lab->{$graph}->{labValueAMVal})) );
						push(@dataVals,[($lcnt, $Y)]);
  push(@tickers, 
	  	$m->interp->apply_escapes(  $lab->{$graph}->{labValuePM} ? qq{$graph: $lab->{$graph}->{labValuePM} (PM)} : qq{$graph: $lab->{$graph}->{labValueAM} (AM)}, 'h' ) 
				);
				( my $tdate=$lab->{Test_Date}->{labValueAM} ) =~s{(\d\d\D)(\d\d\D)20(\d\d)}{$1$3};
				$tmp_ticks{$tdate}->{val}=$m->interp->apply_escapes(  $lab->{$graph}->{labValuePM} ? qq{$graph: $lab->{$graph}->{labValuePM} (PM)} : qq{$graph: $lab->{$graph}->{labValueAM} (AM)}, 'h' ) ;
				my $hilos=defined $better_hilo_labels{$graph} ? $better_hilo_labels{$graph} : $better_hilo_labels{default};
				$tmp_ticks{$tdate}->{hilo} = 
					(defined $lab->{$graph}->{hilo} && $hilos->[$lab->{$graph}->{hilo}] ? 
						$hilos->[$lab->{$graph}->{hilo}]  : 'See Ref Ranges');
 }
 	else { 
				#push(@dataVals,[($lcnt, )]);
				push(@tickers,  'No Data');
			( my $tdate=$lab->{Test_Date}->{labValueAM} ) =~s{(\d\d\D)(\d\d\D)20(\d\d)}{$1$3};
				$tmp_ticks{$tdate}='No Data';

		 }
 						$lcnt++;
 }

push(@chart_title, $labLabel{$graph});
push(@series, 
		{
			data    => \@dataVals,
			name   => $labLabel{$graph},
			tickers => \@tickers
		},
	);
	
	$tick_s{$labLabel{$graph}}=\%tmp_ticks;
 
 }
 # numColumns adjust:
	my $numColumns = 5;
 my $options = {
 	chart => {
						renderTo => 'lab_graphing_area',
						defaultSeriesType => 'line',
						marginRight => 130,
						marginBottom => 25,
						reflow => \0
					},
	shadow => \1,
 	colors =>  [  '#0D7B9C','#3366CC',  '#9B33B0', '#AD461D', '#948D40' ], 
 	credits => {	enabled => \0	},
 	tickers => \%tick_s,
	legend =>  {
	
	
						layout => 'vertical',
						align => 'right',
						verticalAlign => 'top',
						x => -10,
						y => 100,
						borderWidth => 0
	},
	xAxis =>  {
			categories  =>  \@xaxis,
	},
	yAxis =>  {
	labels => {	enabled => \0	},
		n0categories => ['Low', 'In Range', 'High'],
		min => 0,
		max => 100,
		title => {
			text => 'Low . . . . . In Range . . . . . High'
		},
		plotLines => [{
				value => 0,
				width => 1,
				color => '#808080'
			}]	
		},
tooltip => { formatter => q{ function() {    return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y +'';	}					}	},	
ticks =>  [[25, 'Low'], [50, 'In Range'], [75, 'High']],
title => {text => join(', ', @chart_title)},
series => \@series,
 patientID => $patientid,
 
RxN =>  $RxNumber
	
	};

#   my $json = JSON->new->utf8->pretty->encode(\%options);
return $options;

	
	
	
	
	}
	
	




	
	sub getSymGraph {
	
	
			    my $dbh=shift;
			    my $errs=shift;
				my $self=shift;

my $patientid = $self->{patientid};
my $RxNumber = $self->{rxn};
my @graphs = ref $self->{graphs} ? @{$self->{graphs}} : ('Estradiol', 'Testosterone', 'Progesterone', 'UrineNTx');
my @displays = ref $self->{displays} ? @{$self->{displays}} : ();
my @chart_title;
use Math::Complex; # used for ln, http:# //search.cpan.org/~nwclark/perl-5.8.7/lib/Math/Complex.pm
use JSON::XS;
#use feature ':5.10';
my @labs;
my $tess =$RxNumber;
	my $m=$HTML::Mason::Commands::m;
warn RD2::jd([$patientid, $self, \@labs]);



my @hs = ref $self->{hs} ?  @{$self->{hs}} : qw(E P T);
my $barWidth = 10;









my $labelArea = 'symDefLabelArea';
my $numColumns;
#$m->comp('flotr_cortisol.html'); return;
use POSIX qw(floor);
#use RD::Logic2;

use Math::Complex; # used for ln, http:# //search.cpan.org/~nwclark/perl-5.8.7/lib/Math/Complex.pm
# use feature ':5.10';


my %sym_threshold;
unless ($self->{hs}) {	%sym_threshold = ('E_deficiency' => 8, 'P_deficiency' => 8, 'T_deficiency' => 8, 'E_excess' => 8,  'P_excess' => 100, 'T_excess' => 10 );}  #if nothing defined, show a default


my @plabs=RD2::getpatientlabs($dbh, patientID => $patientid);
return unless @plabs;
my %oD;

#static vars:
# 	Thyroid
# 	 Cortisol
# 	  DHEA
# 	   Melatonin
# 	    HGH
# 	     Pregnelone
# 	      Aldosterone
	
foreach my $plab (@plabs) {
	$oD{$plab->{RxNumber}}={};
	
		my $symStmt=q{SELECT };
			my @aasyms;
		push(@aasyms, q{SUM(aa.calcValue * sH.} .$_. q{_excess) AS } .$_. q{_excess, 
			SUM(aa.calcValue * sH.} .$_. q{_deficiency) AS }. $_. q{_deficiency }) for (@hs);
			$symStmt.=join(', ', @aasyms);


			my $symStmts= q{
					FROM aa_symHormones sH 
					JOIN aa_eav t2 ON  sH.keyName=t2.keyName
					JOIN aa_symCalc aa ON sH.keyName=aa.keyName
					AND  t2.keyValue=aa.keyValue
					WHERE t2.rowID=?
				};
				
				$symStmt.=$symStmts;
				
		my @syms2;
		my $symStmt2=q{SELECT };
		push(@syms2, q{SUM(sH.} .$_. q{_excess) AS } .$_. q{_excess, 
			SUM(sH.} .$_. q{_deficiency) AS }. $_. q{_deficiency }) for (@hs);
			$symStmt2.=join(', ', @syms2);
			$symStmt2.= q{
			FROM aa_symHormones sH 
				JOIN aa_eav t2 ON  sH.keyName=t2.keyName
				WHERE t2.rowID=?
			};
			
		my $curSyms=$oD{$plab->{RxNumber}}->{curSyms}=
			$dbh->selectrow_hashref($symStmt, undef, ($plab->{RxNumber}));
		
		my $posSyms=$oD{$plab->{RxNumber}}->{posSyms}=
			$dbh->selectrow_hashref($symStmt2, undef, ($plab->{RxNumber})); #tell us "of answered syms, these were possible..."
		my %symA;
		my $ptotal=$oD{$plab->{RxNumber}}->{ptotal} =
			$dbh->selectrow_array(q{SELECT SUM(calcValue) FROM aa_eav ae JOIN aa_symCalc ac ON ae.keyName=ac.keyName AND ac.qType='aYf0' AND ae.keyValue=ac.keyValue WHERE rowID=?}, undef, ($plab->{RxNumber}));

		for my $hm (@hs) { # 3 is MAX (symptom levels: 0, 1, 2, 3), which is why 3 appears in SQL below

			$oD{$plab->{RxNumber}}->{symA}{$hm.'_deficiency'}=$curSyms->{$hm.'_deficiency'}/(3 * $posSyms->{$hm.'_deficiency'}) if $posSyms->{$hm.'_deficiency'} > 0;
			
#			$oD{$plab->{RxNumber}}->{symA}{$hm.'_deficiency'}+=($curSyms->{$hm.'_deficiency'}/$ptotal)/1 if $ptotal;
			$oD{$plab->{RxNumber}}->{symA}{$hm.'_excess'}=$curSyms->{$hm.'_excess'}/(3 * $posSyms->{$hm.'_excess'}) if $posSyms->{$hm.'_excess'} > 0; 
#			$oD{$plab->{RxNumber}}->{symA}{$hm.'_excess'}+=($curSyms->{$hm.'_excess'}/$ptotal)/1 if $ptotal;			
			
			
			
			
			
			if ($oD{$plab->{RxNumber}}->{curSyms}->{$hm.'_deficiency'} > 0|| $oD{$plab->{RxNumber}}->{curSyms}->{$hm.'_excess'} > 0) {
				#$m->out(qq{<div>$hm: $oD{$plab->{RxNumber}}->{curSyms}->{$hm.'_deficiency'}/$ptotal</div>});
				#$m->out(qq{<div>$hm: $oD{$plab->{RxNumber}}->{curSyms}->{$hm.'_excess'}/$ptotal</div>});	
			}		

		}
my $ED=( int ($oD{$plab->{RxNumber}}->{symA}{'E_deficiency'} *150) + int(50 * $oD{$plab->{RxNumber}}->{symA}{'P_excess'}) );
my $PD=( int ($oD{$plab->{RxNumber}}->{symA}{'P_deficiency'} *100) + int(100 * $oD{$plab->{RxNumber}}->{symA}{'E_excess'}) );
$PD=1 unless $PD;

# unused?
#my $pe=int ($ED/$PD *100) -100;

}














# change plabs to have surveydate



# get x-axis
my @whichlabs;
my %tick_s;

my @series = 
	({
         "points" => {
            "show" => 0
         },
         "data" => [
         ],
         "label" => undef
	});
	#@series=();
my @xaxis;
my %gmap;
		my $lcntr=0;
		my $xaxiscnt=1;
		foreach my $plab (@plabs) {
			for my $hm (@hs) {                
				if ($oD{$plab->{RxNumber}}->{curSyms}->{$hm.'_deficiency'} > $sym_threshold{$hm.'_deficiency'} || $oD{$plab->{RxNumber}}->{curSyms}->{$hm.'_excess'} > $sym_threshold{$hm.'_excess'}) {
					push(@whichlabs, $lcntr);
					(my $tdate=$plab->{sampleDate} ) =~s{(\d\d\D)(\d\d\D)20(\d\d)}{$1$3};
#					push(@xaxis, [$xaxiscnt*100+20 , $tdate]);
					push(@xaxis,  $tdate);
					$xaxiscnt++;
					last; 
					}
		
			}
		$lcntr++;

		 }
#push(@{$series[0]->{data}}, [80, undef],  [(($lcntr+0)*100)-40, undef]);




#########Count MAX num of bars in a series:

	my $mxdtsrs = $#hs; #default
	my $numSeries=@whichlabs *1 ;
	#print "nS: $numSeries";
			for my $hm (@hs) {   for my $eod ('excess', 'deficiency') {
                        my $lcnt=1;
                        my $dataVals;
		                        foreach my $plab (@plabs[@whichlabs]) {
 if ($oD{$plab->{RxNumber}}->{curSyms}->{$hm.'_'.$eod} > $sym_threshold{$hm.'_'.$eod}) {
						++$dataVals;}}
	$mxdtsrs = $mxdtsrs > $dataVals ? $mxdtsrs : $dataVals;
	}}
	$barWidth=int(100/(($mxdtsrs + 1) * ($numSeries || 1)));
	#print $barWidth;
	
	#### since we now have barWidth and max num bars:
#push(@{$series[0]->{data}}, [80, undef],  [(($lcntr+0)*100)-40, undef]);
# 	for my $xa (@xaxis) {
# #	$xa->[0]=$xa->[0]*$mxdtsrs*$barWidth+20;
# 	$xa->[0]=$xa->[0]*150+20;
# 		$xa->[1]=$xa->[0] - 20;
# 	}
                        
	#line legend








			my %hmcnt;
						my $Y=1111111111111111111111; 
						my $lcnt;
						my @tickers;
						my $ttcnt;
						my %tmp_ticks;
						my @dataVals;
		                        foreach my $plab (@plabs[@whichlabs]) {
			for my $hm (@hs) {   for my $eod ('excess', 'deficiency') {
			 if ($oD{$plab->{RxNumber}}->{curSyms}->{$hm.'_'.$eod} > $sym_threshold{$hm.'_'.$eod}) {

						push(@dataVals,[($lcnt, $Y)]);
						(my $tdate=$plab->{sampleDate} ) =~s{(\d\d\D\d\d\D)20(\d\d)}{$1$2};
					  	$tickers[$ttcnt]=$m->interp->apply_escapes(  $hm.' '."\u$eod" .': '. int($oD{$plab->{RxNumber}}->{symA}->{$hm.'_'.$eod}*100).'%' , 'h' ) ;
						$tmp_ticks{$tdate}->{val}=$m->interp->apply_escapes(  $hm.' '."\u$eod" .': '. int($oD{$plab->{RxNumber}}->{symA}->{$hm.'_'.$eod}*100).'%' , 'h' );
						}
			 	else { 
					$tickers[$ttcnt]='No Data'; # if $dataVals[$lcnt-1];
			#20110904
						(my $tdate=$plab->{sampleDate} ) =~s{(\d\d\D\d\d\D)20(\d\d)}{$1$2};
							$tmp_ticks{$tdate}='No Data';
					 }
			
			 if (defined $dataVals[$lcnt-1]) {
 				$dataVals[$lcnt-1]->[0]+=0; # numify dataVals again for JSON::XS.  Otherwise it is stringified and can 
 											#not be properly read by Flotr
			unless($dataVals[$lcnt-1]->[0]) {delete $dataVals[$lcnt-1];}
 											}
 						$lcnt++;
						 $ttcnt++;
			


push(@chart_title,$m->interp->apply_escapes(  ($eod=~m/def/i ? '-' : '+') . "\u$hm"));
push(@series, 
		{
			data    => \@dataVals,
			name   => ($eod=~m/def/i ? '-' : '+') . "\u$hm",
			points  => {show => 0},
			mouse => {	
				track => 'true',		# // => true to track the mouse, no tracking otherwise
				position => 'se',	#	// => position of the value box (default south-east)
				margin => 3,	#	// => margin in pixels of the valuebox
				color => '#ff3f19', #	// => line color of points that are drawn when mouse comes near a value of a series
				trackDecimals => 2,	 # // => decimals for the track values
#				sensibility => 12,		# // => the lower this number, the more precise you have to aim to show a value
				radius => 3,		# // => radius of the tracck point
				tickers => \@tickers
				},

		}
	) if @dataVals;

	$tick_s{$hm.'_'.$eod}=\%tmp_ticks;

 } 
 }
 }
#########Count num labels:
	my %nc;
	for my $mp (keys %gmap) {$nc{$gmap{$mp}->{eod}.$gmap{$mp}->{hormone}}++;}
	$numColumns||=$self->{labelArea} ? ((keys %nc) + 1) > 6 ? 6 :((keys %nc) + 1)  : 1;



 my $options = {
 		        
 	chart => {
						renderTo => 'sym_graphing_area',
						defaultSeriesType => 'line',
						marginRight => 130,
						marginBottom => 25
					},
	shadow => \1,
 	colors =>  [  '#0D7B9C','#3366CC',  '#9B33B0', '#AD461D', '#948D40' ], 
 	credits => {	enabled => \0	},
 	tickers => \%tick_s,

	legend =>  {
		show =>  'true',		# // => setting to true will show the legend, hide otherwise
		noColumns =>  $numColumns * 1 ,		# // => number of colums in legend table
		labelBoxBorderColor =>  '#ccc', # // => border color for the little label boxes
		container =>  $labelArea,	# // => container (as jQuery object) to put legend in, null means default on top of graph
		containerName => 'containerName',
		position =>  'ne',		# // => position of default legend container within plot
		margin =>  5,		# // => distance from grid edge to default legend container within plot
		backgroundColor=> '#3A84AA',
		backgroundOpacity =>  0.15	# // => set to 0 to avoid background, set to 1 for a solid background
	},
	xAxis =>  {
			categories =>  \@xaxis

	},
	yAxis =>  {
		n0categories => ['Low', 'In Range', 'High'],
		min => 0,
		max => 100,
		title => {
			text => 'Minimal . . . . . Moderate . . . . . Severe'
		},
		plotLines => [{
				value => 0,
				width => 1,
				color => '#808080'
			}]	
		},
	tooltip => { formatter => q{ function() {    return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y +'';	}					}	},	
	title => {text => join(', ', @chart_title)},
	series => \@series,
	 patientID => $patientid,
	 
	RxN =>  $RxNumber

	};
	return $options;
	
	
	}
	
	
	sub getLabOrders {
	  	    my $dbh=shift;
			my $self=shift;
			my $patientID=$self->{patientID} || $self->{patientid};
			
			my $labs=$dbh->selectall_arrayref(q{
			(SELECT RxNumber, DrugName, DatePrescribed, DateFilled FROM prescriptions p WHERE patientID=? AND ( DrugName LIKE 'LAB-%' OR DrugCode LIKE 'RR%') AND Voided='NO'
				) UNION (
			SELECT  RxNumber, DrugName, DatePrescribed, NULL FROM prescriptions_unfilled p WHERE patientID=? AND ( DrugName LIKE 'LAB-%' OR DrugCode LIKE 'RR%') AND Voided='NO'
			)
		ORDER BY DatePrescribed DESC}, {Columns => {}}, ($patientID, $patientID));

		return $labs;
	
	
	}
	
		
	
  	sub getAddresses {
  	    my $dbh=shift;
	
  					my $self=shift;	
  					my $opts=$self->{opts};
  					my $patientID = $self->{patientID} || $self->{patientid};
  					my $adds = $dbh->selectall_arrayref(q{SELECT a.* FROM addresses a WHERE a.p_id = ? AND a_status NOT LIKE 'dele%'}, {Columns=>{}}, ($patientID));
  					for my $add (@$adds) {
  						$add->{dates} = $dbh->selectall_arrayref(q{SELECT *, DATE_FORMAT( start_date, '%m/%d/%y') AS start_date_f, DATE_FORMAT(stop_date , '%m/%d/%y') AS stop_date_f FROM addressDates WHERE adr_id=?}, {Columns => {}}, ($add->{id}));
	  					}
	  					return $adds;
  					}

	
	
	
	
	
	
	
	
	
	
	
	
	
  	1;
  }