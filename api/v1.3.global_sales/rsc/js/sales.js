/*jshint sub:true*/
//window.MHIPAA = true;
window.MHIPAA = true;
window.SHIPAA = true;
window.SDEBUG = false;
window.SFAXES = true;
window.ROAR = false;
window.demo_mode = false;

var scroll2 = function(target) {
	if (SDEBUG) {
		console.log('scroll2(): called with target: ', target);
	}
	var os = $(target).offset();
	$("html,body").animate({scrollTop: os.top-70, scrollLeft: os.left});
	//$.scrollTo(target, 600, {offset: -70, onAfter: function() {console.log('callback called');}});
};

var isBlackListed = function(user) {
	var blUsers = [
		'skim'
	];
	
	var u = user.toLowerCase();
	return false;
};

var dumpRDJSON = function() {
		console.log('Dumping RD JSON; You will need to refresh the browser after this.');
		rd.sales_data.ui = {};
		console.log(JSON.stringify(rd));
		console.log('Please refresh your browser.');
	};

var getStatusClass = function(idx) {
		if (rd && _.has(rd.bs_status_labels, rd.sales_data.rxData[idx].rx_status.split(':')[0])) {
			return rd.bs_status_labels[rd.sales_data.rxData[idx].rx_status.split(':')[0]].bsclass;
		} else {
			return 'default';
		}
	};

var clog = function(msg) {
		console.log(msg);
	};

// These are my utility functions for the dashboard
_.mixin({
	sum: function(array) {
		var result = 0;
		for (i = 0; i < array.length; i++) {
			if (array[i] === null || typeof(array[i]) === undefined || array[i] === undefined) {
				array[i] = 0;
			}
			if (_.isNumber(array[i]) && !_.isNaN(array[i])) {
				result += array[i];
			} else if (_.isNumber(parseInt(array[i], 10) && !_.isNaN(parseInt(array[i], 10)))) {
				result += parseInt(array[i], 10);
			}
		}
		return result;
	},
	mean: function(array, precision) {
		if (precision === 0) {
			return (this.sum(array) / array.length).toFixed(precision);
		} else if (!precision || !_.isNumber(precision) || _.isNaN(precision)) {
			return this.sum(array) / array.length;
		} else {
			return (this.sum(array) / array.length).toFixed(precision);
		}
	},
	toCurrency: function(val, symbol) {
		var pre = (!symbol) ? '$' : symbol;
		if (_.isNumber(val)) {
			return pre + val.toFixed(2);
		} else if (_.isString(val)) {
			return pre + parseFloat(val).toFixed(2);
		} else {
			return pre + '0.00';
		}
	},
	safari: function() {
		return !!navigator.userAgent.match(/Safari/i);
	},
	ipad: function() {
		return !!navigator.userAgent.match(/iPad/i);
	},
	iphone: function() {
		return !!navigator.userAgent.match(/iPhone/i);
	},
	ipod: function() {
		return !!navigator.userAgent.match(/iPod/i);
	},
	appleios: function() {
		return (this.ipad() || this.ipod() || this.iphone());
	},
	mapsURL: function(address) {
		var prefix = (this.appleios()) ? 'http://maps.apple.com/?daddr=' : 'http://maps.google.com/maps?daddr=';
		return prefix + address.split(' ').join('+');
	},
	resetDTFilters: function(dtable) {
		var oSettings = dtable.fnSettings();
		_.each(oSettings.aoPreSearchCols, function(col) {
			col.sSearch = '';
		});
		dtable.fnDraw();
	},
	toYMD: function(date) {
		var d = date.getUTCDate();
		var m = date.getUTCMonth() + 1;
		var y = date.getUTCFullYear();
		return '' + y + '-' + (m <= 9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
	},
	uri2Blob: function(dataURI) {
		// convert base64 to raw binary data held in a string
		// doesn't handle URLEncoded DataURIs
		var byteString;
		if (dataURI.split(',')[0].indexOf('base64') >= 0) {
			byteString = atob(dataURI.split(',')[1]);
		} else {
			byteString = unescape(dataURI.split(',')[1]);
		}
		// separate out the mime component
		var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
		
		// write the bytes of the string to an ArrayBuffer
		var ab = new ArrayBuffer(byteString.length);
		var ia = new Uint8Array(ab);
		for (var i = 0; i < byteString.length; i++) {
			ia[i] = byteString.charCodeAt(i);
		}
		
		// write the ArrayBuffer to a blob, and you're done
		return new Blob([ab],{type: mimeString});
	},
	arrayPluck: function(ary, idx) {
		if (typeof(idx) != 'number') {
			return false;
		}

		var tmp = [];

		for (u = 0; u < _.size(ary); u++) {
			tmp.push(ary[u][idx]);
		}
		return tmp;
	}
});

var parseCommissionableData = function(filter_rep_id) {
	if (SDEBUG) {
		console.log('parseCommissionableData(): Called with filter_rep_id = ', filter_rep_id);
		console.log('parseCommissionableData(): typeof(filter_rep_id): ', typeof(filter_rep_id));
	}
	// commissionableSales
	// range items: DateFilled, price, rx_remaining_refills

	if (SDEBUG) {
		console.log('start: ', rd.sales_data.dates.rangeStartMillis);
		console.log('end:   ', rd.sales_data.dates.rangeEndMillis);
	}
	var crtmp = null;
	var initialSet = [];
	var finalSetO = {};
	var finalSetA = [];
	var sumToday = 0;
	var sumYesterday = 0;
	var sumRange = 0;
	var dates = [];
	var repFilter = false;

	if (typeof(filter_rep_id) == 'number') {
		repFilter = filter_rep_id;
	} else if (typeof(filter_rep_id) == 'string' && !_.isNaN(parseInt(filter_rep_id, 10))) {
		repFilter = parseInt(filter_rep_id, 10);
	}

	// Reduce our working set
	if (SDEBUG) {
		console.log('parseCommissionableData(): Building highcharts data');
		console.log('parseCommissionableData(): - Creating working set');
		console.log('parseCommissionableData(): repFilter = ', repFilter);
	}
	if (SDEBUG) {
		rd.sales_data.debug = {
			graphData: {
				filtered: [],
				unfiltered: []
			},
			range: {
				start: {
					string: "",
					millis: 0
				},
				end: {
					string: "",
					millis: 0
				}
			}
		};

//			rd.sales_data.debug.range.start.millis = rangeStartMillis;
//			rd.sales_data.debug.range.end.string = rd.sales_data.date_searched.end;
//			rd.sales_data.debug.range.end.millis = rangeEndMillis;
	}

	_.each(rd.sales_data.rxData, function(data) {
		crtmp = _.pick(data, 'DateFilled', 'projected_refill_amt', 'rx_status', 'rep_id');
		if (crtmp.DateFilled && parseInt(Date.parse(crtmp.DateFilled), 10) >= rd.sales_data.dates.rangeStartMillis && parseInt(Date.parse(crtmp.DateFilled), 10) <= rd.sales_data.dates.rangeEndMillis && crtmp.rx_status && crtmp.rx_status.match(/shipped/i) && crtmp.rep_id) {
			if (repFilter === false) {
				initialSet.push(crtmp);
			} else {
				if (crtmp.rep_id == repFilter) {
					initialSet.push(crtmp);
				}
			}
		}
	});

	var month = {
		1:  "Jan ",
		2:  "Feb ",
		3:  "Mar ",
		4:  "Apr ",
		5:  "May ",
		6:  "Jun ",
		7:  "Jul ",
		8:  "Aug ",
		9:  "Sep ",
		10: "Oct ",
		11:	"Nov ",
		12: "Dec "
	};

	if (repFilter !== false) {
		if (_.has(rd.sales_data.sub_accts, repFilter)) {
			var dates = _.keys(rd.sales_data.sub_accts[repFilter].data_totals);
			dates = dates.sort();
			_.each(dates, function(date) {
				var tmp = date.split('-');
				var lbl = month[parseInt(tmp[1])] + parseInt(tmp[2]);
				finalSetA.push([lbl, rd.sales_data.sub_accts[repFilter].data_totals[date]]);

				if (date === rd.sales_data.dates.today) {
					sumToday = rd.sales_data.data_totals[date];
				} else if (date === rd.sales_data.dates.yesterday) {
					sumYesterday = rd.sales_data.data_totals[date];
				}
			});
			
			sumRange = _.sum(_.values(rd.sales_data.sub_accts[repFilter].data_totals));
		} else {
			finalSetA = [];
		}
	} else {
		var dates = rd.sales_data.date_searched.list.sort();
		_.each(dates, function(date) {
			var tmp = date.split('-');
			var lbl = month[parseInt(tmp[1])] + parseInt(tmp[2]);
			finalSetA.push([lbl, rd.sales_data.data_totals[date]]);
			if (date === rd.sales_data.dates.today) {
				sumToday = rd.sales_data.data_totals[date];
			} else if (date === rd.sales_data.dates.yesterday) {
				sumYesterday = rd.sales_data.data_totals[date];
			}
		});
		
		sumRange = _.sum(_.values(rd.sales_data.data_totals));
	}

	if (SDEBUG) console.log('finalSetA():', finalSetA);

	if (SDEBUG) {
		console.log('################################################################');
		console.log('##  parseCommissionableData(): - finalSetA: ', finalSetA);
		console.log('################################################################');
	}
	
	rd.sales_data.commissionableSales = {};
	rd.sales_data.commissionableSales['sum'] = sumRange;
	rd.sales_data.commissionableSales['today'] = sumToday;
	rd.sales_data.commissionableSales['yesterday'] = sumYesterday;
	rd.sales_data.commissionableSales['refills'] = _.sum(_.pluck(initialSet, 'projected_refill_amt'));
	
	// Graph Setup
	if (SDEBUG) {
		console.log('parseCommissionableData(): inserting Graph Data');
	}
	rd.sales_data.ui.chartOptions.series[0].data = finalSetA;
	
	_.defer(function() {
		if (SDEBUG) console.log('################################## ', rd.sales_data.ui.chartOptions.series[0].data);
		rd.sales_data.ui.commissionableSalesChart = $('#fancyChart').highcharts(rd.sales_data.ui.chartOptions);
	});
};

var redrawGraph = function(filter_rep_id) {
		if (SDEBUG) console.log('redrawGraph(): Called with filter_rep_id = ', filter_rep_id);
		var repFilter = false;
		if (typeof(filter_rep_id) == 'number') {
			repFilter = filter_rep_id;
		} else if (typeof(filter_rep_id) == 'string' && !_.isNaN(parseInt(filter_rep_id, 10))) {
			repFilter = parseInt(filter_rep_id, 10);
		}

		rd.sales_data.ui.chartOptions = {
			chart: {
				type: 'column',
				borderRadius: 0,
				borderWidth: 0
			},
			title: {
				text: 'Commissionable Sales'
			},
			subtitle: {
				text: ''
			},
			xAxis: {
				type: "category",
				showEmpty: true,
				labels: {
					align: 'right',
					rotation: -70
				}
			},
			yAxis: {
				title: {
					text: 'Daily Sales'
				},
				labels: {
					formatter: function() {
						return '$' + this.value;
					}
				}
			},
			tooltip: {
				pointFormat: '<b>${point.y:,.2f}</b>'
			},
			series: [{
				data: [],
				name: 'Daily Commissionable Sales'
			}]
		};
		if (SDEBUG) {
			console.log('redrawGraph(): base rd.sales_data.ui.chartOptions set = ', rd.sales_data.ui.chartOptions);
			console.log('redrawGraph(): calling parseCommissionableData with filter: ', filter_rep_id);
		}
		if (repFilter !== false) {
			parseCommissionableData(repFilter);
		} else {
			parseCommissionableData();
		}
	};

/*
 * data = the rx dataset
 * opts = {
 *     title: 'Page Title',
 *     subtitle: 'Page Subtitle'
 * }
 */
var initDashboard = function(data) {
	rd.sales_data.dates = {};
	var tmp_today = new Date();
	rd.sales_data.dates.today = tmp_today.toJSON().slice(0,10);
	var tmpsplit = rd.sales_data.dates.today.split('-');
	rd.sales_data.dates.yesterday = tmpsplit[0] + '-' + tmpsplit[1] + '-' + ((parseInt(tmpsplit[2])-1 > 9)?parseInt(tmpsplit[2])-1:'0'+(parseInt(tmpsplit[2])-1));

	rd.sales_data.dates.todayMillis = parseInt(Date.parse(rd.sales_data.dates.today), 10);
//	console.log(rd.sales_data.dates.todayMillis);
	
	rd.sales_data.dates.yesterdayMillis = rd.sales_data.dates.todayMillis - (24 * 3600 * 1000);
//	console.log('todayMillies - 1 day: ', rd.sales_data.dates.yesterdayMillis);
	
	rd.sales_data.dates.rangeStartMillis = parseInt(Date.parse(rd.sales_data.date_searched.start), 10);
	rd.sales_data.dates.rangeEndMillis   = parseInt(Date.parse(rd.sales_data.date_searched.end),   10);

	rd.sales_data.actionItems = [];
	rd.sales_data.recentActivityItems = [];
	
	if (!rd.sales_data.ui || typeof(rd.sales_data.ui) != 'Object') {
		rd.sales_data['ui'] = {};
	}
	
	rd.sales_data['ui']['chartOptions'] = {};
	
	if (SDEBUG) {
		console.log('initDashboard(): pushing filtered rxData indexes to rd.sales_data.actionItems and recentActivityItems');
	}
	
	_.each(data.rxData, function(data, idx) {
		if (data.rx_status.match(/rep-asst/i)) { //			//			// Action Items
			rd.sales_data.actionItems.push({
				idx: idx
			});
		} else if (data.rx_status.match(/filled/i)) { //			//			// recentActivityItems
			rd.sales_data.recentActivityItems.push({
				idx: idx
			});
		}
	});
	if (SDEBUG) console.log('initDashboard(): Calling redrawGraph()');
	redrawGraph();
	
	// Prepare and instantiate the templates
	if (SDEBUG) {
		console.log('initDashboard(): Calling the templates');
	}
	_.defer(function() {
		dashCallTemplates();
	});
};

var dashCallTemplates = function() {
	if (SDEBUG) {
		console.log('dashCallTemplates(): Called');
	}
	var salesDashTemplate = _.template($('#tmpl_prescriber_list_dashboard').html());
	$('#bootstrap_area').html(salesDashTemplate(rd.sales_data));
	if (SDEBUG) {
		console.log('dashCallTemplates(): Calling the partials');
	}
	partial_navbar_top();
	partial_page_header();
	partial_dash_today();
//	partial_dash_reports();
	partial_rep_list();
	partial_new_rep_list();
	partial_commissionable_sales();
	partial_commissions_data();
	partial_action_items();
//	if (user_name && user_name.match(/mwong/i)) partial_pending_faxes();
	partial_pending_faxes();
	partial_rx_activity();
	partial_commission_report();
	partial_daily_report();
};

var partial_navbar_top = function() {
	if (SDEBUG) {
		console.log('partial_navbar_top(): Called');
	}
	var partialNavbarTop = _.template($('#tmpl_dash_navbar_top').html());
	$('#dash-navbar-top').html(partialNavbarTop()); // This partial does not yet require data
};

var partial_page_header = function() {
	if (SDEBUG) {
		console.log('partial_page_header(): Called');
	}
	var partialPageHeader = _.template($('#tmpl_sales_dash_top').html());
	$('#sales_dash_top').html(partialPageHeader());
};

var partial_dash_today = function() {
	if (SDEBUG) {
		console.log('partial_dash_today(): Called');
	}
	var partialDashToday = _.template($('#tmpl_panel_today').html());
	$('#panelToday').html(partialDashToday());
};

var partial_dash_reports = function() {
	if (SDEBUG) {
		console.log('partial_dash_reports(): Called');
	}
	var partialDashReports = _.template($('#tmpl_dash_reports').html());
	$('#dash-reports-list').html(partialDashReports());
};

// sls-new-rep-list-table
var partial_new_rep_list = function() {
	if (SDEBUG) {
		console.log('partial_new_rep_list(): Called');
	}
	var doNotCallMyFunction = function() {
		var downlineData = {
			"aoColumnDefs": [
				{ 'aTargets': [ 0 ], 'sDefaultContent':'' },
				{ 'aTargets': [ 1 ], 'sDefaultContent':'' },
				{ 'aTargets': [ 2 ], 'sDefaultContent':'' },
				{ 'aTargets': [ 3 ], 'sDefaultContent':'' },
				{ 'aTargets': [ 4 ], 'sDefaultContent':'' }/*,
				{ 'aTargets': [ 5 ], 'sDefaultContent':'', 'bVisible':false },
				{ 'aTargets': [ 6 ], 'sDefaultContent':'', 'bVisible':false } */
			],
			'aaSorting': [[2,'desc']],
			'aaData': [],
			'fnRowCallback': function(nRow, aData, iDisplayIndex) {
				return nRow;
			}
		};
		
		if (!demo_mode) {
			_.each(rd.sales_data.rep_list, function(rep, key) {
				if (user_name.toLowerCase() != rd.sales_data.rep_list[key].toLowerCase()) {
					if (SDEBUG) console.log('rep: ', rep, ' key: ', key);
	
					var tmp = [
						rep,
						(_.has(rd.sales_data.rep_stats, key) && rd.sales_data.rep_stats[key].comm_sales )?'$' + rd.sales_data.rep_stats[key].comm_sales:"$0.00",
						(_.has(rd.sales_data.rep_stats, key) && rd.sales_data.rep_stats[key].commission_total )?'$' + rd.sales_data.rep_stats[key].commission_total:"$0.00",
						'<button class="btn btn-primary btn-xs func_popup_rep_stats" role="button" data-repid="' + key + '">Stats</button>',
						'<button class="btn btn-xs btn-default" role="button" data-name="gotoFilter" data-filter="' + key + '" data-filterby="rep_id">View</button>'/*,
						(rd.sales_data.reg_group_info[rep])?((rd.sales_data.reg_group_info[rep].length >= 1)?rd.sales_data.reg_group_info[rep][rd.sales_data.reg_group_info[rep].length-1]:'hzulaica'):'hzulaica',
						(rd.sales_data.reg_group_info[rep])?((rd.sales_data.reg_group_info[rep].length >= 2)?rd.sales_data.reg_group_info[rep][rd.sales_data.reg_group_info[rep].length-2]:'hzulaica'):'hzulaica' */
					];
	
					if (SDEBUG) console.log('up1: ', tmp[5], ' up2: ', tmp[6]);
	
					if (SDEBUG) console.log(tmp);
					
					downlineData.aaData.push(tmp);
				}
			});
		} else {
			_.each(rd.sales_data.rep_list, function(rep, key) {
				if (user_name.toLowerCase() != rd.sales_data.rep_list[key].toLowerCase()) {
					var tmp = [
						"Sales Rep " + key,
						(_.has(rd.sales_data.rep_stats, key) && rd.sales_data.rep_stats[key].comm_sales )?'$' + rd.sales_data.rep_stats[key].comm_sales:"$0.00",
						(_.has(rd.sales_data.rep_stats, key) && rd.sales_data.rep_stats[key].commission_total )?'$' + rd.sales_data.rep_stats[key].commission_total:"$0.00",
						'<button class="btn btn-primary btn-xs func_popup_rep_stats" role="button" data-repid="' + key + '">Stats</button>',
						'<button class="btn btn-xs btn-default" role="button" data-name="gotoFilter" data-filter="' + key + '" data-filterby="rep_id">View</button>'/*,
						(rd.sales_data.reg_group_info[rep])?((rd.sales_data.reg_group_info[rep].length >= 1)?rd.sales_data.reg_group_info[rep][rd.sales_data.reg_group_info[rep].length-1]:'hzulaica'):'hzulaica',
	                    (rd.sales_data.reg_group_info[rep])?((rd.sales_data.reg_group_info[rep].length >= 2)?rd.sales_data.reg_group_info[rep][rd.sales_data.reg_group_info[rep].length-2]:'hzulaica'):'hzulaica' */
					];
					downlineData.aaData.push(tmp);
				}
			});
		}
		
		console.log('this is doing something');
		
		rd.sales_data.ui.rep_list_data = downlineData.aaData;
		
		var partialRepList = _.template($('#tmpl_rep_list').html());
		$('#putRowsHere').html(partialRepList());
		
		/*rd.sales_data.ui.downlineTable = $('#downline-table').dataTable(downlineData);/*.rowGrouping({
			'iGroupingColumnIndex': 6,
			'iGroupingColumnIndex2': 5,
			'sGroupingColumnSortDirection': 'asc',
			'sGroupingOrderByColumnIndex': 2,
			'sGroupLabelPrefix2': '&mdash; ',
			'fnGroupLabelFormat': function(label) { return "<strong>" + label + "</strong>"; },
			'fnGroupLabelFormat2': function(label) { return "<strong>" + label + "</strong>"; }
		}); */
	};
	
	var partialULRepList = _.template($('#tmpl_rep_list').html());
	$('#dash-new-reps-list').html(partialULRepList());
};

var partial_rep_list = function() {
	if (SDEBUG) {
		console.log('partial_rep_list(): Called');
	}
	
	//$('#dash-reps-list').html(recurseReps());
	
	// Moved here from the timeout in that other file.
	fire_rl();

	$('.tree').treegrid({
		expanderExpandedClass: 'glyphicon glyphicon-minus',
		expanderCollapsedClass: 'glyphicon glyphicon-plus',
		initialState: 'collapsed'
	});

};

var recurseReps = function () {
	var parentIdx = 1;
	var rowCount = 1;
	var html = "";

	var recurse = function(rec_rep) {
		var dl_list = [];

		var l_grp = parseInt(rd.sales_data.rep_group_list[rec_rep].l_grp);
		var r_grp = parseInt(rd.sales_data.rep_group_list[rec_rep].r_grp);
		var rep_id = rd.sales_data.rep_group_list[rec_rep].rep_id;
//		console.log('this rep: ', {rep: rec_rep, id: rep_id, left: l_grp, right: r_grp});

		for (i = 1 + l_grp; i < r_grp; i++) {
			if (_.has(rd.sales_data.rep_group_inverse, i)) {
				dl_list.push(rd.sales_data.rep_group_inverse[i].rep_id);
			}
		}

		if (dl_list.length > 0) {
			if (!rec_rep.match(RegExp(user_name, 'i'))) {
				html += '<tr class="treegrid-' + rowCount + ' treegrid-parent-' + parentIdx + '"><td>' + rec_rep + '</td><td class="text-right">';
				if (_.has(rd.sales_data.rep_stats, rep_id) && rd.sales_data.rep_stats[rep_id].comm_sales) {
					html += '$' + rd.sales_data.rep_stats[rep_id].comm_sales;
				} else {
					html += "$0.00";
				}
				html += '</td><td class="text-right">$10,000</td><td class="text-center">Stats</td><td class="text-center">View</td></tr>';

				parentIdx = rowCount;
				rowCount++;
			}

			dl_list.forEach(function (rid, index, array) {
				recurse(rd.sales_data.rep_list[rid]);
			});
		} else {
			if (!rec_rep.match(RegExp(user_name, 'i'))) {
				html += '<tr class="treegrid-' + rowCount + ' treegrid-parent-' + parentIdx + '"><td>' + rec_rep + '</td><td class="text-right">';
				if (rd.sales_data.rep_stats[rep_id] && rd.sales_data.rep_stats[rep_id].comm_sales) {
					html += '$' + rd.sales_data.rep_stats[rep_id].comm_sales;
				} else {
					html += '$0.00';
				}
				html += '</td><td class="text-right">$10,000</td><td class="text-center">Stats</td><td class="text-center">View</td></tr>';

				rowCount++;
			}
		}
	};

//	recurse(user_name);
//fire_rl();
	return html;
};

var parseCommissionsData = function(filter_rep_id) {
	if (SDEBUG) {
		console.log('parseCommissionableData(): Called with filter_rep_id = ', filter_rep_id);
		console.log('parseCommissionableData(): typeof(filter_rep_id): ', typeof(filter_rep_id));
	}
	// commissionableSales
	// range items: DateFilled, price, rx_remaining_refills

	if (SDEBUG) {
		console.log('start: ', rd.sales_data.dates.rangeStartMillis);
		console.log('end:   ', rd.sales_data.dates.rangeEndMillis);
	}
	var crtmp = null;
	var initialSet = [];
	var finalSetO = {};
	var finalSetA = [];
	var sumToday = 0;
	var sumYesterday = 0;
	var sumRange = 0;
	var dates = [];
	var repFilter = false;
	
	rd.sales_data.ui.commChartOptions = {
		chart: {
			type: 'column',
			borderRadius: 0,
			borderWidth: 0
		},
		title: {
			text: 'Commissions'
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			type: "category",
			showEmpty: true,
			labels: {
				align: 'right',
				rotation: -70
			}
		},
		yAxis: {
			title: {
				text: 'Daily Commissions'
			},
			labels: {
				formatter: function() {
					return '$' + this.value;
				}
			}
		},
		tooltip: {
			pointFormat: '<b>${point.y:,.2f}</b>'
		},
		series: [{
			data: [],
			name: 'Daily Commissions'
		}]
	};

	if (typeof(filter_rep_id) == 'number') {
		repFilter = filter_rep_id;
	} else if (typeof(filter_rep_id) == 'string' && !_.isNaN(parseInt(filter_rep_id, 10))) {
		repFilter = parseInt(filter_rep_id, 10);
	}

	// Reduce our working set
	if (SDEBUG) {
		console.log('parseCommissionsData(): Building highcharts data');
		console.log('parseCommissionsData(): - Creating working set');
		console.log('parseCommissionsData(): repFilter = ', repFilter);
	}
	if (SDEBUG) {
//			rd.sales_data.debug.range.start.millis = rangeStartMillis;
//			rd.sales_data.debug.range.end.string = rd.sales_data.date_searched.end;
//			rd.sales_data.debug.range.end.millis = rangeEndMillis;
	}

	var month = {
		1:  "Jan ",
		2:  "Feb ",
		3:  "Mar ",
		4:  "Apr ",
		5:  "May ",
		6:  "Jun ",
		7:  "Jul ",
		8:  "Aug ",
		9:  "Sep ",
		10: "Oct ",
		11:	"Nov ",
		12: "Dec "
	};

	if (repFilter !== false) {
		if (_.has(rd.sales_data.sub_accts, repFilter)) {
			var dates = _.keys(rd.sales_data.sub_accts[repFilter].data_totals);
			dates = dates.sort();
			_.each(dates, function(date) {
				var tmp = date.split('-');
				var lbl = month[parseInt(tmp[1])] + parseInt(tmp[2]);
				finalSetA.push([lbl, rd.sales_data.sub_accts[repFilter].commission_totals[date] || 0]);
			});
		} else {
			finalSetA = [];
		}
	} else {
		var dates = rd.sales_data.date_searched.list.sort();
		_.each(dates, function(date) {
			var tmp = date.split('-');
			var lbl = month[parseInt(tmp[1])] + parseInt(tmp[2]);
			if (rd.sales_data.commission_totals) {
				finalSetA.push([lbl, rd.sales_data.commission_totals[date] || 0]);
			}
		});
	}

	if (SDEBUG) console.log('finalSetA():', finalSetA);

	if (SDEBUG) {
		console.log('################################################################');
		console.log('##  parseCommissionsData(): - finalSetA: ', finalSetA);
		console.log('################################################################');
	}
	
//	rd.sales_data.commissionableSales = {};
//	rd.sales_data.commissionableSales['sum'] = sumRange;
//	rd.sales_data.commissionableSales['today'] = sumToday;
//	rd.sales_data.commissionableSales['yesterday'] = sumYesterday;
//	rd.sales_data.commissionableSales['refills'] = _.sum(_.pluck(initialSet, 'projected_refill_amt'));
	
	// Graph Setup
	if (SDEBUG) {
		console.log('parseCommissionsData(): inserting Graph Data');
	}
	rd.sales_data.ui.commChartOptions.series[0].data = finalSetA;
	
	_.defer(function() {
		if (SDEBUG) console.log('################################## ', rd.sales_data.ui.commChartOptions.series[0].data);
		rd.sales_data.ui.commissionsDataChart = $('#commissionsChart').highcharts(rd.sales_data.ui.commChartOptions);
	});
};

var redrawGraphComm = function(filter_rep_id) {
	console.log('redrawGraphComm(): Called with filter_rep_id = ', filter_rep_id);
	var repFilter = false;
	if (typeof(filter_rep_id) == 'number') {
		repFilter = filter_rep_id;
	} else if (typeof(filter_rep_id) == 'string' && !_.isNaN(parseInt(filter_rep_id, 10))) {
		repFilter = parseInt(filter_rep_id, 10);
	}

	rd.sales_data.ui.commChartOptions = {
		chart: {
			type: 'column',
			borderRadius: 0,
			borderWidth: 0
		},
		title: {
			text: 'Commissions'
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			type: "category",
			showEmpty: true,
			labels: {
				align: 'right',
				rotation: -70
			}
		},
		yAxis: {
			title: {
				text: 'Daily Commissions'
			},
			labels: {
				formatter: function() {
					return '$' + this.value;
				}
			}
		},
		tooltip: {
			pointFormat: '<b>${point.y:,.2f}</b>'
		},
		series: [{
			data: [],
			name: 'Daily Commissions'
		}]
	};
	
	if (SDEBUG) {
		console.log('redrawGraphComm(): base rd.sales_data.ui.commChartOptions set = ', rd.sales_data.ui.commChartOptions);
		console.log('redrawGraphComm(): calling parseCommissionsData with filter: ', filter_rep_id);
	}
	if (repFilter !== false) {
		parseCommissionsData(repFilter);
	} else {
		parseCommissionsData();
	}
};

var partial_commissions_data = function(filter) {
	if (SDEBUG) {
		console.log('partial_commissions_data(): Called');
		console.log('partial_commissions_data(): filter = ', filter);
	}
	var repFilter = false;
	if (typeof(filter) == 'number') {
		repFilter = filter;
	} else if (typeof(filter) == 'string' && !_.isNaN(parseInt(filter, 10))) {
		repFilter = parseInt(filter, 10);
	}
	if (repFilter !== false) {
		parseCommissionsData(repFilter);
	} else {
		parseCommissionsData();
	}
//	var partialCommissionsData = _.template($('#tmpl_commissions_data').html());
//	$('#tmpl_commissions_data').html(partialCommissionsData());
	if (rd.sales_data.ui.commissionsDataChart) {
		redrawGraphComm(filter);
	}
};

var partial_commission_report = function() {
	var tmp_tmpl = _.template($('#tmpl_distributor_commission_report').html());
	$('#dash-commissions-report').html(tmp_tmpl(rd.sales_data.commission_report_data));
};

var partial_commissionable_sales = function(filter) {
	if (SDEBUG) {
		console.log('partial_commissionable_sales(): Called');
		console.log('partial_commissionable_sales(): filter = ', filter);
	}
	var repFilter = false;
	if (typeof(filter) == 'number') {
		repFilter = filter;
	} else if (typeof(filter) == 'string' && !_.isNaN(parseInt(filter, 10))) {
		repFilter = parseInt(filter, 10);
	}
	if (repFilter !== false) {
		parseCommissionableData(repFilter);
	} else {
		parseCommissionableData();
	}
	var partialCommissionableSales = _.template($('#tmpl_commissionable_sales').html());
	$('#dash-commissionable-sales').html(partialCommissionableSales());
	if (rd.sales_data.ui.commissionableSalesChart) {
		redrawGraph(filter);
	}
};

var partial_daily_report = function() {
	rd.sales_data.qtyByDate = {};
/*
	_.each(rd.sales_data.rxData, function(rx) {
	if (rx.date_entered != null) {
    var tdate = rx.date_entered.substring(0, 10);
    if (typeof rd.sales_data.qtyByDate[tdate] == "undefined") {
      rd.sales_data.qtyByDate[tdate] = {sales: 0, qtyFilled:1, fills: {new:0, refill: 0, total: 0}};
    } else {
      rd.sales_data.qtyByDate[tdate].qtyFilled++;
    }
    }
  });
  */
	_.each(rd.sales_data.rxData, function(rx) {
		//
		// Rxs Entered
		//
		if (rx.date_entered != null && !rx.date_entered.match(/0000-00-00/)) {
			var tdate = rx.date_entered.substring(0, 10);
			if (typeof rd.sales_data.qtyByDate[tdate] == "undefined") {
				rd.sales_data.qtyByDate[tdate] = {sales: 0, qtyFilled:1, fills: {new:0, refill: 0, total: 0}};
			} else {
				rd.sales_data.qtyByDate[tdate].qtyFilled++;
			}
		}
		
		//
		// Rx New, Rx Refills, Rx Total
		if (rx.rx_status.match(/ship/i)) {
			var tdate = rx.LDU.substring(0, 10);
			if (typeof rd.sales_data.qtyByDate[tdate] == "undefined") {
				if (parseInt(rx.fill_num) === 0) {
					rd.sales_data.qtyByDate[tdate] = {sales: 0, qtyFilled:0,fills: {new:1, refill:0, total: 1}};
				} else {
					rd.sales_data.qtyByDate[tdate] = {sales: 0, qtyFilled:0, fills: {new:0, refill: 1, total: 1}};
				}
			} else {
				if (parseInt(rx.fill_num) === 0) {
					rd.sales_data.qtyByDate[tdate].fills.new++;
				} else {
					rd.sales_data.qtyByDate[tdate].fills.refill++;
				}
				rd.sales_data.qtyByDate[tdate].fills.total++;
			}
		}
	});
  
	//
	// Comm Sales
	//
	_.each(rd.sales_data.date_searched.list, function(date) {
		if (rd.sales_data.commission_totals && typeof rd.sales_data.commission_totals[date] != "undefined") {
			if (typeof rd.sales_data.qtyByDate[date] == "undefined") {
				rd.sales_data.qtyByDate[date] = {sales: rd.sales_data.commission_totals[date], qtyFilled:0, fills: {new:0, refill:0, total: 0}};
			} else {
				rd.sales_data.qtyByDate[date].sales = rd.sales_data.commission_totals[date];
			}
		}
	});
  
  var dailyReportData = {
	  "aoColumnDefs": [
	  	{ 'aTargets': [ 0 ], 'sDefaultContent':'&mdash;', 'sTitle': 'Date', 'sType': 'date' },
	  	{ 'aTargets': [ 1 ], 'sDefaultContent':'&mdash;', 'sTitle': 'Rxs Entered' },
	  	{ 'aTargets': [ 2 ], 'sDefaultContent':'&mdash;', 'sTitle': 'Comm Sales' },
	  	{ 'aTargets': [ 3 ], 'sDefaultContent':'&mdash;', 'sTitle': 'Rx New' },
	  	{ 'aTargets': [ 4 ], 'sDefaultContent':'&mdash;', 'sTitle': 'Rx Refills' },
	  	{ 'aTargets': [ 5 ], 'sDefaultContent':'&mdash;', 'sTitle': 'Rx Total' }
	  ],
	  'aaSorting': [[0, 'desc']],
	  'aaData':[]
  };
  
  _.each(rd.sales_data.date_searched.list.sort(), function(date) {
  if (typeof rd.sales_data.qtyByDate[date] != 'undefined') {
  	dailyReportData.aaData.push([date, rd.sales_data.qtyByDate[date].qtyFilled, _.toCurrency(rd.sales_data.qtyByDate[date].sales), rd.sales_data.qtyByDate[date].fills.new, rd.sales_data.qtyByDate[date].fills.refill, rd.sales_data.qtyByDate[date].fills.total]);
  	}
  });
  
  
  
	var partialDailyReport = _.template($('#tmpl_daily_report').html());
	$('#dash-daily-report').html(partialDailyReport());
	
	rd.sales_data.ui['dailyReportTable'] = $('#dailyReportTable').dataTable(dailyReportData);
	rd.sales_data.ui.dailyReportTableData = dailyReportData.aaData;
  
  
};

var partial_action_items = function() {
	if (SDEBUG) {
		console.log('partial_action_items(): Called');
	}
	
	var actionItemsData = {
		"aoColumnDefs": [
			{ 'aTargets': [ 0 ], 'sDefaultContent':'', 'sTitle': 'Doctor' },
			{ 'aTargets': [ 1 ], 'sDefaultContent':'', 'sTitle': 'PT' },
			{ 'aTargets': [ 2 ], 'sDefaultContent':'', 'sTitle': 'RxN' },
			{ 'aTargets': [ 3 ], 'sDefaultContent':'', 'sTitle': 'Status' },
			{ 'aTargets': [ 4 ], 'sDefaultContent':'', 'sTitle': 'Updated' },
			{ 'aTargets': [ 5 ], 'sDefaultContent':'', 'sTitle': 'Rep' },
			
			{ 'aTargets': [ 6 ], 'sDefaultContent':'{}', 'bSearchable': false, 'bVisible': false }

		],
		'aaSorting': [[4,'desc']],
		'aaData': [],
		'fnRowCallback': function(nRow, aData, iDisplayIndex) {
			var jdata = JSON.parse(aData[6]);
			
			$(nRow).attr('data-rxidx', jdata.rxidx).addClass('dtorig');
			
			$('td:eq(3)', nRow).attr('data-rxn', jdata.rxn).attr('data-fill', jdata.fillnum);
			
			$('td:eq(5)', nRow).addClass('rep-' + jdata.rep_id + ' rep-' + rd.sales_data.rep_list[jdata.rep_id]).css({
				'color':'white',
				'font-weight':'bold',
				'letter-spacing':'0.5px'
			}).css('background-color', '#' + rd.rep_colors[_.indexOf(_.keys(rd.sales_data.rep_list), jdata.rep_id)] + ' !important');

			return nRow;
		},
		'fnDrawCallback': function(oSettings) {
			$('#actionItemsTable tr.dtorig').each(function() {
				var rxidx = $(this).data('rxidx');
				
				$(this).after('<tr><td colspan="6"><strong>Status Note: </strong>' + ((rd.sales_data.rxData[rxidx] && rd.sales_data.rxData[rxidx].rx_status_note) ? rd.sales_data.rxData[rxidx].rx_status_note : ((!demo_mode)?'No Rx Status Note to Display':'Demo Mode &mdash; Rx status note displays here.')) + '</td></tr>');
			});
		}
	};
	
	if (!demo_mode) {
		_.each(rd.sales_data.actionItems, function(item, idx) {
			var tmp = [
				rd.sales_data.rxData[item.idx].doctorName,
				rd.sales_data.rxData[item.idx].PT,
				rd.sales_data.rxData[item.idx].rxn,
				'<span class="' + rd.sales_data.rxData[item.idx].rx_status + ' label label-' + (_.has(rd.bs_status_labels, rd.sales_data.rxData[item.idx].rx_status)?rd.bs_status_labels[rd.sales_data.rxData[item.idx].rx_status].bsclass:"default") + '  func_show_status_note" data-rxid="' + item.idx + '">' + (_.has(rd.bs_status_labels, rd.sales_data.rxData[item.idx].rx_status)?rd.bs_status_labels[rd.sales_data.rxData[item.idx].rx_status].desc:rd.sales_data.rxData[item.idx].rx_status) + '</span>',
				rd.sales_data.rxData[item.idx].LDU,
				rd.sales_data.rep_list[rd.sales_data.rxData[item.idx].rep_id],
				'{"rxn":"' + rd.sales_data.rxData[item.idx].rxn + '", "rep_id":"' + rd.sales_data.rxData[item.idx].rep_id + '", "fillnum":"' + rd.sales_data.rxData[item.idx].fill_num + '", "rxidx":"' + item.idx +'"}'
			];
			
			if (SDEBUG) console.log(tmp);
			
			actionItemsData.aaData.push(tmp);
		});
	} else {
		_.each(rd.sales_data.actionItems, function(item, idx) {
			var tmp = [
				"Demo Mode",
				"Demo Mode",
				rd.sales_data.rxData[item.idx].rxn,
				'<span class="' + rd.sales_data.rxData[item.idx].rx_status + ' label label-' + (_.has(rd.bs_status_labels, rd.sales_data.rxData[item.idx].rx_status)?rd.bs_status_labels[rd.sales_data.rxData[item.idx].rx_status].bsclass:"default") + '  func_show_status_note" data-rxid="' + item.idx + '">' + (_.has(rd.bs_status_labels, rd.sales_data.rxData[item.idx].rx_status)?rd.bs_status_labels[rd.sales_data.rxData[item.idx].rx_status].desc:rd.sales_data.rxData[item.idx].rx_status) + '</span>',
				rd.sales_data.rxData[item.idx].LDU,
				"Sales Rep " + rd.sales_data.rxData[item.idx].rep_id,
				'{"rxn":"' + rd.sales_data.rxData[item.idx].rxn + '", "rep_id":"' + rd.sales_data.rxData[item.idx].rep_id + '", "fillnum":"' + rd.sales_data.rxData[item.idx].fill_num + '", "rxidx":"' + idx +'"}'
			];
			
			actionItemsData.aaData.push(tmp);
		});
	}
	
	var partialActionItems = _.template($('#tmpl_action_items').html());
	$('#dash-action-items').html(partialActionItems());
	
	rd.sales_data.ui['actionItemsTable'] = $('#actionItemsTable').dataTable(actionItemsData);
	rd.sales_data.ui.actionItemsTableData = actionItemsData.aaData;
};

var partial_pending_faxes = function() {
	if (SDEBUG) {
		console.log('partial_pending_faxes(): Called');
	}
	
	var pendingFaxesData = {
		"aoColumnDefs": [
			{ 'aTargets': [ 0 ], 'sDefaultContent':'', 'sTitle': 'Fax Date' },
			{ 'aTargets': [ 1 ], 'sDefaultContent':'', 'sTitle': 'From (Fax)' },
			{ 'aTargets': [ 2 ], 'sDefaultContent':'', 'sTitle': 'Page' },
			{ 'aTargets': [ 3 ], 'sDefaultContent':'', 'sTitle': 'Rep' },
			{ 'aTargets': [ 4 ], 'sDefaultContent':'', 'sTitle': 'Status' },
			{ 'aTargets': [ 5 ], 'sDefaultContent':'', 'sTitle': '&nbsp;' },
			
			{ 'aTargets': [ 6 ], 'sDefaultContent':'{}', 'bSearchable': false, 'bVisible': false }

		],
		'aaSorting': [[0,'desc']],
		'aaData': [],
		'fnRowCallback': function(nRow, aData, iDisplayIndex) {
			var jdata = JSON.parse(aData[6]);
			
			$('td:eq(4)', nRow).addClass('fax-status').attr('data-fid', jdata.fid).attr('data-fill', jdata.fillnum);
			
			$('td:eq(3)', nRow).addClass('rep-' + jdata.rep_id + ' rep-' + rd.sales_data.rep_list[jdata.rep_id]).css({
				'color':'white',
				'font-weight':'bold',
				'letter-spacing':'0.5px'
			}).css('background-color', '#' + rd.rep_colors[_.indexOf(_.keys(rd.sales_data.rep_list), jdata.rep_id)] + ' !important');
			
			return nRow;
		}
	};
	
	if (!demo_mode) {
		_.each(rd.sales_data.faxes, function(item, idx) {
			var tmp = [
				item.date_created,
				item.from_did,
				parseInt(item.page_num,10) + ' / ' + item.num_pages,
				rd.sales_data.rep_list[item.rep_id],
				(item.fstatus && item.fstatus.match(/assist/i))?'<span class="func_show_fax_status_note ' + item.fstatus +' label label-warning" data-fidx="' + idx + '">Contacting Prescriber</span>':'&mdash;',
				'<span class="label label-success func_view_fax" data-pdfid="' + item.pdf_id + '" data-pngid="' + item.png_id + '" data-previewid="' + item.preview_id + '" data-repid="' + item.rep_id + '" data-rhcid="' + item.rhc_id + '" data-faxid="' + item.fax_id + '">View Fax</span>',
				'{"rxn":"' + item.rxn + '", "rep_id":"' + item.rep_id + '", "fid":"' + item.rhc_id + '"}'
			];
			
			pendingFaxesData.aaData.push(tmp);
		});
	} else {
		_.each(rd.sales_data.faxes, function(item, idx) {
			var tmp = [
				item.date_created,
				item.from_did,
				parseInt(item.page_num,10) + ' / ' + item.num_pages,
				"Sales Rep " + item.rep_id,
				(item.fstatus && item.fstatus.match(/assist/i))?'<span class="func_show_fax_status_note ' + item.fstatus +' label label-warning" data-fidx="' + idx + '">Contacting Prescriber</span>':'&mdash;',
				'<span class="label label-success func_view_fax" data-pdfid="' + item.pdf_id + '" data-pngid="' + item.png_id + '" data-previewid="' + item.preview_id + '" data-repid="' + item.rep_id + '" data-rhcid="' + item.rhc_id + '" data-faxid="' + item.fax_id + '">View Fax</span>',
				'{"rxn":"' + item.rxn + '", "rep_id":"' + item.rep_id + '", "fid":"' + item.rhc_id + '"}'
			];
			
			pendingFaxesData.aaData.push(tmp);
		});
	}
	
	var partialPendingFaxes = _.template($('#tmpl_pending_faxes').html());
	$('#dash-pending-faxes').html(partialPendingFaxes());
	
	rd.sales_data.ui['recentActivityTable'] = $('#recentActivityTable').dataTable(pendingFaxesData);
};

var partial_rx_activity = function() {
	if (SDEBUG) {
		console.log('partial_rx_activity(): Called');
	}
	
	var rxActivityData = {};

	rxActivityData = {
		"aoColumnDefs": [
			{ 'aTargets': [ 0 ], 'sDefaultContent':'', 'sTitle': 'Last Updated', 'sType': 'date' },
			{ 'aTargets': [ 1 ], 'sDefaultContent':'', 'sTitle': 'Date Prescribed', 'sType': 'date' },
			{ 'aTargets': [ 2 ], 'sDefaultContent':'', 'sTitle': 'Date Filled', 'sType': 'date' },
			{ 'aTargets': [ 3 ], 'sDefaultContent':'', 'sTitle': 'Prescriber' },
			{ 'aTargets': [ 4 ], 'sDefaultContent':'', 'sTitle': 'RxN' },
			{ 'aTargets': [ 5 ], 'sDefaultContent':'', 'sTitle': 'PT' },
			{ 'aTargets': [ 6 ], 'sDefaultContent':'', 'sTitle': 'Refills' },
			{ 'aTargets': [ 7 ], 'sDefaultContent':'', 'sTitle': 'Formula' },
			{ 'aTargets': [ 8 ], 'sDefaultContent':'', 'sTitle': 'Qty Disp' },
			{ 'aTargets': [ 9 ], 'sDefaultContent':'', 'sTitle': 'Billable Price' },
			{ 'aTargets': [ 10 ], 'sDefaultContent':'', 'sTitle': 'COGs' },
			{ 'aTargets': [ 11 ], 'sDefaultContent':'', 'sTitle': "Comm'l Amnt" },
			{ 'aTargets': [ 12 ], 'sDefaultContent':'', 'sTitle': 'Commission' },
			{ 'aTargets': [ 13 ], 'sDefaultContent':'', 'sTitle': 'Status' },
			{ 'aTargets': [ 14 ], 'sDefaultContent':'', 'sTitle': '3rd Party' },
			{ 'aTargets': [ 15 ], 'sDefaultContent':'', 'sTitle': 'Rep', 'sClass': 'rep-name' },
			
			{ 'aTargets': [ 16 ], 'sDefaultContent':'{}', 'bSearchable': false, 'bVisible': false }
		],
		'aaSorting': [[0,'desc']],
		'aaData': [],
		'fnRowCallback': function(nRow, aData, iDisplayIndex) {
			var jdata = JSON.parse(aData[16]);
			
			$('td:eq(13)', nRow).addClass('rx-activity-status').attr('data-rxn', jdata.rxn).attr('data-fill', jdata.fillnum);
			
			$('td:eq(15)', nRow).addClass('rep-' + jdata.rep_id + ' rep-' + rd.sales_data.rep_list[jdata.rep_id]).css({
				'color':'white',
				'font-weight':'bold',
				'letter-spacing':'0.5px'
			}).css('background-color', '#' + rd.rep_colors[_.indexOf(_.keys(rd.sales_data.rep_list), jdata.rep_id)] + ' !important');
			
			return nRow;
		}
	};
	
	if (!demo_mode) {
		_.each(rd.sales_data.rxData, function(item, idx) {
			var tmp = [
				item.LDU,
				item.DatePrescribed,
				(!item.DateFilled)?"&mdash;":item.DateFilled,
				item.doctorName,
				item.rxn,
				item.PT,
				item.rx_remaining_refills + ' / ' + item.rx_refills_auth,
				(!item.formula_id)?"&mdash;":item.formula_id,
				(!item.QtyDisp)?"&mdash;":item.QtyDisp,
				_.toCurrency(item.primary_price),
				(item.rx_status.match(/shipped/i))?_.toCurrency(item.COGs):'$0.00',
				(item.rx_status.match(/shipped/i))?_.toCurrency(item.primary_price - item.COGs):'$0.00',
				(item.rx_status.match(/shipped/i))?_.toCurrency(item.c_cut_amt):'$0.00',
				'<span class="func_show_status_note rx-activity-status ' + item.rx_status + ' label label-xs label-' + (_.has(rd.bs_status_labels, item.rx_status)?rd.bs_status_labels[item.rx_status].bsclass:'default') + '" data-rxid="' + idx + '">' + (_.has(rd.bs_status_labels, item.rx_status)?rd.bs_status_labels[item.rx_status].desc:item.rx_status) + '</span>',
				item.insurance_plan,
				rd.sales_data.rep_list[item.rep_id],
				'{"rxn":"' + item.rxn + '", "rep_id":"' + item.rep_id + '", "fillnum":"' + item.fill_num + '"}'
			];
			
			if (SDEBUG) console.log(tmp);
			
			rxActivityData.aaData.push(tmp);
		});
	} else {
		_.each(rd.sales_data.rxData, function(item, idx) {
			var tmp = [
				item.LDU,
				item.DatePrescribed,
				(!item.DateFilled)?"&mdash;":item.DateFilled,
				"Demo Mode",
				item.rxn,
				"Demo Mode",
				item.rx_remaining_refills + ' / ' + item.rx_refills_auth,
				(!item.formula_id)?"&mdash;":item.formula_id,
				(!item.QtyDisp)?"&mdash;":item.QtyDisp,
				_.toCurrency(item.primary_price),
				(item.rx_status.match(/shipped/i))?_.toCurrency(item.COGs):'$0.00',
				(item.rx_status.match(/shipped/i))?_.toCurrency(item.primary_price - item.COGs):'$0.00',
				(item.rx_status.match(/shipped/i))?_.toCurrency(item.c_cut_amt):'$0.00',
				'<span class="func_show_status_note rx-activity-status ' + item.rx_status + ' label label-xs label-' + (_.has(rd.bs_status_labels, item.rx_status)?rd.bs_status_labels[item.rx_status].bsclass:'default') + '" data-rxid="' + idx + '">' + (_.has(rd.bs_status_labels, item.rx_status)?rd.bs_status_labels[item.rx_status].desc:item.rx_status) + '</span>',
				item.insurance_plan,
				"Sales Rep " + item.rep_id,
				'{"rxn":"' + item.rxn + '", "rep_id":"' + item.rep_id + '", "fillnum":"' + item.fill_num + '"}'
			];
			
			rxActivityData.aaData.push(tmp);
		});	
	}
	
	if (SDEBUG) console.log(rxActivityData);

	var partialRxActivity = _.template($('#tmpl_rx_activity').html());
	$('#dash-table-of-data').html(partialRxActivity());
	
	rd.sales_data.ui['rxDataTable'] = $('#dynamic-table').dataTable(rxActivityData);
};

var refreshPrescriberList = function() {
		if (SDEBUG) {
			console.log('refreshPrescriberList(): Called');
		}
		var eff_user = user_name;
		
		if (typeof effective_user == "undefined" || effective_user == null) {
			if (rd && rd.sales_data && typeof rd.sales_data.effective_user != "undefined" && rd.sales_data.effective_user) {
				eff_user = rd.sales_data.effective_user;
			}
		} else {
			eff_user = effective_user;
		}
		$.postJSON('/api/v1.3.global_sales/get_prescriber_list', {effective_user:eff_user}, function(data) {
			if (SDEBUG) {
				console.log('refreshPrescriberList(): data');
				console.log(data);
			}
			if (_.size(data.errs) === 0) {
				if (SDEBUG) {
					console.log('refreshPrescriberList(): inserting prescriberdata');
				}
				rd.sales_data.prescriberdata = data.data;
				_.defer(function() {
					if (SDEBUG) {
						console.log('refreshPrescriberList(): first defer');
						console.log('refreshPrescriberList(): Calling Prescriber List Template');
					}
					
					var prescriberData = {
						"aoColumnDefs": [
							{ 'aTargets': [ 0 ], 'sDefaultContent':'&mdash;', 'sTitle': 'Rep', 'sClass': 'repName' },
							{ 'aTargets': [ 1 ], 'sDefaultContent':'Awaiting First Rx', 'sTitle': 'Prescriber Name' },
							{ 'aTargets': [ 2 ], 'sDefaultContent':'&mdash;', 'sTitle': 'Phone' },
							{ 'aTargets': [ 3 ], 'sDefaultContent':'&mdash;', 'sTitle': 'Address' },
							{ 'aTargets': [ 4 ], 'sDefaultContent':'&mdash;', 'sTitle': 'Fax' },
							{ 'aTargets': [ 5 ], 'sDefaultContent':'&mdash;', 'sTitle': 'NPI' },
							{ 'aTargets': [ 6 ], 'sDefaultContent':'0', 'sTitle': 'Scripts&nbsp;0&nbsp;-&nbsp;30&nbsp;Days' },
							{ 'aTargets': [ 7 ], 'sDefaultContent':'0', 'sTitle': 'Scripts&nbsp;30&nbsp;-&nbsp;90&nbsp;Days' },
							{ 'aTargets': [ 8 ], 'sDefaultContent':'{}', 'bSearchable': false, 'bVisible': false }
						],
						'aaSorting': [[0,'asc']],
						'aaData': [],
						'fnRowCallback': function(nRow, aData, iDisplayIndex) {
							var jdata = JSON.parse(aData[8]);
							$('td:eq(1)', nRow).addClass('view_prescriber_details').attr('id', 'prescriberListName' + jdata.npi).attr('npi', jdata.npi);
							
							$('td:eq(6)', nRow).addClass((parseInt(aData[6], 10) >= parseInt(aData[7], 10)/2)?'success':'danger').html(aData[6] + ' <span class="' + ((parseInt(aData[6], 10) > parseInt(aData[7], 10)/2)?'glyphicon glyphicon-arrow-up':((parseInt(aData[6], 10) < parseInt(aData[7], 10)/2))?'glyphicon glyphicon-arrow-down':'') + '"></span>');
							
//							$('td:eq(0)', nRow).addClass('rep-' + jdata.rep_id + ' rep-' + rd.sales_data.rep_list[jdata.rep_id]).css({
//								'color':'white',
//								'font-weight':'bold',
//								'letter-spacing':'0.5px'
//							}).css('background-color', '#' + rd.rep_colors[_.indexOf(_.keys(rd.sales_data.rep_list), jdata.rep_id)] + ' !important');

							return nRow;
						}
					};
					
					if (!demo_mode) {
						_.each(rd.sales_data.prescriberdata, function(item) {
							var tmp = [
								rd.sales_data.rep_list[item.rep_id],
								item.doctorName,
								item.doctorPhone1,
								item.doctorAddress1,
								item.doctorFax,
								item.npi,
								item.rxs_0_30,
								item.rxs_30_90,
								'{"npi":"' + item.npi + '", "rep_id":"' + item.rep_id + '"}'
							];
							
							prescriberData.aaData.push(tmp);
						});
					} else {
						_.each(rd.sales_data.prescriberdata, function(item) {
							var tmp = [
								"Sales Rep " + item.rep_id,
								"Demo Mode",
								"555-555-1234",
								"Demo Mode",
								"555-555-9876",
								"Demo Mode",
								item.rxs_0_30,
								item.rxs_30_90,
								'{"npi":"' + item.npi + '", "rep_id":"' + item.rep_id + '"}'
							];
							
							prescriberData.aaData.push(tmp);
						});
					}
					
					var prescriberListTemplate = _.template($('#tmpl_dash_prescriber_list').html());
					$('#dash-prescriber-list').html(prescriberListTemplate());
					//				console.log(rd.sales_data);
					_.defer(function() {
						if (SDEBUG) {
							console.log('refreshPrescriberList(): second defer');
						}
						rd.sales_data.ui['prescriberListTable'] = $('#prescriber-list-table').dataTable(prescriberData);
						rd.sales_data.ui['notesChanged'] = {};
					});
				});
			} else {
				console.log('Error(s) occurred when acquiring prescriber list from server.');
				_.each(data.errs, function(err) {
					console.log(err);
				});
			}
		});
	};

var dashClearFilters = function() {
	if (rd && typeof(rd) != 'undefined') {
		if (rd.sales_data && typeof(rd.sales_data) != 'undefined') {
			rd.sales_data.dashFilters = {
				rx_status: [],
				rep_id: "",
				date_range: {
					start: "",
					end: ""
				}
			};
		}
	}
};

var startDashboard = function(start_date, end_date, effective_user) {
		if (typeof end_date == "undefined" || end_date == null) {
			var tmpend_date = new Date();
			end_date = tmpend_date.toJSON().slice(0, 10);
		}
		if (typeof start_date == "undefined" || start_date == null) {
			var tmpstart_date = new Date();
			//start with month to date:
			var firstDay = new Date(tmpstart_date.getFullYear(), tmpstart_date.getMonth(), 1);
			tmpstart_date=firstDay;
			//console.log(tmpstart_date);
//			tmpstart_date.setDate(-30);
			var splt = tmpstart_date.toJSON().slice(0, 10).split('-');
			//console.log(splt)
			// go back 1 month
//			splt[1]--;
			//console.log(splt[1]);
			start_date = splt[0] + '-' + ((parseInt(splt[1]) <= 9)? '0' +  parseInt(splt[1]) : splt[1]) + '-' + splt[2];
		}
		
		var tz_offset = new Date().getTimezoneOffset();
		
		var eff_usr = user_name;
		
		if (typeof effective_user == "undefined" || effective_user == null) {
			if (rd && rd.sales_data && typeof rd.sales_data.effective_user != "undefined" && rd.sales_data.effective_user) {
				eff_usr = rd.sales_data.effective_user;
			}
		} else {
			eff_usr = effective_user;
		}

		$.postJSON('/api/v1.3.global_sales/get_sales_data', {
			start_date: start_date,
			end_date: end_date,
			tz_offset: tz_offset,
			effective_user: eff_usr
		}, function(data) {
			rd.sales_data = {};
			$('#bootstrap_area').html("");
			if (SDEBUG) {
				console.log('startDashboard(): data');
				console.log(data);
			}
			if (_.size(data.errs) === 0) {
				if (SDEBUG) {
					console.log('startDashboard(): get_sales_data has no errors');
				}
				rd.sales_data = data.data;

				//			console.log(data.data);
				if (!_.has(rd.sales_data, 'severityMap') || _.size(rd.sales_data.severityMap) <= 0 || !_.isArray(rd.sales_data.severityMap)) {
					//
					// If rd doesn't give a priority map (or gives it incorrectly), default to this.
					//
					rd.sales_data['severityMap'] = ['success', 'primary', 'info', 'warning', 'danger', 'default'];
				}
				
				if (!_.has(rd.sales_data, 'prescriberStatuses' || _.size(rd.sales_data.prescriberStatuses) <= 0)) {
					rd.sales_data.prescriberStatuses = {
						"REPLACED W/NEW RX": [
							"NO RX INS, FAX FOR SPRX:",
							"BULK INGRED NOT COVERED, FAX FOR SF03:",
							"COMPOUNDS NOT COVERED, FAX FOR SPRX:",
							"MEDICARE PART D, FAX FOR SF03:",
							"OTHER:"
						],
						"CONTACTING PATIENT": [
							"CLM TO VERIFY PT INS & ADDRESS:",
							"PT HAS COPAY-NEED PAYMENT INFO & AUTH TO SHIP:",
							"INFORM PT INS DOESN'T COVER, ASK IF PT WANTS STD FORMULA:",
							"WORK COMP/PIP REJECT, NEED COMMERCIAL INS INFO:",
							"OTHER:"
						],
						"INSURANCE PROCESSING": [
							"WORKMAN COMP/PIP CLAIM:",
							"PRIOR AUTH PENDING W/INS:",
							"OTHER:"
						],
						"CONTACTING PRESCRIBER": [
							"RX MISSING INFO, NEED FOLLWING:",
							"NO SIGNATURE FROM PRESCRIBER:",
							"PT HAS ALLERGY/CONTRAINDICATION:",
							"NEED VERBAL OK FOR CONTROLLED SUBSTANCE (KETAMINE):",
							"OTHER:"
						],
						"ASSISTANCE REQUIRED": [
							"PRIOR AUTH CLINICAL INFO NEEDED-DIAGNOSIS CODE, PRIOR MEDICATIONS TRIED W/ DATES:",
							"PT HAS COPAY-NEED PAYMENT INFO & AUTH TO SHIP:",
							"PT INFO NEEDED:",
							"OTHER:"
						],
						"NO GO": [
							"NO INS, PT DOESN'T WANT STANDARD FORMULA:",
							"PT DECLINE INS COPAY/COB PRICE:",
							"PT UNAWARE MD PRESCRIBED RX:",
							"RX FILLED AT OTHER PHARMACY:",
							"NOT NEEDED, NO SYMPTOMS:",
							"OTHER:"
						]
					};
				}
				
				rd.sales_data.rep_list_inverse = _.invert(rd.sales_data.rep_list);

			} else {
				console.log('An error occurred acquiring sales data from server.');
				console.log('Errors:');
				_.each(data.errs, function(err) {
					console.log(err);
					if (err.msg.match(/authentication/i)) {
						console.log('startDashboard(): postJSON returned authentication error.');
					}
				});
				window.location.href = '/login.html';	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			}

//
// Originally defer'd to force wait until dependencies loaded
//
			_.defer(function() {
				if (SDEBUG) {
					console.log('startDashboard(): first defer');
				}
				initDashboard(rd.sales_data);
				refreshPrescriberList();
				
			});
		});
	
	//                     min   sec   ms
	window.idleTime = 14 * 60 * 1000;
	$(document).idleTimer(window.idleTime);
	var x = setInterval(function() {
      var perc = (($( document ).idleTimer("getRemainingTime") / (window.idleTime)).toFixed(4) * 100).toFixed(2);
      
      if (perc >= 25) {
        $('.progress-bar').removeClass('progress-bar-warning progress-bar-danger');
      } else if (perc < 25 && perc >= 10) {
        $('.progress-bar').removeClass('progress-bar-danger').addClass('progress-bar-warning');
      } else {
        $('.progress-bar').removeClass('progress-bar-warning').addClass('progress-bar-danger');
      }
      
      $('.progress-bar').css('width', perc + '%').attr('aria-valuenow', perc);
    }, (window.idleTime / 1000)); // divide by 1000 to iterate ~ 0.1% of total timeout milliseconds for smooth animation

	$('td', '#sls-rep-list-table tr:eq(0)').each(function(idx) {
//	console.log('td:eq(' + idx + ').width() = ' + $(this).width());
		$('th:eq(' + idx + ')', '.floatThead-table thead').innerWidth($(this).width());
	});
	
};

// Do it
$(document).ready(function() {
	if (user_name && (user_name.match(/restore-user/i) || user_name.match(/restore_user/i) || user_name.match(/restoreuser/i))) {
		window.location.href = "/login.html";	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}
	
	/********** Demo Mode CONTROL **********************************/
	if (SHIPAA && user_name && (user_name.match(/demo-rep/i) || user_name.match(/restore-user/i) || user_name.match(/restore_user/i) || user_name.match(/restoreuser/i) || (user_name.match(/mwong-sales/i) && MHIPAA) )) {
		window.demo_mode = true;	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	} /****************************************************************/
	//bsclass corresponds to bootstrap3 built-ins: default, primary, info, success, warning and danger
	rd['rep_colors'] = [
	'335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a', '335', '88a' ];
	rd['bs_status_labels'] = {};
	
	rd.bs_status_labels = {
		'Pending': {
			bsclass: 'default',
			// will equate to label-default
			name: 'Pending',
			// human readable
			desc: 'Prescription Received' // meaningful description (for tooltip?)
		},
		'FAIL': {
			bsclass: 'danger',
			// will equate to label-danger, etc.
			name: 'Failed',
			desc: 'Failed'
		},
		'PT-CON': {
			bsclass: 'default',
			name: 'Contact Patient',
			desc: 'Contacting Patient'
		},
		'INS-PROC': {
			bsclass: 'default',
			name: 'INS-PROC',
			desc: 'Insurance Processing'
		},
		'INS-AUTH': {
			bsclass: 'warning',
			name: 'INS-AUTH',
			desc: 'Prior Authorization'
		},
		'REP-ASST': {
			bsclass: 'warning',
			name: 'Rep Assist',
			desc: 'Assistance Requested'
		},
		'RX-PROC': {
			bsclass: 'success',
			name: 'RX-PROC',
			desc: 'In Fulfillment'
		},
		'Filled': {
			bsclass: 'success',
			name: 'Filled',
			desc: 'Filled'
		},
		'Shipped': {
			bsclass: 'success',
			name: 'Shipped',
			desc: 'Shipped'
		},
		'REPLACE': {
			bsclass: 'default',
			name: 'Replaced',
			desc: 'Replaced'
		},
		'MD': {
			bsclass: 'warning',
			name: 'MD',
			desc: 'Contacting Prescriber'
		}
	};
	
	addToHomescreen({startDelay: 5, maxDisplayCount: 1, skipFirstVisit: true});
	
	startDashboard();
});
