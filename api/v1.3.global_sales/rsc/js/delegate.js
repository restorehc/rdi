/*jshint sub:true*/
/*jshint smarttabs:true */

$(document).ready(function() { /* delegates */
	$(document).on("active.idleTimer", function(evt, elm, obj, triggerevent) {
		if (SDEBUG) {
			console.log('idle Timer active event:');
			console.log('evt:');
			console.log(evt);
			console.log('elm:');
			console.log(elm);
			console.log('obj:');
			console.log(obj);
			console.log('triggerevent:');
			console.log(triggerevent);
		}
		if (rd.sales_data.ui.idlemodal) {
			rd.sales_data.ui.idlemodal.modal("hide");
			rd.sales_data.ui.idlemodal = false;
		}
		$(document).idleTimer("destroy");
		$(document).idleTimer((14 * 60 * 1000));
	});
	$(document).on("idle.idleTimer", function(evt, elm, obj) {
		if (SDEBUG) {
			console.log('IDLE TIMEOUT ====================');
			console.log('evt:');
			console.log(evt);
			console.log('elm:');
			console.log(elm);
			console.log('obj:');
			console.log(obj);
		}
		if (obj.timeout == (14 * 60 * 1000)) {
			if (SDEBUG) {
				console.log('Caught 14min idle');
			}
			rd.sales_data.ui['idlemodal'] = bootbox.alert("We noticed you've been idle for a while. Please click OK in the next 60sec to remain logged in.", function() {
				console.log('idlemodal ok clicked. resetting timer to 14min');
				$(document).idleTimer("destroy");
				$(document).idleTimer((14 * 60 * 1000));
			});
			// Resetting idle timer to 60 sec
			if (SDEBUG) {
				console.log('Resetting idle timer to 60sec');
			}
			$(document).idleTimer("destroy"); // nix the old timer to set a new one
			$(document).idleTimer((1 * 60 * 1000));
		} else if (obj.timeout == (1 * 60 * 1000)) {
			if (SDEBUG) {
				console.log('Caught 60sec idle');
				console.log('Redirecting to login.html');
			}
			window.location.href = '/login.html';
		} else {
			if (SDEBUG) {
				console.log('Caught some other idle: ' + obj.timeout + 'ms');
			}
		}
	});

	var select_dates_form = function(form_class) {
			return false;
		};
	$("body").on(click_event, '.func_select_dash_date_range', function(eml) {
		var lo = $(this).data();
		eml.preventDefault();
		console.log(lo);

		var mobile_form = '<div class="row"><form class="form-horizontal <%= form_class %>"><div class="form-group"><label for="dash_start_date" class="col-sm-4 control-label">Start Date:</label><div class="input-group date col-sm-7" data-date-format="YYYY-MM-DD" id="dt_start"><input class="form-control" type="date" placeholder="YYYY-MM-DD" name="start_date" id="dash_start_date"></div></div><div class="form-group"><label for="dash_end_date" class="col-sm-4 control-label">End Date:</label><div class="input-group date col-sm-7" data-date-format="YYYY-MM-DD" id="dt_end"><input class="form-control" type="date" name="end_date" placeholder="YYYY-MM-DD" id="dash_end_date"></div></div><div class="form-group"><div class="col-sm-12"><input class="form-control btn btn-primary" type="submit" /></div></div></form></div>';
		var desktop_form = '<div class="row"><form class="form-horizontal <%= form_class %>"><div class="form-group"><label for="dash_start_date" class="col-sm-4 control-label">Start Date:</label><div class="input-group date col-sm-7" data-date-format="YYYY-MM-DD" id="dt_start"><input class="form-control" type="text" placeholder="YYYY-MM-DD" name="start_date" id="dash_start_date"><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span></div></div><div class="form-group"><label for="dash_end_date" class="col-sm-4 control-label">End Date:</label><div class="input-group date col-sm-7" data-date-format="YYYY-MM-DD" id="dt_end"><input class="form-control" type="text" name="end_date" placeholder="YYYY-MM-DD" id="dash_end_date"><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span></div></div><div class="form-group"><div class="col-sm-12"><input class="form-control btn btn-primary" type="submit" /></div></div></form></div>';

		var sel_form = _.template((!!_.appleios())?mobile_form:desktop_form);
		popup(sel_form({form_class:'func_reset_salesboard'}), {className:'datePickModal'});
		
		if (!_.appleios()) {
			setTimeout(function() {
				$(function () {
		            $('#dt_start').datetimepicker({
		                pickTime: false
		            });
		            $('#dt_end').datetimepicker({
		                pickTime: false
		            });
		        });
	        }, 150);
		}
		// ?? change_date_input(lo.dateinput, lo.days);
	});
	$(document).on('show.bs.modal', '.datePickModal', function(e) {
        $('#dt_start').datetimepicker({
            pickTime: false
        });
        $('#dt_end').datetimepicker({
            pickTime: false
        });
	});
	$(document).on(click_event, '.func_download_dash_excel', function(e) {
		e.preventDefault();
		var tz_offset = new Date().getTimezoneOffset();
		var eff_user = user_name;
		
		if (typeof effective_user == "undefined" || effective_user == null) {
			if (rd && rd.sales_data && typeof rd.sales_data.effective_user != "undefined" && rd.sales_data.effective_user) {
				eff_user = rd.sales_data.effective_user;
			}
		} else {
			eff_user = effective_user;
		}
		$.postJSON('/api/v1.3.global_sales/get_sales_data_excel', {
			start_date: rd.sales_data.date_searched.start,
			end_date: rd.sales_data.date_searched.end,
			tz_offset: tz_offset,
			effective_user: eff_user
		}, function(data) {
			downloadURI(data.data.xlsx_data, 'sales_data.xlsx');
		});
	});
	$("body").on('submit', '.func_reset_salesboard', function(eml) {
		eml.preventDefault();
		$('#view_fax').html("");
		if (SDEBUG) {
			console.log('.func_reset_salesboard submitted');
			console.log('event object:');
			console.log(eml);
		}
		var dinfo = $(this).serializeObject();

		rd.sales_data.ui.popup.modal('hide');

		if (SDEBUG) console.log('dinfo: ', dinfo);
		startDashboard(dinfo.start_date, dinfo.end_date, null);
	});
	
	$(document).on(click_event, '.func_get_rep_list_csv', function(e) {
		var csvString = "data:text/csv,";
//		_.each(rd.sales_data.ui.rep_list_data, function(data, idx) {
		_.each(rd.sales_data.ui.comm_tbl_data, function(data, idx) {
			for (i = 0; i < 5; i++) {
				if (i < 4) {
					csvString += data[i] + ',';
				} else {
					csvString += data[i] || 0;
				}
			}
			csvString += "%0A";
		});
		downloadURI(csvString, "rep_list.csv");
	});
	
	// Phone and Address functions
	$(document).on(click_event, "[data-role='func_phone_call']", function(e) {
		var num = e.target.dataset.target;
		if (SDEBUG) {
			console.log(num);
			console.log("Do something with it.");
		}
		$.postJSON('/api/v1.3.global_sales/originate_call', {
			"destination": num
		}, function(data) {
			if (_.size(data.errs) === 0) {
				if (SDEBUG) console.log('phone call winning');
			} else {
				console.log('Error(s) occurred originating phone call to prescriber.');
				_.each(data.errs, function(err) {
					console.log(err);
				});
			}
		});
	});
	$(document).on(click_event, "[data-role='func_get_directions']", function(e) {
		var addr = e.target.dataset.target;
		if (SDEBUG) {
			console.log(addr);
			console.log("Do something with it.");
			console.log(_.appleios());
		}
		window.open(addr);
	});
	$(document).on('submit', '.func_add_prescriber', function(e) {
		e.preventDefault();
		var args = $(this).serializeObject();
		//console.log(args);
		$.postJSON('/api/v1.3.global_sales/add_prescriber', args, function(data) {
			display_errs(data.errs);
			refreshPrescriberList();
			$('.bootbox').hide();
			$('.modal-backdrop').hide();
		});
	});
	
	$(document).on(click_event, ".func_view_fax", function(e) {
		if (!demo_mode) {
			var pdf_id = e.target.dataset.pdfid;
			var pre_id = e.target.dataset.previewid;
			var rep_id = e.target.dataset.repid;
			var rhc_id = e.target.dataset.rhcid;
			var fax_id = e.target.dataset.faxid;
			var png_id = e.target.dataset.pngid;
			
			//console.log(e);

/*			
			var fax = {
				original_fax_id: 123456,
				pages : {
					0: {
						pdf_id: 69879087,
						png_id: 49876876,
						pre_id: 98774698,
						fax_id: 23987462
					},
					1: {
						pdf_id: 79823746,
						png_id: 98237498,
						pre_id: 98685786,
						fax_id: 10928493
					}
				}
			};
*/
	
			$.postJSON('/api/v1.3.global_sales/get_fax_img', {fax_id:fax_id, data_record_id:png_id}, function(data) {
				//console.log(data);
				if (data.success) {
					var viewFaxTemplate = _.template($("#tmpl_view_fax").html());
					data.data.fax_id = fax_id;
					$('#view_fax').html(viewFaxTemplate(data.data));
	//				$('img#faximg').attr('src', URL.createObjectURL(_.uri2Blob(data.data.img_data)));
	//				$('img#faximg').attr('src', _.uri2Blob(data.data.img_data));
					$('img#faximg').attr('src', data.data.img_data);
					scroll2('#view_fax');
				}
			});
		} else {
			bootbox.alert('Faxes cannot be viewed while in Demo Mode.');
		}
	});
	$(document).on(click_event, ".func_hide_fax", function(e) {
		$('#view_fax').html("");
		scroll2('#dash-pending-faxes');
	});
	// Modal Pieces
	$(document).on(click_event, "#btnAddPrescriber", function() {
		$('#view_fax').html("");
		var tmplModal = _.template($('#tmpl_add_prescriber').html());
		var modalMsg = tmplModal();
		bootbox.dialog({
			message: modalMsg,
			backdrop: true,
			title: "Add a new prescriber"
		});
	});
	$(document).on(click_event, 'button#hidePrescriberDetailsWell', function(e) {
		var npi = $('#' + e.target.id).attr('npi');
		if (!_.has(rd.sales_data.ui.notesChanged, npi) || rd.sales_data.ui.notesChanged[npi] !== true) {
			$('#prescriber-details-table').html("");
		} else {
			bootbox.confirm("You have unsaved changes in Prescriber Notes. You will lose your changes if you hide details. Continue?", function(result) {
				if (SDEBUG) console.log('bootbox confirm: ', result);
				if (result === true) {
					rd.sales_data.ui.notesChanged[npi] = false;
					$('#prescriber-details-table').html("");
				}
			});
		}
	});
	$(document).on(click_event, '.func_filter_assist_faxes', function(e) {
		$('#view_fax').html("");
		rd.sales_data.ui.recentActivityTable.fnFilter('Contacting Prescriber', 4);
//		scroll2('#dash-pending-faxes');
		$('#mainTabNav a[href="#Faxes"]').tab('show');
	});
	
	
	$(document).on(click_event, '.func_reset_rx_activity', function(e) {
		e.preventDefault();
		_.resetDTFilters(rd.sales_data.ui.rxDataTable);
	});
	
	$(document).on(click_event, '.func_reset_assist_faxes', function(e) {
		e.preventDefault();
		_.resetDTFilters(rd.sales_data.ui.recentActivityTable);
	});
	
	$(document).on('change', '.func_toggle_sales_dash_demo_mode', function(e) {
		window.demo_mode = !window.demo_mode;
		startDashboard();
	});
	
	$(document).on(click_event, '[data-name="gotoFilter"]', function(e) {
		if (SDEBUG) console.log('onClick: gotoFilter; e.currentTarget.dataset = ', e.currentTarget.dataset);
		var filter = e.currentTarget.dataset.filter;
		var filterby = e.currentTarget.dataset.filterby;
		if (SDEBUG) {
			console.log('todayItem Clicked');
			console.log('Filter: ', filter);
			console.log('Filterby: ', filterby);
		}
		// rxDataTable is effected by all filters; always reset this filter
		_.resetDTFilters(rd.sales_data.ui.rxDataTable);
		if (filterby == "rx_status") {
			$('#view_fax').html("");
			partial_commissionable_sales();
			partial_commissions_data();
			rd.sales_data.ui.rxDataTable.fnFilter(rd.bs_status_labels[filter].desc, 13); // Change the column if more columns added to table!!!!!!
			
			$('#mainTabNav a[href="#RxActivity"]').tab('show');
//			scroll2('#dynamic-table');
//			$.scrollTo('#dynamic-table', 600, rd.scrollToOptions);
		} else if (filterby == 'rep_id') {
			$('#view_fax').html("");
			// These filters are only effected in certain circumstances; reset the filters only when needed.
			_.resetDTFilters(rd.sales_data.ui.prescriberListTable);
			_.resetDTFilters(rd.sales_data.ui.actionItemsTable);
			_.resetDTFilters(rd.sales_data.ui.recentActivityTable);
			
			if (SDEBUG) {
				console.log('rd.sales_data.rep_list[filter]: ', rd.sales_data.rep_list[filter]);
				console.log('Demo Mode: ', demo_mode);
			}
			
			partial_commissionable_sales(filter);
			partial_commissions_data(filter);
//			redrawGraph(filter);

			rd.sales_data.ui.prescriberListTable.fnFilter((!demo_mode) ? rd.sales_data.rep_list[filter] : 'Sales Rep ' + filter, 0); // Checks for demo_mode; change the column if more columns added to table!!!!!!!
			rd.sales_data.ui.recentActivityTable.fnFilter((!demo_mode) ? rd.sales_data.rep_list[filter] : 'Sales Rep ' + filter, 3); // Checks for demo_mode; change the column if more columns added to table!!!!!!!
			rd.sales_data.ui.actionItemsTable.fnFilter((!demo_mode) ? rd.sales_data.rep_list[filter] : 'Sales Rep ' + filter, 5); // Checks for demo_mode; change the column if more columns added to table!!!!!!!
			rd.sales_data.ui.rxDataTable.fnFilter((!demo_mode) ? rd.sales_data.rep_list[filter] : 'Sales Rep ' + filter, 15); // Checks for demo_mode; change the column if more columns added to table!!!!!!!

			if (demo_mode) {
				bootbox.alert("Displaying information for rep: Sales Rep " + filter); // Let the reps know the page has been filtered
			} else {
				bootbox.alert("Displaying information for rep: " + rd.sales_data.rep_list[filter]); // Let the reps know the page has been filtered
			}
			// No longer needed, as this filters the whole page, not just the one table
			//		$.scrollTo('#dynamic-table', 600, rd.scrollToOptions);
		} else {
			console.log('Cannot filter by [' + filterby + '] with filter [' + filter + ']');
		}
	});
	$(document).on(click_event, '.func_popup_rep_stats', function(e) {
		var thisrep = $(this).data('repid');
		console.log('thisrep: ', thisrep);
		
		var tmpl = _.template($('#tmpl_popup_rep_stats').html());
		bootbox.alert(tmpl({rep:thisrep}));
	});
	$(document).on('keyup', '#rep-filter', function(e) {
		var filterValue = $(this).val();
		if (SDEBUG) console.log('filterValue: ', filterValue);
		$('#rep_list_accordion_even .rep').each(function() {
			var val = $(this).data('rep-name');
			//if (SDEBUG) console.log('data-rep-name: ', val);
			if (val !== "" && val !== null) {
				if (val.indexOf(filterValue) === -1) {
					$(this).hide();
				} else {
					$(this).show();
				}
			} else {
				$(this).show();
			}
		});
		
	});
	$(document).on(click_event, '#prescriber-list-table td.view_prescriber_details', function(e) {
		//		console.log('Prescriber Name clicked.');
		var npi = $('#' + e.target.id).attr('npi');
		var detailsHTML = _.template($('#tmpl_prescriber_details_table').html());
		$('#prescriber-details-table').html(detailsHTML(rd.sales_data.prescriberdata[npi]));

		scroll2('#prescriber-details-table');
//		$.scrollTo('#prescriber-details-table', 600, rd.scrollToOptions);
	});
	$(document).on('keyup', 'textarea.dashRxrNotesEdit', _.throttle(function(e) {
		//console.log(e);
		var npi = $('#' + e.target.id).attr('npi');
		//console.log(npi);
		if ($('#editNotes_' + npi).parent().hasClass('has-success')) {
			$('#editNotes__' + npi).parent().removeClass('has-success');
		} else if ($('#editNotes_' + npi).parent().hasClass('has-danger')) {
			$('#editNotes_' + npi).parent().removeClass('has-danger');
		}
		if ($('#statusLabel_' + npi).hasClass('label-success')) {
			$('#statusLabel_' + npi).removeClass('label-success');
		} else if ($('#statusLabel_' + npi).hasClass('label-danger')) {
			$('#statusLabel_' + npi).removeClass('label-danger');
		}
		$('#statusLabel_' + npi).text("");
		if (!_.has(rd.sales_data.ui.notesChanged, npi) || !rd.sales_data.ui.notesChanged[npi]) {
			rd.sales_data.ui.notesChanged[npi] = true;
			//			console.log('should be inserting button');
			if ($('#btn' + npi).hasClass('disabled')) {
				//console.log('Removine class "disabled" from save button.');
				$('#btn' + npi).removeClass('disabled');
			}
			//			$('#btn' + npi).enable();
		} else {
			// do different stuff;
		}
		//		console.log(npi);
	}, 1500, {
		trailing: false
	}));
	/*
	 * Send prescriber notes to server
	 */
	$(document).on(click_event, 'button.rxrSaveNotes', function(e) {
		var npi = $('#' + e.target.id).attr('npi');
		//		console.log('Save notes button clicked for npi: ' + npi);
		//		$('#' + e.target.id).remove();
		var notes = $('textarea[npi=' + npi + ']').val();
		rd.sales_data.ui.notesChanged[npi] = false;
		//		console.log(notes);
		var setResult = function(npi, result) {
				var res = (result == 'success') ? 'success' : 'danger';
				$('#editNotes_' + npi).parent().addClass('has-' + res);
				$('#statusLabel_' + npi).addClass('label-' + res);
				$('#statusLabel_' + npi).text((res == 'success') ? 'Saved' : 'Error');
				$('#btn' + npi).addClass('disabled');
				//			$('#btn' + npi).disable();
			};
		$.postJSON('/api/v1.3.global_sales/update_prescriber_note', {
			'note_text': notes,
			'npi': npi
		}, function(data) {
			if (SDEBUG) {
				console.log('Sending notes to server -- Server response:');
				console.log(data);
			}
			if (_.size(data.errs) > 0) {
				console.log('Error[s] occurred when posting prescriber notes.');
				_.each(data.errs, function(err) {
					console.log(err);
				});
				setResult(npi, 'fail');
			} else if (!_.has(data.data, 'success') || !data.data.success) {
				setResult(npi, 'fail');
			} else {
				setResult(npi, 'success');
				rd.sales_data.prescriberdata[npi].note_text = notes;
				//				rd.prescriber_data_list[npi].note_text = notes;
			}
		});
		//		setResult(npi, 'success');
	});
	
	
	$(document).on(click_event, 'a[data-role="scrollToNav"]', function(e) {
		console.log('scrollToNav event fired with: ', '#'+e.currentTarget.dataset.target);
		e.preventDefault();
		
		$('#view_fax').html("");

		scroll2('#' + e.currentTarget.dataset.target);
//		$.scrollTo('#' + e.currentTarget.dataset.target, 600, rd.scrollToOptions);
	});
	$(document).on(click_event, 'a[data-role="resetFilters"]', function(e) {
		e.preventDefault();
		$('#view_fax').html("");
		startDashboard();
	});
	$(document).on(click_event, '.func_show_status_note', function(e) {
		var rxid = $(this).data('rxid');
		if (SDEBUG) {
			console.log('.func_show_status_note -- rxid: ' + rxid);
			console.log(rd.sales_data.rxData[rxid].rx_status_note);
		}
		if (!rd.sales_data.rxData[rxid].attachment_id || rd.sales_data.rxData[rxid].attachment_id == null) {
		console.log(rxid);
			bootbox.alert((rd.sales_data.rxData[rxid].rx_status_note) ? ((!demo_mode)?rd.sales_data.rxData[rxid].rx_status_note:'Demo Mode &mdash; Rx status note displays here.') : 'No Rx Status Note to Display');
		} else {
			$.postJSON('/api/v1.3.global_sales/get_rx_status_attachment', {'rxn': rd.sales_data.rxData[rxid].rxn, 'fill_num':rd.sales_data.rxData[rxid].fill_num}, function(data) {
				//console.log('Stat Attach: ', data);
				if (data.errs.length !== 0) {
					;
				} else {
					if (SDEBUG) console.log('.func_show_status_note: server response: ', data);
					var tmp = data.data[1];	// This is the dataURI
					var t_msg = (!demo_mode)?(((rd.sales_data.rxData[rxid].rx_status_note)?rd.sales_data.rxData[rxid].rx_status_note:'No Rx Status Note to Display') + '<br />&nbsp;<br /><a class="btn btn-primary" href="' + URL.createObjectURL(_.uri2Blob(tmp)) + '" target="_blank">View Attachment</a>'):'Demo Mode &mdash; Rx status note and link to attachment displays here.';
					bootbox.alert(t_msg);
				}
			});
		}
	});
	$(document).on(click_event, '.navbar-brand', function(e) {
		e.preventDefault();
	});
	/*
	 *	Sales management delegates (show/hide/form submissions/etc) *****************
	 */
	$(document).on(click_event, '.func_display_sales_management', function(e) {
		if (SDEBUG) console.log('.func_display_sales_management clicked');
		e.preventDefault();
		$('#view_fax').html("");
		var salesManagementTemplate = _.template($('#tmpl_sales_management').html());
				
		$('#sales_management_container').html(salesManagementTemplate());

		scroll2('#sales_management_container');
//		$.scrollTo('#' + e.currentTarget.dataset.target, 600, rd.scrollToOptions);
	});
	$(document).on(click_event, '.func_hide_sales_management', function(e) {
		if (SDEBUG) console.log('.func_hide_sales_management clicked');
		e.preventDefault();
		$('#sales_management_container').html("");
		startDashboard();
	});
	$(document).on('submit', '.func_add_new_rep', function(e) {
		e.preventDefault();
		var args = $(this).serializeObject();
		if (SDEBUG) console.log('add_sales_rep args: ', JSON.stringify(args));
		$.postJSON('/api/v1.3.global_sales/add_sales_rep', args, function(data) {
			console.log('adding new rep: ', args);
			console.log('response: ', data);
			
			if (data.errs.length === 0 || (data.errs[0].type && data.errs[0].type.match(/success/i))) {
				console.log('Server returned with a success.');
				if (data.errs[0].msg) console.log('Server Response: ', data.errs[0].msg);
				$('.func_add_new_rep')[0].reset();
				var err_msg = "";
				for (i = 0; i < data.errs.length; i++) {
					err_msg += '<p><code>' + data.errs[i].msg + '</code></p>';
				}
				bootbox.alert('<p>User has been successfully added.</p><p>Server msgs:</p>' + err_msg);
			} else {
				console.log('Server returned the following: ', data);
				
				var err_msg = "";
				for (i = 0; i < data.errs.length; i++) {
					err_msg += '<p><code>' + data.errs[i].msg + '</code></p>';
				}
				
				bootbox.alert('<p>The user could not be added to the server.</p><p>Server msgs:</p>' + err_msg);
			}
		});
	});
	
	$(document).on(click_event, '.func_masq_as_rep', function(e) {
		var rnme = $(this).data('repname');
		
		startDashboard(null, null, rnme);
	});
	$(document).on(click_event, '.func_return_to_user', function(e) {
		e.preventDefault();
		startDashboard(null, null, user_name);
	});
	
	$(document).on('click', '.func_display_commissions_report', function(e) {
		e.preventDefault();
		
		var tmp_tmpl = _.template($('#tmpl_distributor_commission_report').html());
		$('#dash-commissions-report').html(tmp_tmpl(rd.sales_data.commission_report_data));
		scroll2('#dash-commissions-report');
	});

	$(document).on('submit', '.func_reassign_prescriber', function(e) {
		e.preventDefault();
		var args = $(this).serializeObject();
		
		$.postJSON('/api/v1.3.global_sales/set_npi_to_rep', args, function(data) {
			if (data.errs.length == 0) {
				bootbox.alert('<p>The prescriber was unable to be reassigned to a different rep.</p>');
			} else {
				$('.set_npi_to_rep')[0].reset();
				bootbox.alert('<p>The prescriber was successfully reassigned.</p>');
			}
		});
	});
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$(document).on('submit', '.func_reset_rep_password', function(e) {
		e.preventDefault();
		var args = $(this).serializeObject();
		if (SDEBUG) console.log('func_reset_rep_password args: ', JSON.stringify(args));
		//		$.postJSON('/api/v1.3.global_sales/reset_rep_password', args, function(data) {
		//			display_errs(data.errs);
		//			$('.func_reset_rep_password')[0].reset();
		//		});
	});
	$(document).on(click_event, '.func_show_fax_status_note', function(e) {
		var fidx = $(this).data('fidx');
		if (SDEBUG) {
			console.log('.func_show_fax_status_note -- fidx: ' + fidx);
			console.log(rd.sales_data.faxes[fidx].status_note);
		}
		bootbox.alert((rd.sales_data.faxes[fidx].status_note) ? rd.sales_data.faxes[fidx].status_tstamp + '<br />' + rd.sales_data.faxes[fidx].status_note : 'No Fax Status Note to Display');
	});
});
