/*jshint sub:true*/

$(document).ready(function() {
	rd['bootstrap'] = {};
	rd['_'] = {};
	
	rd._['tmpl_display_errs'] = '<% _.each(errs, function(err) { console.log(err); %><p class="errMsg status bg-<%= err.type %> <%= err.type %>"><%= err.msg %></p><% }); %>';
	
	popup = function(message, opts) {
		if (!message || message === null || message === '') {
			if (SDEBUG) console.log('popup called with empty message.');
		} else {
			console.log('popup() -- Called');
			var bsopt = {};
			bsopt['message'] = message;
			
			if(typeof(opts) != 'object') {opts={}; }
			
			opts['message'] = message;
			rd.sales_data.ui.popup = bootbox.dialog(opts);
		}
	};
	
	display_errs = function (errs, title, cb, data, opts) {
		if (typeof errs && errs[0]) {
			if(typeof(opts) != 'object') {opts={}; }
			opts.title=title || "Messages";
			
			if (!errs[0].noclose && !opts.noclose) {
				opts['buttons'] = {
					main: {
						label: "Close",
						className: "btn-danger",
						callback: cb
					}
				};
			}
			
			popup(_.template(rd._.tmpl_display_errs)({errs:errs}), opts);
		}
	};
});
