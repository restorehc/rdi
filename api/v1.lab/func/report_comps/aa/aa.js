
<script async type="text/javascript">

var impInfs= <% JSON::XS::encode_json(\%impInfs) %>;
var statusesLI = new Template('<li class="#{boldMe} #{sv}" >#{shortName}: #{sVal}</li>');

var quotedHI=new Array('lastPeriod', 'currentIRMA.lastPeriod', 'hysterectomyDate', 'hysterectomy', 'vaginalBleeding', 'vaginalBleedingLastOccurrence');
var quotedHI_refs={'vaginalBleeding': 'vaginalBleedingLastOccurrence'};

var fillImpInfo =
function (c_rxn) {
if( typeof c_rxn == 'undefined') {c_rxn=gRxN}
if (typeof  Pat["HH"][Pat['instances'][c_rxn]]['EvaluationText'] && Pat["HH"][Pat['instances'][c_rxn]]['EvaluationText']['importantInfo'] && Pat["HH"][Pat['instances'][c_rxn]]['hhstatus'] !='New') {
			$('importantInfo').innerHTML=Pat["HH"][Pat['instances'][c_rxn]]['EvaluationText']['importantInfo'];
} else {

	var statHTML='	<ul class="healthConditionsList">';
		$H(impInfs).keys().each(function(sv) {

		if (Pat['HH'][Pat['instances'][c_rxn] || 0]['EAV'][sv]) {
			var snVal=Pat['HH'][Pat['instances'][c_rxn] || 0]['EAV'][sv];
			if (quotedHI.include(sv) && snVal.match("[a-zA-Z]") && !(['yes', 'no', 'Yes', 'No'].include(snVal)) ) {				snVal='<span id="qHI_' +sv+ '">"'+snVal+'"</span>';			} 
			if (typeof quotedHI_refs[sv]  !='undefined' && snVal.match("yes")) {console.log(sv);	snVal='<span id="qHI_' +sv+ '">"'+ Pat['HH'][Pat['instances'][c_rxn] || 0]['EAV'][ quotedHI_refs[sv] ]  +'"</span>';		}
			statHTML+=statusesLI.evaluate({shortName: impInfs[sv], sVal: snVal, sv: sv});
			}
			});
		statHTML+='	</ul>';
		$('importantInfo').innerHTML=statHTML;
	}
};








clearForNewPatient = function () {
Control.Window.windows.invoke('close');
$('pChartArea').innerHTML=clearHTML;
$('evalMsgs').update();

$('recommendations_Area').innerHTML='';
$('rxhist_tbody').innerHTML='';
$('currentEvalRecommendtions').innerHTML='';
$('restoreRecords').innerHTML='';
$('editCoverLetter').innerHTML='';

return;
displayPinfo();
$('HormoneSummary').innerHTML=determineDefs(0,Pat);
createMedList();
activateMedList(gRxN|| Pat['instances']['all'][0]['RxNumber']);
fillSection(['currentIRMA.commentsDiagnoses','currentIRMA.commentsDiagnosticConsiderations'], 0, Pat['instances'][gRxN], "diagnosesArea");
fillSection(['currentIRMA.commentsTherapyEvaluation'], 0, Pat['instances'][gRxN], "hhNotes");
fillSection(['currentIRMA.commentsDietLifestyle'],0, Pat['instances'][gRxN], 'SelfCareNotes');
tryThis();
tryThisNow();
new Ajax.Updater('symptomList', '/aa/symptomhistory.html', {parameters: {RxN: gRxN, patientID: patID, ajax: 1}});
makeRxPad();
new Ajax.Updater('LabArea', '/aa/labresults.html', {parameters: {RxN: gRxN, patientID: patID, ajax: 1}});
fillImpInfo();

// recs, rxhist, any floating windows, flotrs, 
}









var Pat=new Object();
var gRxN;
// Templates:
var cmListE = new Template('<div id="cm_#{ID}" class="cmMed even combined#{medicationCombined} #{showCombinedInfo}">#{medicationName} #{medicationDosage} #{medicationUnit} <span class="medicationDelMethod">#{medicationDeliveryMethod}</span><div class="cmNotes">#{medicationNotes}</div></div>');
var cmListO = new Template('<div  id="cm_#{ID}"  class="cmMed odd combined#{medicationCombined} #{showCombinedInfo}">#{medicationName} #{medicationDosage} #{medicationUnit} <span class="medicationDelMethod">#{medicationDeliveryMethod}</span><div class="cmNotes">#{medicationNotes}</div></div>');
var cmHeader = new Template('<li class="#{active} #{RxNumber}" id="cm_#{RxNumber}" onclick="activateMedList(\'#{RxNumber}\');">#{ShortDate}</li>');

var piDisplay = new Template(
'	<div id="patientMoreInfo">	<img id="patientImage" src="/private_imgs/#{patientID}patientPhoto.jpg?DoctorCode=#{DoctorCode}" alt=" " style=" float:left;" />		#{PatientLastName}, #{PatientFirstName} #{PatientMiddleInit}	<img id="morepatientinfoimage" style="max-height: 1.25em;" src="/images/aa_icons/next_icon.gif" onclick="showmorePinfo();" alt="More..."  class="showControl">	<ul id="morepinfo" style=""><li>DOB:  #{DOB}</li>			<li> #{PatientAddressLine1} <span style="display:block"> #{PatientAddressLine2} </span></li>			<li>#{PatientCity}, #{PatientState} #{PatientZIP}</li>			<li>H: #{PatientPhone} W: #{PatientWorkPhone}</li>			<li><a href="mailto: #{email}">#{email}</a></li>		</ul>	</div>'
);


var medhist_list_header = new Template('<li class="#{active} #{RxNumber}" id="cm_#{RxNumber}" onclick="updateMedHistList(\'#{RxNumber}\', this);">#{ShortDate}</li>');
var hlhist_list_header = new Template('<li class="#{active} #{RxNumber}" id="cm_#{RxNumber}" onclick="updateHLHistList(\'#{RxNumber}\', this);">#{ShortDate}</li>');

displayPinfo = function() {
		var mlhtml='';
		
			 mlhtml+=piDisplay.evaluate(Pat['info']['pinfo']);
			 $('patientInfo').innerHTML=mlhtml;
		
}
showmorePinfo = function() {
		if ($('morepinfo').style.display=='none') {$('morepatientinfoimage').style.webkitTransform = 'rotate(90deg)'; $('morepinfo').style.display="block"} else {$('morepatientinfoimage').style.webkitTransform = 'rotate(0deg)'; $('morepinfo').style.display='none';}
}

showMoreInfo = function(mim,controller) {
		if ($(mim).style.display=='none') {$(controller).style.webkitTransform = 'rotate(90deg)'; $(mim).style.display="block"; $(controller).style.webkitTransform ='opacity(1)';} else {$(controller).style.webkitTransform = 'rotate(0deg)'; $(controller).style.webkitTransform ='opacity(0)'; $(mim).style.display='none';}
}

focusHormone = function(h) {
	// update symptom list:
	if (h != 'default' && h) {
	switchSymptomsListing(h);
	} else {
		// should update Patient.PM to include this
	new Ajax.Updater('symptomList', '/aa/symptomhistory.html', {parameters: {sortType: h, RxN: gRxN, ajax: 1}});
	}
	
}

createMedList = function()
	{
		$('curMedContainer').style.display='block';
		var mlhtml='';
		 Pat['instances']['all'].each( function(conv){ if (conv['RxNumber']) {mlhtml=cmHeader.evaluate(conv)+mlhtml;}});
		 $('curMedList').innerHTML=mlhtml;
	}
	
activateMedList = function(RxN) {
	$('curMedList').childElements().invoke('removeClassName', 'active');
//	each(function(s) {s.removeClassName('active');});
//	invoke('removeClassName', 'active');
	$('cm_'+RxN).addClassName('active');
	fillMedHistory(Pat['instances'][RxN]);
}

fillMedHistory = function(inst) {var mhhtml=''; var mhcnt=0; Pat['MedHistory'][inst].each( function(conv, index){

if (typeof Pat['MedHistory'][inst][index+1] != 'undefined') { 

	conv['showCombinedInfo']=Pat['MedHistory'][inst][index+1]['medicationCombined'] == 'no' ? 'showCombinedInfo' : ' ';
if (conv['showCombinedInfo']==null) {conv['showCombinedInfo']='showLastMed';}
} else {conv['showCombinedInfo']='showLastMed';}
mhcnt % 2 ? mhhtml+=cmListE.evaluate(conv) : mhhtml+=cmListO.evaluate(conv); mhcnt++;}); $('MedHistoryDisplay').innerHTML=(mhhtml||'<div class="cmMed even">No Hormone Therapy Reported</div>');}



% # need to have labels...
% my %tmp_v43=@{$dbh->selectcol_arrayref(q{SELECT keyName, question FROM aa_symHormones sH JOIN hhQuestions hQ ON sH.keyName=hQ.saveValue},{Columns => [1,2]})};
% my $labelsJson=JSON::XS::encode_json(\%tmp_v43);

	var symptomLabels = <% $labelsJson %>;
	var defLabels = {'E_deficiency': 'Estrogen Deficiency', 'Estriol_deficiency':'Estriol Deficiency', 'P_deficiency': 'Progesterone Deficiency',
					 'T_deficiency': 'Testosterone Deficiency', 'DHEA_deficiency':'DHEA Deficiency', 'Thyroid_deficiency': 'Thyroid Deficiency',
					 'Cortisol_deficiency': 'Cortisol Deficiency', 'Melatonin_deficiency':'Melatonin Deficiency', 'HGH_deficiency': 'HGH Deficiency',
					 'Pregnelone_deficiency': 'Pregnelone Deficiency', 'Aldosterone_deficiency':'Aldosterone Deficiency',
					 
					 'E_excess': 'Estrogen Dominance / Progesterone - Ratio', 'Estriol_excess':'Estriol Excess', 'P_excess': 'Progesterone Excess',
					 'T_excess': 'Testosterone Dominance / Estrogen - Ratio', 'DHEA_excess':'DHEA Excess', 'Thyroid_excess': 'Thyroid Excess',
					 'Cortisol_excess': 'Cortisol Excess', 'Melatonin_excess':'Melatonin Excess', 'HGH_excess': 'HGH Excess',
					 'Pregnelone_excess': 'Pregnelone Excess', 'Aldosterone_excess':'Aldosterone Excess'
					};
					
	var hs=new Array('E_deficiency','E_excess','P_deficiency','T_deficiency','T_excess', 'Cortisol_excess',  'Cortisol_deficiency',  'Thyroid_deficiency'); // 'P_excess', 'Thyroid_excess', has been removed
	var focusHormoneTimer;
		startHormoneTimer = function(def, tme) {// alert("Starting "+ def + " with " + tme);
		 focusHormoneTimer = setTimeout ( 'focusHormone("'+def+'")', tme || 1500 );}
        determineDefs = function(inst, pat) {
        	var dlist="<ul class='HormoneSummaryList'>";
			if (typeof Pat["HH"][inst]['SymptomsTotal'] != 'undefined' && typeof Pat["HH"][inst]['SymptomsDefTotal'] != 'undefined' ) {
        	for (var vv=0; vv <= hs.length; vv++ ) {	//hormones _e or _d
        		def=hs[vv];
				if (Pat["HH"][inst]['SymptomsTotal'][def] && Pat["HH"][inst]['SymptomsDefTotal'][def]) { 
        		var dP = parseInt(100*(
        		Pat["HH"][inst]['SymptomsDefTotal'][def] / Pat["HH"][inst]['SymptomsTotal'][def]
        		));
				if (dP > 20) 
				{
					dlist+="<li  onmouseover='startHormoneTimer(\""+def+"\", 550);' onmouseout='clearTimeout(focusHormoneTimer);'>"+defLabels[def]+": "+ '<span class="symptomPoints">'+ dP +"%</span>\n\t<ul>";
					for (var vvv=0; vvv <= Pat["HH"][inst]['order'][def].length; vvv++)
					{
						var key = Pat["HH"][inst]['order'][def][vvv];
						if (!key) {continue;}
						var defP = parseInt(100*(	Pat["HH"][inst]['Symptoms'][key][def]	/	Pat["HH"][inst]['SymptomsDefTotal'][def]	)	);
						if (defP >= 5) 
						{
							// do something
							dlist+="\n\t\t<li  class=\"" + Pat["HH"][inst]["Symptoms"][key]['keyValue']+ "\" onclick=\"triggerMoreInfo('"+key+"')\" class=''>"+symptomLabels[key]+'<span class="symptomPoints">('+defP+")</span> </li>\n";
							
						}
					}
					dlist+="\n\t</ul>\n</li>\n";
				}	//	else {dlist+="<li>"+defLabels[def]+" is OK</li>";}
	        } }
	        }
	        if (dlist == "<ul class='HormoneSummaryList'>") {
	        		if ($H(Pat["HH"][inst]['Symptoms']).keys().length) {	dlist +='<li style="color: green; font-weight:bold;" >No issues found <br/> based on reported symptoms</li>';	}
	        		else {	dlist+=	'<li style="color: black; font-weight:bold;" >No current symptoms reported</li>';	}
	        	
	        	} 
	        dlist+="\n</ul>";
	        return dlist; 
	        
        }

//XRX0000f0a94692




var symBEW=new Object();
symBEW['0']= 'symequal';
symBEW[ '1']= 'symbetter';
symBEW[ '-1']= 'symworse';

var swSymTR = new Template('<tr id="#{res}" class="symListingTR #{eoClass} #{symBEW}">\n'+
'			<th scope="row"  valign="top" class="column1 #{symptomslineClass}">\n' +
'			 <span class=" #{symBEW} #{otherClass}">#{labelText}</span></th>\n'+ '#{tds}\n</tr>');

var swSymTD = new Template('<td  class="#{symptomslineClass}"><span style="z-index:10;" class="symptomsImg #{symLevel}" >&#160;</span></td>');



switchSymptomsListing =
function (sorter) {
	if(!sorter) {sorter='default';}
	var swSymTRHTML='';
	// gRxN
	Pat['HH'][Pat['instances'][gRxN]]['order'][sorter].each(function(res, num) {
			swSymTDHTML='';
			$A(Pat['HH']).reverse().each(function(hh){ 
				nres=res;
				if (! hh['Symptoms'][nres] || ! hh['Symptoms'][nres]['keyValue'] ) {
				 	if ( hh['Symptoms']['currentSymptoms.' + nres]['keyValue']) { nres = 'currentSymptoms.' + nres; } else {return;} 
				 }
//				 console.log(nres + ': ' + hh['Symptoms'][nres]['keyValue']);
				 swSymTDHTML+=swSymTD.evaluate({'symptomslineClass': (num % 5 ? '' : 'symptomsline'), 'symLevel': hh['Symptoms'][nres]['keyValue']});
			});
			//gRxn
			swSymTRHTML+=swSymTR.evaluate({'res': res, 'eoClass': (num % 2 ? 'odd' : 'even'), 'symBEW': symBEW[
			Pat['HH'][
			(Pat['instances'][gRxN] || Pat['instances']['official'][0])]
			['Symptoms'][res]['relativeToLast']], 'symptomslineClass': (num % 5 ? '' : 'symptomsline'), 'otherClass': Pat['HH'][Pat['instances'][gRxN]]['Symptoms'][res]['otherClass'], 'labelText': symptomLabels[res], tds: swSymTDHTML});
	});
	$('symptomListingTBODY').update(swSymTRHTML);
};

	var reportEvalTextTemeplate_w_edit_logic = new Template('<li id="lid_#{logicID}"><span><div>#{alltext}</div><a href="/aa/evalLogic.html?lID=#{logicID}"  id="etid_#{logicID}" class="editLogicLink" onclick=""  >Edit Logic - #{logicID}</a></span></li>');
		var reportEvalTextTemeplate = new Template('<li id="lid_#{logicID}"><span><div>#{alltext}</div></span></li>');
	
	var reportEvalTherapyOuterTemeplate = new Template('<li id="lid_#{logicID}"><span>#{alltext}</span></li>');
	
	var reportEvalTherapyTemeplate = new Template('<span><div id="sid_#{logicID}" class="#{class}">#{alltext}</div></span>');
		var reportEvalTherapyTemeplate_w_edit_logic = new Template('<span><div id="sid_#{logicID}" class="#{class}">#{alltext}</div><a href="/aa/evalLogic.html?lID=#{logicID}"  id="etid_#{logicID}" class="editLogicLink" onclick=""  >Edit Logic - #{logicID}</a></span>');
	
	var reportEvalTextTemeplateInner = new Template('<span id="tid_#{textID}">#{HTML}</span>');





fillSection =
function (sect, append, inst, storeIn) {
	if (!inst) {inst=Pat['instances'][gRxN];}
if (!sect) {
// FIX
//defaults:

if (storeIn=="diagnosesArea") {fillSection(['currentIRMA.commentsDiagnoses'], 0, Pat['instances'][gRxN], 'diagnosesArea');}

if (storeIn=="TherapyEvaluation") {
	fillSection(['currentIRMA.commentsTherapyEvaluation'], 0, Pat['instances'][gRxN], "TherapyEvaluation");
	}

if (storeIn=="FurtherTherapyConsiderations") {
	fillSection(['currentIRMA.commentsDiagnosticConsiderations'],0, Pat['instances'][gRxN], 'FurtherTherapyConsiderations');}

if (storeIn=="AdditionalTesting") {
	fillSection(['currentIRMA.commentsAdditionalTesting'],0,Pat['instances'][gRxN], 'AdditionalTesting');
	}
if (storeIn=="SelfCareNotes") {
	fillSection(['currentIRMA.commentsDietLifestyle'],0,Pat['instances'][gRxN], 'SelfCareNotes');
	}
if (storeIn=="LabNotes"){
// WRONG!	fillSection(['currentIRMA.commentsDiagnoses','currentIRMA.commentsDiagnosticConsiderations'], 0, Pat['instances'][gRxN], "diagnosesArea");
	}
 return;} 
//	if (!sect) {$H(TS).keys().each(function(key, index) {fillSection(key, index);}); return;} 
	if (typeof Pat["HH"][inst]['TextLogic'][sect]=='undefined') 		{ 	return ;}
	var aHTML='';
	var sortedSections={
	'currentIRMA.commentsDiagnoses': ['CFS'],
	'currentIRMA.commentsTherapyEvaluation': ['generalLogic', 'Estradiol', 'Progesterone', 'Testosterone', 'DHEA', 'Cortisol', 'Adrenal', 'Thyroid', 'NTx'],
	'currentIRMA.commentsAdditionalTesting': ['Ovarian', 'Adrenal', 'Thyroid', 'NTx', 'Minerals'],
	'currentIRMA.commentsDietLifestyle': ['Diet', 'Bone', 'Lifestyle', 'OTC']

	};


	//hash

// CHANGE: we need to check Pat["HH"][inst]['TextLogic'][sect] and add to sortedSections
	if ( typeof sortedSections[sect] !='undefined') {
	
	sortedSections[sect].each(function(ss) {
		var sLI='';
	if (typeof Pat["HH"][inst]['TextLogic'][sect][ss]=='undefined') 		{ 	return ;}
		// var to store into

		//array
		Pat["HH"][inst]['TextLogic'][sect][ss].each(function(lid) {
			//hash
			$H(lid).each(function(tid) {
			var iHTML='';
			$A(tid.value).each(function(ttid) {
			// we just reused reportDiagnosesTemplate over and over...
			iHTML+=reportEvalTextTemeplateInner.evaluate(ttid);
				lid['logicID']=ttid['logicID'];
				lid['class']=ttid['class'];
				});
				lid['alltext']=iHTML;
		});
		sLI +=	reportEvalTherapyTemeplate.evaluate(lid) || '';

	});
		aHTML+=reportEvalTherapyOuterTemeplate.evaluate({alltext:sLI, logicID:ss});
	});
	}
		$H(Pat["HH"][inst]['TextLogic'][sect]).keys().sort().each(function(ss) {

			if ( typeof sortedSections[sect] !='undefined' && sortedSections[sect].indexOf(ss)  != -1) {return;}
		//array
		Pat["HH"][inst]['TextLogic'][sect][ss].each(function(lid) {
			//hash
			$H(lid).each(function(tid) {
			var iHTML='';
			$A(tid.value).each(function(ttid) {

			// we just reused reportDiagnosesTemplate over and over...
			iHTML+=reportEvalTextTemeplateInner.evaluate(ttid);
				lid['logicID']=ttid['logicID'];
				lid['class']=ttid['class'];
				});
				lid['alltext']=iHTML;

				//it can be found else where, but it is here too
		});

		aHTML+=reportEvalTextTemeplate.evaluate(lid);
	});
	});

		if(append) {$(storeIn).innerHTML+=aHTML;} else {$(storeIn).innerHTML=aHTML;}
};




fillEvaluationArea = function(sects, inst, notesonly) {
	// 2009-01-09:  notesonly option added to fill notes when New
	if (typeof Pat["HH"][inst] != 'undefined') {
		if (typeof Pat["HH"][inst]['EvaluationText'] != 'undefined' &&  (Pat['HH'][inst]['hhstatus'] != 'New' || notesonly)) {
			sects.each(function(secti) {
			if ($(secti) && typeof Pat["HH"][inst]['EvaluationText'][secti]!= 'undefined') {
					$(secti).update(Pat["HH"][inst]['EvaluationText'][secti]);
					} else 	{    fillSection(null, 0, Pat['instances'][gRxN], secti);}
		});
		} else { sects.each(function(secti) { fillSection(null, 0,  Pat['instances'][gRxN], secti)}); }
		return;
}
}


% if ($session{userType} eq 'cs') 	{
editLogicText = function(etem) {
 new Control.Modal(etem,{  
    overlayOpacity: 0.5,  
    className: 'modal',  
    iframe: false,
    position:  Element.viewportOffset(etem),
    fade: true  
});
return false;
}

% }


// ############################### ACTIVATE PATIENT $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
// ############################### ACTIVATE PATIENT $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

var activatep_w_pat = function() {
	if (!gRxN){gRxN=Pat['instances']['all'][0]['RxNumber'];}

$w($('pChartArea').className).each(function(cn) {$('pChartArea').removeClassName(cn);}	);
$('pChartArea').addClassName(Pat['instances']['all'][Pat['instances'][gRxN]]['qType']);
$('pChartArea').addClassName(Pat['instances']['all'][Pat['instances'][gRxN]]['hhstatus']);
displayPinfo();

//alert('test');
$A(document.getElementsByClassName('lp_tabs')).each(function(tab_group){      new Control.Tabs(tab_group, {activeClassName: 'current'});  }); 
//$A(document.getElementsByClassName('rlp_tabs')).each(function(tab_group){    console.log('need new tabs');  });  
createSwitchList();

% if ($session{userType} eq 'cs') {
if (gRxN) {
	if (Pat['HH'][Pat['instances'][gRxN]]['hhstatus'] =='New') {$('statusWarning').update();} else {$('statusWarning').update(Pat['HH'][Pat['instances'][gRxN]]['hhstatus']);}
	$('activeRxDisplay').innerHTML=gRxN;
	if ( typeof	Pat['HH'][Pat['instances'][gRxN]]	) {
	Pat['HH'][Pat['instances'][gRxN]]['followedProtocol'] =='no' ? $('followedProtocolNo').checked=true : $('followedProtocolYes').checked=true;
	}
	$('setEvalStatus').options[0].text=Pat['HH'][Pat['instances'][gRxN]]['hhstatus'];
	$('setEvalStatus').options[0].value=Pat['HH'][Pat['instances'][gRxN]]['hhstatus'];
	$('setEvalStatus').options[0].selected=true;
	$('evalRecommendationsDrags').innerHTML='';

	fillQS1Meds(); fillPatientReportedCMeds();
	fillEditMedHistory(Pat['instances'][gRxN]);
	setTimeout('activateIPEcm()', 1000);



		setTimeout('activateEvaluation()', 1300);
	new Ajax.Updater('rxTemplateArea', '/aa/rxtemplatearea.html', {parameters: {RxN: gRxN, patientID: Pat['info']['pinfo']['patientID'],  ajax: 1, qType: Pat['instances']['all'][Pat['instances'][gRxN]]['qType'], print: 1}, evalJS: 'force', evalScripts: true});


		setTimeout("if (Pat['info']['pinfo']['_hasRestoreNotes'] > 0) {$('showRestoreNotes').addClassName('hasRestoreNotes');} else {$('showRestoreNotes').removeClassName('hasRestoreNotes');}", 1900);

}
% }

//$('HormoneSummary').update(determineDefs(0,Pat));
$('HormoneSummary').innerHTML=determineDefs(Pat['instances'][gRxN],Pat);

createMedList();
activateMedList(gRxN || Pat['instances']['all'][0]['RxNumber']);
// 2009-01-09 - Added Diagnoses -> MedicationNotes
fillEvaluationArea(['diagnosesArea','TherapyEvaluation','SelfCareNotes','AdditionalTesting', 'FurtherTherapyConsiderations',
	'TherapyEvaluationNotes',
],Pat['instances'][gRxN]);


fillEvaluationArea([
	'LabNotes',
	'SymptomNotes',
	'HealthStatusNotes',
	'DiagnosesNotes',
	'MedicationsNotes'
],Pat['instances'][gRxN], 1); //notesonly

labChart();

// symDefChart(); fix graph, then re-enable someday 2011-05
cortisolChart();

new Ajax.Updater('symptomList', '/aa/symptomhistory.html', {parameters: {patientID: Pat['info']['pinfo']['patientID'], ajax: 1}});
new Ajax.Updater('LabArea', '/aa/labresults.html', {parameters: {RxN: gRxN, patientID: Pat['info']['pinfo']['patientID'],  ajax: 1}});

new Ajax.Updater('medicalHistoryArea', '/aa/medicalHistoryArea.html', {parameters: {RxN: gRxN, patientID: Pat['info']['pinfo']['patientID'],  ajax: 1, qType: Pat['instances']['all'][Pat['instances'][gRxN]]['qType']}});
% if ($session{userType} eq 'doctor') {
makeRxPad();
showrPs();
% }

fillImpInfo();
showRxHistory();
showOTCHistory();
if (Pat['HH'][Pat['instances'][gRxN]]['Recommendations']) {
$('recommendations_Area').update(showRecommendations(null, Pat['HH'][Pat['instances'][gRxN]]['Recommendations'])); activateRecommendationDragging();
}
refreshLabs();
};

// ############################### ACTIVATE PATIENT $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

activatePatient = function(patID, RxN, uPat) {
clearForNewPatient();

gRxN=RxN;

if (typeof uPat =='object' &&  typeof uPat['instances'] =='object') {
		console.log('uPat');
		console.log(uPat);
		activatep_w_pat();
} else {

		// get Pat here
		//	new Ajax.Request('/your/url', {  onSuccess: function(transport) {    transport.headerJSON  }});
		 new Ajax.Request("<% $session{userType} eq 'doctor' ? '/doctors/patients/patientChart.html' : '/shared/ajax/patientchart.html' %>", { parameters: {patientID: patID, RxN: RxN}, onSuccess: function(transport) {
		
		 Pat = transport.responseText.evalJSON();
		// $('jsonArea').innerHTML='<pre>'+transport.responseText+'</pre>';
		//	if (!gRxN){gRxN=Pat['info']['instances']['all'][Pat['info']['instances']['official'][0]]['RxNumber'];}
		//above doesnot work but should.  Below is a bad and temp fix
			activatep_w_pat();
		
		
		}});
}
$('pChartArea').style.display='block';
console.log("Pat['info']['pinfo']['_hasRestoreNotes']");



$('enterPatientName').clear();
$('enterPatientName').blur();
//alert(Pat["HH"][0]['Symptoms']['currentSymptoms.hotFlashes']['E_deficiency']);
}



// ############################### ACTIVATE PATIENT $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

refreshLabs = function() {
	new Ajax.Request('/aa/labresults.html', {parameters: {patientID: Pat['info']['pinfo']['patientID'], ajax: 1}, 
	onSuccess: function(transport) {
		fillFlotrOptions(transport.headerJSON, 'optionsTable_flotrcontainer3');
		$('LabArea').update(transport.responseText);
		}
		});
}
//importantinfo.html
Column2fillImpInfo = function() {
$('importantInfo').innerHTML='<table>' + 
'	<tr>' + 
'		<td>Number of Ovaries: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.numOvaries'] || 'n/a')+'</td>' + 
'		<td>Feels Excessive Stress: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.stress'] || 'n/a')+'</td>' + 
'	</tr>' + 
'	<tr>' + 
'		<td>Cycles in last year: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.periodsLastYear'] || 'n/a')+'</td>' + 
'		<td>Exercises: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.weeklyExercise'] || 'n/a')+'</td>' + 
'	</tr>' + 
'	<tr>' + 
'		<td>Still Cycling: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.menstruation'] || 'n/a')+'</td>' + 
'		<td>Ideal Weight: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.idealWeight'] || 'n/a')+'</td>' + 
'	</tr>' + 
'	<tr>' + 
'		<td>Hysterectomy: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.hysterectomy'] || 'n/a')+'</td>' + 
'		<td>Cholesterol Level: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.cholesterolLevel'] || 'n/a')+'</td>' + 
'	</tr>' + 
'	<tr>' + 
'		<td>Regular Cycles: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.regularMenstrualCycle'] || 'n/a')+'</td>' + 
'		<td>Smoker: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.smoker'] || 'n/a')+'</td>' + 
'	</tr>' + 
'	<tr>' + 
'		<td>Last Menstrual Period: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.lastPeriod'] || 'n/a')+'</td>' + 
'		<td>Alcohol Consumption: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.alcohol'] || 'n/a')+'</td>' + 
'	</tr>' + 
'	<tr>' + 
'		<td>Height: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.height'] || 'n/a')+'</td>' + 
'		<td>Weight: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.weight'] || 'n/a')+'</td>' + 
'	</tr>' + 
'</table>' ;
};
<%doc>
old-fillImpInfo = function() {
$('importantInfo').innerHTML=
typeof  Pat["HH"][Pat['instances'][gRxN]]['EvaluationText'] && Pat["HH"][Pat['instances'][gRxN]]['EvaluationText']['importantInfo'] && Pat["HH"][Pat['instances'][gRxN]]['hhstatus'] !='New'
?
Pat["HH"][Pat['instances'][gRxN]]['EvaluationText']['importantInfo']
:
'	<ul class="healthConditionsList">' + 
'		<li>Number of Ovaries: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.numOvaries'] || 'n/a')+'</li>' + 
'		<li>Cycles in last year: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.periodsLastYear'] || 'n/a')+'</li>' + 
'		<li>Still Cycling: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.menstruation'] || 'n/a')+'</li>' + 
'		<li>Regular Cycles: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.regularMenstrualCycle'] || 'n/a')+'</li>' + 
'		<li>Hysterectomy: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.hysterectomy'] || 'n/a')+'</li>' + 
'		<li>Last Menstrual Period: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.lastPeriod'] || 'n/a')+'</li>' + 

'		<li>Feels Excessive Stress: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.stress'] || 'n/a')+'</li>' + 
'		<li>Weight Bearing Exercise: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.weeklyExercise'] || 'n/a')+'</li>' + 
'		<li>Ideal Weight: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.idealWeight'] || 'n/a')+'</li>' + 
'		<li>Cholesterol Level: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.cholesterolLevel'] || 'n/a')+'</li>' + 
'		<li>Smoker: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.smoker'] || 'n/a')+'</li>' + 
'		<li>Alcohol Consumption (weekly): '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.alcohol'] || 'n/a')+'</li>' + 
'		<li>Height: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.height'] || 'n/a')+'</li>' +
'		<li>Weight: '+(Pat['HH'][Pat['instances'][gRxN] || 0]['Status']['currentIRMA.weight'] || 'n/a')+'</li>'
// ect
'	</ul>';
};
</%doc>
var obo=new Object();
// recommendations.html
var Recs=new Object();
var hder={
	'yes' : { 'yes' : 'If patient followed protocol & is still cycling', 'no': 'If patient followed protocol & is not cycling', 'NULL' : 'If patient followed protocol'},
	'no': {'yes': 'If patient did not follow protocol & is still cycling', 'no': 'If patient did not follow protocol & is not cycling', 'NULL': 'If patient did not follow protocol'},
	'NULL': {'yes': 'If patient is still cycling', 'no': 'If patient is not cycling'}
	};

var mthree= new Array('yes', 'no', 'NULL');

var recTemplate = new Template(
'<div id="drag_#{ID}#{sectName}" class="recommendationList andor_#{andor} recCategoryDiv_#{medCategory} recSystem_#{medSystem}" >\n' + 
'	<div class="medPreclass">#{preclass}</div><b  class="recCategory_#{medCategory} andor_#{andor} " >#{MedicationDN}</b> <span class="recMedMethod">#{Method}</span> #{Dose}#{DoseUnit}<div>#{Instructions}</div>\n' +
'</div>'
);

showRecommendations =

function (nrecs, crecs) {


var recHTML='';
var numOfRecs=0;
if (nrecs) {Pat.info.Recommendations=nrecs;}
if (typeof Pat != 'object' || !Pat["info"]["Recommendations"] ) {  return "No Recommendations for this Patient"; }
if (!crecs) {  crecs=Pat.info.Recommendations;}
 for (var o=0; o < mthree.length; o++) {
		var opts = mthree[o];
	 if (!crecs[opts]) { continue;}
	for (var v=0; v < mthree.length; v++) {
		var val = mthree[v];
	 if (!crecs[opts][val]) { continue;}
		var sectName=opts+'_'+val;
		 if (hder[opts][val]) {
			recHTML+='<h4  onclick="$(\'s'+sectName+'n\').show();">'+hder[opts][val]+' <span class="showControl" style="color:grey; font-size:smaller;" >(Click to view)</span></h4>\n';
 }
recHTML+='<div   id="s'+sectName+'n" style="display:'+ (hder[opts][val] ? 'none' : 'block')+'; " class="recSectionGroup" >\n';
	 if (hder[opts][val]) {	
		recHTML+='<div class="showControl" style="float:right; color: #428BA5; text-align:right; text-decoration: underline;" onclick="$(\''+sectName+'\').hide();">Hide</div>\n';
	 }
	var lastCat;
	var last_medSystem;
	for (var r=0; r < crecs[opts][val].length; r++) {
		var rec= crecs[opts][val][r];
		if (rec["ID"]) {
			if (last_medSystem != rec['medSystem'] && !last_medSystem) {recHTML+='\n<div class="medSystemBreak" id="rec_medSys_' + rec['medSystem'] + '">\n';}
			if (last_medSystem != rec['medSystem'] && last_medSystem) {recHTML+='\n</div>\n<!-- END of' + last_medSystem + ' -->\n\n<!-- BEGIN of' + rec['medSystem'] + ' -->\n<div class="medSystemBreak" id="rec_medSys_' + rec['medSystem'] + '">\n';}

			if (lastCat != rec['medCategory'] && lastCat) {recHTML+='<div class="recBreaker"></div>\n';}
			recHTML+=recTemplate.evaluate(rec);
			numOfRecs++;


			lastCat=rec['medCategory'];
			last_medSystem= rec['medSystem'] ;
		}

	}
	recHTML+='\t</div>\n<!-- FINAL: ' + last_medSystem + ' -->\n';
	recHTML+='</div>\n<!-- sect: s'+sectName+'n -->\n';
	last_medSystem=null; lastCat=null;
	}
	}	
	
	if (numOfRecs==0) {return '<div>No Recommendations at this time</div>';}
	return recHTML;
};


activateRecommendationDragging = function() {

	 for (var o=0; o < mthree.length; o++) {
		var opts = mthree[o];
		 if (!Pat["info"]["Recommendations"][opts]) { continue;}
		for (var v=0; v < mthree.length; v++) {
			var val = mthree[v];
			 if (!Pat["info"]["Recommendations"][opts][val]) { continue;}
			var sectName=opts+'_'+val;
			for (var r=0; r < Pat["info"]["Recommendations"][opts][val].length; r++) {
				var rec= Pat["info"]["Recommendations"][opts][val][r];
				Recs['drag_'+rec["ID"]+rec["sectName"]]=new Array(opts, val,r);
				if (rec["ID"]){new Draggable('drag_'+rec["ID"]+rec["sectName"],{revert:true});}
			}
		}
	}

}


//HoverBox  
var relative = new Control.Window($(document.body).down('[href=#hoverbox]'),{  
    position: 'relative',  
    hover: true,  
    offsetLeft: 75,  
    width: 175,  
    className: 'tooltip'  
});



//styled examples use the window factory for a shared set of behavior  
var window_factory = function(container,options){  
    var window_header = new Element('div',{  
        className: 'window_header'  
    });  
    var window_title = new Element('div',{  
        className: 'window_title'  
    });  
    var window_close = new Element('div',{  
        className: 'window_close'  
    });  
    var window_contents = new Element('div',{  
        className: 'window_contents'  
    });  
    var w = new Control.Window(container,Object.extend({  
        className: 'window',  
        closeOnClick: window_close,  
        draggable: window_header,  
        insertRemoteContentAt: window_contents,  
        afterOpen: function(){  
            window_title.update(container.readAttribute('title'))  
        }  
    },options || {}));  
    w.container.insert(window_header);  
    window_header.insert(window_title);  
    window_header.insert(window_close);  
    w.container.insert(window_contents);  
    return w;  
};  
  
  
 
// var styled_window_two = window_factory($('RecWindow'), {}); 
</script>
<script src="/js/scriptaculous/dragdropextra.js"></script>
<script>
//rxhistory.html
var rxhist_row = new Template('<div id="RxHistory_#{rxhist_num}" class="#{rwClass} rxDrop" ><span>#{DrugName}</span><span>#{QuantityDisp}</span><span>#{DatePrescribedADJ}</span><span class="RX-FillDates">#{FillDatesADJ}</span><span>#{RemainingRefills}</span><div class="sig">#{Sig}</div></div>');

var otchist_row = new Template('<div id="OTCHistory_#{rxhist_num}" class="#{rwClass} rxDrop" ><span>#{DrugName}</span><span>#{QuantityDisp}</span><span>#{DatePrescribedADJ}</span><span class="OTC-FillDates">#{FillDatesADJ}</span><span>#{RemainingRefills}</span><div class="sig">#{Sig}</div></div>');

// First make the table
//onmousedown="$(\'rxhistory\').style.overflow=\'hidden\';"

showRxHistory = function() {
var recHTML='';
//var dateRegExp = new RegExp( '(\d\d\d\d)\D(\d\d)\D(\d\d)', "gims");
if (typeof Pat != 'object' || !Pat["info"]["RxHistory"] || !Pat["info"]["RxHistory"][0]) {  $('rxhist_tbody').innerHTML='<div style="text-align:center;">No Prescriptions on file for this Patient</div>'; return "<tr><td colspan='6'>No Prescriptions on file for this Patient</td></tr>"; }
 for (var o=0; o < Pat["info"]["RxHistory"].length; o++) {
 	var rec=Pat["info"]["RxHistory"][o];
 	rec["rxhist_num"]=o;
 	rec["rwClass"]= o %2 ? 'odd' : 'even';

	rec['FillDatesADJ']=rec['FillDates'] || rec['DateFilled_formatted'];
	rec['DatePrescribedADJ']=rec['DatePrescribed_formatted'];

	recHTML+=rxhist_row.evaluate(rec);
 	}
 	$('rxhist_tbody').innerHTML=recHTML;


//	var rxhist_sb = new Control.ScrollBar('rxhist_content','rxhist_track');  
	rxAct();
 	return;
 	};
 	
rxAct = function() {
 for (var o=0; o < Pat["info"]["RxHistory"].length; o++) {
 					if ( $('RxHistory_'+o).id ) {new Draggable('RxHistory_'+o,{superghosting: true});}
 					else {alert(o);}
	}	

}


showOTCHistory = function() {
var recHTML='';
if (typeof Pat != 'object' || !Pat["info"]["OTCHistory"] || !Pat["info"]["OTCHistory"][0]) {  $('otcHist_tbody').innerHTML='<div style="text-align:center;">No OTCs on file for this Patient</div>'; return "<tr><td colspan='6'>No OTCs on file for this Patient</td></tr>"; }
 for (var o=0; o < Pat["info"]["OTCHistory"].length; o++) {
 	var rec=Pat["info"]["OTCHistory"][o];
 	rec["rxhist_num"]=o;
 	rec["rwClass"]= o %2 ? 'odd' : 'even';
 	
 	rec['FillDatesADJ']=rec['FillDates'] || rec['DateFilled_formatted'];
	rec['DatePrescribedADJ']=rec['DatePrescribed_formatted'];
	
 	recHTML+=otchist_row.evaluate(rec);
 	}
 	$('otcHist_tbody').innerHTML=recHTML;


//	var rxhist_sb = new Control.ScrollBar('rxhist_content','rxhist_track');  
//	rxAct();
 	return;
 	}
 	
</script>
% # rxpadheader.html
% if ($session{userType} eq 'doctor') {
<script>
var rxpDinfo=new Template('<h3 style="text-align:center;">#{doctorName}, #{doctorType}#{supervisingProviderStr}</h3>\n' +
'<p>\n' +
'<table cellSpacing="0" cellPadding="0" width="100%" border=0>\n' +
'	<tr>\n' +
'		<td rowSpan="2"><span style="font-size: smaller;">ADDRESS:</span></td>\n' +
'		<td rowSpan="2" class="field" nowrap width="100%"><span style="font-size: smaller;">\n' +
'			<div></div>#{doctorAddress1}</div>\n' +
'			<div>#{doctorAddress2}</div>\n' +
'			 #{doctorCity}, #{doctorState} #{doctorZIP}</span>\n' +
'		</td>\n' +
'		<td><span style="font-size: smaller;">PHONE/FAX:</span></td>\n' +
'		<td class="field" nowrap><span style="font-size: smaller;">#{doctorPhone1} / #{doctorFax}</span></td>\n' +
'	</tr><tr></tr>\n'
);
var rxpDAdds= new Template(
'	<tr>\n' +
'		<td rowSpan="2"><span style="font-size: smaller;">ADDRESS:</span></td>\n' +
'		<td rowSpan="2" class="field" nowrap width="100%"><span style="font-size: smaller;">\n' +
'			<div>#{line1}</div>\n' +
'			<div>#{line2}</div>\n' +
'			 #{city}, #{state} #{zip}</span>\n' +
'		</td>\n' +
'		<td><span style="font-size: smaller;">PHONE/FAX:</span></td>\n' +
'		<td class="field" nowrap><span style="font-size: smaller;">#{phone1} / #{fax}</span></td>\n' +
'	</tr><tr></tr>\n' 
);

var rxpDNPI =
new Template(
'<tr>\n' +
'<td nowrap><span style="font-size: smaller;">REG No:</span></td>\n' +
'<td class="field" nowrap><span style="font-size: smaller;">#{DEANum}</span></td>\n' +
'</tr>\n' +
'<tr>\n' +
'<td></td><td></td>\n' +
'<td nowrap><span style="font-size: smaller;">NPI No:</span></td>\n' +
'<td class="field" nowrap><span style="font-size: smaller;">#{NPI}</span></td>\n' +
'</tr>\n' +
'</table>\n' +
'<div class="hr"><span></span></div>\n'
);
var rxpPinfo=new Template(
'<table width="100%">\n' +
'<tr> <td width="150" rowSpan="3" style="padding-right:100px" vAlign="top"><img src="/images/rxlogo.gif" alt="Rx"></td> <td class="title">FOR</td> <td class="field" width="100%"><span style="font-weight:bold;">#{PatientFirstName} #{PatientLastName} (#{DOB})</span></td> <td class="title">DATE: </td> <td class="field"><span style="font-size:smaller;">'+todaydate+'</span></td> </tr>\n' +
'<tr> <td class="title">ADDRESS</td> <td class="field" colSpan="3">#{PatientAddressLine1} #{PatientAddressLine2} #{PatientCity}, #{PatientState} #{PatientZIP}</td> </tr>\n' +
'<tr> <td class="title">TELEPHONE</td> <td class="field" colSpan="3">H: #{PatientPhone} W: #{PatientWrokPhone}</td> </tr>\n' +
'<tr> <td>&nbsp;</td> </tr>\n' +
'</table>\n'
);

makeRxPad = function()
	{
		var padhtml = rxpDinfo.evaluate(Provider["info"]);
		[Provider["info"]["Addresses"]].each( function(conv){      if(typeof conv == 'object' && conv['city']) {padhtml+=rxpDAdds.evaluate(conv);}	});
		padhtml+=rxpDNPI.evaluate(Provider["info"]);
		padhtml+=rxpPinfo.evaluate(Pat["info"]['pinfo']);
		$('rxpadHeader').innerHTML=padhtml;
	}
</script>
% } # /If 'doctor'
% # rxpadbody:

<%init>
   my $cache = $m->cache; # save this line
   
   
#       first get the column memebers of Category
#       then get the methods for that category
#       next get the common dosages and dosing unit
#       maybe getrestoremeds.mas should do all that and assemble into an array of hashes of arrays/scalars
# deprecated:


# replaced with (06/2008) for autocomplete:
# but does not work - fix me!
	 my $mh = $cache->get('StaticRestoreMeds_AC');
   unless ($mh) {
my $meds=$dbh->selectcol_arrayref(q{SELECT  DISTINCT CONCAT(medDisplayName, ' ', medMethod) AS info,  CONCAT(medDisplayName, ' ', medMethod) AS value, id  FROM restoreMedications});
		for my $med (@$meds) {
						$mh->{$med}=$dbh->selectcol_arrayref(q{SELECT DISTINCT CONCAT(medCommonDosage, ' ', medDosageUnit) FROM restoreMedications WHERE CONCAT(medDisplayName, ' ', medMethod)=?}, undef,($med));
							}
       $cache->set('StaticRestoreMeds_AC', $mh, '1w');
       warn "setting cache for StaticRestoreMeds_AC for 1 week";
   }



# labtesting:
my $prods=$dbh->selectall_hashref(q{SELECT * FROM restoreProducts WHERE productAvailable='Y'}, ['productType', 'productCategory', 'productID']);


# Status Area:
  use Tie::Hash::Indexed;
  tie my %impInfs, 'Tie::Hash::Indexed';
 %impInfs = @{$dbh->selectcol_arrayref(q{SELECT DISTINCT saveValue, shortName FROM hhQuestions WHERE shortName IS NOT NULL ORDER BY sectionOrder, questionOrder},  { Columns=>[1,2] })}; # maybe should have qType

</%init>


% if ($session{userType} eq 'doctor' || $session{doctorMode} == 1) {
% ## Blank Rows


<script type="text/javascript">

var medList = $H(<% JSON::XS::encode_json($mh) %>);



var meds = new Array();
//<![CDATA[
var clone;var rows;var root;
cloneRPH = function(){
rows=document.getElementById('rxTable').getElementsByTagName('tr');
root=rows[0].parentNode;
clone=rows[rows.length-1].cloneNode(true);
var el = clone.getElementsByTagName('*');
for(var i=0;i<el.length;i++){
el[i].nodeName.toLowerCase()=='input'||el[i].nodeName.toLowerCase()=='textarea'?el[i].name=el[i].name.replace(/\d/g,rows.length-1):null;
el[i].nodeName.toLowerCase()=='div'||el[i].nodeName.toLowerCase()=='input'||el[i].nodeName.toLowerCase()=='textarea'?el[i].id=el[i].id.replace(/\d/g,rows.length-1):null;
}
}

var first_row = 1;
addRR = function(){
% #	if (first_row < 1) {
% #			document.getElementById('rxTable').style.display="inline";
% #	}
% #	else {
	root.appendChild(clone);cloneRPH();
	
new Autocompleter.Local('med'+first_row+'Medication', 'med'+first_row+'Medication_select', medList.keys(), { });

$('med'+first_row+'Medication').observe('blur', function()	{ 	 new Autocompleter.Local('med'+(first_row-1)+'Dose', 'med'+(first_row-1)+'DosageList', medList.get($('med'+(first_row-1)+'Medication').value), { });});
		first_row++;
}
onload = cloneRPH
//]]>
</script>

% ## END BLANK ROWS

<script language="javascript">


rmSpaces = function(inputstr) {
        var outputstr="";
        var regex=/[ \-+]/gi;
        outputstr=inputstr.replace(regex,"_");
return outputstr;}
</script>

% if (1) {
<script type="text/javascript" src="/js/validate.js"></script>
<script>
	verifyRxForm = function() {
		var errs=0;
		var inpts = document.getElementById('outputs').getElementsByTagName('input');
		for (var i=0;i<inpts.length;i++){
				if (inpts[i].type=='text') {
					//alert(inpts[i].name+': '+ inpts[i].type);
					if (!validatePresent(inpts[i])) { if(!errs) {inpts[i].focus();} errs += 1; }
//			errors=+1 unless inpts.value; inputs.class=error;
					}
				}
			

	//next check med0-??
	 var brows=document.getElementById('blank_row_area').getElementsByTagName('input');
	 		for (var i=0;i<brows.length;i++){
					if (brows[i].name.match(/^med\d+Medication$/) && brows[i].value) { 
//										check more...
							if (!validatePresent(brows[i++])) { errs += 1;}
							if (!validatePresent(brows[i++])) { errs += 1;}
							if (!validatePresent(brows[i++])) { errs += 1;}
							if (!validatePresent(brows[i++])) { errs += 1;} //I do not understand why I need a 4th...
					}
//					var re = new RegExp(/^med(\d+)Medication$/);
//					var m = re.exec(brows[i].name);
	 		}
			if (errs) { alert("Please enter values for the highlighted fields!"); return false; }
			else {

			
			//ready for next page:
			
			$('RxpageHeader').innerHTML='<h3>Verify Prescription</h3>';
			$('ptmsg').innerHTML='<h3>Please Verify the following:</h3>';
			new Effect.SlideUp('bottomrxpad');
			$('patientID_rxid').value=Pat['info']['pinfo']['patientID'];
			$('DoctorCode_rxid').value=Provider['info']['DoctorCode'];
			//$('bottomrxpad', 'bottomrxpad2').invoke('hide');
			new Ajax.Updater('bottomrxpad2', '/aa/rxverify.html', {parameters: $('prescribeoptions').serialize(true) });
			//new Effect.SlideDown('bottomrxpad2');
			$('bottomrxpad2').style.display='block';
			}
			
			return false;
	}
	finalCheck = function() {
	return VerifySigner();
	}
</script>
<script language="JavaScript">
var prompt = "";
VerifySigner = function()
{
    fvalue = document.rxpad.signer.value;  // entered words
        if (fvalue == "")
        {
            prompt = "You must electronically sign the prescription.";
            alert (prompt);
            return false;
        }
        else
        {
			//new Effect.SlideUp('bottomrxpad');
			//$('bottomrxpad', 'bottomrxpad2').invoke('hide');
			new Ajax.Updater('bottomrxpad2', '/aa/rxsubmit.html', {parameters: $('avrxpad').serialize(true) });
		 return true;
        }
}

resetForNewRx = function () {
			$('prescribeoptions').reset();
			new Effect.SlideDown('bottomrxpad');
			$('bottomrxpad2').style.display='none';
			$('choosePatients').style.display='block';
//			$('autocomplete').clear();
//			$('autocomplete').focus();
			$('RxpageHeader').innerHTML='<h3>Write Prescription</h3>';
			$('ptmsg').innerHTML='';


}
</script>


% }



% # rxpad after divs:

    <script>
    is_dose_other = function(f) {
			  //var ar = eval((f.category+"_"+f.medication+"_"+f.method).replace(/[\-\+ ]/g,"_"));
			  //for (var z=0; z<ar.length; z++) if (ar[z]==f.dose && ar[z]!="Other") return false;
			  return true;
			}
			

    medication = function(f) {
    		meds[f.id]=true;
            this.id = f.ID;
            this.category = f.medCategory;
            this.type = '';
            this.method = f.Method;
            this.dose = f.Dose + ' ' + f.DoseUnit;
			this.QtyVal = f.QtyVal ? f.QtyVal : '';
			this.RxNumber = f.RxNumber;
		this.instructions = f.Instructions ? f.Instructions : '';
            this.form = function () {
                    var QtyVal = document.getElementById( this.id+"Qty" ) ? document.getElementById( this.id+"Qty" ).value : this.QtyVal;
                    var RefillVal = document.getElementById( this.id+"Refill" ) ? document.getElementById( this.id+"Refill" ).value : '';
                    var InstructionsVal = document.getElementById( this.id+"Instructions" ) ? document.getElementById( this.id+"Instructions" ).value : this.instructions;
                    var dose_is_other = is_dose_other( {category:this.category,medication:this.type,method:this.method,dose:this.dose} );
                    var dose = ( this.dose == 'Other' ) ? '' : this.dose;

                    var res = '<tr>'
                    + '<td><a href="" onclick="rxpad.remove_medication(\''+this.id+'\'); return false;" title="click to remove this medication"><img src="/images/redremove.gif"></a></td>'
                    + '<td class="field"><input type="checkbox" name="'+this.id+'combine" /></td>'
                    + '<td class="field"><b>'+f.Medication+'</b><input type="hidden" name="med'+this.id+'Medication" value="'+f.Medication+'"></td>'
                    + '<td class="field"><input type="hidden" name="med'+this.id+'Method" value="'+f.method+'">'+f.Method+'</td>'
                     
                    + (dose_is_other ? (this.RxNumber ? '<td class="field"><input type="hidden" id="'+this.id+'Dose" name="med'+this.id+'"Dose value="'+f.dose+'">'+this.dose :'<td nowrap><input type="text" id="'+this.id+'Dose" name="med'+this.id+'Dose" value="'+this.dose+'" size="8">'):'<td class="field"><input type="hidden" id="'+this.id+'Dose" name="med'+this.id+'"Dose value="'+f.dose+'">'+this.dose)+'</td>'
                    + '<td><input type="text" id="'+this.id+'Qty" name="med'+this.id+'Qty" size="6" value="'+QtyVal+'"></td>'
                    + '<td><textarea id="'+this.id+'Instructions" name="med'+this.id+'Instructions" rows="2" cols="25" wrap="PHYSICAL">'+InstructionsVal+'</textarea></td>'
                    + '<td><input type="text" id="'+this.id+'Refill" name="med'+this.id+'Refill" size="6" value="'+RefillVal+'"></td>'
                    + '<input type="hidden" name="med'+this.id+'refillRxNumber" value="'+this.RxNumber+'" />\n';
                    + "</tr>\n";

                    return res;
            }
    }
    


    rxpad = function() {
            this.obj = document.getElementById( 'outputs' );
            this.items = new Array();

            this.add = function (f) {
            		// FIX FIX FIX FIX
            		// currently uses ID, Dose, Medication, Method
            		
                    var z = this.search( f.id );
                    if (z===false) this.items.push(new medication(f));
                    else {
                            this.items[z].method = f.method;
                            this.items[z].dose = f.dose;
                    }
                    this.update();
            }
            this.del = function ( id ) {
                    var z = this.search( id );
                    if ( z === false ) return;
                    this.items.splice( z, 1 );
                    this.update();
					meds[id]=false;
            }
            this.search = function ( val, field ) {
                    if ( !field ) field = 'id'; // default is searching by id
                    for ( var z=0; z<this.items.length; z++ ) if ( eval( 'this.items[z].'+field ) == val ) return z;
                    return false;
            }
            this.update = function () {
                    var str = '<table border="0" cellspacing="10" cellpadding="0" width="100%">'
                    + '<tr> <td /><td>Combine?</td><td colspan="2">Medication</td> <td>Dose</td> <td>Qty</td> <td>Instructions</td> <td>Refill</td> </tr>';
                    for (var z=0; z<this.items.length; z++)
                        str += this.items[z].form();
                    str += "</table>\n";
% #                    + '<p><input type="submit" value="submit" /></p>';
                    this.obj.innerHTML = this.items.length ? str : '';
            }
            this.remove_category = function(category) {
                    while ( ( z = this.search( category, 'category' ) ) !== false ) {
                            this.del( this.items[z].id );
                    }
            }
            // remove medication from RxPad
            this.remove_medication=function(id) {
					//var dropdown=document.getElementById(id);
					//dropdown.selectedIndex=0;
					// document.getElementById(id+"span").innerHTML="";
                    this.del(id);
					meds[id]=false;
            }


            // show/hide green arrow above RxPad
            this.green_arrow=document.getElementById('green_arrow');
            this.rxblank=document.getElementById('rxblank');
            this.show_garrow=function(show) {
                    this.green_arrow.style.display=(show!==false)?"":"none";
                    this.rxblank.style.background=(show!==false)?"#f2f2f2":'white';
                    this.green_arrow.style.position="absolute";
                    this.green_arrow.style.top=getTop(this.rxblank)+Math.round(this.rxblank.offsetHeight/2);
                    this.green_arrow.style.left=getLeft(this.rxblank)+Math.round(this.rxblank.offsetWidth/2);
            }
    }
    rxpad = new rxpad();
    </script>




<script type="text/javascript">
 // from the shopping cart demo
 Droppables.add('rxblank', {
 hoverclass: 'rxpadDrop',
   accept: ['recommendationList', 'rxDrop' ],
   onDrop: function(element) 
     { 
        dropRecommendationEvent(element);
        }
        });


</script>

<script language="javascript">
	//This is here to come after DOM is loaded:
	new Autocompleter.Local('med0Medication', 'med0Medication_select', medList.keys(), { });
$('med0Medication').observe('blur', function()	{ new Autocompleter.Local('med0Dose', 'med0DosageList', medList.get($('med0Medication').value), { });});
	// Above sets first autolist
	
	
    dropRecommendationEvent = function(drag) {
	    	var o = new Array();
	    	alert(drag.id);
	    	o = drag.id.match(/RxHistory_(\d+)/);
			if (o) {
				var rxh =Pat['info']['RxHistory'][o[1]];
				rxpad.add({ID:rxh['RxNumber']+o[1], medCategory: '', Method:'', Dose: '\t', DoseUnit: '', RxNumber: rxh['RxNumber'], Instructions: rxh['Sig'], QtyVal:rxh['QuantityDisp'], Medication: rxh['DrugName']			});
				return;
			}
           	else{ rxpad.add(Pat['info']['Recommendations'][Recs[drag.id][0]][Recs[drag.id][1]][Recs[drag.id][2]]); //ddID,f.method,f.dose,f.instructions
    			}
    }
    
    Pat['info']['Recommendations'][Recs['drag_103084ifp_ic'][0]][Recs['drag_103084ifp_ic'][1]][Recs['drag_103084ifp_ic'][2]]
    drag_103084ifp_ic
</script>
% } # /END if 'doctor'
% #


<script>

var Prods=<% JSON::XS::encode_json($prods) %>;
showmoreinfo = function(arw, div) { //remove this bad func!
	if ($(div).style.display=='none') {arw.style.webkitTransform = 'rotate(90deg)'; $(div).style.display="block"} else {arw.style.webkitTransform = 'rotate(0deg)'; $(div).style.display='none';}
}


addtolaborder = function(elm) {
	if(elm.checked) {Element.insert($('labOrderList'), "<div id='"+elm.id+"order'>"+$('labLabel'+elm.value).innerHTML+"</div>");}
	else {Element.remove($(elm.id+'order'));}
}
var rPCategoryTemplate = new Template('	<li ><span onclick="showmoreinfo($(\'more#{productCategoryID}\'), \'#{productCategoryID}\' );">#{productCategory} <img id="more#{productCategoryID}" style="max-height: 1.25em;" src="/images/aa_icons/next_icon.gif" alt="More...">	</span><ul id="#{productCategoryID}" style="display:none;">\n');
var rPProductTemplate = new Template('		<li><input type="checkbox" name="programOptions" value="#{productID}" id="labProduct#{productID}" onchange="addtolaborder(this);"/><label for="labProduct#{productID}" id="labLabel#{productID}">#{productDisplayName} - #{productPrice} <img src="/imgs/restoreProducts.png?productID=#{productID}" alt=" " style="display:none" /></label>	<div class="labProductDescription">#{productDescription}</div>			</li>\n');

showrPs = function() {
	var rPHTML='';
	$H(Prods).each(function(tpe) {
		$H(tpe.value).each(function(cat) {
			var cid=cat.key;
			cid.replace(/\s/, '_');
			rPHTML+=rPCategoryTemplate.evaluate({productCategory:cat.key, productCategoryID:cid});
			$H(cat.value).each(function(prod) {	rPHTML+=rPProductTemplate.evaluate(prod.value);	});
			rPHTML+="</ul></li>";
		});
	});
	$('rPList').innerHTML=rPHTML;
};







var createSwitchList = function()
	{
		var mlhtml='';
		 Pat['instances']['all'].each( function(conv){ if (conv['RxNumber']) {mlhtml=medhist_list_header.evaluate(conv)+mlhtml;}});
		 $('mha_tabs').innerHTML=mlhtml;

		var iilhtml='';
		 Pat['instances']['all'].each( function(conv){ if (conv['RxNumber']) {iilhtml=hlhist_list_header.evaluate(conv)+iilhtml;}});
		 $('ii_tabs').innerHTML=iilhtml;
	};






var updateMedHistList = function(c_rxn, elm) {		var cact=$(elm).adjacent('li.active')[0];   console.log(cact); Element.removeClassName(cact, 'active'); Element.addClassName(elm, 'active');  new Ajax.Updater('medicalHistoryArea', '/aa/medicalHistoryArea.html', {parameters: {RxN: c_rxn, patientID: Pat['info']['pinfo']['patientID'],  ajax: 1, qType: Pat['instances']['all'][Pat['instances'][c_rxn]]['qType']}});	};


var updateHLHistList = function(c_rxn, elm) {	var cact=$(elm).adjacent('li.active')[0];  Element.removeClassName(cact, 'active'); Element.addClassName(elm, 'active'); 	fillImpInfo(c_rxn); };


</script>
<script type="text/javascript">
submitThisText = function(f, div){
	new Ajax.Request('/shared/admin/updateText.html', {method: 'post', parameters: {'commentText': div.textContent, 'commentHTML': div.innerHTML, textID: f.textID.value} ,
		onSuccess: function(){ $('msg'+f.logicID.value+f.textID.value).innerHTML='<b style="color:green;">Form data saved!</b>' },
	    onFailure: function(){ $('msg'+logicID.value+f.textID.value).innerHTML='<b style="color:red;">UPDATE FAILED!</b>' }
	});


return false;
}

var activeReqs=new Object;
Ajax.Responders.register({ 
  onCreate: function(request){ 
  activeReqs[request.url]=request.url;
    var loading = $('loading'); 
    if (loading){ 
      loading.update('Loading (' + Ajax.activeRequestCount + ')<img src="/images/ajax-loader.gif" />') 
      // only appear the element if it is not already visible 
      if (!loading.visible() && Ajax.activeRequestCount) {
      //loading.appear({duration:0.5});
      loading.show();
      } 
    } 
  }, 
  onComplete: function(request){ 
  activeReqs[request.url]='completed';
    var loading = $('loading'); 
    if (loading){ 
      loading.update('Loading (' + Ajax.activeRequestCount + ')'); 
      if (Ajax.activeRequestCount == 0) {
//    alert(Ajax.activeRequestCount);
//loading.fade({duration:0.5}); 
		loading.hide();
    }
    }
  }
  }); 

</script>
