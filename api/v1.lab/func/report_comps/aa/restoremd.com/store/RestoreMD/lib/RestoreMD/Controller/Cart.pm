package RestoreMD::Controller::Cart;
use strict;
use warnings;
use Handel::Constants qw(:returnas :cart);
use Data::FormValidator 4.00;
use base 'Catalyst::Base';

our $DFV;

# Until this patch [hopefully] get's dumped into DFV 4.03, I've inlined the msgs
# method below with the following path applied to it:
#
#--- Results.pm.orig Wed Aug 31 22:27:27 2005
#+++ Results.pm  Wed Sep 14 17:40:28 2005
#@@ -584,7 +584,9 @@
#    if ($self->has_missing) {
#        my $missing = $self->missing;
#        for my $m (@$missing) {
#-           $msgs{$m} = _error_msg_fmt($profile{format},$profile{missing});
#+            $msgs{$m} = _error_msg_fmt($profile{format},
#+                (ref $profile{missing} eq 'HASH' ?
#+                    ($profile{missing}->{$m} || $profile{missing}->{default} || 'Missing') : $profile{missing}));
#        }
#    }

BEGIN {
    eval 'use Data::FormValidator 4.00';
    if (!$@) {
        #############################################################
        # This is here until the patch makes it to release
        #############################################################
        no warnings 'redefine';
        sub Data::FormValidator::Results::msgs {
            my $self = shift;
            my $controls = shift || {};
            if (defined $controls and ref $controls ne 'HASH') {
                die "$0: parameter passed to msgs must be a hash ref";
            }


            # Allow msgs to be called more than one to accumulate error messages
            $self->{msgs} ||= {};
            $self->{profile}{msgs} ||= {};
            $self->{msgs} = { %{ $self->{msgs} }, %$controls };

            # Legacy typo support.
            for my $href ($self->{msgs}, $self->{profile}{msgs}) {
                if (
                     (not defined $href->{invalid_separator})
                     &&  (defined $href->{invalid_seperator})
                 ) {
                    $href->{invalid_separator} = $href->{invalid_seperator};
                }
            }

            my %profile = (
                prefix  => '',
                missing => 'Missing',
                invalid => 'Invalid',
                invalid_separator => ' ',

                format  => '<span style="color:red;font-weight:bold"><span class="dfv_errors">* %s</span></span>',
                %{ $self->{msgs} },
                %{ $self->{profile}{msgs} },
            );


            my %msgs = ();

            # Add invalid messages to hash
                #  look at all the constraints, look up their messages (or provide a default)
                #  add field + formatted constraint message to hash
            if ($self->has_invalid) {
                my $invalid = $self->invalid;
                for my $i ( keys %$invalid ) {
                    $msgs{$i} = join $profile{invalid_separator}, map {
                        Data::FormValidator::Results::_error_msg_fmt($profile{format},($profile{constraints}{$_} || $profile{invalid}))
                        } @{ $invalid->{$i} };
                }
            }

            # Add missing messages, if any
            if ($self->has_missing) {
                my $missing = $self->missing;
                for my $m (@$missing) {
                    $msgs{$m} = Data::FormValidator::Results::_error_msg_fmt($profile{format},
                      (ref $profile{missing} eq 'HASH' ?
                          ($profile{missing}->{$m} || $profile{missing}->{default} || 'Missing') : $profile{missing}));
                }
            }

            my $msgs_ref = Data::FormValidator::Results::prefix_hash($profile{prefix},\%msgs);

            $msgs_ref->{ $profile{any_errors} } = 1 if defined $profile{any_errors};

            return $msgs_ref;
        }
        #############################################################

        $DFV = Data::FormValidator->new({
            cart_add => {
                required => [qw/sku quantity/],
                optional => [qw/price description/],
                field_filters => {
                    sku         => ['trim'],
                    quantity    => ['pos_integer'],
                    price       => ['pos_decimal'],
                    description => ['trim']
                },
                msgs => {
                    missing => {
                        default  => 'Field is blank!',
                        sku      => 'The SKU field is required',
                        quantity => 'The quantity field is required and must be a positive number'
                    },
                    format => '%s'
                }
            },
            cart_update => {
                required      => [qw/id quantity/],
                field_filters => {
                    id       => ['trim'],
                    quantity => ['pos_integer']
                },
                msgs => {
                    missing => {
                        default  => 'Field is blank!',
                        id       => 'The Id field is required for updating a cart item',
                        quantity => 'The quantity field is required and must be a positive number'
                    },
                    format => '%s'
                }
            },
            cart_delete => {
                required => ['id'],
                field_filters => {
                    id => ['trim']
                },
                msgs => {
                    missing => {
                        id => 'The Id field is required for delete a cart item'
                    },
                    format => '%s'
                }
            },
            cart_save => {
                required => [qw/name/],
                field_filters => {
                    name => ['trim']
                },
                msgs => {
                    missing => {
                        default => 'Field is blank',
                        name    => 'The Name field is required to save a cart'
                    },
                    format => '%s'
                }
            },
            cart_restore => {
                required => [qw/id mode/],
                field_filters => {
                    id   => ['trim'],
                    mode => ['digit']
                },
                msgs => {
                    missing => {
                        default => 'Field is blank',
                        id      => 'The id field is required for restoring saved cartds',
                        mode    => 'The mode field is required for restoring saved carts'
                    },
                    format => '%s'
                }
            },
            cart_destroy => {
                required => ['id'],
                field_filters => {
                    id => ['trim']
                },
                msgs => {
                    missing => {
                        id => 'The Id field is required for deleting saved carts'
                    },
                    format => '%s'
                }
            },
        });
    };
};

sub begin : Private {
    my ($self, $c) = @_;

    if (!$c->req->cookie('shopperid')) {
        $c->stash->{'shopperid'} = $c->model('Cart')->uuid;
        $c->res->cookies->{'shopperid'} = {value => $c->stash->{'shopperid'}, path => '/'};

        $c->stash->{'cart'} = $c->model('Cart')->new({
            shopper => $c->stash->{'shopperid'},
            type    => CART_TYPE_TEMP
        });
    } else {
        $c->stash->{'shopperid'} = $c->req->cookie('shopperid')->value;

        $c->stash->{'cart'} = $c->model('Cart')->load({
            shopper => $c->stash->{'shopperid'},
            type    => CART_TYPE_TEMP
        }, RETURNAS_ITERATOR)->first;

        if (!$c->stash->{'cart'}) {
            $c->stash->{'cart'} = $c->model('Cart')->new({
                shopper => $c->stash->{'shopperid'},
                type    => CART_TYPE_TEMP
            });
        };
    };
};

sub end : Private {
    my ($self, $c) = @_;

    $c->forward($c->view('TT'))
        unless ( $c->res->output || $c->res->body || ! $c->stash->{template} );
};

sub default : Private {
    my ($self, $c) = @_;

    $c->forward('view');
};

sub view : Local {
    my ($self, $c) = @_;

    $c->stash->{'template'} = 'cart/view.tt';
};

sub add : Local {
    my ($self, $c) = @_;
    my @messages;
    my $sku         = $c->request->param('sku');
    my $quantity    = $c->request->param('quantity');
    my $price       = $c->request->param('price');
    my $description = $c->request->param('description');

    if ($c->req->method eq 'POST') {
        my $results;

        if ($DFV) {
            $results = $DFV->check($c->req->parameters, 'cart_add');
        };

        if ($results || !$DFV) {
            if ($results) {
                $sku         = $results->valid('sku');
                $quantity    = $results->valid('quantity');
                $price       = $results->valid('price');
                $description = $results->valid('description');
            };

            $quantity ||= 1;

            eval {
                $c->stash->{'cart'}->add({
                    sku         => $sku,
                    quantity    => $quantity,
                    price       => $price,
                    description => $description
                });
            };
            if ($@) {
                push @messages, $@;
            };
        } else {
            push @messages, map {$_} values %{$results->msgs};
        };
    };

    if (scalar @messages) {
        $c->stash->{'template'} = 'cart/view.tt';
        $c->stash->{'messages'} = \@messages;
    } else {
        $c->res->redirect($c->req->base . 'cart/');
    };
};

sub update : Local {
    my ($self, $c) = @_;
    my @messages;
    my $id       = $c->request->param('id');
    my $quantity = $c->request->param('quantity');

    if ($c->req->method eq 'POST') {
        my $results;

        if ($DFV) {
            $results = $DFV->check($c->req->parameters, 'cart_update');
        };

        if ($results || !$DFV) {
            if ($results) {
                $id       = $results->valid('id');
                $quantity = $results->valid('quantity');
            };

            $quantity ||= 1;

            my $item = $c->stash->{'cart'}->items({
                id => $id
            });

            eval {
                if ($item) {
                    $item->quantity($quantity);
                };
            };
            if ($@) {
                push @messages, $@;
            };

            undef $item;
        } else {
            push @messages, map {$_} values %{$results->msgs};
        };
    };

    if (scalar @messages) {
        $c->stash->{'template'} = 'cart/view.tt';
        $c->stash->{'messages'} = \@messages;
    } else {
        $c->res->redirect($c->req->base . 'cart/');
    };
};

sub clear : Local {
    my ($self, $c) = @_;
    my @messages;

    if ($c->req->method eq 'POST') {
        eval {
            $c->stash->{'cart'}->clear;
        };
        if ($@) {
            push @messages, $@;
        };
    };

    if (scalar @messages) {
        $c->stash->{'template'} = 'cart/view.tt';
        $c->stash->{'messages'} = \@messages;
    } else {
        $c->res->redirect($c->req->base . 'cart/');
    };
};

sub delete : Local {
    my ($self, $c) = @_;
    my @messages;
    my $id = $c->request->param('id');

    if ($c->req->method eq 'POST') {
        my $results;

        if ($DFV) {
            $results = $DFV->check($c->req->parameters, 'cart_delete');
        };

        if ($results || !$DFV) {
            if ($results) {
                $id       = $results->valid('id');
            };

            eval {
                $c->stash->{'cart'}->delete({
                    id => $id
                });
            };
            if ($@) {
                push @messages, $@;
            };
        } else {
            push @messages, map {$_} values %{$results->msgs};
        };
    };

    if (scalar @messages) {
        $c->stash->{'template'} = 'cart/view.tt';
        $c->stash->{'messages'} = \@messages;
    } else {
        $c->res->redirect($c->req->base . 'cart/');
    };
};

sub empty : Local {
    my ($self, $c) = @_;

    $c->forward('clear');
};

sub list : Local {
    my ($self, $c) = @_;
    my @messages;

    eval {
        $c->stash->{'carts'} = $c->model('Cart')->load({
            shopper => $c->stash->{'shopperid'},
            type    => CART_TYPE_SAVED
        }, RETURNAS_ITERATOR);
    };
    if ($@) {
        push @messages, $@;

        $c->stash->{'messages'} = \@messages;
    };

    $c->stash->{'template'} = 'cart/list.tt';
};

sub save : Local {
    my ($self, $c) = @_;
    my @messages;
    my $name = $c->req->param('name');

    if ($c->req->method eq 'POST') {
        my $results;

        if ($DFV) {
            $results = $DFV->check($c->req->parameters, 'cart_save');
        };

        if ($results || !$DFV) {
            if ($results) {
                $name = $results->valid('name');
            };

            eval {
                $c->stash->{'cart'}->name($name);
                $c->stash->{'cart'}->save;
            };
            if ($@) {
                push @messages, $@;
            };

        } else {
            push @messages, map {$_} values %{$results->msgs};
        };
    };

    if (scalar @messages) {
        $c->stash->{'template'} = 'cart/view.tt';
        $c->stash->{'messages'} = \@messages;
    } else {
        $c->res->redirect($c->req->base . 'cart/');
    };
};

sub restore : Local {
    my ($self, $c) = @_;
    my @messages;
    my $id   = $c->req->param('id');
    my $mode = $c->req->param('mode') || CART_MODE_APPEND;

    if ($c->req->method eq 'POST') {
        my $results;

        if ($DFV) {
            $results = $DFV->check($c->req->parameters, 'cart_restore');
        };

        if ($results || !$DFV) {
            if ($results) {
                $id   = $results->valid('id');
                $mode = $results->valid('mode');
            };

            eval {
                $c->stash->{'cart'}->restore({id => $id}, $mode);
            };
            if ($@) {
                push @messages, $@;
            };

            $c->res->redirect($c->req->base . 'cart/');
        } else {
            push @messages, map {$_} values %{$results->msgs};
        };
    };

    if (scalar @messages) {
        $c->stash->{'messages'} = \@messages;
        $c->forward('list');
    } else {
        $c->res->redirect($c->req->base . 'cart/');
    };
};

sub destroy : Local {
    my ($self, $c) = @_;
    my @messages;
    my $id = $c->req->param('id');

    if ($c->req->method eq 'POST') {
        my $results;

        if ($DFV) {
            $results = $DFV->check($c->req->parameters, 'cart_destroy');
        };

        if ($results || !$DFV) {
            if ($results) {
                $id   = $results->valid('id');
            };

            eval {
                $c->model('Cart')->destroy({
                    id => $id
                });
            };
            if ($@) {
                push @messages, $@;
            };
        } else {
            push @messages, map {$_} values %{$results->msgs};
        };
    };

    if (scalar @messages) {
        $c->stash->{'messages'} = \@messages;
        $c->forward('list');
    } else {
        $c->res->redirect($c->req->base . 'cart/list/');
    };
};

1;
