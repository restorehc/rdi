use Test::More tests => 2;
use strict;
use warnings;

use_ok(Catalyst::Test, 'RestoreMD');
use_ok('RestoreMD::Controller::Checkout');
