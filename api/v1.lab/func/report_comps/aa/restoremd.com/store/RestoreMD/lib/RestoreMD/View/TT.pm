package RestoreMD::View::TT;

use strict;
use base 'Catalyst::View::TT';

=head1 NAME

RestoreMD::View::TT - Catalyst TT View

=head1 SYNOPSIS

See L<RestoreMD>

=head1 DESCRIPTION

Catalyst TT View.

=head1 AUTHOR

User &

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
