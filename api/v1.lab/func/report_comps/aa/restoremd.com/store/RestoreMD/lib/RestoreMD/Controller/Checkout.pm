package RestoreMD::Controller::Checkout;
use strict;
use warnings;
use Handel::Checkout;
use Handel::Constants qw(:returnas :order :cart :checkout);
use Data::FormValidator 4.00;
use HTML::FillInForm 1.04;
use base 'Catalyst::Base';

our $DFV;
our $FIF;

# Until this patch [hopefully] get's dumped into DFV 4.03, I've inlined the msgs
# method below with the following path applied to it:
#
#--- Results.pm.orig Wed Aug 31 22:27:27 2005
#+++ Results.pm  Wed Sep 14 17:40:28 2005
#@@ -584,7 +584,9 @@
#    if ($self->has_missing) {
#        my $missing = $self->missing;
#        for my $m (@$missing) {
#-           $msgs{$m} = _error_msg_fmt($profile{format},$profile{missing});
#+            $msgs{$m} = _error_msg_fmt($profile{format},
#+                (ref $profile{missing} eq 'HASH' ?
#+                    ($profile{missing}->{$m} || $profile{missing}->{default} || 'Missing') : $profile{missing}));
#        }
#    }

BEGIN {
    eval 'use HTML::FillInForm 1.04';
    if (!$@) {
        $FIF = HTML::FillInForm->new;
    };

    eval 'use Data::FormValidator 4.00';
    if (!$@) {
        #############################################################
        # This is here until the patch makes it to release
        #############################################################
        no warnings 'redefine';
        sub Data::FormValidator::Results::msgs {
            my $self = shift;
            my $controls = shift || {};
            if (defined $controls and ref $controls ne 'HASH') {
                die "$0: parameter passed to msgs must be a hash ref";
            }


            # Allow msgs to be called more than one to accumulate error messages
            $self->{msgs} ||= {};
            $self->{profile}{msgs} ||= {};
            $self->{msgs} = { %{ $self->{msgs} }, %$controls };

            # Legacy typo support.
            for my $href ($self->{msgs}, $self->{profile}{msgs}) {
                if (
                     (not defined $href->{invalid_separator})
                     &&  (defined $href->{invalid_seperator})
                 ) {
                    $href->{invalid_separator} = $href->{invalid_seperator};
                }
            }

            my %profile = (
                prefix  => '',
                missing => 'Missing',
                invalid => 'Invalid',
                invalid_separator => ' ',

                format  => '<span style="color:red;font-weight:bold"><span class="dfv_errors">* %s</span></span>',
                %{ $self->{msgs} },
                %{ $self->{profile}{msgs} },
            );


            my %msgs = ();

            # Add invalid messages to hash
                #  look at all the constraints, look up their messages (or provide a default)
                #  add field + formatted constraint message to hash
            if ($self->has_invalid) {
                my $invalid = $self->invalid;
                for my $i ( keys %$invalid ) {
                    $msgs{$i} = join $profile{invalid_separator}, map {
                        Data::FormValidator::Results::_error_msg_fmt($profile{format},($profile{constraints}{$_} || $profile{invalid}))
                        } @{ $invalid->{$i} };
                }
            }

            # Add missing messages, if any
            if ($self->has_missing) {
                my $missing = $self->missing;
                for my $m (@$missing) {
                    $msgs{$m} = Data::FormValidator::Results::_error_msg_fmt($profile{format},
                      (ref $profile{missing} eq 'HASH' ?
                          ($profile{missing}->{$m} || $profile{missing}->{default} || 'Missing') : $profile{missing}));
                }
            }

            my $msgs_ref = Data::FormValidator::Results::prefix_hash($profile{prefix},\%msgs);

            $msgs_ref->{ $profile{any_errors} } = 1 if defined $profile{any_errors};

            return $msgs_ref;
        }
        #############################################################

        $DFV = Data::FormValidator->new({
            checkout_update => {
                required => [qw/billtofirstname
                                billtolastname
                                billtoaddress1
                                billtocity
                                billtostate
                                billtozip
                                billtocountry
                                billtoemail
                                shiptofirstname
                                shiptolastname
                                shiptoaddress1
                                shiptocity
                                shiptostate
                                shiptozip
                                shiptocountry
                                shiptoemail/],
                field_filters => {
                    billtofirstname => ['trim'],
                    billtolastname  => ['trim'],
                    billtoaddress1  => ['trim'],
                    billtocity      => ['trim'],
                    billtostate     => ['trim'],
                    billtozip       => ['trim'],
                    billtocountry   => ['trim'],
                    billtoemail     => ['trim'],
                    shiptofirstname => ['trim'],
                    shiptolastname  => ['trim'],
                    shiptoaddress1  => ['trim'],
                    shiptocity      => ['trim'],
                    shiptostate     => ['trim'],
                    shiptozip       => ['trim'],
                    shiptocountry   => ['trim'],
                    shiptoemail     => ['trim'],
                },
                msgs => {
                    missing => {
                        default => 'Field is blank!',
                        billtofirstname => 'The bill to first name field is required',
                        billtolastname  => 'The bill to last name field is required',
                        billtoaddress1  => 'The bill to address line 1 field is required',
                        billtocity      => 'The bill to city field is required',
                        billtostate     => 'The bill to state field is required',
                        billtozip       => 'The bill to zip field is required',
                        billtocountry   => 'The bill to country field is required',
                        billtoemail     => 'The bill to email field is required',
                        shiptofirstname => 'The ship to first name field is required',
                        shiptolastname  => 'The ship to last name field is required',
                        shiptoaddress1  => 'The ship to address line field is required',
                        shiptocity      => 'The ship to city field is required',
                        shiptostate     => 'The ship to state field is required',
                        shiptozip       => 'The ship to zip field is required',
                        shiptocountry   => 'The ship to country field is required',
                        shiptoemail     => 'The ship to email field is required',
                    },
                    format => '%s'
                }
            },
            checkout_payment => {
                required => [qw/ccname cctype ccn ccm ccy/],
                field_filters => {
                    ccname => ['trim'],
                    cctype => ['trim'],
                    ccn    => ['trim'],
                    ccm    => ['digit'],
                    ccy    => ['digit'],
                    ccvn   => ['digit'],
                },
                msgs => {
                    missing => {
                        default => 'Field is blank!',
                        ccname  => 'The credit card name field is required',
                        cctype  => 'The credit card type field is required',
                        ccn     => 'The credit card number is required',
                        ccm     => 'The credit card expiration month field is required',
                        ccy     => 'The credit card expiration year is required',
                        ccvn    => 'THe credit card verification number is required'
                    },
                    format => '%s'
                }
            }
        });
    };
};

sub begin : Private {
    my ($self, $c) = @_;
    my $shopperid = $c->req->cookie('shopperid');

    if (!$shopperid || !$shopperid->value) {
        $c->res->redirect($c->req->base . 'cart/');
    } else {
        $shopperid = $shopperid->value;

        $c->stash->{'shopperid'} = $shopperid;

        my $cart = $c->model('Cart')->load({
            shopper => $shopperid,
            type    => CART_TYPE_TEMP
        }, RETURNAS_ITERATOR)->first;

        if (!$cart || !$cart->count) {
            $c->res->redirect($c->req->base . 'cart/');
        } else {
            my $order = $c->model('Orders')->load({
                shopper => $shopperid,
                type    => ORDER_TYPE_TEMP
            }, RETURNAS_ITERATOR)->first;

            if (!$order) {
                $order = $c->model('Orders')->new({
                    shopper => $shopperid,
                    cart    => $cart
                });

                my $checkout = Handel::Checkout->new({
                    order   => $order,
                    phases => 'CHECKOUT_PHASE_INITIALIZE'
                });

                $c->stash->{'order'} = $checkout->order;

                if ($checkout->process == CHECKOUT_STATUS_OK) {

                } else {
                    $c->stash->{'messages'} = $checkout->messages;
                };
            } else {
                $order->reconcile($cart);
                $c->stash->{'order'} = $order;
            };
        };
    };
};

sub end : Private {
    my ($self, $c) = @_;

    $c->forward($c->view('TT'))
        unless ( $c->res->output || $c->res->body || ! $c->stash->{template} );

    if ($c->req->method eq 'POST' && $c->stash->{'messages'} && $FIF) {
        ## Merge (erase) DFV Missing/Invalid fields from params before formfill
        my %missing = ();
        my %invalid = ();
        my $results = $c->stash->{'_dfv_results'};
        if (ref $results) {
            %missing = map {$_ => ''} ($results->missing);
            %invalid = map {$_ => ''} ($results->invalid);
        };

        my %parameters = (%{$c->req->parameters}, %missing, %invalid);

        $c->res->output(
            $FIF->fill(
                scalarref => \$c->response->{body},
                fdat => \%parameters
            )
        );
    };
};

sub default : Local {
    my ($self, $c) = @_;

    $c->forward('edit');
};

sub edit : Local {
    my ($self, $c) = @_;

    $c->stash->{'template'} = 'checkout/edit.tt';
};

sub update : Local {
    my ($self, $c) = @_;
    my @messages;

    if ($c->req->method eq 'POST') {
        my $results;

        if ($DFV) {
            $results = $DFV->check($c->req->parameters, 'checkout_update');
        };

        if ($results || !$DFV) {
            eval {
                my $order = $c->stash->{'order'};
                if (!$order) {
                    $c->res->redirect($c->req->base . 'cart/');
                } else {
                    foreach my $param ($c->req->param) {
                        $order->autoupdate(0);
                        if ($order->can($param)) {
                            if (($order->$param || '') ne ($c->req->param($param || ''))) {
                                $order->$param($c->req->param($param));
                            };
                        };
                        $order->autoupdate(1);
                        $order->update;
                    };

                    my $checkout = Handel::Checkout->new({
                        order  => $order,
                        phases => 'CHECKOUT_PHASE_VALIDATE'
                    });

                    if ($checkout->process == CHECKOUT_STATUS_OK) {

                    } else {
                        push @messages, 'Failed to update your bill to/ship to address!';
                        push @messages, @{$checkout->messages};
                    };
                };
            };
            if ($@) {
                push @messages, $@;
            };
        } else {
            $c->stash->{'_dfv_results'} = $results;
            push @messages, map {$_} values %{$results->msgs};
        };
        if (scalar @messages) {
            $c->stash->{'template'} = 'checkout/edit.tt';
            $c->stash->{'messages'} = \@messages;
        } else {
            $c->res->redirect($c->req->base . 'checkout/preview/');
        };
    };
};

sub preview : Local {
    my ($self, $c) = @_;

    $c->stash->{'template'} = 'checkout/preview.tt';
};

sub payment : Local {
    my ($self, $c) = @_;
    my @messages;

    $c->stash->{'template'} = 'checkout/payment.tt';

    if ($c->req->method eq 'POST') {
        my $results;

        if ($DFV) {
            $results = $DFV->check($c->req->parameters, 'checkout_payment');
        };

        if ($results || !$DFV) {
            eval {
                my $order = $c->stash->{'order'};
                if (!$order) {
                    $c->res->redirect($c->req->base . 'cart/');
                } else {
                    foreach my $param ($c->req->param) {
                        if ($order->can($param)) {
                            if (($order->$param || '') ne ($c->req->param($param || ''))) {
                                $order->$param($c->req->param($param));
                            };
                        };
                    };

                    my $checkout = Handel::Checkout->new({
                        order  => $order,
                        phases => 'CHECKOUT_PHASE_AUTHORIZE, CHECKOUT_PHASE_FINALIZE, CHECKOUT_PHASE_DELIVERY'
                    });

                    if ($checkout->process == CHECKOUT_STATUS_OK) {
                        eval {
                            $c->model('Cart')->destroy({
                                shopper => $c->stash->{'shopperid'},
                                type      => CART_TYPE_TEMP
                            });
                        };
                        $c->forward('complete');
                    } else {
                        push @messages, @{$checkout->messages};
                    };
                };
            };
            if ($@) {
                push @messages, $@;
            };
        } else {
            $c->stash->{'_dfv_results'} = $results;
            push @messages, map {$_} values %{$results->msgs};
        };
    };
    if (scalar @messages) {
        $c->stash->{'messages'} = \@messages;
    };
};

sub complete : Local {
    my ($self, $c) = @_;

    $c->stash->{'template'} = 'checkout/complete.tt';
};

1;
