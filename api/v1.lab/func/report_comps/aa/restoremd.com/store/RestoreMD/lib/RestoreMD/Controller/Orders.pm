package RestoreMD::Controller::Orders;
use strict;
use warnings;
use Handel::Constants qw(:returnas :order);
use Data::FormValidator 4.00;
use base 'Catalyst::Base';

our $DFV;

# Until this patch [hopefully] get's dumped into DFV 4.03, I've inlined the msgs
# method below with the following path applied to it:
#
#--- Results.pm.orig Wed Aug 31 22:27:27 2005
#+++ Results.pm  Wed Sep 14 17:40:28 2005
#@@ -584,7 +584,9 @@
#    if ($self->has_missing) {
#        my $missing = $self->missing;
#        for my $m (@$missing) {
#-           $msgs{$m} = _error_msg_fmt($profile{format},$profile{missing});
#+            $msgs{$m} = _error_msg_fmt($profile{format},
#+                (ref $profile{missing} eq 'HASH' ?
#+                    ($profile{missing}->{$m} || $profile{missing}->{default} || 'Missing') : $profile{missing}));
#        }
#    }

BEGIN {
    eval 'use Data::FormValidator 4.00';
    if (!$@) {
        #############################################################
        # This is here until the patch makes it to release
        #############################################################
        no warnings 'redefine';
        sub Data::FormValidator::Results::msgs {
            my $self = shift;
            my $controls = shift || {};
            if (defined $controls and ref $controls ne 'HASH') {
                die "$0: parameter passed to msgs must be a hash ref";
            }


            # Allow msgs to be called more than one to accumulate error messages
            $self->{msgs} ||= {};
            $self->{profile}{msgs} ||= {};
            $self->{msgs} = { %{ $self->{msgs} }, %$controls };

            # Legacy typo support.
            for my $href ($self->{msgs}, $self->{profile}{msgs}) {
                if (
                     (not defined $href->{invalid_separator})
                     &&  (defined $href->{invalid_seperator})
                 ) {
                    $href->{invalid_separator} = $href->{invalid_seperator};
                }
            }

            my %profile = (
                prefix  => '',
                missing => 'Missing',
                invalid => 'Invalid',
                invalid_separator => ' ',

                format  => '<span style="color:red;font-weight:bold"><span class="dfv_errors">* %s</span></span>',
                %{ $self->{msgs} },
                %{ $self->{profile}{msgs} },
            );


            my %msgs = ();

            # Add invalid messages to hash
                #  look at all the constraints, look up their messages (or provide a default)
                #  add field + formatted constraint message to hash
            if ($self->has_invalid) {
                my $invalid = $self->invalid;
                for my $i ( keys %$invalid ) {
                    $msgs{$i} = join $profile{invalid_separator}, map {
                        Data::FormValidator::Results::_error_msg_fmt($profile{format},($profile{constraints}{$_} || $profile{invalid}))
                        } @{ $invalid->{$i} };
                }
            }

            # Add missing messages, if any
            if ($self->has_missing) {
                my $missing = $self->missing;
                for my $m (@$missing) {
                    $msgs{$m} = Data::FormValidator::Results::_error_msg_fmt($profile{format},
                      (ref $profile{missing} eq 'HASH' ?
                          ($profile{missing}->{$m} || $profile{missing}->{default} || 'Missing') : $profile{missing}));
                }
            }

            my $msgs_ref = Data::FormValidator::Results::prefix_hash($profile{prefix},\%msgs);

            $msgs_ref->{ $profile{any_errors} } = 1 if defined $profile{any_errors};

            return $msgs_ref;
        }
        #############################################################

        $DFV = Data::FormValidator->new({
            orders_view    => {
                required => [qw/id/],
                field_filters => {
                    id => ['trim']
                },
                msgs => {
                    missing => {
                        default => 'Field is blank!',
                        id      => 'The order id is required to view an order'
                    },
                    format => '%s'
                }
            }
        });
    };
};

sub begin : Private {
    my ($self, $c) = @_;
    my $shopperid = $c->req->cookie('shopperid');

    if (!$shopperid || !$shopperid->value) {
        $c->res->redirect($c->req->base . 'cart/');
    } else {
        $shopperid = $shopperid->value;
        $c->stash->{'shopperid'} = $shopperid;
    };
};

sub end : Private {
    my ($self, $c) = @_;

    $c->forward($c->view('TT'))
        unless ( $c->res->output || $c->res->body || ! $c->stash->{template} );
};

sub default : Private {
    my ($self, $c) = @_;

    $c->forward('list');
};

sub view : Local {
    my ($self, $c, $id) = @_;
    my @messages;
    my $results;

    if ($DFV) {
        $results = $DFV->check({id => $id}, 'orders_view');
    };

    if ($results || !$DFV) {
        if ($results) {
            $id = $results->valid('id');
        };

        eval {
            $c->stash->{'order'} = $c->model('Orders')->load({
                shopper => $c->stash->{'shopperid'},
                type    => ORDER_TYPE_SAVED,
                id      => $id
            }, RETURNAS_ITERATOR)->first;
        };
        if ($@) {
            push @messages, $@;
        };
    } else {
        push @messages, map {$_} values %{$results->msgs};
    };

    if (scalar @messages) {
        $c->stash->{'messages'} = \@messages;
    };

    $c->stash->{'template'} = 'orders/view.tt';
};

sub list : Local {
    my ($self, $c) = @_;

    $c->stash->{'orders'} = $c->model('Orders')->load({
        shopper => $c->stash->{'shopperid'},
        type    => ORDER_TYPE_SAVED
    }, RETURNAS_ITERATOR);

    $c->stash->{'template'} = 'orders/list.tt';
};

1;
