var empty       = /^\s*$/
var ssn         = /^\d{3}\-?\d{2}\-?\d{4}$/;
var num3         = /^\d{3}$/;
var num2         = /^\d{2}$/;
var num4         = /^\d{4}$/;
var date        = /^\d{2}\/\d{2}\/\d{4}$/;
var date2        = /^\d{2}\/\d{2}\/\d{2}$/;
var zip         = /^\d{5}(-\d{4})?$/;
var state       = /^[A-Za-z]{2}$/;
var phone       = /^\+?[0-9 ()-]+[0-9]$/
var email       = /^[^@]+@[^@.]+\.[^@]*\w\w$/;
var password    = /^\w{6,}$/;

function setElementClass(elem, cls) {
        var element = document.getElementById(elem);
        elem.className = cls;
}
function validateRadio(element) {
    var valid = false;
        
        for (var x = 0;x < element.length; x++) {
                if (element[x].checked) {
            valid = true;
        }
        }
        return valid;
}
function validateList(element) {

    if(element.selectedIndex == 0) {
      setElementClass(element, "error");  
      return false;
    } else {
      setElementClass(element, "valid");  
      return true;
    }
}

function validatePresent(element) {
        
        if (empty.test(element.value)) {
      setElementClass(element, "error");  
      return false;
    } else {
      setElementClass(element, "valid");  
      return true;
    }
}

function validateOnRegExp(element, regex) {
        var re = eval(regex)
        
        if (re.test(element.value)) {
      setElementClass(element, "valid");  
      return true;
    } else {
      setElementClass(element, "error");  
      return false;
    }
}

function validateMatch(element, match) {

    if(element.value != match.value) {
      setElementClass(element, "error");  
      return false;
    } else {
      setElementClass(element, "valid");
      return true;
    }
}



/**
 * manager of errors messages
 */
function errors( id ) {

        // id of div for errors
        this.id = ( typeof id == 'undefined') ? 'errors' : id ;
        // list of errors messages
        this.msg = new Array();
        // number of errors
        this.num = 0;

        /**
         * add new error
         */
        this.add = function ( msg ) {
                if ( msg ) this.msg[this.msg.length] = msg;
                this.num++
        }
        /**
         * sent all messages to <div id="errors"></div> element
         */
        this.show = function () {
                var errors = document.getElementById( this.id );
                if ( this.num ) {
                        errors.innerHTML = this.msg.join( "<br><br>" );
                        errors.style.display = "block";
                } else this.hide();
        }
        /**
         * hide the div element with messages
         */
        this.hide = function () {
                document.getElementById( this.id ).style.display = "none";
        }
        
}
