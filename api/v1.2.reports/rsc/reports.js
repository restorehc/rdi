var show_metrics_charts= function() {
	$('#ma').html('<div id="monthly_rev_chart"></div><div id="new_provider_chart"></div><div id="new_patient_chart"></div><div id="state_rev"></div><div id="state_provs"></div>');


	$('#tmpl_reporting_comments').tmpl({'chart':'monthly_rev_chart'}).insertAfter('#monthly_rev_chart');
	$('#tmpl_reporting_comments').tmpl({'chart':'new_provider_chart'}).insertAfter('#new_provider_chart');
	$('#tmpl_reporting_comments').tmpl({'chart':'new_patient_chart'}).insertAfter('#new_patient_chart');
	$('#tmpl_reporting_comments').tmpl({'chart':'state_rev'}).insertAfter('#state_rev');
	$('#tmpl_reporting_comments').tmpl({'chart':'state_provs'}).insertAfter('#state_provs');
	
	$.postJSON('/api/v1.2.reports/get_metrics_commentary',		{}, function(msg) {
		if (msg.success) {
			var keys = Object.keys(msg.data);
			for (i = 0; i < keys.length; i++) {
				document.getElementById(keys[i] + '_display').innerHTML = msg.data[keys[i]].comment_html;
				document.getElementById(keys[i] + '_comment').innerHTML = msg.data[keys[i]].comment_html;
			}
		}
	});

	$.postJSON('/api/v1.2.reports/get_monthly_rx_revenue',		{}, function(msg) {	chart_monthly_rev(msg.data, '#monthly_rev_chart');				});
	$.postJSON('/api/v1.2.reports/get_monthly_new_providers',	{}, function(msg) {	chart_monthly_new_providers(msg.data, '#new_provider_chart');	});
	$.postJSON('/api/v1.2.reports/get_monthly_new_patients',	{}, function(msg) {	chart_monthly_new_patients(msg.data, '#new_patient_chart');		});

	$.postJSON('/api/v1.2.reports/get_monthly_state_data', {}, function(msg) {
		chart_monthly_state_rev(msg.data, '#state_rev');
		chart_monthly_state_provs(msg.data, '#state_provs');
	});
};

			var chart_monthly_rev = function(data, elm) {
				console.log(	data.totals );
        $(elm).highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Monthly Prescriptions'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: data.months || [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Money'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>&#36;{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [
			{
                name: 'Prior Year', /* s ## this 's' was commented out 2013.12.16@22:14 LC */
                data: $.map( data.yoy_totals, function(va, ix) {return parseInt(va);})
    
            },

            {
                name: 'Total',
                data: $.map( data.totals, function(va, ix) {return parseInt(va);})
    
            },
            {
                name: 'Third-Party',
                data: $.map( data.third_parties, function(va, ix) {return parseInt(va);})
    
            },
            
            ]
        });
    };






			var chart_monthly_new_providers = function(data, elm) {
        $(elm).highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'New Providers'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: data.months || [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'New Providers'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'New Providers',
                data: $.map( data.new_providers, function(va, ix) {return parseInt(va);}) || [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
    
            },
            
            ]
        });
    };


			var chart_monthly_new_patients = function(data, elm) {
        $(elm).highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'New Patients'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: data.months || [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'New Patients'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'New Patients',
                data: $.map( data.new_patients, function(va, ix) {return parseInt(va);}) || [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
    
            },
            {
                name: 'Dropped Patients',
                data: $.map( data.dropped_patients, function(va, ix) {return parseInt(va);}) || [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
    
            },
            {
                name: 'Dropped Patients w/active Rx',
                data: $.map( data.dropped_patients_w_active_rx, function(va, ix) {return parseInt(va);}) || [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
    
            },
            ]
        });
    };    
    
    
    
    
    
    
	var chart_monthly_state_rev = function(data, elm) {
        $(elm).highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Monthly Prescriptions'
            },
            subtitle: {
                text: 'By State'
            },
            xAxis: {
                categories: data.months },
            yAxis: {
                min: 0,
                title: {
                    text: 'Money'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>&#36;{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: 
                          $.map( data.states_data, function(va, ix) {
                          		var ret;
                          		var data_obj=$.map(va, function(da, di) { return   $.inArray(ix, data.states_list) == -1 ? 0 : parseInt(da.total_value); }  );
                          		var dobj = data_obj.map(function(v) {return (v==0) ? null : v;} );
                          		if (dobj.every(function(v) { return (v === null || v==0); })) {} else { return {name: ix, data:   dobj}; }
								 })
        })
    };


	var chart_monthly_state_provs = function(data, elm) {
        $(elm).highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Monthly Patient Count'
            },
            subtitle: {
                text: 'By State'
            },
            xAxis: {
                categories: data.months },
            yAxis: {
                min: 0,
                title: {
                    text: '# Patients'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: 
                          $.map( data.states_data, function(va, ix) {
                          		var ret;
                          		var data_obj=$.map(va, function(da, di) { return  $.inArray(ix, data.states_list) == -1 ? 0 : parseInt(da.pt_cnt); }  );
                          		var dobj = data_obj.map(function(v) {return (v==0) ? null : v;} );
                          		if (dobj.every(function(v) { return (v === null || v==0); })) {} else { return {name: ix, data:   dobj}; }
								 })
        })
    };