$(document).ready(function(){

		/**
		 * Very simple basic rule set for WYSIWYG
		 *
		 * Allows
		 *    <i>, <em>, <b>, <strong>, <p>, <div>, <a href="http://foo"></a>, <br>, <span>, <ol>, <ul>, <li>
		 *
		 * For a proper documentation of the format check advanced.js
		 */
		var wysihtml5ParserRules = {
			tags: {
				strong: { "rename_tag": "b" },
				b:      1,
				i:      1,
				em:     { "rename_tag": "i" },
				u:		1,
				br:     1,
				p:      1,
				ul:     1,
				li:     1
			}
		};


		$('body').on('submit', '.func_show_all_metrics' , function(elm) {
	    	elm.preventDefault();
			show_metrics_charts();
		});
		var new_width=$( document ).width() - 100 > 1400 ? '1400px' : '1200px'
		
		$('body > div').css('width', new_width);
		$('#rightside #ma_parent.contentcontainer').css('width', new_width);		



		$('body').on('dblclick', '#monthly_rev_chart_display', function() {
			$('#monthly_rev_chart_display').hide();
			$('#monthly_rev_chart_comments_form').show();
			var monthlyEditor = new wysihtml5.Editor("monthly_rev_chart_comment", {
				toolbar: "toolbar_monthly_rev_chart",
				parserRules: wysihtml5ParserRules
			});
		});

		$('body').on('dblclick', '#new_provider_chart_display', function() {
			$('#new_provider_chart_display').hide();
			$('#new_provider_chart_comments_form').show();
			var monthlyEditor = new wysihtml5.Editor("new_provider_chart_comment", {
				toolbar: "toolbar_new_provider_chart",
				parserRules: wysihtml5ParserRules
			});
		});

		$('body').on('dblclick', '#new_patient_chart_display', function() {
			$('#new_patient_chart_display').hide();
			$('#new_patient_chart_comments_form').show();
			var monthlyEditor = new wysihtml5.Editor("new_patient_chart_comment", {
				toolbar: "toolbar_new_patient_chart",
				parserRules: wysihtml5ParserRules
			});
		});

		$('body').on('dblclick', '#state_rev_display', function() {
			$('#state_rev_display').hide();
			$('#state_rev_comments_form').show();
			var monthlyEditor = new wysihtml5.Editor("state_rev_comment", {
				toolbar: "toolbar_state_rev",
				parserRules: wysihtml5ParserRules
			});
		});

		$('body').on('dblclick', '#state_provs_display', function() {
			$('#state_provs_display').hide();
			$('#state_provs_comments_form').show();
			var monthlyEditor = new wysihtml5.Editor("state_provs_comment", {
				toolbar: "toolbar_state_provs",
				parserRules: wysihtml5ParserRules
			});
		});

		$('body').on('submit', '.func_get_shipping_partners_report' , function(elm) {
	    	elm.preventDefault();
	    	var lo= $(this).serializeObject() ;
	 			$.postJSON('/api/v1.2.reports/get_shipping_partners_report', lo, function(msg) {				
						if (msg.success) {
						var tbl_body='<table style="padding:.25em; border: 1px solid black;">';
						$.each(msg.data, function() {
						        var tbl_row = "";
						        $.each(this, function(k , v) {
						            tbl_row += "<td>"+v+"</td>";
						        });
						        tbl_body += "<tr>"+tbl_row+"</tr>";                 
						    });
						    tbl_body += '</table>';
						   popup_new_window(tbl_body);

					   	}
	 				
	 			});
		});


		/* Comments Submission */
		$('body').on('submit', '.func_save_reporting_comments', function(elm) {
			elm.preventDefault();
			var comments = $(this).serializeObject();
			var tmp_post_data={comment_id: comments.chart, comment_html: comments.comment};
			$.postJSON('/api/v1.2.reports/set_metrics_commentary', tmp_post_data, function(msg) {
				if (msg.success) {		display_errs(data.errs);		}
			});
		});




});
