#!/usr/bin/perl
 use strict;
 use feature ':5.10'; # loads all features available in perl 5.10
 
  use PDF::Reuse;
 
  use strict;

  my $date ='TODAY';
  my $n      =  1;
  my $incr   = 14;
  my $infile = 'examples/customer.txt';

  prFile("blank_porting_form.pdf");

  prCompress(1);
  prFont('Arial');
  prForm("examples/sample-letter.pdf");

  open (my $fh, "<$infile") || die "Couldn't open $infile, $!\n aborts!\n";

  while (my $line = <$fh>)  {
      my $x = 60;
      my $y = 760;

      my ($first, $last, $street, $zipCode, $city, $country) = split(/,/, $line);
      last unless $country;

      prPage() if $n++ > 1 ;
      prText($x, $y, "$first $last");

      $y -= $incr;
      prText($x, $y, $street);

      $y -= $incr;
      prText($x, $y, $zipCode);
      prText(($x + 40), $y, $city);

      $y -= $incr;
      prText($x,   $y, $country);
      prText(60,  600, "Dear $first $last,");
      prText(400, 630, "Berlin, $date");
  }

  prEnd();
  close $fh;