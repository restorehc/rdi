/*jshint sub:true*/
/*jshint smarttabs:true */

$(document).ready(function() { /* delegates */
	$("body").on('submit', '.func_set_fax_status', function(e) {
		e.preventDefault();
		console.log('stopping submit');
		return false;
	});

	$('body').on(click_event, '.func_submit_set_fax_status', function(e) {
	 	var args = $('.func_set_fax_status').serializeObject();
	 	console.log(args);
	 	
	 	$.postJSON('/api/v1.3.faxes/set_fax_status', args, function(data) {
	 		$('.func_set_fax_status')[0].reset();
	 		console.log('Set Fax Status Server Response: ', data);
	 		if (data.data.success) {
		 		popup('Fax status has been successfully set.');
	 		} else {
		 		display_errs(data.errs);
	 		}
	 	});
	});
	$('body').on(click_event, '.func_view_fax_funcs', function(e) {
		$('#ma').empty();
		$('#tmpl_faxes_set_fax_status').tmpl({}).appendTo('#ma');
	});
});