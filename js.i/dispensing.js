 /* delegates */

$(document).ready(function(){

	$("body").on('click', '.func_dispensing_queue', function(href) { href.preventDefault(); 
		$.postJSON('/api/v1.2.disp/get_dispensing_queue', {}, function(data) {
			$('#ma').html('<table id="dispensing_queue_tbl"></table>');
			show_dq_data('#dispensing_queue_tbl', data.data);
		});
	});
	$("body").on('submit', '.func_show_disp_qe_details', function(href) { href.preventDefault(); 
		var linfo = $('form.func_show_disp_qe_details').serializeObject() ;
		$.postJSON('/api/v1.2.disp/update_dispensing_queue_entry', linfo, function(data) {
			$('#ma').html('<table id="dispensing_queue_tbl"></table>');
			show_dq_data('#dispensing_queue_tbl', data.data);
			display_errs(data.errs, null, function(){eval(data.errs[0].cb)} );
		});
	});
	$("body").on('click', '.func_filled_wo_imgs', function(href) { href.preventDefault(); 
		$.postJSON('/api/v1.2.disp/get_filled_wo_imgs', {}, function(data) {
			$('#ma').html('<table id="dispensing_queue_tbl"></table>');
			show_dq_data('#dispensing_queue_tbl', data.data);
		});
	});
	
	// Removes an item from the dispensing
	$("body").on('click', '.func_remove_from_cf_q', function(e) {
		// In the event the button tries to fire submit event
		e.preventDefault();
		
		var mydispid = $(this).data('dispid');
		console.log('id || disp.id: ', mydispid);
		$.postJSON('/api/v1.2.disp/clear_from_disp_queue', {id: mydispid}, function(data) {
			display_errs(data.errs, null, function(){eval(data.errs[0].cb)} );
			$.postJSON('/api/v1.2.disp/get_dispensing_queue', {}, function(data) {
				$('#ma').html('<table id="dispensing_queue_tbl"></table>');
				show_dq_data('#dispensing_queue_tbl', data.data);
			});
		});
	});

	$("body").on('submit', 'form.func_scan_for_disp_img', function(href) { href.preventDefault();
		var finfo = $('form.func_scan_for_disp_img').serializeObject();
		var fspnum = { sp_num: finfo.sp_num };

		$.postJSON('/api/v1.2.disp/get_dispensing_entry', fspnum, function(msg) {
			var jdata=msg.data;
			var keys=Object.keys(jdata);
			
			var fixdata = jdata.rx_data;

			if (typeof(fixdata) == "undefined" || fixdata == null) {
				popup("ScriptPro Number was not found.", {
					title: "Error - SP_NUM not found"
				});
			} else {
				fixdata["disp"] = new Object();
				
				keys.forEach(function(val, idx, ary) {
					if (val != "rx_data") {
						this[val] = jdata[val];
					}
				}, fixdata.disp);
			
			
				popup($('#tmpl_disp_qe_details_w_camera').tmpl(fixdata).html(), {
					title: fixdata.disp.PatientLastName + ", " + fixdata.disp.PatientFirstName + " - " + fixdata.rxi.rx_data.RxN,
					open: function() {
						$("#dialog-message .dispensing_details").tabs({
						    disabled: [],
						    selected: 1
						});
						$('input[value="Generate Label"]').button({
						    disabled: false
						});
						$('#cfdisp_capBtn').button({
						    disabled: false
						});
						$('input[value="Mark Filled"]').button({
						    disabled: false
						});
					}
				});
			}
		});
	});
/*
	$("body").on('submit', '.func_clear_from_disp_queue', function(href) { href.preventDefault(); 
		var dq_info = $(this).serializeObject() ;
		$.postJSON('/api/v1.2.disp/clear_from_disp_queue', dq_info, function(data) {
						display_errs(data.errs, null, function(){eval(data.errs[0].cb)} );
				});
		});
*/
	$("body").on('click', '.func_generate_dispensing_label', function(href) { href.preventDefault(); 
		var linfo = $('form.disp_qe_details').serializeObject() ;
		$.postJSON('/api/v1.2.disp/generate_cf_dispensing_label', linfo, function(data) {
//			popup_new_window(data.data.label_html);
			var w = window.open(null, 'cf_disp').document.writeln(data.data.label_html);
//			w.print(); 

			if (data.data.receipt_html) {
				var w_r = window.open(null, 'cf_disp_receipt').document.writeln(data.data.receipt_html);
//				w_r.print(); 
//				w_r.close();
			}
		});
	});
	
	
	$("body").on('click', '.func_show_dispensing_search_area', function(href) { href.preventDefault();
		// var tmp_scan table = $('#tmpl_ni_table_with_form').tmpl().html();
		
		$('#ma').html('');
		$('#tmpl_dispensing_search_area').tmpl().appendTo('#ma');
	});
	
});




var disp_queue_table;
var show_dq_data = function(tblid, dq_data) {
//	console.log(toTableArray( map_as_array(pharmco_data),  [  'TransactionRcd', 'rxn', 'PatientName' , 'Voided', 'drug_name', 'qty_dispensed', 'price', 'copay', 'date_prescribed', 'date_filled', 'insurance_plan', 'doctorName', 'PCO',  'doctorState', 'shipDate', 'trackingNum', 'data' ]));
	disp_queue_table=$(tblid).dataTable( {
	   "sPaginationType": "full_numbers",
		"sDom": '<"top"i>rpflt<"bottom"><"clear">',
		"bDestroy": true,
		"bJQueryUI": true,
		"bProcessing": true,
		"bDeferRender": true,
		"bRetrieve": true,
		'bAutoWidth':false,
		"aaData": toTableArray( map_as_array(dq_data),  [  'pname', 'rxn', 'drug_name', 'qty_disp', 'received', 'data' ]),
		"aoColumnDefs": [
			{ "sTitle": "Patient", "sClass": "center small" , "aTargets": [ 0 ] , "sWidth":"9em", "bSearchable": true, "bVisible": true },
			{ "sTitle": "RxN", "sClass": "center small clickable func_show_disp_qe_details" , "aTargets": [ 1 ] , "sWidth": "5em", "bSearchable": true },
			{ "sTitle": "Medication", "sClass": " long_med center small" , "aTargets": [ 2 ] , "sWidth": "8em", 'bVisible': true, "bSearchable": true },
			{ "sTitle": "QTY", "sClass": "center small" , "aTargets": [ 3 ] , "sWidth": "1em", "bSearchable": false },
			{ "sTitle": "Revceived", "sClass": "center small" , "aTargets": [ 4 ] , "sWidth": "1em", "bSearchable": false },
			{"bSearchable": true, "bVisible": false, "aTargets": [5]}
		],
		"OFF_aaSorting": [[4,'desc']],
	    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			//jQuery.data( nRow, 'patientID', jdata.patientID );
			nRow.onclick=function(telm) {
				var jdata=JSON.parse(aData[5]);
				jdata.rx_data["disp"] = new Object();
				console.log(jdata.rx_data);
				jdata.rowIndex=nRow.rowIndex; 
				jdata.rx_data;
				jdata.rx_data.id=jdata.id;
				
//				popup($('#tmpl_disp_qe_details').tmpl(jdata.rx_data).html());
				popup($('#tmpl_disp_qe_details_w_camera').tmpl(jdata.rx_data).html());
				
				$("#dialog-message .dispensing_details").tabs({
				    disabled: []
				});
				$('input[value="Generate Label"]').button({
				    disabled: false
				});
				$('#cfdisp_capBtn').button({
				    disabled: false
				});
				$('input[value="Mark Filled"]').button({
				    disabled: false
				});
			};
	    },
		"fnDrawCallback": function () {			}
	} );
};







/************************************************************************************************************
 * @Name: Show NonImage Data Queue Data (show_ni_dq_data)
 * @Description: Same as above, but for the /api/v1.2.disp/fet_filled_wo_imgs api call
 ************************************************************************************************************/

var show_ni_dq_data = function(tblid, dq_data) {
	disp_queue_table=$(tblid).dataTable( {
	    "sPaginationType": "full_numbers",
		"sDom": '<"top"i>rpflt<"bottom"><"clear">',
		"bDestroy": true,
		"bJQueryUI": true,
		"bProcessing": true,
		"bDeferRender": true,
		"bRetrieve": true,
		'bAutoWidth':false,
		"aaData": toTableArray( map_as_array(dq_data),  [  'pname', 'rxn', 'drug_name', 'qty_disp','data' ]),
		"aoColumnDefs": [
			{ "sTitle": "Patient", "sClass": "center small" , "aTargets": [ 0 ] , "sWidth":"9em", "bSearchable": true, "bVisible": true },
			{ "sTitle": "RxN", "sClass": "center small clickable func_show_disp_qe_details" , "aTargets": [ 1 ] , "sWidth": "5em", "bSearchable": true },
			{ "sTitle": "Medication", "sClass": " patient_name center small" , "aTargets": [ 2 ] , "sWidth": "9em", 'bVisible': true, "bSearchable": true },
			{ "sTitle": "QTY", "sClass": "center small" , "aTargets": [ 3 ] , "sWidth": "1em", "bSearchable": false },
			{"bSearchable": true, "bVisible": false, "aTargets": [4]}
		],
		"OFF_aaSorting": [[2,'desc']],
    	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {		},
		"fnDrawCallback": function () {			}
	} );
};








var tmp_exp_plus_formulas={
"MAC 1/FGLBC/C4586" : [
{"52372-0843-04" : { "name" : "Flurbiprofen", "qty" : 20 } },
{"52372-0909-10" : { "name" : "Baclofen", "qty" : 4 } },
{"52372-7316-05" : { "name" : "Cyclobenzaprine", "qty" : 2.264 } },
{"52372-0912-10" : { "name" : "Gabapentin", "qty" : 10 } },
{"52372-0873-04" : { "name" : "Lidocaine", "qty" : 5 } },
{"52372-0695-02" : { "name" : "Butylated HT", "qty" : 0.05 } },
{"52372-0692-04" : { "name" : "Sodium Metabisulfite", "qty" : 0.2 } },
{"52372-0682-04" : { "name" : "Vitamin E", "qty" : 1 } },
{"38779-1903-08" : { "name" : "Ethoxy Diglycol", "qty" : 20 } },
{"52372-0750-03" : { "name" : "LipoPen Ultra", "qty" : 37.486 } }
],

"MAC 2/FTCCB/C4585": [
{"52372-0843-04" : { "name" : "Flurbiprofen", "qty" : 20 } },
{"52372-0879-03" : { "name" : "Tramadol", "qty" : 5.69 } },
{"52372-8111-25" : { "name" : "Clonidine", "qty" : 0.2316 } },
{"52372-7316-05" : { "name" : "Cyclobenzaprine", "qty" : 4.528 } },
{"52372-0882-02" : { "name" : "Bupivacaine", "qty" : 3.357 } },
{"52372-0695-02" : { "name" : "Butylated HT", "qty" : 0.05 } },
{"52372-0692-04" : { "name" : "Sodium Metabisulfite", "qty" : 0.2 } },
{"52372-0682-04" : { "name" : "Vitamin E", "qty" : 1 } },
{"52372-0750-03" : { "name" : "LipoPen Ultra", "qty" : 64.9434 } }
],
"MAC 3/AGCLBD/C4910": [
{"52372-0763-04" : { "name" : "Amantadine", "qty" : 24.82 } },
{"52372-0909-10" : { "name" : "Baclofen", "qty" : 3 } },
{"52372-8111-25" : { "name" : "Clonidine", "qty" : 0.232 } },
{"52372-0910-10" : { "name" : "Diclofenac", "qty" : 2.148 } },
{"52372-0912-10" : { "name" : "Gabapentin", "qty" : 6 } },
{"52372-0873-04" : { "name" : "Lidocaine", "qty" : 2 } },
{"52372-0695-02" : { "name" : "Butylated HT", "qty" : 0.05 } },
{"52372-0692-04" : { "name" : "Sodium Metabisulfite", "qty" : 0.2 } },
{"52372-0682-04" : { "name" : "Vitamin E", "qty" : 1 } },
{"52372-0750-03" : { "name" : "LipoPen Ultra", "qty" : 60.55 } }
],
		
"MAC 4/FADGBA/C4907" : [
{"52372-0811-01" : { "name" : "Acyclovir", "qty" : 4 } },
{"52372-0907-01" : { "name" : "Amitriptyline", "qty" : 2.262 } },
{"51552-0579-02" : { "name" : "2-Deoxy-D-Glucose", "qty" : 2 } },
{"52372-0843-04" : { "name" : "Flurbiprofen", "qty" : 5 } },
{"52372-0912-10" : { "name" : "Gabapentin", "qty" : 10 } },
{"52372-0695-02" : { "name" : "Butylated HT", "qty" : 0.05 } },
{"52372-0692-04" : { "name" : "Sodium Metabisulfite", "qty" : 0.2 } },
{"52372-0682-04" : { "name" : "Vitamin E", "qty" : 1 } },
{"52372-0750-03" : { "name" : "LipoPen Ultra", "qty" : 64.369 } },
{"38779-1903-08" : { "name" : "Ethoxy Diglycol", "qty" : 10 } },
{"52372-0882-02" : { "name" : "Bupivacaine", "qty" : 1.119 } }
],

"MAC 5/MIFU/G1311" : [
{"52372-0716-04" : { "name" : "MUPIROCIN", "qty" : 5 } },
{"52372-0883-03" : { "name" : "ITRACONAZOLE", "qty" : 5 } },
{"52372-0858-03" : { "name" : "FLUTICASONE", "qty" : 1 } },
{"52372-0780-01" : { "name" : "UREA", "qty" : 40 } },
{"52372-0740-01" : { "name" : "PEG 300", "qty" : 5 } },
{"52372-0684-03" : { "name" : "CEPAPRO", "qty" : 44 } }
],

"MAC 6/MTTLP/C4911" : [
{"52372-0873-04" : { "name" : "LIDOCAINE", "qty" : 2 } },
{"52372-0715-01" : { "name" : "MELOXICAM", "qty" : 0.09 } },
{"52372-8080-10" : { "name" : "PRILOCAINE", "qty" : 2.33 } },
{"38779-2443-04" : { "name" : "TOPIRAMATE", "qty" : 1 } },
{"52372-0879-03" : { "name" : "TRAMADOL", "qty" : 0.285 } },
{"52372-0695-02" : { "name" : "BUTYLATED HT", "qty" : 0.05 } },
{"52372-0692-04" : { "name" : "SODIUM METABISULFITE", "qty" : 0.2 } },
{"52372-0682-04" : { "name" : "VITAMIN E", "qty" : 1 } },
{"52372-0750-03" : { "name" : "LIPOPEN ULTRA", "qty" : 93.045 } }
],

"MAC 7/PMPLMV/G1312" : [
{"38779-0216-04" : { "name" : "PHENYTOIN", "qty" : 5.435 } },
{"51927-3870-00" : { "name" : "MISOPROSTOL", "qty" : 0.24 } },
{"52372-8080-10" : { "name" : "PRILOCAINE", "qty" : 2.33 } },
{"52372-0654-01" : { "name" : "LEVOFLOXACIN", "qty" : 2 } },
{"52372-0761-01" : { "name" : "METRONIDAZOLE", "qty" : 2 } },
{"52372-7575-25" : { "name" : "VANCOMYCIN", "qty" : 5.185 } },
{"52372-0684-03" : { "name" : "CEPAPRO GEL", "qty" : 72.81 } },
{"52372-0740-01" : { "name" : "PEG 300", "qty" : 10 } }
],

"MAC 8/PMP/G1313": [
{"38779-0216-04" : { "name" : "PHENYTOIN", "qty" : 5.435 } },
{"51927-3870-00" : { "name" : "MISOPROSTOL", "qty" : 0.24 } },
{"52372-8080-10" : { "name" : "PRILOCAINE", "qty" : 2.33 } },
{"52372-0740-01" : { "name" : "PEG 300", "qty" : 10 } },
{"52372-0684-03" : { "name" : "CEPAPRO GEL", "qty" : 81.995 } }
],

"MAC 9/FLPPG/G1304": [
{"52372-0858-03" : { "name" : "FLUTICASONE PROPIONATE", "qty" : 1 } },
{"52372-0699-03" : { "name" : "LEVOCETIRIZINE", "qty" : 2 } },
{"52372-0899-02" : { "name" : "PENTOXIFYLLINE", "qty" : 0.5 } },
{"52372-8080-10" : { "name" : "PRILOCAINE", "qty" : 3 } },
{"52372-0912-10" : { "name" : "GABPENTIN", "qty" : 15 } },
{"38779-1903-08" : { "name" : "ETHOXY DIGLYCOL", "qty" : 10 } },
{"52372-0694-04" : { "name" : "SILOMAC", "qty" : 68.5 } }
],

"MAC 10/TTC/G1305": [
{"52372-0756-02" : { "name" : "TAMOXIFEN", "qty" : 0.1 } },
{"52372-0770-02" : { "name" : "TRANILAST", "qty" : 1 } },
{"52372-0782-01" : { "name" : "CAFFEINE", "qty" : 0.01 } },
{"52372-0887-02" : { "name" : "LIPOIC ACID", "qty" : 0.5 } },
{"51552-0103-08" : { "name" : "PROPYLENE GLYCOL", "qty" : 5 } },
{"52372-0694-04" : { "name" : "SILOMAC", "qty" : 93.3 } }
],

"MAC 11/BT/G1316": [
{"52372-0767-02" : { "name" : "BETAMETHASONE VALERATE", "qty" : 0.1 } },
{"52372-0770-02" : { "name" : "TRANILAST", "qty" : 1 } },
{"51552-0103-08" : { "name" : "PROPYLENE GLYCOL", "qty" : 5 } },
{"52372-0694-04" : { "name" : "SILOMAC", "qty" : 93.9 } }
]
};








/************************************** Currently all disabled restrictions are off. ****************************/
/* **************** DELEGATE ******************* */
$(document).on('change', '#cfdisp_beyond_use_date', function () {
    if ($('#cfdisp_beyond_use_date').val() != "" && $('#dispensing_details_lot_num').val().length > 3 && $('#dispensing_details_sp_num').val().length > 4) {
        $('input[value="Generate Label"]').button("enable");
    } else {
        $('input[value="Generate Label"]').button("enable");
    }
});
$(document).on('keypress', '#dispensing_details_lot_num', function () {
    if ($('#cfdisp_beyond_use_date').val() != "" && $('#dispensing_details_lot_num').val().length > 3 && $('#dispensing_details_sp_num').val().length > 4) {
        $('input[value="Generate Label"]').button("enable");
    } else {
        $('input[value="Generate Label"]').button("enable");
    }
});
$(document).on('keypress', '#dispensing_details_sp_num', function () {
    if ($('#cfdisp_beyond_use_date').val() != "" && $('#dispensing_details_lot_num').val().length > 3 && $('#dispensing_details_sp_num').val().length > 4) {
        $('input[value="Generate Label"]').button("enable");
    } else {
        $('input[value="Generate Label"]').button("enable");
    }
});
$(document).on('click', 'input[value="Generate Label"]', function () {
    /*
    $("#dialog-message .dispensing_details").tabs("option", "disabled", [2]);
    $('#cfdisp_capBtn').button("enable");
    */
    $('#confirmName').text($('.patient_name').text());
    $('#confirmQTY').text($('.med_qty').text());
    
    setTimeout(function () {
        $("#dialog-message .dispensing_details").tabs("option", "selected", 1)
    }, 100);
});
$(document).on('click', '#cfdisp_capBtn', function() {cam_cap();});


/* **************** CAM CAPTURE **************** */
/* function assumes NOT to allow camera skipping */
var cam_cap = function () {
    'use strict';
    $('#captureImg').hide();
    $('#streamVideo').show();

    var video = document.querySelector('video');
    var canvas = document.getElementById('captureCanvas');
    var img = document.getElementById('captureImg');
    var ctx = canvas.getContext('2d');

    var vW = 0;
    var vH = 0;

    var imgData = false;

    var constraints = {
        video: {
            mandatory: {
                "minHeight": "768",
                 "minWidth": "1024",
                 "minFrameRate": "10"
            }
        }
    };
    video.addEventListener('loadeddata', onLoadedData, false);
    function successCallback(stream) {
        window.stream = stream;
        // Set the source of the video element with the stream from the camera
        if (video.mozSrcObject !== undefined) {
            video.mozSrcObject = stream;
        } else {
            video.src = (window.URL && window.URL.createObjectURL(stream)) || stream;
        }
        video.play();
    }

    function errorCallback(error) {
        console.log('An error occurred: [CODE ' + error.code + ']');
        // Display a friendly "sorry" message to the user
    }

    function snapshot() {
//	console.log('snapshot(): Hiding #vidOverlay');
        $('#vidOverlay').hide();

	// This is what turns off the stream; most cameras will turn off when they no longer send the stream
      //  video.src = null;
 //       window.stream.stop();

        if (video.videoWidth != 0 && video.videoHeight != 0) {
	//console.log('snapshot(): drawing image to canvas');
            ctx.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
//console.log('snapshot(): video.src = null and stopping stream');
	    video.src = null;
	    window.stream.stop();
//console.log('snapshot(): setting image src to canvas data');
            imgData = canvas.toDataURL('image/png');
            img.src = imgData;
//console.log('snapshot(): setting input[hidden].val to imgData');
            $('input[name="rx_worksheet_img"]').val(imgData);
//console.log('snapshot(): hiding #streamVideo');
            $('#streamVideo').hide();
//console.log('snapshot(): showing #captureImg');
            $('#captureImg').show();

            if ($('input[name="rx_worksheet_img"]').val().length > 0) {
                $("#dialog-message .dispensing_details").tabs("option", "disabled", []);
                $('input[value="Mark Filled"]').button("enable");
                $('#captureImg').clone().appendTo('#confirmImg');
            } else {
                console.log('snapshot() -- There was an error; hidden input value was not set.');
            }
        }
    }

    function countDown(countFrom) {
        var n = countFrom;
        var i = n;
        $('#vidOverlay').show();

	img.src = "";

	$('#captureImg').hide();
	$('#streamVideo').show();

        function decI() {
            if (i <= 0) {

                $('#vidOverlay').text('0');
                clearInterval(a);
                snapshot();
            } else {

                $('#vidOverlay').text(i);
                i--;
            }
        }

        var a = setInterval(decI, 1000);
    }

    function onLoadedData() {
        console.log('onLoadedData() -- Called');

        vW = video.videoWidth;
        vH = video.videoHeight;

        console.log('Video dimensions: ' + vW + ' x ' + vH);

        canvas.width = vW;
        canvas.height = vH;
        img.height = vH;
        img.width = vW;

        countDown(5); // was 10 seconds
    }

    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
    window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL;

    // Call the getUserMedia method with our callback functions
    if (navigator.getUserMedia) {
        navigator.getUserMedia(constraints, successCallback, errorCallback);
    } else {
        console.log('Native web camera streaming (getUserMedia) not supported in this browser.');
        // Display a friendly "sorry" message to the user
    }
};
