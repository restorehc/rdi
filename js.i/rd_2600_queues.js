$(document).ready(function() {
	rd['m2600'] = {
		htmlInsert : '<div id="tb_qoptions" class=""><div id="tb_qstatus"></div><button id="tb_qcontrol" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"></button><select name="qsel"><option value="none">Queues</option><option value="100">CS</option><option value="200">Lab</option><option value="300">RX</option></select></div>',
		
		qcur: false,
		qsel: false,
		qstate: false,
	
		url: '/api/v1.2.i/toggle_queue',
	
		qfuncs : {
			applyStyle: function() {
				$('#tb_qoptions').css({'float': 'right', 'margin-top': '0', 'padding-bottom': '2', 'height': '22', 'width': '190', 'padding': '1'});
			    $('#tb_qoptions>*').css('display', 'inline-block');
			    $('#tb_qselect>*').css('display', 'inline-block');
			    $('#tb_qstat>*').css('display', 'inline-block');
			    
			    $('#tb_qcontrol').css('height', '16');
			    $('#tb_qstatus').css({
			        'display': 'inline-block',
			        'border': 'none',
			        'margin': '0px 0px -4px 3px',
			        'padding': '0',
			        'width': '16',
			        'height': '16',
			        'background-color': 'red',
			        'border-radius': '52%',
			        'box-shadow': 'inset -1px -1px 4.5px rgba(0, 0, 0, 0.5)',
			        'background-image': '-webkit-linear-gradient(-45deg, rgba(255,255,220,.3) 0%, transparent 100%)',
			        '-webkit-transition': 'background-color 0.2s ease-in-out',
			        'transition': 'background-color 0.2s ease-in-out'
			    });
			},
		    init: function(queue) {
		    	// Insert the Queue Controls into RD's topbar
		    	//$('ul#nav').append(rd.m2600.htmlInsert);
		    	$('div#loginout').after(rd.m2600.htmlInsert);
		    	rd.m2600.qfuncs.applyStyle();
		    	
		    	// Use a default queue if we're provided one
		        if (queue && (queue.match(/100/) || queue.match(/200/) || queue.match(/300/))) {
		            rd.m2600.qsel = queue;
		            $('option[value=' + queue + ']').prop('selected', true);
		        } else if (queue) {
			        // continue without a default set, but tell us
		            console.log('rd.m2600.qfuncs.init(): Requested queue is invalid.');
		            console.log(queue);
		        }
		        
		        // Do the jQueryUI jazz
		        $('#tb_qcontrol').button();
		        $('#tb_qcontrol').text('Toggle Queue');
		        
		        /*
		         * DELEGATES
		         */
		        $(document).on('change', '[name="qsel"]', function(e) {
		            console.log(e);
		            console.log(this.value);
		            var id = this.id;
		            rd.m2600.qsel = this.value;
		            
		            // Are we currently Logged in? (Do we need to log out elsewhere first?)
		            if (rd.m2600.qstate) {
		                // Log out of current queue, and give custom callback (don't forget to adjust qstate, etc)
		                rd.m2600.qfuncs.logOut(rd.m2600.qcur, function(data) {
		                    if (!data.errs || data.errs.length < 1 && data.data.Response.match(/success/i)) {
		                    	console.log(data.Message);
		                    	rd.m2600.qcur = false;
		                    	rd.m2600.qstate = false;
		                        rd.m2600.qfuncs.changeDot();
		                        
		                        // then log into selected queue
		                        rd.m2600.qfuncs.logIn();
		                        rd.m2600.qcur = rd.m2600.qsel;
		                        popup("You have successfully switched to queue " + rd.m2600.qcur);
		                    } else {
		                        // Perform error management and display
		                        console.log('logOff Errors:');
		                        console.log(data.errs);
		                        console.log(data.data.Message);
		                        if (data.data.Message.match(/Unable to remove interface/i) && data.data.Message.match(/not there/i)) {
				                    rd.m2600.qcur = false;
				                    rd.m2600.qstate = false;
				                    rd.m2600.qfuncs.changeDot();
				                    popup('You have already been remotely removed from queue. Switching...');
				                    rd.m2600.qfuncs.logIn();
			                    } else {
				                    popup(data.data.Response + ': ' + data.data.Message);
			                    }
		                    }
		                });
		            }
		        });
		        
		        $(document).on('click', '#tb_qcontrol', function(e) {
		        	rd.m2600.qfuncs.toggle();
/*
		            if (!rd.m2600.qstate) {
		                if (!rd.m2600.qsel) {
		                    popup('Please select a queue before logging in.');
		                } else {
		                    rd.m2600.qfuncs.logIn();
		                }
		            } else {
		                rd.m2600.qfuncs.logOut();
		            }
*/
		        });
		    },
		    logIn: function(q, cb) {
			    // If we're passed a queue, us it; otherwise, use qsel
		        var qu = q || rd.m2600.qsel;
		        console.log('logIn(): qu = ' + qu);
		        
		        // Are we already logged in? If so, notify and discontinue
		        if (rd.m2600.qstate) {
			        popup('You are already logged in.');
			        return;
		        }
		        
		        // If we're not passed a callback function, use this default behavior
		        if (!cb || typeof(cb) != 'function') {
		            $.postJSON(rd.m2600.url, {add: true, queue: qu}, function(data) {
			            console.log(data);
		                if ((!data.errs || data.errs.length < 1) && data.data.Response.match(/success/i)) {
		                	rd.m2600.qcur = qu;
		                	rd.m2600.qstate = true;
		                    rd.m2600.qfuncs.changeDot();
		                } else {
		                    // Perform error management and display
		                    console.log('logIn Errors:');
		                    console.log(data.errs);
		                    console.log(data.data.Message);
		                    popup(data.data.Response + ': ' + data.data.Message);
		                }
		            });
		        } else {	// otherwise, use the callback we're provided
		            $.postJSON(rd.m2600.url, {add: true, queue: qu}, cb);
		        }
		    },
		    logOut: function(q, cb) {    // Don't forget to add arguments
		    	// If we're passed a queue, us it; otherwise, use qcur
		        var qu = q || rd.m2600.qcur;
		        console.log('logOut(): qu = ' + qu);
		        
		        // Are we already logged out? If so, notify and discontinue
		        if (!rd.m2600.qstate) {
			        popup('You are already logged out.');
			        return;
		        }
		        
		        // If we're not passed a callback function, use this default behavior
		        if (!cb || typeof(cb) != 'function') {
		            $.postJSON(rd.m2600.url, {add: false, queue: qu}, function(data) {
		            	console.log(data);
		                if ((!data.errs || data.errs.length < 1) && data.data.Response.match(/success/i)) {
		                	rd.m2600.qcur = false;
		                	rd.m2600.qstate = false;
		                    rd.m2600.qfuncs.changeDot();
		                } else {
		                    // Perform error management and display
		                    console.log('logIn Errors:');
		                    console.log(data.errs);
		                    console.log(data.data.Message);
		                    if (data.data.Message.match(/Unable to remove interface/i) && data.data.Message.match(/not there/i)) {
			                    rd.m2600.qcur = false;
			                    rd.m2600.qstate = false;
			                    rd.m2600.qfuncs.changeDot();
			                    popup('You have already been remotely removed from queue.');
		                    } else {
			                    popup(data.data.Response + ': ' + data.data.Message);
		                    }
		                }
		            });
		        } else {	// otherwise, use the callback we're provided
		            $.postJSON(rd.m2600.url, {add: false, queue: qu}, cb);
		        }
		    },
		    toggle: function(q, cb) {
		    	console.log(q);
		    	console.log(rd.m2600.qcur);
		    	console.log(rd.m2600.qsel);
		    	console.log(defaultQ());
		    	
			    var qu = (q)?q:((rd.m2600.qsel)?rd.m2600.qsel:rd.m2600.qcur);
			    
			    if (!cb || typeof(cb) != 'function') {
				    $.postJSON(rd.m2600.url, {queue: qu, add: !rd.m2600.qstate}, function(data) {
					    console.log(data);
					    if ((!data.errs || data.errs.length < 1) && data.data.Response.match(/success/i)) {
						    rd.m2600.qcur = (data.data.GOOD === 1)?qu:false;
						    rd.m2600.qstate = (data.data.GOOD === 1 && data.data.Message == "Added interface to queue")?true:false;
						    rd.m2600.qfuncs.changeDot(rd.m2600.qstate);
						    popup(((rd.m2600.qstate)?"You have successfully joined":"You have succesfully left") + " queue " + rd.m2600.qcur);
					    } else if (data.data.Message == "Unable to add interface: Already there") {
						    rd.m2600.qcur = rd.m2600.qsel;
						    rd.m2600.qstate = true;
						    rd.m2600.qfuncs.changeDot(true);
						    popup("You are already logged into queue " + rd.m2600.qcur);
					    } else {
						    popup(data.data.Message);
					    }
				    });
			    }
		    },
		    changeDot: function(state) {
		    	// If we're passed a state, use it; otherwise use rd.m2600.qstate
			    var st = state || rd.m2600.qstate;
			    
		        if (st) {
		            $('#tb_qstatus').css('background-color', 'green');
		            $('#tb_qstatus').css('border-color', 'green');
		        } else {
		            $('#tb_qstatus').css('background-color', 'red');
		            $('#tb_qstatus').css('border-color', 'red');
		        }
		    }
		}
	};
	
	var defaultQ = function() {
		if (user_type == 'cs') {
			return '100';
		} else if (user_type == 'lab') {
			return '200';
		} else if (user_type == 'rx') {
			return '300';
		} else {
			return false;
		}
	};
	
	rd.m2600.qfuncs.init(defaultQ());
});