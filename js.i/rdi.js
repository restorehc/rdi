var popup_new_window=function(nhtml) {var w = window.open(); $(w.document.body).html(nhtml);};
var rpp=new Object();
var rpcp=new Object();
function redfine_rdi() {


$.postJSON('/api/v1.lab/get_restore_product_pricing', {}, function(data) { rpp=data.data;}); 
$.postJSON('/api/v1.lab/get_restore_provider_custom_panels', {}, function(data) { rpcp=data.data;}); 
rd.tv=new Object();
	$("body").off('submit', '#order_labs_saliva_frm');
	$("body").on('submit', '#order_labs_saliva_frm',   function(eml) {
								var lo= $(this).serializeObject() ;
								eml.preventDefault(); 
								$.postJSON('/api/v1.lab/update_order', lo, function(data) {
									if (data.data.success ) {
												fill_lo_form(data.data.lo);											
									}
								});
							});


	$("body").off('submit', '#order_labs_serum_frm');
	$("body").on('submit', '#order_labs_serum_frm',   function(eml) {
								var lo= $(this).serializeObject() ;
								eml.preventDefault(); 
								$.postJSON('/api/v1.lab/update_order', lo, function(data) {
									if (data.data.success ) {
												fill_lo_form(data.data.lo);											
									}
								});
							});


 activate_patientchart = function(pid, opts) {
	var tmp_lh_on=lh_on;
//	push_lh({loc: 'apc', self:[pid,opts] });
		select_patient(pid, plist[pid]);
		//,  disabled: true
		var pct = [
			{active: true, id:'tb_pc_sum', title: 'Summary', href: 'pc_summary'   },
			{active: false, id:'tb_pc_lab', title: 'Chart', href: 'pc_full_chart'   },
			{active: false, id:'tb_pc_hist', title: 'History', href: 'pc_history'   },
			{active: false, id:'tb_pc_records', title: 'Records', href: 'pc_history' , onclick_event: 'display_records(this); return false;'  }
		];
							activate_area({ title: 'Patient Chart', tabs: pct});		
							
							if (typeof opts !='undefined' &&  opts.active_tab !='undefined' && $('#' + opts.active_tab)) {		
									$( "#ma_parent" ).tabs('select', opts.active_tab);
							}

							
/*
			{active: false, id:'tb_pc_hh', title: 'Health History', href: 'pc_hh'   },
			{active: false, id:'tb_pc_notes', title: 'Notes', href: 'pc_notes'   },
			{active: false, id:'tb_pc_labs', title: 'Labs', href: 'pc_labs'   },
			{active: false, id:'tb_pc_symptoms', title: 'Symptoms', href: 'pc_sym'   },
			{active: false, id:'tb_pc_rxh', title: 'Rx/OTC History', href: 'rx_history_pc'   }
*/				

				// get patient obj
	if (typeof opts == 'object' && typeof opts.pobj == 'object') { console.log('object available'); activate_pc_wobj(opts.pobj, opts); } else {
	
		xhrs['pat_chart']=$.postJSON('/api/v1.i/getPatientObj', {patientid: pid}, function(data) {  
			rd.pobj=data['data'];
			var tmp_apw=activate_pc_wobj(rd.pobj, opts);
if (user_type == "rx") {
			$('#pc_new_recs').html('				<div id="rxp_toolset" class="ui-buttonset" >						<input type="radio" id="radio1" name="radio" class="patient_selected rx_entry  recommendation_popup ui-helper-hidden-accessible" ><label for="radio1" aria-pressed="false" class="patient_selected rx_entry   ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-state-hover" role="button"><span class="ui-button-text rxp_recommendations_button">Recommendations</span></label></div>');
		}
if (user_type == "lab" || 1) {

	$.postJSON('/api/v1.lab/get_testing_status', {patientid: pid}, function(data) {  
				$('#tmpl_testing_status').tmpl({tkresults: data.data.tkresults}).appendTo('.pc_testing_status');
		});
}

/*  	Communications:		*/

/*
				<div id="rx_notes_div"></div>
				<div id="sms_history_area"></div>
				<div id="call_history_area"></div>
*/
if (typeof rd.pobj.communications == "object") {
console.log(rd.pobj.communications);
		if (typeof rd.pobj.communications.rx_notes == 'object') {
			$('#tmpl_display_note_history').tmpl({notes: rd.pobj.communications.rx_notes}).appendTo('#rx_notes_div');
		}
		
		if (typeof rd.pobj.communications.sms == 'object') {
			$('#tmpl_display_sms_history').tmpl({smss: rd.pobj.communications.sms, sms_pt:  rd.pobj.info.pinfo.sms_nbr}).appendTo('#sms_history_area');
		}
		
		if (typeof rd.pobj.communications.calls == 'object') {
			$('#tmpl_display_call_history').tmpl({calls: rd.pobj.communications.calls}).appendTo('#call_history_area');
		}
 } 

		$('.visible_pt_id').html(rd.patientid);
	});

	
	
// because we are setting this 2x:
/* MOVING TO WOBJ
	lh_on = tmp_lh_on;
	push_lh({loc: 'pc', self:[pid,opts] });
*/
}
};






getRxHistory = function (pid) {

			var t_data=  JSON.stringify({data:{pID: pid, includeUnfilled: 1}});
		var nd = $.ajax({
		  url: '/api/v1.i/getRxHist_summary',
		  async: false,
		  type: 'post',
		  data: t_data,
		  dataType: 'json',
		  timeout: 5000
 		});
 		xhrs['rx_hist']=nd;

			var njd=jQuery.parseJSON(nd.responseText);
			rxpad.rx_history=njd.data;
			rxpad.has_rxhistory(typeof rxpad.rx_history == 'object' && rxpad.rx_history.length > 0);
		return rxpad.rx_history;
		} // This looks to be a culprit.


};



var activate_cs_addl_funcs = function() {
//	$('#ma').html('');
	activate_area('');
	$('#action_info_area').html('');
	$('#tmpl_cs_addl_funcs').tmpl({}).appendTo('#ma');
	$( "#cs_addl_funcs_tbs_div" ).tabs( "destroy" );
	$( "#cs_addl_funcs_tbs_div" ).tabs({
		select: function(event, ui) {  console.log(ui.panel.id); console.log(ui);}
	} );
};




var activate_lab_addl_funcs = function() {
//	$('#ma').html('');
activate_area('');   $('#action_info_area').html('');
	$('#tmpl_lab_addl_funcs').tmpl({}).appendTo('#ma');
	$( "#lab_addl_funcs_tbs_div" ).tabs( "destroy" ); $( "#lab_addl_funcs_tbs_div" ).tabs({
		         select: function(event, ui) {  console.log(ui.panel.id); console.log(ui);}
		         } );
};





var activate_lab_order_area = function() {
//	$('#ma').html('');
activate_area('');   $('#action_info_area').html('');
	$('#tmpl_lab_order_mgmt').tmpl().appendTo('#ma'); 
	
};


var fill_lo_form = function(lo) {
	 rd.tv.lo=lo;
	 var lo_type_sel=$('#lo_type_selection');
	lo_type_sel.accordion({ header: '.accordion_header',  collapsible: true, icons: false});
	var lo_accordion = (lo.lo.labType == 'saliva' ? 0 : 1 );
	
	if  ( lo_type_sel.accordion("option", "active" ) != lo_accordion) {	lo_type_sel.accordion("option", 'active', lo_accordion ); }
	$('#order_labs_saliva_frm input:checked').prop('checked', false); // uncheck all
	$('#order_labs_serum_frm input:checked').prop('checked', false); // uncheck all
	
	$.each(lo.loi, function(ix, li) { if (typeof $('#labp_' + (lo.lo.labType || '' )+ li.labType)[0] != "undefined") { $('#labp_' + (lo.lo.labType || '' ) + li.labType).prop('checked', true); }    });
	console.log(lo);
	$('#lo_pname').html(plist[lo.lo.patientID].first_name + ' ' + plist[lo.lo.patientID].last_name);
	$('#lo_patientid').html(plist[lo.lo.patientID].QS1CODE ? '' : lo.lo.patientID);
	$('#lo_DOB').html(plist[lo.lo.patientID].dob_f);
	if (typeof provider_list[lo.lo.DC] == "object") {$('#lo_provider').html(provider_list[lo.lo.DC].doctorName);} else {$('#lo_provider').html('--Provider not available  [' + lo.lo.DC + ']--');}
	$('#lo_id').html(lo.lo.id);
	
	if (1 || lo.lo.id < 9000000) {$('#lo_transfer').html('<form id="lo_transfer_frm"><input type="hidden" value="' + lo.lo.id + '" name="oldrid" /><input type="text" name="new_id" placeholder="Transfer To..." /> &nbsp; <input type="submit" /></form>"');} else {$('#lo_transfer').html('');}
	$('#lo_paid').html(lo.lo.paid);
	$('.lo_amt_paid').prop('value', lo.lo.paid);
	$('.lo_lab_id').prop('value', lo.lo.id);
	$('.lo_pid').prop('value', lo.lo.patientID);
	$('.lo_DC').prop('value', lo.lo.DC);
	$('#lo_paid').removeClass('has-paid');
	$('#lo_paid').removeClass('not-paid');
	parseFloat(lo.lo.paid) == 0 ? $('#lo_paid').addClass('not-paid') : $('#lo_paid').addClass('has-paid');
	$('#lo_price').html(parseFloat(lo.lo.price ) || 0);
	$('#lo_instruction_saved').html(lo.lo.instructions);
	lo_check_price();
	if ($('.lo_frm_rxn')[0]) {	$('.lo_frm_rxn').prop('value', lo.lo.id);	} else {
			$('#order_labs_saliva_frm').append('<input class="lo_frm_rxn" type="hidden" name="rxn" value="' + lo.lo.id  + '" />');	
			$('#order_labs_serum_frm').append('<input class="lo_frm_rxn" type="hidden" name="rxn" value="' + lo.lo.id  + '" />');	
			}
	$('#lo_LDU').html( lo.lo.LDU + ' by ' + lo.lo.updatedBy);
	$('#lo_statuses').html('');
	$('#tmpl_lab_order_statuses').tmpl(lo).appendTo('#lo_statuses');	
	$('#lo_notes_i').prop('value', lo.lo.instructions);
	$('#lo_scheduledDate').prop('value', lo.lo.scheduledDate);
	$('#lo_status_selected').prop('value', lo.lo.loStatus); 	$('#lo_status_selected').html(lo.lo.loStatus);
	if(lo.lo.bill_to == 'cc') {		$('#lab_bill_CC').prop('checked', true);}
	if(lo.lo.bill_to == '3rd') {	$('#lab_bill_3rd_party').prop('checked', true);}
	console.log('LO: ' + lo.lo.DC);
	$('.panel_custom').hide(); 
	if(typeof rpcp[lo.lo.DC] == "object" ) {
		$.each(rpcp[lo.lo.DC], function(rpid) {		console.log(rpid); $('.rpid_' + rpid).show();	});
		}
	if(typeof rpp[lo.lo.DC] == "object" ) {
		$('#lo_display .custom_rpp.labProductPrice').each(function() {
//				console.log($(this).dataset('lpid'));
//				console.log(rpp[lo.lo.DC]);
				if (typeof rpp[lo.lo.DC][$(this).dataset('lpid')] == "object") {
						$(this).html(rpp[lo.lo.DC][$(this).dataset('lpid')]['price']);
						}
				});
		$('#lo_display .custom_rpp').each(function() {
					if (typeof rpp[lo.lo.DC][$(this).dataset('lpid')] == "object") {
						$(this).dataset('price', rpp[lo.lo.DC][$(this).dataset('lpid') ]); 
						}
					});
	} else {
				$('#lo_display .custom_rpp').each(function() { var tmp_prc = $(this).dataset('sprice'); console.log(tmp_prc); if(tmp_prc) { $(this).dataset('price',tmp_prc);} });
	}

};


var lo_check_price= function() {
				var new_lo_price=0;
						$("#order_labs_saliva_frm input:checked").each(function(ix, vl) {new_lo_price= parseFloat(new_lo_price) + parseFloat($(vl).dataset('price')); });

						$("#order_labs_serum_frm input:checked").each(function(ix, vl) {new_lo_price= parseFloat(new_lo_price) + parseFloat($(vl).dataset('price')); });
						$('#lo_price').html(new_lo_price);
						return new_lo_price;
						};

 var lo_update_indicator = function(ind) { $('#lo_update_indicator').html('Updated!'); $('#lo_update_indicator').show(); $('#lo_update_indicator').fadeOut(2500, function() {}); };

 var lo_receive_kit = function(rxn) {
				$.postJSON('/api/v1.lab/get_kit_info', {rxn: rxn}, function(msg) {
						if (msg.success) {

							popup(	$('#tmpl_labs_receive_kit').tmpl({ls: msg.data}).html(), {html:true, title: 'Kit ' + msg.data.rxn , width: '600px'});
				$.postJSON('/api/v1.lab/get_lab_order', {rxn: rxn}, function(msg_lo) {
							if (msg_lo.success)  {
							 rd.tv.lo=msg_lo.data.lo;
										$('#tmpl_labs_review_kit').tmpl({id: rxn , slks: rd.tv.lo.lks, lttr: rd.tv.lo.lttr, salk: $.map($.grep(Object.keys(rd.tv.lo.lk), function(nn) {if (nn.match(/^(ss[1234]|su[5])$/) && parseInt(rd.tv.lo.lk[nn])==1) { console.log(nn + ': ' + rd.tv.lo.lk[nn]); return 1;} }), function(smpl) {return {smpl: smpl, stime: rd.tv.lo.lk[smpl + '_time'] }}).sort(function(a,b){return a.smpl > b.smpl;}) }	).appendTo('#lk_review_area');
								}
						});
					}
					});
};

 var display_records=function(eml){  
 
	 var form = document.createElement("form");
		    form.setAttribute("method", "post");
		    form.setAttribute("action", '/api/v1.lab/func/showRecords.html' );
		    form.setAttribute("target",  '_restoreRecords');
		        var hiddenField = document.createElement("input");		
		        hiddenField.setAttribute("name", "pid");
		        hiddenField.setAttribute("value", rd.patientid);
		        form.appendChild(hiddenField);
		        document.body.appendChild(form);    // Not entirely sure if this is necessary			
		        form.submit();
		        form.parentNode.removeChild(form);
		        return false;
	};
	
	var display_recent_scans = function() {
				$.postJSON('/api/v1.lab/view_scans', {}, function(msg) {
						if (msg.success) {
								$('#manual_scans_list').html('');
								$('#tmpl_manual_scans').tmpl({manualDisplay: msg.data}).appendTo('#manual_scans_list');
							}
						});
	};






	var display_enter_labs = function(rxn) {
			$.postJSON('/api/v1.lab/enter_results', {rxn: rxn, RxNumber: rxn}, function(msg) {
						if (msg.success) {
								$('#enter_lab_results_area').html(msg.data.html);
							}
						});
		
	};

var display_errs_inline = function (errs, ida,opts,cb) {
if (typeof errs && errs[0]) {
	if(! (typeof opts == 'object')) {opts=new Object(); }
	opts.title="Messages";
	if(typeof cb== "undefined") {cb="$(this).parent().parent().html('');"}
	opts.keep_dialog=true;
	$('#tmpl_display_errs').tmpl({errs:errs, cb: cb, opt: opts || {}}).appendTo('#' + ida);

		}
	};

var display_cmeds_area = function(lr) {
	// save msgs
	var scmmsgs=$('#cmed_msg_area').html();
	$.postJSON('/api/v1.lab/get_cmeds_area', lr, function(data) {
											$('#cmedsAreaDivo').html(data.data.shtml);
												$('#cmed_msg_area').html(scmmsgs);
											
										});
};






// }; //?


$(document).ready(function () {setTimeout('redfine_rdi();' , 2000);});




var activate_meta_search_on_elm = function (search_elm, activation_act) {
	$(search_elm).autocomplete({ 
						    minLength: 3,
		    source:function(event, ui) { 
		    
		    // lab, rx, patientName, DOB
		    
		    if (isNaN(event.term)) {

		    var re=new RegExp(event.term, 'i');
		    // work on regex that parse dates better
				var res_arr=$.map(plist, function(value, key) { 
				var pname=value['last_name']+ ', ' + value['first_name'] + ' - ' + value['dob_f'];
					  if (     re.test(pname) ) {
					  var l_pname=clean_hipaa_val(value['last_name'])+ ', ' + clean_hipaa_val(value['first_name']) + ' - ' + value['dob_f'];

					  return {label: l_pname, value: l_pname, pid: value.patientid, patientid: value.patientid,
								  action:function(aobj) { rd.patientid=aobj.patientid; activation_act(aobj.patientid);}	}; }	});
				ui(res_arr);
				} else {
				if (event.term.length < 5) {ui([]); return;}
				//number search
				var fd=event.term.toString().charAt(0);
				var re=new RegExp('^' + event.term);
				if (fd==8) {
					// 
					var res_arr=$.map(rlu, function(value, key) { if (     re.test(key) ) {  var h_l_n=clean_hipaa_val(plist[value]['last_name']);
					return [
					{
					 		label: key + ' - Chart ' + h_l_n + ' ' +plist[value]['dob_f'] , 
					 		value:key + ' - Chart ' + h_l_n + ' ' +plist[value]['dob_f'], 
					 		pid: value, 
					 	patientid: value,
					 	rxn: key,
								  action:function(aobj) { rd.patientid=aobj.patientid; activation_act(aobj.patientid);}	}]; } 	});
					
					
					}
				if (fd==4 || fd==6) {
				
					var res_arr=$.map(rlu, function(value, key) {  if (     re.test(key) ) {
						var h_l_n= clean_hipaa_val(plist[value]['last_name']);
					 return [{
					 	label: key + ' - Rx History', 
					 	value:key,
					 	pid: value, 
					 	patientid: value,
					 	rxn: key,
					 	action:function(aobj) { display_rxpad(aobj); popup_rx_hist(aobj)}	},
					 	{
					 		label: key + ' - Chart ' +h_l_n + ' ' +plist[value]['dob_f'] , 
					 		value:key + ' - Chart ' + h_l_n + ' ' +plist[value]['dob_f'] , 
					 		pid: value, 
					 	patientid: value,
					 	rxn: key,
					 		action:function(aobj) {console.log(aobj); activate_patientchart(aobj.patientid)}	}	]; } 	})	;
					
					}
				ui(res_arr);
				}
	},
		 select: function(event, ui) {
				if (ui.item.action) {ui.item.action(ui.item)} else { select_patient(ui.item.pid, ui.item); }
		 },
		 close: function(event, ui) {
			//	$(this).prop('value', ''); // this clear it
		 	// not sure what to do yet...
		 }
	});
				
				
};


/* custom ma_partner */
var ma_partner_table;
var show_ma_partner_data = function(tblid, ma_partner_data) {
//	console.log(toTableArray( map_as_array(ma_partner_data),  [  'TransactionRcd', 'rxn', 'PatientName' , 'Voided', 'drug_name', 'qty_dispensed', 'price', 'copay', 'date_prescribed', 'date_filled', 'insurance_plan', 'doctorName', 'PCO',  'doctorState', 'shipDate', 'trackingNum', 'data' ]));
	$(ma_partner_data).each(function(ix, elm){if (elm['note_text']){elm['note_text'].replace('|', '<p/>');} } );
	ma_partner_table=$(tblid).dataTable( {
	   "sPaginationType": "full_numbers",
"sDom": '<"top"irpflt><"bottom"><"clear">',
	"bDestroy": true,
	"bJQueryUI": true,
	"bProcessing": true,
		"bDeferRender": true,
	"bRetrieve": true,
	'bAutoWidth':false,
		"aaData": toTableArray( map_as_array(ma_partner_data),  [  'TransactionRcd', 'rxn', 'PatientName' , 'Voided', 'drug_name', 'qty_dispensed', 'price', 'copay', 'date_prescribed', 'date_filled', 'insurance_plan', 'doctorName', 'misc_id',  'doctorState', 'shipDate', 'trackingNum', 'note_text', 'data' ]),
		"aoColumnDefs": [
				{ "sTitle": "Trans#", "sClass": "center small" , "aTargets": [ 0 ] , "sWidth": "3em", "bSearchable": true, "bVisible": false},
			{ "sTitle": "RxN", "sClass": "center small" , "aTargets": [ 1 ] , "sWidth": "4em", "bSearchable": true},
				{ "sTitle": "Patient", "sClass": " patient_name center small" , "aTargets": [ 2 ] , "sWidth": "6em", 'bVisible': true, "bSearchable": true},
				{ "sTitle": "Voided", "sClass": "center small" , "aTargets": [ 3 ] , "sWidth": "4em",  'bVisible': false, "bSearchable": false},
			{ "sTitle": "Drug", "sClass": "center rsmall" , "aTargets": [ 4 ] , "sWidth": "3em", "bSearchable": false},
			{ "sTitle": "QTY", "sClass": "center small" , "aTargets": [ 5 ] , "sWidth": "1em", "bSearchable": false},
			{ "sTitle": "Price", "sClass": "center small" , "aTargets": [ 6 ] , "sWidth": "2em", "bSearchable": false},
				{ "sTitle": "Co-pay", "sClass": "center small" , "aTargets": [ 7 ] , "sWidth": "4em",  "bVisible": true, "bSearchable": false},
			{ "sTitle": "Rx'd", "sClass": "center small" , "aTargets": [ 8 ] , "sWidth": "3em", "bSearchable": true},
			{ "sTitle": "Filled", "sClass": "center small" , "aTargets": [ 9 ] , "sWidth": "3em", "bVisible": true, "bSearchable": true},
			{ "sTitle": "Plan", "sClass": "insurance_plan center rsmall" , "aTargets": [ 10 ] , "sWidth": "5em", "bVisible": true, "bSearchable": true},
			{ "sTitle": "Provider", "sClass": " provider_name center rsmall" , "aTargets": [ 11 ] , "sWidth": "4em", "bVisible": true, "bSearchable": false},
			{ "sTitle": "MISC", "sClass": "center small" , "aTargets": [ 12 ] , "sWidth": "2em", "bVisible": true, "bSearchable": false},
			{ "sTitle": "State", "sClass": "center small" , "aTargets": [ 13 ] , "sWidth": "2em", "bVisible": true, "bSearchable": true},
			{ "sTitle": "Shipped", "sClass": "center small" , "aTargets": [ 14 ] , "sWidth": "3em", "bVisible": true, "bSearchable": false},
			{ "sTitle": "Track#", "sClass": "center rsmall" , "aTargets": [ 15 ] , "sWidth": "4em", "bVisible": true, "bSearchable": false},
			{ "sTitle": "Notes", "sClass": "center rsmall" , "aTargets": [ 16 ] , "sWidth": "6em", "bVisible": true, "bSearchable": false},
			{"bSearchable": true, "bVisible": false, "aTargets": [0]}
						],
		"OFF_aaSorting": [[1,'desc']],
    "OFFfnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {        $('td:nth-child(3)', nRow).html( '<span style="font-size:x-small; max-width: 10em; overflow-x:hidden; text-overflow: ellipsis; white-space:nowrap; display: inline-block;">' + aData[4] + '</span>' );
		  if (aData[6] || aData[7]) {$('td:nth-child(5)', nRow).html( aData[6] +'<span style="font-size:x-small; max-width: 10em; overflow-x:hidden; text-overflow: ellipsis; white-space:nowrap; display: inline-block;">' +  ' <div>Co-Pay: $' +aData[7] + '</div></span>' );}
		  if (aData[15] || aData[16]) {$('td:nth-child(7)', nRow).html( ( aData[9] ? aData[9] : '' ) + '<span style="font-size:x-small; max-width: 10em; overflow-x:hidden; text-overflow: ellipsis; white-space:nowrap; display: inline-block;">' + (aData[15] ? ' <div><a href="http://www.packagetrackr.com/track/' + aData[15] +'" target="_track">' +(aData[15].match(/^1Z/) ? 'UPS' : 'USPS' ) + '</a></div>' : '' )  +'</span>'  + ( aData[16] ?  '<div class="rx_notes">' + aData[16].split('|').join('<p/>') + '</div>' : '' )   );}


    },

		"fnDrawCallback": function () {			}
	} );
	

	
};


var show_ma_partner_data_orig = function(tblid, ma_partner_data) {
//	console.log(toTableArray( map_as_array(ma_partner_data),  [  'TransactionRcd', 'rxn', 'PatientName' , 'Voided', 'drug_name', 'qty_dispensed', 'price', 'copay', 'date_prescribed', 'date_filled', 'insurance_plan', 'doctorName', 'PCO',  'doctorState', 'shipDate', 'trackingNum', 'data' ]));
	ma_partner_table=$(tblid).dataTable( {
	   "sPaginationType": "full_numbers",
"sDom": '<"top"irpflt><"bottom"><"clear">',
	"bDestroy": true,
	"bJQueryUI": true,
	"bProcessing": true,
		"bDeferRender": true,
	"bRetrieve": true,
	'bAutoWidth':false,
		"aaData": toTableArray( map_as_array(ma_partner_data),  [  'TransactionRcd', 'rxn', 'PatientName' , 'Voided', 'drug_name', 'qty_dispensed', 'price', 'copay', 'date_prescribed', 'date_filled', 'insurance_plan', 'doctorName', 'misc_id',  'doctorState', 'shipDate', 'trackingNum', 'note_text', 'data' ]),
		"aoColumnDefs": [
				{ "sTitle": "Trans#", "sClass": "center small" , "aTargets": [ 0 ] , "sWidth": "9em", "bSearchable": true, "bVisible": false},
			{ "sTitle": "RxN", "sClass": "center small" , "aTargets": [ 1 ] , "sWidth": "5em", "bSearchable": true},
				{ "sTitle": "Patient", "sClass": " patient_name center small" , "aTargets": [ 2 ] , "sWidth": "9em", 'bVisible': true, "bSearchable": false},
				{ "sTitle": "Voided", "sClass": "center small" , "aTargets": [ 3 ] , "sWidth": "4em",  'bVisible': false, "bSearchable": false},
			{ "sTitle": "Drug", "sClass": "center small" , "aTargets": [ 4 ] , "sWidth": "4em", "bSearchable": false},
			{ "sTitle": "QTY", "sClass": "center small" , "aTargets": [ 5 ] , "sWidth": "1em", "bSearchable": false},
			{ "sTitle": "Price", "sClass": "center small" , "aTargets": [ 6 ] , "sWidth": "2em", "bSearchable": false},
				{ "sTitle": "Co-pay", "sClass": "center small" , "aTargets": [ 7 ] , "sWidth": "4em",  "bVisible": false, "bSearchable": false},
			{ "sTitle": "Rx'd", "sClass": "center small" , "aTargets": [ 8 ] , "sWidth": "3em", "bSearchable": true},
			{ "sTitle": "Filled", "sClass": "center small" , "aTargets": [ 9 ] , "sWidth": "9em", "bVisible": true, "bSearchable": true},
			{ "sTitle": "Plan", "sClass": "insurance_plan center small" , "aTargets": [ 10 ] , "sWidth": "6em", "bVisible": true, "bSearchable": true},
			{ "sTitle": "Provider", "sClass": " provider_name center small" , "aTargets": [ 11 ] , "sWidth": "6em", "bVisible": true, "bSearchable": false},
			{ "sTitle": "MISC", "sClass": "center small" , "aTargets": [ 12 ] , "sWidth": "4em", "bVisible": false, "bSearchable": true},
			{ "sTitle": "State", "sClass": "center small" , "aTargets": [ 13 ] , "sWidth": "4em", "bVisible": true, "bSearchable": true},
			{ "sTitle": "Shipped", "sClass": "center small" , "aTargets": [ 14 ] , "sWidth": "4em", "bVisible": false, "bSearchable": false},
			{ "sTitle": "Track#", "sClass": "center small" , "aTargets": [ 15 ] , "sWidth": "9em", "bVisible": false, "bSearchable": false},
			{ "sTitle": "Notes", "sClass": "center small" , "aTargets": [ 16 ] , "sWidth": "9em", "bVisible": false, "bSearchable": false},
			{"bSearchable": true, "bVisible": false, "aTargets": [0]}
						],
		"OFF_aaSorting": [[1,'desc']],
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {        $('td:nth-child(3)', nRow).html( '<span style="font-size:x-small; max-width: 10em; overflow-x:hidden; text-overflow: ellipsis; white-space:nowrap; display: inline-block;">' + aData[4] + '</span>' );
		  if (aData[6] || aData[7]) {$('td:nth-child(5)', nRow).html( aData[6] +'<span style="font-size:x-small; max-width: 10em; overflow-x:hidden; text-overflow: ellipsis; white-space:nowrap; display: inline-block;">' +  ' <div>Co-Pay: $' +aData[7] + '</div></span>' );}
		  if (aData[15] || aData[16]) {$('td:nth-child(7)', nRow).html( ( aData[9] ? aData[9] : '' ) + '<span style="font-size:x-small; max-width: 10em; overflow-x:hidden; text-overflow: ellipsis; white-space:nowrap; display: inline-block;">' + (aData[15] ? ' <div><a href="http://www.packagetrackr.com/track/' + aData[15] +'" target="_track">' +(aData[15].match(/^1Z/) ? 'UPS' : 'USPS' ) + '</a></div>' : '' )  +'</span>'  + ( aData[16] ?  '<div class="rx_notes">' + aData[16].split('|').join('<p/>') + '</div>' : '' )   );}


    },

		"fnDrawCallback": function () {			}
	} );
	

	
};


/* END ma_partner */






var sum_array=function(tobj, okey) {var vammm=0; $.each(tobj,function(ii,vv) { vammm+= parseFloat(vv[okey]);}); var tmp_ret=parseInt(vammm * 100) / 100; return tmp_ret.toFixed(2)};

var change_date_input = function(delm, days) {
	var stds =      $(delm).val() ?  new Date( Date.parse($(delm).val() ) + ((1 + days) *24*60*60*1000) )   :          new Date(new Date().setDate(new Date().getDate() + days  ));                       //(new Date().getDate() + days *24*60*60*1000);
	var tmpdate=stds.getFullYear() + '-' + (stds.getMonth() +1 > 9 ? ''+ (stds.getMonth() +1) : '0' + (stds.getMonth() +1)) + '-' + (stds.getDate() > 9 ? ''+ stds.getDate()  : '0' + (stds.getDate() ) );
	$(delm).val(  tmpdate   );
};



var datauri_to_blob= function(dataURI) {
  // convert base64 to raw binary data held in a string
  // doesn't handle URLEncoded DataURIs
  var byteString;
  if (dataURI.split(',')[0].indexOf('base64') >= 0)
      byteString = atob(dataURI.split(',')[1]);
  else
      byteString = unescape(dataURI.split(',')[1]);
  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

  // write the bytes of the string to an ArrayBuffer
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
  }

  // write the ArrayBuffer to a blob, and you're done
  return new Blob([ab],{type: mimeString});
};

var display_Qs= function(qsd, qhtml) {
				if (!qhtml) { qhtml = ''; }
				$.postJSON('/api/v1.i/getQ_all', {}, function(data) {
					if (data.success) { 
							$('#ma').html( qhtml + '<table id="qs_tbl"></table>' + '<div class="addl_q_funcs"><a href="#" class="func_show_addl_queue_funcs">Show Additional Functions</a></div>');
					 								qsd=data.data;
													activate_qs_tbl(qsd);

						$.each(data.data, function(index, val) { if(seenNotifications[val.id]) {return true;}  displayNofication(val, {title: 'New Queue Item'}); seenNotifications[val.id]=1;	 return false; });
					}
					});
};
